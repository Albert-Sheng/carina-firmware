cmake_minimum_required(VERSION 3.19)

# Include external dependencies
include(external.cmake)

# Include useful stuff
include(${LOGITECH_CMAKE_TOOLS}/toolchains/gcc_arm_embedded.cmake)
include(${LOGITECH_CMAKE_TOOLS}/devices/stm32/stm32.cmake)

# project
project(
    carina
    LANGUAGES
        ASM
        C
        CXX
)

# Include actual firmware
add_subdirectory(firmware)
