Firmware for the "Carina" project.


# Architecture
This project mainly consists of two parts: The bootloader and the main application.

The bootloader (target `carina-bootloader`) gets stored into the microcontroller's internal flash from which the device boots on power-on or reset. The bootloader initializes the system code and peripherals required to load the main application (target `carina-app`) from the external QSPI flash.


# Dependencies
This project has been setup to automatically pull in any necessary dependencies. There is no need to manually mess with git submodules or anything similar. Just follow the steps provided in the `Examples` section below and you're good to go.

When running `cmake` for the very first time, it can take a few moments to complete as the first run requires to download all dependencies, looking up package & program paths etc. Subsequent calls of `cmake` run more quickly. By default, `cmake` does not print any progress when downloading dependencies. You can make it more verbose by setting `FETCHCONTENT_QUIET` to `OFF`:
```shell
cmake -DFETCHCONTENT_QUIET=OFF [...]
```

Dependencies are pulled in as git repositories. CMake will place those under `<build_dir>/_deps`. The dependency git repository is placed under `<build_dir>/_deps/dependency-src` where `dependency` is the name of the dependency.
If you need to modify code of a dependency, use the git repository under that location like you would with any other dependency. There is usually no need to manually clone the dependency, placing it elsewhere and telling CMake where to find it.


# Building
**Note:** This is **not** a guide on how to use cmake.

This is a cmake based project. Therefore, there is a great deal of flexibility available. This project can be built using any build system supported by cmake including but not limited to `make` and `ninja`. The same goes for IDEs and other tooling.

## Toolchain
While this will work with pretty much any compiler that can target Cortex-M architectures it's recommended to use `gcc-arm-embedded` as this project provides built-in support for automatically detecting and setting up this toolchain. Similar tooling can be easily added for different toolchains if necessary.

If your `gcc-arm-embedded` toolchain is not available from your system's `PATH`, you can easily specify the path to the toolchain's binary directory via `TOOLCHAIN_PATH`:

```shell
cmake -DTOOLCHAIN_PATH=/usr/local/gcc-arm-embedded/bin/ ...
```

## SEGGER J-Link
By default, this project will generate the necessary tooling to flash & debug the target using SEGGER's J-Link infrastructure. If the J-Link software is not available from your system's `PATH`,  you can easily specify the path to the J-Link software binaries via the `SEGGER_PATH` variable:

```shell
cmake -DSEGGER_PATH=/path/to/segger/binaries ...
```

## Examples
The following section provides several examples for building this firmware.
Commonly used `cmake` flags:

| Flag | Commonly used values | Description |
|---|---|--|
| `-B` | `build` | The path to the build directory. |
| `-G` | `"Unix Makefiles"`, `Ninja` | Which build system to use (eg. Makefile, Ninja, ...) |
| `-D` | `TOOLCHAIN_PATH=/path/to/toolchain` | Allows specifying `key=value` pairs to pass options/settings/values/variables to CMake. Multiple `-D` options are allowed. |

### Terminal (agnostic)
CMake is pretty good in figuring stuff out by self. Therefore, building this firmware can be as easy as:
```shell
git clone <this_repository> carina_firmware
cmake -B build
cmake --build build
```

### Terminal (ninja)
Building using `ninja`:
```shell
git clone <this_repository> carina_firmware
cd carina_firmware
cmake -B build -G "Ninja" ..
cmake --build build
```

### Terminal (make)
Building using `make`:
```shell
git clone <this_repository> carina_firmware
cd carina_firmware
cmake -B build -G "Unix Makefiles" ..
cmake --build build -- -j4
```
Note that `make` does not use parallel jobs by default (multi CPU-core builds). When using `cmake --build`, anything after the `--` separator will be passed to the build system as command line arguments. Therefore, we can pass the `-j` option as shown in the example above to build on multiple core (`-j4` in the example above: four parallel jobs).

### IDEs
Many IDEs support loading CMake based projects out of the box. Follow your IDE's documentation accordingly.

IDEs which do not support CMake based projects can still be used by generating IDE specific project files by passing the appropriate generator to CMake's `-G` flag.
To get a list of available generators, simply run:
```shell
cmake -G
```


# Flashing/Debugging

## Bootloader
Flash the `carina-bootloader` binary to the microcontroller's internal FLASH as you would with any regular firmware.

## App
Flashing the `carina-app` binary into the external QSPI flash is accomplished using J-Link. For ease of use, running `cmake` on this project will generate a corresponding `debug_*` directory in the top level project directory. This directory contains tooling to make this process easier. For most systems/scenarios, this tooling should work out of the box.

### Terminal
In a shell, launch `debug_server.sh` to start the J-Link GDB server. Keep this running.

After successful starting of the GDB server, launch `debug_client.sh` to spawn a GDB command line interface.
In the GDB shell, type `load` to download the app image into the external QSPI flash.

### IDE
Each IDE has its own ways of starting the GDB server and client. Look up the corresponding information.
The existing scripts might still be useful as they can be invoked by the IDE (if possible).
