#include "app.h"
#include "led.h"
#include "dfu.h"
#include "usbcfg.h"
#include "usb.h"
#include "common/centpp/centpp.h"

static const SerialConfig serial_Config = {
    .speed = 115200,
    .cr1 = 0,
    .cr2 = USART_CR2_STOP1_BITS | USART_CR2_LINEN,
    .cr3 = 0,
};

static const ADCConversionGroup hwrev_adc_config = {
    .circular       = false,
    .num_channels   = 1,
    .end_cb         = NULL,
    .error_cb       = NULL,
    .cfgr           = ADC_CFGR_CONT | ADC_CFGR_RES_12BITS | ADC_CFGR_OVRMOD,
    .pcsel          = ADC_SELMASK_IN4,
    .smpr[0]        = ADC_SMPR1_SMP_AN4(ADC_SMPR_SMP_2P5),
    .sqr[0]         = ADC_SQR1_SQ1_N(ADC_CHANNEL_IN4),
};

/*
 * SDRAM driver configuration structure.
 */
static const SDRAMConfig sdram_cfg = {
    .sdcr = (uint32_t)(FMC_ColumnBits_Number_8b |
                       FMC_RowBits_Number_12b |
                       FMC_SDMemory_Width_16b |
                       FMC_InternalBank_Number_4 |
                       FMC_CAS_Latency_3 |
                       FMC_Write_Protection_Disable |
                       FMC_SDClock_Period_2 |
                       FMC_Read_Burst_Enable |
                       FMC_ReadPipe_Delay_0),

    .sdtr = (uint32_t)((3   -  1) |  // FMC_LoadToActiveDelay = 3 (TMRD: 3 Clock cycles)
                       (12 <<  4) |  // FMC_ExitSelfRefreshDelay = 12 (TXSR: min=67ns (12x6=72))
                       (10 <<  8) |  // FMC_SelfRefreshTime = 10 (TRAS: min=60ns)
                       (12 << 12) |  // FMC_RowCycleDelay = 7 (TRC: min=67 (12x=72))
                       (2 << 16) |   // FMC_WriteRecoveryTime = 2 (TWR:  min=1+6ns (12ns))
                       (2 << 20) |   // FMC_RPDelay = 2 (TRP)
                       (2 << 24)),   // FMC_RCDDelay = 2 (TRCD)

    .sdcmr = (uint32_t)((FMC_SDCMR_NRFS_3) |
                        ((FMC_SDCMR_MRD_BURST_LENGTH_1 |
                          FMC_SDCMR_MRD_BURST_TYPE_SEQUENTIAL |
                          FMC_SDCMR_MRD_CAS_LATENCY_3 |
                          FMC_SDCMR_MRD_OPERATING_MODE_STANDARD |
                          FMC_SDCMR_MRD_WRITEBURST_MODE_SINGLE) << 9)),

    /* if (STM32_SYSCLK == 200000000) ->
       64ms / 4096 = 15.625us
       15.625us * 100MHz = 1563 - 20 = 1543  = 0x607*/
    .sdrtr = (uint32_t)(0x607 << 1),
};

/*
 * QSPI & flash related
 */
static const WSPIConfig WSPIcfg1 = {
    .end_cb           = NULL,
    .error_cb         = NULL,
    .dcr              = STM32_DCR_FSIZE(22U) |        /* 8MB device.          */
                        STM32_DCR_CSHT(2U)            /* NCS 3 cycles delay.  */
};

static const SNORConfig snorcfg1 = {
    .busp             = &WSPID1,
    .buscfg           = &WSPIcfg1
};

static THD_WORKING_AREA(waHID, 1024);

#define ENCODER_SELECT_LINE     PAL_LINE(GPIOI, 3)
#define MIC_MUTE_LINE           PAL_LINE(GPIOG, 2)

static void centpp_features_register(void)
{
    CentPPRegister_iGenericDFU(&s_centpp_cfg.igenericdfu_cfg);
}

static void app_soft_reset(void)
{
    //USB_stop();
    __NVIC_SystemReset();
}

/*
 * Application entry point.
 */
int main(void) {
    static centpp_config_t s_centpp_cfg = {
        .register_features_cb = centpp_features_register,
    };

    static const dfu_callback_t s_dfu_cb = {
        .soft_reset_cb = app_soft_reset,
        .verify_complete_cb = CentPPVerifyCompleteEvent,
        .prepare_complete_cb = CentPPPrepareCompleteEvent,
        .install_complete_cb = CentPPInstallCompleteEvent,
    };

    /*
    * System initializations.
    * - HAL initialization, this also initializes the configured device drivers
    *   and performs the board-specific initializations.
    * - Kernel initialization, the main() function becomes a thread and the
    *   RTOS is active.
    */
    halInit();
    chSysInit();

    /*
     * Debug UART Initialization.
     * - LPUART module is initialized.
     */
    sdStart(&DBG_SERIAL, &serial_Config);

    /*
     * Initialize SNOR
     */
    flash_init(&snorcfg1);

    /*
     * Force device to stay in bootloader mode if mic mute button and encoder select button are pressed together
     * during bootup time
     */
    bool force_Bootloader = (palReadLine(ENCODER_SELECT_LINE) == PAL_LOW) && (palReadLine(MIC_MUTE_LINE) == PAL_LOW);

    if(!force_Bootloader && (DFU_get_prepare_flag() != DBG_BOOT_FLAG_DFU) && App_Verify()) {
        sdStop(&DBG_SERIAL);
        SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
        App_Jump();
    }

    /*
     * - Hardware revision and header information is printed out.
     */
    Dbg_hwrev_init(&ADCD1, &hwrev_adc_config, GPIOG, 3, GPIOC, 4);
    Dbg_print_header();
    chThdSleepMilliseconds(500);

    /*
     * USB initializations.
     * - USB HS PHY reset is released.
     * - Delay placed for the PHY 60MHz clock to stabilize.
     * - USBD2 is started and bus is connected.
     */
    const USBModeConfig_t * const usb_mode = usbGetModePC();
    palSetPad(GPIOD, 2);
    chThdSleepMilliseconds(1);
    usbModeStart(&USBD2, usb_mode);

    DFU_init(&s_dfu_cb);

    DFU_erase_partition_for_upgrade();

    /*
     * CentPP Initialization.
     * - Start CentPP engine.
     */
    CentPP_Start(&s_centpp_cfg);

    /*
     * Initialize SDRAM
     */
    sdramInit();
    sdramStart(&SDRAMD1, &sdram_cfg);

    ledInit();

    chThdCreateStatic(waHID, sizeof(waHID), NORMALPRIO, HID_Thread, NULL);
    Dbg_printf(DBG_INFO, "Startup done.");

    /*
     * Normal main() thread activity
     */
    int count = 0;
    while(1) {
#ifdef PRINT_CPU_USAGE
        Dbg_print_cpu_usage();
        chThdSleepSeconds(5);
#else
        chThdSleepMilliseconds(500);
#endif
    }

    return 0;
}
