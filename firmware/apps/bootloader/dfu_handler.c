#include "crc.h"
#include "debug.h"
#include "dfu.h"

#define DFU_HANDLER_COMMIT_BUFFER_SIZE 512

static
flash_status_t
DFU_commit_firmware(const uint8_t* source_addr, const size_t recv_fw_size, const uint8_t* dest_addr)
{
    Dbg_printf(DBG_INFO, "%s(0x%08x, %u, 0x%08x)", __func__, source_addr, recv_fw_size, dest_addr);

    size_t bytes_read = 0;
    static uint8_t buffer[DFU_HANDLER_COMMIT_BUFFER_SIZE] __attribute__((section(".axi_nocache"))) = {0};

    for (size_t offset = 0; offset < recv_fw_size; offset += bytes_read) {
        bytes_read = (recv_fw_size > CHUNK_SIZE) ? CHUNK_SIZE : recv_fw_size;

        // Read
        if (FLASH_STATUS_OK != flash_read(source_addr + offset, buffer, bytes_read)) {
            Dbg_printf(DBG_INFO, "%s: flash_read() failed at address 0x%08x", __func__, source_addr+offset);
            return FLASH_STATUS_ERROR;
        }

        // Write
        if (FLASH_STATUS_OK != flash_write(dest_addr + offset, buffer, bytes_read)) {
            Dbg_printf(DBG_INFO, "%s: flash_write() failed at address 0x%08x", __func__, dest_addr+offset);
            return FLASH_STATUS_ERROR;
        }
    }

    return FLASH_STATUS_OK;
}

uint8_t
DFU_check_header(const dfuHeader_t* const hdr)
{
    // Magic number
    if (hdr->magic_number != DFU_HEADER_MAGIC_NUMBER)
        return DFU_HEADER_ERROR_MAGIC_NUMBER;

    // Header version
    // Currently only version 1 is supported
    if (hdr->version != 0x01)
        return DFU_HEADER_ERROR_VERSION;

    // FW type
    if (hdr->fw_type == 0)
        return DFU_HEADER_ERROR_FW_TYPE;

    // FW version
    // ToDo: Ensure that we're not downgrading (?)

    // FW size
    if (hdr->fw_size == 0)
        return DFU_HEADER_ERROR_FW_SIZE;

    // Header checksum
    // ToDo: Implement this
#if 0
    const uint32_t chksm_calc = crc32_mem((uint32_t*)hdr, (DFU_HEADER_DATA_SIZE-4)/4);     // -4 to exclude checksum field (4 bytes)
    Dbg_printf(DBG_INFO, "dfu header checksum calculated: %08x", chksm_calc);
    Dbg_printf(DBG_INFO, "dfu header checksum read      : %08x", hdr->crc32);
    if (chksm_calc != hdr->crc32)
        return DFU_HEADER_ERROR_CHECKSUM;
#endif

    // Padding
    for (uint16_t i = 0; i < DFU_HEADER_PADDING_SIZE; i++) {
        if (hdr->padding[i] != DFU_HEADER_PADDING_BYTE)
            return DFU_HEADER_ERROR_PADDING;
    }

    // All good
    return DFU_HEADER_OK;
}

flash_status_t
DFU_install_firmware(void)
{
    flash_status_t result = FLASH_STATUS_ERROR;
    dfu_state_t* dfuState = DFU_get_state();
    static dfuHeader_t header __attribute__((section(".axi_nocache")))__attribute__((aligned(32))) = {0};

    // Check whether there is a new image to install
    if (dfuState->Partition1active == 0) {
        Dbg_printf(DBG_ERROR, "DFU_install_firmware(): no new firmware image found.");
        return FLASH_STATUS_ERROR;
    }

    // Retrieve DFU header
    result = flash_read((uint8_t*)PARTITION_ONE_ADDRESS, (uint8_t*)(&header), sizeof(dfuHeader_t));
    if (result != FLASH_STATUS_OK) {
        Dbg_printf(DBG_ERROR, "DFU_install_firmware(): could not retrieve DFU header.");
        return FLASH_STATUS_ERROR;
    }

    // Check DFU header
    const uint8_t dfu_HeaderResult = DFU_check_header(&header);
    if (dfu_HeaderResult != DFU_HEADER_OK) {
        Dbg_printf(DBG_ERROR, "DFU_install_firmware(): DFU header check failed. error code = %d", dfu_HeaderResult);
        return FLASH_STATUS_INSTALL_FAIL;
    }

    // Determine flash sector information
    const size_t sector_size = flash_get_sector_size();
    const size_t start_addr = (PARTITION_ONE_ADDRESS + DFU_HEADER_SIZE);
    uint32_t sector_count = (((header.fw_size) % sector_size) == 0) ? (header.fw_size) / sector_size : ((header.fw_size) / sector_size) + 1;

    // Erase partition zero
    Dbg_printf(DBG_INFO, "DFU_install_firmware(): erasing partition zero...");
    if (flash_erase_partition_at_address((uint8_t*)__flash_application__, sector_count) != FLASH_STATUS_OK) {
        Dbg_printf(DBG_ERROR, "DFU_install_firmware(): failed to erase partition at address (0x%x)", __flash_application__);
        return FLASH_STATUS_ERROR;
    }
    Dbg_printf(DBG_INFO, "DFU_install_firmware(): erased partition zero successfully");

    // Commit firmware (copy from partition 1 to partition 0)
    Dbg_printf(DBG_INFO, "DFU_install_firmware(): committing DFU file");
    if (DFU_commit_firmware((uint8_t*)start_addr, header.fw_size, (uint8_t *)__flash_application__) != FLASH_STATUS_OK) {
        Dbg_printf(DBG_ERROR, "DFU_install_firmware(): failed to commit firmware");
        return FLASH_STATUS_ERROR;
    }
    Dbg_printf(DBG_INFO, "DFU_install_firmware(): committed firmware successfully. bytes copied = %u", header.fw_size);

    // Verify App info.
    if (!App_Verify()) {
        Dbg_printf(DBG_ERROR, "DFU_install_firmware(): app info verification failed.");
        return FLASH_STATUS_ERROR;
    }

    return FLASH_STATUS_OK;
}
