#include <stdint.h>
#include <stdbool.h>

#include "hid_proj.h"
#include "app.h"

#define HID_CMD_BOOT_APPINFO        0x41
#define HID_CMD_BOOT_DFUSTART_MCU   0x42
#define HID_CMD_BOOT_DFUDATA_MCU    0x43
#define HID_CMD_BOOT_DFUEND_MCU     0x44

bool
hid_command_handler_proj(hid_message_t* command, hid_message_t* response)
{
    switch(command->reportID) {
        case HID_ASTRO_REPORT_ID:
            // Message is for us, handle it here.
            switch(command->type) {
                case HID_CMD_BOOT_APPINFO:
                    if(App_Verify()) {
                        const appinfo_t* app_info = App_Info_Get();
                        HID_data_response_prepare(response, (uint8_t *)app_info->product, sizeof(product_t));
                    } else {
                        HID_error_response_prepare(response, HID_ERROR_NO_APP);
                    }
                    return true;

                case HID_CMD_BOOT_DFUSTART_MCU:
                    // if(Flash_Erase(FLASH_SPACE_APPLICATION) != FLASH_STATUS_OK) {
                    //     HID_error_response_prepare(response, HID_ERROR_FLASH_FAILURE);
                    //     return true;
                    // }
                    // if(DFU_image_write_start(FLASH_SPACE_APPLICATION) != FLASH_STATUS_OK) {
                    //     HID_error_response_prepare(response, HID_ERROR_FLASH_FAILURE);
                    //     return true;
                    // }
                    HID_ack_response_prepare(response);
                    return true;

                case HID_CMD_BOOT_DFUDATA_MCU:
                    // if(DFU_image_write_block(command->payload, command->bytes) != FLASH_STATUS_OK) {
                    //     HID_error_response_prepare(response, HID_ERROR_FLASH_FAILURE);
                    //     return true;
                    // }
                    HID_ack_response_prepare(response);
                    return true;

                case HID_CMD_BOOT_DFUEND_MCU:
                    // if(DFU_image_write_stop() != FLASH_STATUS_OK) {
                    //     HID_error_response_prepare(response, HID_ERROR_FLASH_FAILURE);
                    //     return true;
                    // }
                    // if(!App_Verify()) {
                    //     HID_error_response_prepare(response, HID_ERROR_NO_APP);
                    //     return true;
                    // }
                    HID_ack_response_prepare(response);
                    return true;

                default:
                    return false;
            }
            break;

        default:
            return false;
    }
}
