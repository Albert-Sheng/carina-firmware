#pragma once

#include "debug.h"
#include "dfu.h"
#include "project.h"
#include "common/centpp/iGenericDFU.h"

typedef void (*registerFeaturesCb)(void);

typedef struct {
    registerFeaturesCb register_features_cb;
    const igenericdfu_cfg_t igenericdfu_cfg;
} centpp_config_t;

static centpp_config_t s_centpp_cfg = {
    // iGenericDFU
    .igenericdfu_cfg.prepareEvent                 = DFU_prepare_event,
    .igenericdfu_cfg.getSignatureResult           = DFU_get_signature_result,
    .igenericdfu_cfg.dfuWriteSize                 = DFU_WRITE_SIZE,
    .igenericdfu_cfg.maxEraseDurationSecs         = DFU_MAX_ERASE_DURATION_SECS,
    .igenericdfu_cfg.maxVerifyDurationSecs        = DFU_MAX_VERIFY_DURATION_SECS,
    .igenericdfu_cfg.maxInstallDurationSecs       = DFU_MAX_INSTALL_DURATION_SECS,
    .igenericdfu_cfg.writeBlock                   = DFU_image_write_block,
    .igenericdfu_cfg.initiateFirmwareVerification = DFU_initiate_firmware_verification,
    .igenericdfu_cfg.initiateFirmwareInstallation = DFU_initiate_firmware_installation,
};
