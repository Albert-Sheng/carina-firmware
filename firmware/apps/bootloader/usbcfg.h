/**
 * @file    usbcfg.h
 * @brief   USB config header.
 *
 * @addtogroup USB
 * @{
 */

#ifndef USBCFG_H
#define USBCFG_H

#include "usb.h"
#include "hid_proj.h"
#include "usb_hid_transport.h"

extern USBHIDDriver UHD1;

#ifdef __cplusplus
extern "C" {
#endif

const USBModeConfig_t *usbGetModePC(void);
size_t hidGetReport(uint8_t id, uint8_t *bp, size_t n);
msg_t hidSetReport(uint8_t id, uint8_t *bp, size_t n);

#ifdef __cplusplus
}
#endif

#endif  /* USBCFG_H */

/** @} */
