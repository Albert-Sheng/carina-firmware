/**
 * @file    usbcfg.c
 * @brief   USB config.
 *
 * @addtogroup USB
 * @{
 */
#include "usbcfg.h"
#include "usb_descriptors.h"

#include "hal_usb.h"

#include "project.h"
#include "build_version.h"

/*
 * USB Endpoints definitions.
 */
#define USB_ENDPOINT_HID                1U
/* Includes both IN & OUT endpoint directions if used. */
#define USB_HID_ENDPOINT_TOTAL          2U

/*
 * USB string indices
 */
#define STRING_LANG_INDEX               0U
#define STRING_MANUF_INDEX              1U
#define STRING_PRODUCT_INDEX            2U

/*
 * USB interface addresses
 */
#define INTERFACE_HID                   0U

/*
 * Total USB interfaces for each group
 */
#define INTERFACE_COUNT_HID             1U

/*
 * Total USB interfaces in all
 */
#define INTERFACE_TOTAL                 (INTERFACE_COUNT_HID)

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * Capability UUID of Microsoft. Supports
 * Microsoft Windows v8.1 and later.
 */
#define MS_UUID_TIME_LOW                0xD8DD60DFU
#define MS_UUID_TIME_MID                0x4589U
#define MS_UUID_TIME_HIGH               0x4CC7U
#define MS_UUID_CLOCK_SEQ               0xD29CU
#define MS_UUID_NODE_ONE                0x649E9D65U
#define MS_UUID_NODE_TWO                0x9F8AU

/*
 * Total number of device capability descriptors
 */
#define USB_DEVICECAPS_TOTAL            1U

/*
 * NTDDI Windows version constants taken from sdkddkver.h
 * of the Windows 10 SDK. Used by the MS_PlatformDesc_t struct
 * below.
 */
#define NTDDI_WINBLUE                   0x06030000  /* Win 8.1 */
#define NTDDI_WINTHRESHOLD              0x0A000000  /* ABRACADABRA_THRESHOLD */
#define NTDDI_WIN10                     0x0A000000  /* ABRACADABRA_THRESHOLD */
#define NTDDI_WIN10_TH2                 0x0A000001  /* ABRACADABRA_WIN10_TH2 */
#define NTDDI_WIN10_RS1                 0x0A000002  /* ABRACADABRA_WIN10_RS1 */
#define NTDDI_WIN10_RS2                 0x0A000003  /* ABRACADABRA_WIN10_RS2 */
#define NTDDI_WIN10_RS3                 0x0A000004  /* ABRACADABRA_WIN10_RS3 */
#define NTDDI_WIN10_RS4                 0x0A000005  /* ABRACADABRA_WIN10_RS4 */
#define NTDDI_WIN10_RS5                 0x0A000006  /* ABRACADABRA_WIN10_RS5 */
#define NTDDI_WIN10_19H1                0x0A000007  /* ABRACADABRA_WIN10_19H1*/

/*
 * Microsoft specific Descriptors
 */
#pragma pack(push,1)
typedef struct {
    uint32_t        dwWindowsVersion;
    uint16_t        wMSOSDescriptorSetTotalLength;
    uint8_t         bMS_VendorCode;
    uint8_t         bAltEnumCode;
} desc_MS_InformationSet_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint32_t        dwWindowsVersion;
    uint16_t        wTotalLength;
} desc_SetHeader_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint8_t         bConfigurationValue;
    uint8_t         bReserved;
    uint16_t        wTotalLength;
} desc_ConfigSubset_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint8_t         bFirstInterface;
    uint8_t         bReserved;
    uint16_t        wSubsetLength;
} desc_FuncSubset_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint8_t         CompatibleID[8];
    uint8_t         SubCompatibleID[8];
} desc_CompatibleID_t;
#pragma pack(pop)

#define MS_OS_20_SET_HEADER_DESCRIPTOR          0x00U
#define MS_OS_20_SUBSET_HEADER_CONFIGURATION    0x01U
#define MS_OS_20_SUBSET_HEADER_FUNCTION         0x02U
#define MS_OS_20_FEATURE_COMPATIBLE_ID          0x03U

/*
 * Microsoft specific vendor codes used to retreive
 * set descriptors on Windows OS's.
 */
#define USB_MS_SET1_VENDOR_CODE         0x80U
#endif

/*
 * USB endpoint periodic interval
 * Refer to the below link for calculating the interval.
 * https://docs.microsoft.com/en-us/windows-hardware/drivers/usbcon/transfer-data-to-isochronous-endpoints
 * The STM32H750 requires the interval rate to be 1, since it expects to transmit at least 1 packet every
 * frame (FS) or micro-fram (HS).
 */
#define PERIODIC_INTERVAL_HID           4U

/*
 * USB HID Driver structure.
 */
USBHIDDriver UHD1;

/*
 * USB HID driver configuration.
 */
static const USBHIDConfig usbhidcfg = {
    &USBD2,
    USB_ENDPOINT_HID,
    USB_ENDPOINT_HID
};

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * USB Device Descriptor.
 */
static const desc_Device_t device_descriptor_data = {
    .bLength            = sizeof(desc_Device_t),
    .bDescriptorType    = USB_DESCRIPTOR_DEVICE,
    .bcdUSB             = 0x0201,
    .bDeviceClass       = USB_DEVICE_CLASS_MISC,
    .bDeviceSubClass    = USB_DEVICE_SUBCLASS_COMMON,
    .bDeviceProtocol    = USB_DEVICE_PROTOCOL_IAD,
    .bMaxPacketSize     = 64,
    .idVendor           = USB_VID,
    .idProduct          = USB_PID_PC,
    .bcdDevice          = ((VERSION_VMAJOR << 8) | VERSION_VMINOR),
    .iManufacturer      = STRING_MANUF_INDEX,
    .iProduct           = STRING_PRODUCT_INDEX,
    .iSerialNumber      = 0,
    .bNumConfigurations = 1
};
#else
/*
 * USB Device Descriptor.
 */
static const desc_Device_t device_descriptor_data = {
    .bLength            = sizeof(desc_Device_t),
    .bDescriptorType    = USB_DESCRIPTOR_DEVICE,
    .bcdUSB             = 0x0200,
    .bDeviceClass       = USB_DEVICE_CLASS_COMPOSITE,
    .bDeviceSubClass    = 0,
    .bDeviceProtocol    = 0,
    .bMaxPacketSize     = 64,
    .idVendor           = USB_VID,
    .idProduct          = USB_PID_PC,
    .bcdDevice          = ((VERSION_MAJOR << 8) | VERSION_MINOR),
    .iManufacturer      = STRING_MANUF_INDEX,
    .iProduct           = STRING_PRODUCT_INDEX,
    .iSerialNumber      = 0,
    .bNumConfigurations = 1
};
#endif

#if USB_PC_USE_DEVICE_QUALIFIER
/*
 * USB Device Qualifier Descriptor.
 */
static const desc_Qualifier_t device_qualifier_descriptor_data = {
    .bLength            = sizeof(desc_Qualifier_t),
    .bDescriptorType    = USB_DESCRIPTOR_DEVICE,
    .bcdUSB             = 0x0200,
    .bDeviceClass       = device_descriptor_data.bDeviceClass,
    .bDeviceSubClass    = device_descriptor_data.bDeviceSubClass,
    .bDeviceProtocol    = device_descriptor_data.bDeviceProtocol,
    .bMaxPacketSize     = 64,
    .bNumConfigurations = 1,
    .bReserved          = 0
};

/*
 * Device Qualifier Descriptor wrapper.
 */
static const USBDescriptor device_qualifier_descriptor = {
    .ud_size    = sizeof(device_qualifier_descriptor_data),
    .ud_string  = (uint8_t *)&device_qualifier_descriptor_data
};
#endif

/*
 * Device Descriptor wrapper.
 */
static const USBDescriptor device_descriptor = {
    .ud_size    = sizeof(device_descriptor_data),
    .ud_string  = (uint8_t *)&device_descriptor_data
};

/*
 * HID Report Descriptor
 *
 * This is the description of the format and the content of the
 * different IN or/and OUT reports that your application can
 * receive/send
 *
 * See "Device Class Definition for Human Interface Devices (HID)"
 * (http://www.usb.org/developers/hidpage/HID1_11.pdf) for the
 * detailed description of all the fields
 */
static const uint8_t hid_report_descriptor_data[] = {
    0x06, 0x32, 0xff,       // usage page 0xff32 used to tell software tools to use Report ID's.
    0x09, 0x74,             // usage 0x74

    0xa1, 0x01,             // Collection Application
    0xa1, 0x03,             // Collection Report
    0x85, 0x02,             // Report ID 2

    // Read from device, 64 raw bytes
    0x75, 0x08,             // report size: 8 bits
    0x15, 0x00,             // logical minimum: 0
    0x26, 0xff, 0x00,       // logical maximum: 255
    0x95, 63,               // report count: 63
    0x09, 0x75,             // usage
    0x81, 0x02,             // input (array)

    // Write to device, 64 raw bytes
    0x75, 0x08,             // report size: 8 bits
    0x15, 0x00,             // logical minimum: 0
    0x26, 0xff, 0x00,       // logical maximum: 255
    0x95, 63,               // report count: 63
    0x09, 0x76,             // usage
    0x91, 0x02,             // output (array)
    0xc0,
    0xc0                    // end collection
};

/*
 * Configuration descriptor tree.
 */
static const struct {
    desc_Config_t config;

    struct {
        desc_Interface_t    interface;
        desc_HID_t          header;
        desc_Endpoint_t     endpoint_in;
        desc_Endpoint_t     endpoint_out;
    } interface_hid;
} configuration_descriptor_data = {
    .config = {
        .bLength                = sizeof(desc_Config_t),
        .bDescriptorType        = USB_DESCRIPTOR_CONFIGURATION,
        .wTotalLength           = sizeof(configuration_descriptor_data),
        .bNumInterface          = INTERFACE_TOTAL,
        .bConfigurationValue    = 1,
        .iConfiguration         = 0,
        .bmAttributes           = (0x80 | USB_CONFIG_ATTR_BUSPOWERED),
        .bMaxPower              = 250,
    },

    .interface_hid = {
        .interface = {
            .bLength            = sizeof(desc_Interface_t),
            .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
            .bInterfaceNumber   = INTERFACE_HID,
            .bAlternateSetting  = 0,
            .bNumEndpoints      = USB_HID_ENDPOINT_TOTAL,
            .bInterfaceClass    = HID_INTERFACE_CLASS,
            .bInterfaceSubclass = HID_NONE_INTERFACE,
            .bInterfaceProtocol = HID_NONE_PROTOCOL,
            .iInterface         = 0,
        },
        .header = {
            .bLength            = sizeof(desc_HID_t),
            .bDescriptorType    = USB_DESCRIPTOR_HID,
            .bcdHID             = 0x100,
            .bCountryCode       = 0,
            .bNumDescriptors    = 1,
            .bExtDescType       = HID_REPORT,
            .wExtDescLength     = sizeof(hid_report_descriptor_data),
        },
        .endpoint_in = {
            .bLength            = sizeof(desc_Endpoint_t),
            .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
            .bEndpointAddress   = USB_ENDPOINT_ADDR_DIR_IN | USB_ENDPOINT_HID,
            .bmAttributes       = USB_ENDPOINT_ATT_TYPE_INT,
            .wMaxPacketSize     = USB_HID_BUFFERS_SIZE,
            .bInterval          = PERIODIC_INTERVAL_HID,
        },
        .endpoint_out = {
            .bLength            = sizeof(desc_Endpoint_t),
            .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
            .bEndpointAddress   = USB_ENDPOINT_ADDR_DIR_OUT | USB_ENDPOINT_HID,
            .bmAttributes       = USB_ENDPOINT_ATT_TYPE_INT,
            .wMaxPacketSize     = USB_HID_BUFFERS_SIZE,
            .bInterval          = PERIODIC_INTERVAL_HID,
        },
    },
};

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * MS OS 2.0 Set Descriptor for Vendor Code 0x80
 */
static const struct {
    desc_SetHeader_t    header;

    struct {
        desc_ConfigSubset_t cfg_subset;
        struct {
            desc_FuncSubset_t   func_subset;
            desc_CompatibleID_t compat_id;
        } func_one;
    } config_one;

} ms_set_desc = {
    .header = {
        .wLength            = sizeof(desc_SetHeader_t),
        .wDescriptorType    = MS_OS_20_SET_HEADER_DESCRIPTOR,
        .dwWindowsVersion   = NTDDI_WINBLUE,
        .wTotalLength       = sizeof(ms_set_desc),
    },

    .config_one = {
        .cfg_subset = {
            .wLength            = sizeof(desc_ConfigSubset_t),
            .wDescriptorType    = MS_OS_20_SUBSET_HEADER_CONFIGURATION,
            .bConfigurationValue    = 1,
            .bReserved          = 0,
            .wTotalLength       = sizeof(ms_set_desc.config_one),
        },
        .func_one = {
            .func_subset = {
                .wLength            = sizeof(desc_FuncSubset_t),
                .wDescriptorType    = MS_OS_20_SUBSET_HEADER_FUNCTION,
                .bFirstInterface    = INTERFACE_AUDIO_CONTROL_STREAM,
                .bReserved          = 0,
                .wSubsetLength      = sizeof(ms_set_desc.config_one.func_one),
            },
            .compat_id = {
                .wLength            = sizeof(desc_CompatibleID_t),
                .wDescriptorType    = MS_OS_20_FEATURE_COMPATIBLE_ID,
                .CompatibleID = {
                    0x4D, 0x54, 0x50, 0x00,
                    0x00, 0x00, 0x00, 0x00
                },
                .SubCompatibleID = {
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00
                },
            },
        },
    },
};

/*
 * Device Level Descriptor
 */
static const struct {
    desc_BOS_t  bos;

    struct {
        desc_PlatformHeader_t header;
        struct {
            desc_MS_InformationSet_t    info_set_one;
        } info_set;
    } microsoft_compat_platform_desc;
} device_level_desc = {
    .bos = {
        .bLength                = sizeof(desc_BOS_t),
        .bDescriptorType        = USB_DESCRIPTOR_BOS,
        .wTotalLength           = sizeof(device_level_desc),
        .bNumDeviceCaps         = USB_DEVICECAPS_TOTAL
    },

    .microsoft_compat_platform_desc = {
        .header = {
            .bLength                = sizeof(device_level_desc.microsoft_compat_platform_desc),
            .bDescriptorType        = USB_DESCRIPTOR_DEVICE_CAPABILITY,
            .bDevCapabilityType     = USB_CAPABILITY_TYPE_PLATFORM,
            .bReserved              = 0,
            .PlatformCapabilityUUID1    = MS_UUID_TIME_LOW,
            .PlatformCapabilityUUID2    = MS_UUID_TIME_MID,
            .PlatformCapabilityUUID3    = MS_UUID_TIME_HIGH,
            .PlatformCapabilityUUID4    = MS_UUID_CLOCK_SEQ,
            .PlatformCapabilityUUID5    = MS_UUID_NODE_ONE,
            .PlatformCapabilityUUID6    = MS_UUID_NODE_TWO
        },
        .info_set = {
            .info_set_one = {
                .dwWindowsVersion               = ms_set_desc.header.dwWindowsVersion,
                .wMSOSDescriptorSetTotalLength  = ms_set_desc.header.wTotalLength,
                .bMS_VendorCode                 = USB_MS_SET1_VENDOR_CODE,
                .bAltEnumCode                   = 0
            },
        },
    },
};
#endif

/*
 * Configuration Descriptor wrapper.
 */
static const USBDescriptor configuration_descriptor = {
    .ud_size    = sizeof(configuration_descriptor_data),
    .ud_string  = (uint8_t *)&configuration_descriptor_data
};

/*
 * HID Descriptor wrapper.
 */
static const USBDescriptor hid_descriptor = {
    .ud_size    = sizeof(configuration_descriptor_data.interface_hid.header),
    .ud_string  = (uint8_t *)&configuration_descriptor_data.interface_hid.header
};

/*
 * HID Report Descriptor wrapper
 */
static const USBDescriptor hid_report_descriptor = {
    .ud_size    = sizeof(hid_report_descriptor_data),
    .ud_string  = (uint8_t *)&hid_report_descriptor_data
};

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * BOS Descriptor wrapper.
 */
static const USBDescriptor bos_descriptor = {
    .ud_size    = sizeof(device_level_desc),
    .ud_string  = (uint8_t *)&device_level_desc
};

/*
 * MS OS 2.0 Set Descriptor wrapper.
 */
static const USBDescriptor ms_set_one_descriptor = {
    .ud_size    = sizeof(ms_set_desc),
    .ud_string  = (uint8_t *)&ms_set_desc
};
#endif

/*
 * U.S. English language identifier.
 */
static const uint8_t lang_string[] = {
    4,                                    /* bLength.                         */
    USB_DESCRIPTOR_STRING,                /* bDescriptorType.                 */
    0x04, 0x09                            /* wLANGID (U.S. English).          */
};

/*
 * Vendor string.
 */
static const uint8_t vend_string[] = {
    26,                                   /* bLength.                         */
    USB_DESCRIPTOR_STRING,                /* bDescriptorType.                 */
    'A', 0, 's', 0, 't', 0, 'r', 0, 'o', 0, ' ', 0, 'G', 0, 'a', 0, 'm', 0, 'i', 0, 'n', 0, 'g', 0
};

/*
 * Device Description string.
 */
static const uint8_t dev_string[] = {
    26,                                   /* bLength.                         */
    USB_DESCRIPTOR_STRING,                /* bDescriptorType.                 */
    'A', 0, 's', 0, 't', 0, 'r', 0, 'o', 0, ' ', 0, 'S', 0, 't', 0, 'u', 0, 'd', 0, 'i', 0, 'o', 0
};

/*
 * Strings wrappers array.
 */
static const USBDescriptor desc_strings[] = {
    {sizeof(lang_string), lang_string},
    {sizeof(vend_string), vend_string},
    {sizeof(dev_string), dev_string},
};

/**
 * @brief   Returns a descriptor string.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return a descriptor string during
 *          enumeration.
 *
 * @param[in]   dindex      index value indicating which string to return.
 * @retval hid_report_descriptor    Pointer to the USB PC hid report descriptor.
 */
static const USBDescriptor *get_descriptor_str(uint8_t dindex) {
    return &desc_strings[dindex];
}

/**
 * @brief   Returns the number of string descriptors.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the number of string descriptors
 *          so that if the host requests a string descriptor it doesn't try to access
 *          memory that is out of bounds.
 *
 * @retval size_t total number of string descriptors.
 */
static size_t get_str_desc_total(void) {
    return (sizeof(desc_strings) / sizeof(USBDescriptor));
}

/**
 * @brief   Returns the hid report descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the hid report descriptor during
 *          enumeration.
 *
 * @retval hid_report_descriptor    Pointer to the USB PC hid report descriptor.
 */
static const USBDescriptor *get_hid_report_desc(void) {
    return &hid_report_descriptor;
}

/**
 * @brief   Returns the hid descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the hid descriptor during
 *          enumeration.
 *
 * @retval hid_descriptor    Pointer to the USB PC hid descriptor.
 */
static const USBDescriptor *get_hid_desc(void) {
    return &hid_descriptor;
}

/**
 * @brief   Returns the configuration descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the configuration descriptor during
 *          enumeration.
 *
 * @retval configuration_descriptor    Pointer to the USB PC configuration descriptor.
 */
static const USBDescriptor *get_config_desc(void) {
    return &configuration_descriptor;
}

/**
 * @brief   Returns the device descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the device descriptor during
 *          enumeration.
 *
 * @retval device_descriptor    Pointer to the USB PC device descriptor.
 */
static const USBDescriptor *get_device_desc(void) {
    return &device_descriptor;
}

#if USB_PC_USE_DEVICE_QUALIFIER
/**
 * @brief   Returns the device qualifier descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the device qualifier descriptor during
 *          enumeration.
 *
 * @retval device_qualifier_descriptor    Pointer to the USB PC device qualifier descriptor.
 */
static const USBDescriptor *get_device_qualifier_desc(void) {
    return &device_qualifier_descriptor;
}
#endif

#if USB_PC_USE_MS_OS_DESCRIPTORS
/**
 * @brief   Returns the BOS descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the BOS descriptor during
 *          enumeration.
 *          This is required to support MS OS 2.0 descriptors by Microsoft.
 *
 * @retval bos_descriptor   Pointer to the USB PC BOS descriptor.
 */
static const USBDescriptor *get_bos_descriptor(void) {
    return &bos_descriptor;
}
#endif

/**
 * @brief   IN endpoint states.
 */
static USBInEndpointState ep_state_hid_in;

/**
 * @brief   OUT endpoint states.
 */
static USBOutEndpointState ep_state_hid_out;

/**
 * @brief   USB_ENDPOINT_HID initialization structure (both IN and OUT).
 */
static const USBEndpointConfig ep_hid_config = {
    .ep_mode        = USB_EP_MODE_TYPE_INTR,
    .setup_cb       = NULL,
    .in_cb          = hidDataTransmitted,
    .out_cb         = hidDataReceived,
    .in_maxsize     = USB_HID_BUFFERS_SIZE,
    .out_maxsize    = USB_HID_BUFFERS_SIZE,
    .in_state       = &ep_state_hid_in,
    .out_state      = &ep_state_hid_out,
    .in_multiplier  = 1,
    .setup_buf      = NULL
};

/**
 * @breif   Application specific USB configurations
 * @details This callback function is application specific.
 *          It's executed when a USB configured event occurs.
 *
 * @param[in]   usbp        usb driver pointer
 */
static void usb_configured(USBDriver *usbp) {
    /* Enables the endpoints specified into the configuration.
       Note, this callback is invoked from an ISR so I-Class functions
       must be used.*/
    usbInitEndpointI(usbp, USB_ENDPOINT_HID, &ep_hid_config);

    /* Resetting the state of the HID subsystem.*/
    hidConfigureHookI(&UHD1);
}

/**
 * @breif   USB initializations
 * @details This callback function is application specific.
 *          It's executed before the USB peripheral is started
 *          if started by calling usbModeStart. Place any USB
 *          driver/variable initializations here.
 *
 * @param[in]   usbp        usb driver pointer
 */
void usb_obj_init(USBDriver *usbp) {
    (void)usbp;
    /*
     * Initializes a USB HID driver.
     */
    hidObjectInit(&UHD1);
    hidStart(&UHD1, &usbhidcfg);
}

/**
 * @breif   USB requests handler
 * @details This callback function is application specific.
 *          It's executed when a USB Requests event occurs.
 *          Any specific functions needed to occur when a requests
 *          event occurs, should be placed here.
 *
 * @param[in]   usbp        usb driver pointer
 */
static bool usb_req_handler(USBDriver *usbp) {
    setupPacket_t *setup = (setupPacket_t*)usbp->setup;

    if(((setup->bmRequestType & USB_RTYPE_DIR_MASK) == USB_RTYPE_DIR_DEV2HOST) &&
       ((setup->bmRequestType & USB_RTYPE_RECIPIENT_MASK) == USB_RTYPE_RECIPIENT_DEVICE) &&
       ((setup->bmRequestType & USB_RTYPE_TYPE_MASK) == USB_RTYPE_TYPE_VENDOR)) {
        switch(setup->bRequest) {
#if USB_PC_USE_MS_OS_DESCRIPTORS
            case USB_MS_SET1_VENDOR_CODE:
                switch(setup->wIndex) {
                    case MS_OS_20_DESCRIPTOR_INDEX:
                        usbSetupTransfer(usbp, (uint8_t *)ms_set_one_descriptor.ud_string, ms_set_one_descriptor.ud_size, NULL);
                        return true;
                }
#endif

            default:
                return false;
        }
    }

    return false;
}

#if 0
static bool tones_fb_update = false;
/*
 * Handles the USB driver global events.
 */
static void pc_sof_handler(USBDriver *usbp) {
    (void)usbp;

    static uint8_t sof_counter = 0;

    osalSysLockFromISR();

    sof_counter++;
    if(sof_counter == 10) {
        sof_counter = 0;
        tones_fb_update = true;
    }

    osalSysUnlockFromISR();
}
#endif

/*
 * USB Mode driver configuration.
 */
static const USBModeConfig_t usb_mode_pc = {
    .usb_configured_cb  = usb_configured,
    .usb_obj_init_cb    = usb_obj_init,
    .get_desc_str_cb    = get_descriptor_str,
    .get_hid_report_desc_cb = get_hid_report_desc,
    .get_hid_desc_cb    = get_hid_desc,
    .get_config_desc_cb = get_config_desc,
    .get_device_desc_cb = get_device_desc,

#if USB_PC_USE_DEVICE_QUALIFIER
    .get_device_qualifier_desc_cb = get_device_qualifier_desc,
#else
    .get_device_qualifier_desc_cb = NULL,
#endif

    .usb_requests_cb    = usb_req_handler,
    .get_str_desc_total_cb = get_str_desc_total,

#if USB_PC_USE_MS_OS_DESCRIPTORS
    .get_bos_desc_cb    = get_bos_descriptor,
#else
    .get_bos_desc_cb    = NULL,
#endif
    .usb_sof_cb         = NULL,
};

/**
 * @brief   Returns the PC configuration struct.
 * @details This function returns the configuration struct
 *          that contains the USB PC specific callback functions.
 *
 * @retval usb_mode_pc       The USB PC config struct.
 */
const USBModeConfig_t *usbGetModePC(void) {
    return &usb_mode_pc;
}
