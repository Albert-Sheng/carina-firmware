/**
 * @file    systemMgr.c
 * @brief   The system manager. Basically the central nervous system of Carina
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */

#ifndef _SYSTEMMGR_H_
#define _SYSTEMMGR_H_

#include "buttons.h"

#include <ch.h>

/*===========================================================================*/
/* App constants.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* App  pre-compile time settings.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* App  macros.                                                              */
/*===========================================================================*/

/*===========================================================================*/
/* App  exported variables.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

THD_FUNCTION(SYS_Thread, arg);

void sysVolumeIncrement(void);
void sysVolumeDecrement(void);
void sysVolumeSet(uint8_t level);
void sysMixerIncrement(void);
void sysMixerDecrement(void);
void sysMixerSet(uint8_t level);
void sysHandleMuteEvent(button_event_t evt);
void sysHandlePhantomPowerEvent(button_event_t evt);
void sysHandleHPJackEvent(bool present);
void sysHandleAuxJackEvent(bool present);
uint8_t sysGetBalanceMix(void);
uint8_t sysGetVolume(void);
bool sysGetMute(void);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // _SYSTEMMGR_H_
