#ifndef _GUI_CFG_H
#define _GUI_CFG_H

/**
 * Static X offset applied to the entire GUI.
 */
#define GUI_OFFSET_X 0

/**
 * Static Y offset applied to the entire GUI.
 */
#define GUI_OFFSET_Y 63

/**
 * Whether to use doublebuffering.
 */
#define GUI_RENDERING_DOUBLEBUFFERING GFXON

/**
 * Whether to use software doublebuffering.
 *
 * @note Use this only when necessary. It is much, much slower than hard doublebuffering.
 */
#define GUI_RENDERING_DOUBLEBUFFERING_SOFT GFXOFF

/**
 * Whether to use hardware doublebuffering.
 *
 * @note Use this whenever possible as it is much, much better/faster than soft doublebuffering.
 */
#define GUI_RENDERING_DOUBLEBUFFERING_HARD GFXON

/**
 * The index of the first display when using doublebuffering.
 */
#define GUI_RENDERING_DOUBLEBUFFERING_DISPLAY_0 0

/**
 * The index of the second display when using doublebuffering.
 */
#define GUI_RENDERING_DOUBLEBUFFERING_DISPLAY_1 1

/**
 * The GDISP LLD command for setting the active buffer when using hard doublebuffering.
 */
#define GUI_RENDERING_DOUBLEBUFFERING_LLD_CMD STM32LTDC_CONTROL_SHOW_BUFFER

/**
 * Whether to use optimized rendering techniques.
 *
 * @note It's up to each individual component (mainly widgets) to implement this accordingly.
 */
#define GUI_RENDERING_OPTIMIZED GFXOFF

/**
 * The size of a dot.
 *
 * @details This GUI is designed to mainly use individual dots. This controls the size of these dots.
 *
 * @note This does not affect text/string rendering.
 */
#define GUI_RENDERING_DOTSIZE 2

/**
 * The space between dots
 *
 * @details This GUI is designed to mainly use individual dots. This controls the size between these dots.
 *
 * @note This does not affect text/string rendering.
 */
#define GUI_RENDERING_IDS 8

/**
 * The starting position for the pairing animation
 *
 * @details This GUI is designed to mainly use individual dots. This controls the size between these dots.
 *
 * @note This does not affect text/string rendering.
 */
#define GUI_RENDERING_PAIRING_MIN   40

/**
 * The dot size used for the pairing animation
 *
 * @details This GUI is designed to mainly use individual dots. This controls the size of the animation dots
 *
 * @note This does not affect text/string rendering.
 */
#define GUI_RENDERING_PAIRING_DOTSIZE 1

/**
 * The dot size used for the pairing animation
 *
 * @details This GUI is designed to mainly use individual dots. This controls the size of the animation dots
 *
 * @note This does not affect text/string rendering.
 */
#define GUI_RENDERING_PAIRING_IDS 12

/**
 * Check rules.
 */
#include "gui_rules.h"

#endif
