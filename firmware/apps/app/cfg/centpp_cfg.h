#pragma once

#include "debug.h"
#include "dfu.h"
#include "common/centpp/iGenericDFU.h"

typedef void (*registerFeaturesCb)(void);

typedef struct {
    registerFeaturesCb register_features_cb;
    const igenericdfu_cfg_t  igenericdfu_cfg;
} centpp_config_t;

static centpp_config_t s_centpp_cfg = {
    // iGenericDFU
    .igenericdfu_cfg.prepareEvent                 = DFU_prepare_event,
    .igenericdfu_cfg.getSignatureResult           = NULL,
    .igenericdfu_cfg.dfuWriteSize                 = DFU_WRITE_SIZE,
    .igenericdfu_cfg.maxEraseDurationSecs         = 20,
    .igenericdfu_cfg.maxVerifyDurationSecs        = 0,
    .igenericdfu_cfg.maxInstallDurationSecs       = 0,
    .igenericdfu_cfg.writeBlock                   = NULL,
    .igenericdfu_cfg.initiateFirmwareVerification = NULL,
    .igenericdfu_cfg.initiateFirmwareInstallation = NULL,
};
