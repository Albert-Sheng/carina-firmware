#include "hid_proj.h"

#include "avnera.h"
#include "coprocessor.h"
#include "dsp.h"
#include "hid.h"
#include "io.h"
#include "led.h"
#include "proximity_snsr.h"
#include "qualcomm.h"
#include "pga2505/pga2505.h"

// Device driver instances
extern pga2505 pga2505_inst;

bool hid_command_handler_proj(hid_message_t *command, hid_message_t *response) {
    switch(command->reportID) {
        case HID_ASTRO_REPORT_ID:
            // Message is for us, handle it here.
            switch(command->type) {
                case HID_CMD_IMPERIUM_EXIST: {
                    uint8_t imperium_existing = isImperiumExisting();
                    HID_data_response_prepare(response, (uint8_t*)&imperium_existing, sizeof(imperium_existing));
                    return true;
                }

                case HID_CMD_LED_CTRL: {
                    uint8_t led_type = command->payload[0];
                    uint8_t led_index = command->payload[1];
                    uint8_t led_brightness = command->payload[2];

                    if(LED_TYPE_NORMAL == led_type) {
                        if(led_index < LED_NUMBER && led_brightness <= LED_MAX_BRIGHTNESS) {
                            ledSetBrightness(led_index, led_brightness);
                        } else if(led_index == LED_NUMBER && led_brightness <= LED_MAX_BRIGHTNESS) {
                            ledSetBrightnesslevel(led_brightness);
                            ledLightAllOn();
                        } else {
                            HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                            return true;
                        }

                    } else if(LED_TYPE_PHANTOM == led_type) {
#if PHANTOM_PWR_LED_EXIST
                        if(led_brightness == LED_MIN_BRIGHTNESS) {
                            ledPhantomPwrLedOff();
                        } else {
                            ledPhantomPwrLedOn();
                        }
#endif
                    } else {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return true;
                    }
                    HID_ack_response_prepare(response);
                    return true;
                }

                case HID_CMD_PHANTOM_PWR_CTRL: {
                    msg_t msg = MSG_OK;
                    uint8_t phantom_pwr_enable = command->payload[0];

                    msg = coProcessorEnablePhantomPwr((phantom_pwr_enable > 0) ? TRUE : FALSE);
                    if(MSG_OK != msg) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_MAC_CTRL: {
                    uint32_t val = 0;
                    uint8_t subcmd = command->payload[0];
                    uint8_t *rw_data = &(command->payload[1]);

                    switch(subcmd) {
                        case HID_SUBCMD_SET_AVNERA_ARBITER_ID:
                            val = (rw_data[0] << 24) | (rw_data[1] << 16) | (rw_data[2] << 8) | (rw_data[3]);
                            if(MSG_OK != avneraSetArbiterID(val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_GET_AVNERA_ARBITER_ID:
                            val = avneraGetArbiterID();
                            break;

                        case HID_SUBCMD_SAVE_AVNERA_ARBITER_ID:
                            if(MSG_OK != avneraSaveArbiterID()) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_GET_AVNERA_LINK_STATE:
                            val = avneraGetLinkState();
                            break;

                        default:
                            HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                            return true;
                    }

                    HID_data_response_prepare(response, (uint8_t *)&val, sizeof(val));
                    return true;
                }

                case HID_CMD_AVNERA_PAIRING_CTRL: {
                    uint8_t pairing_op = command->payload[0];
                    uint8_t channel = command->payload[1];

                    switch(pairing_op) {
                        case MacPair_Normal_Pairing:
                            if(MSG_OK != avneraSetOpenPairing()) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;
                        case MacPair_Fixed_Channel_Pairing:
                            if(MSG_OK != avneraSetFixedChannelPairing(channel)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case MacPair_Cancel_Pairing:
                            if(MSG_OK != avneraCancelPairing()) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        default:
                            HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                            return true;

                    }

                    HID_ack_response_prepare(response);
                    return true;
                }

                case HID_CMD_AVNERA_RF_CTRL: {
                    uint8_t rf_param_op = command->payload[0];
                    uint8_t *rf_param = &(command->payload[1]);
                    uint32_t rf_param_val = (rf_param[0] << 24) | (rf_param[1] << 16) | (rf_param[2] << 8) | (rf_param[3]);

                    switch(rf_param_op) {
                        case HID_SUBCMD_SET_AVNERA_MODULATION_MODE:
                            if(MSG_OK != avneraSetModulationMode(rf_param_val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;
                        case HID_SUBCMD_SET_AVNERA_CARRIER:
                            if(MSG_OK != avneraSelectCarrier(rf_param_val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_SET_AVNERA_CHANNEL:
                            if(MSG_OK != avneraSelectChannel(rf_param_val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_SET_AVNERA_ANTENNA:
                            if(MSG_OK != avneraSelectAntenna(rf_param_val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_SET_AVNERA_TXPWR:
                            if(MSG_OK != avneraSetTxPower(rf_param_val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_SET_AVNERA_FIXED_FREQ:
                            if(MSG_OK != avneraSelectFixedFreq(rf_param_val)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_ENABLE_AVNERA_CONT_TX:
                            if(MSG_OK != avneraEnableContTx()) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_ENABLE_AVNERA_CONT_RX:
                            if(MSG_OK != avneraEnableContRx()) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        default:
                            HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                            return true;
                    }

                    HID_ack_response_prepare(response);
                    return true;
                }

                case HID_CMD_AVNERA_MAC_STATS_CTRL: {
                    uint8_t mac_stats_op = command->payload[0];

                    switch(mac_stats_op) {
                        case HID_SUBCMD_START_AVNERA_MAC_STATS:
                            if(MSG_OK != avneraStartMacStats(TRUE)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;
                        case HID_SUBCMD_STOP_AVNERA_MAC_STATS:
                            if(MSG_OK != avneraStartMacStats(FALSE)) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        case HID_SUBCMD_CLEAR_AVNERA_MAC_STATS:
                            if(MSG_OK != avneraClearMacStats()) {
                                HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                                return true;
                            }
                            break;

                        default:
                            HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                            return true;
                    }

                    HID_ack_response_prepare(response);
                    return true;
                }

                case HID_CMD_AVNERA_READ_MEM: {
                    uint32_t addr = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t len = command->payload[4];

                    if(len > sizeof(response->payload)) {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return true;
                    }

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    if(MSG_OK != avneraReadMem(response->payload, addr, len)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_data_response_prepare(response, NULL, len);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_WRITE_MEM: {
                    uint32_t addr = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t len = command->payload[4];

                    if(len > sizeof(command->payload) - 5) {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return true;
                    }

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    if(MSG_OK != avneraWriteMem(&(command->payload[5]), addr, len)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_GET_FLASH_OPSTS: {
                    uint32_t op_sts = avneraGetFlashOpSts();

                    if(0xFFFFFFFF == op_sts) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_data_response_prepare(response, (uint8_t *)&op_sts, sizeof(op_sts));
                    }
                    return true;
                }

                case HID_CMD_AVNERA_READ_FLASH: {
                    uint32_t addr = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t len = command->payload[4];

                    if(len > sizeof(response->payload)) {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return true;
                    }

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    if(MSG_OK != avneraReadFlash(response->payload, addr, len)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_data_response_prepare(response, NULL, len);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_WRITE_FLASH: {
                    uint32_t addr = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t len = command->payload[4];

                    if(len > sizeof(command->payload) - 5) {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return true;
                    }

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    if(MSG_OK != avneraWriteFlash(&(command->payload[5]), addr, len)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_ERASE_FLASH: {
                    uint32_t addr = 0;
                    uint32_t size = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t *size_data = &(command->payload[4]);

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    size = (size_data[0] << 24) | (size_data[1] << 16) | (size_data[2] << 8) | (size_data[3]);

                    if(MSG_OK != avneraEraseFlash(addr, size)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_READ_MAC_PER: {
                    if(MSG_OK != avneraReadMacPer(response->payload)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_data_response_prepare(response, NULL, 4);
                    }
                    return true;
                }

                case HID_CMD_PROXIMITY_SENSOR_STS: {
                    uint32_t proxi_snsr_sts = isProxiSensorDetected();

                    HID_data_response_prepare(response, (uint8_t *)&proxi_snsr_sts, sizeof(proxi_snsr_sts));
                    return true;
                }

                case HID_CMD_HDMI_PASSTHROUGH_CTRL: {
                    msg_t msg = MSG_OK;
                    uint8_t hdmi_passthrough_enable = command->payload[0];

                    msg = coProcessorEnableHDMIPassthrough((hdmi_passthrough_enable > 0) ? TRUE : FALSE);
                    if(MSG_OK != msg) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_CS47L90_ACCESS: {
                    uint32_t addr = 0;
                    uint32_t val = 0;
                    size_t response_size = 0;

                    uint8_t rw = command->payload[0];
                    uint8_t *reg_data = &(command->payload[1]);
                    uint8_t *rw_data  = &(command->payload[5]);

                    addr = (reg_data[0] << 24) | (reg_data[1] << 16) | (reg_data[2] << 8) | (reg_data[3]);

                    if(rw == 0) {
                        if(addr >= 0x3000) {
                            val = (rw_data[0] << 24) | (rw_data[1] << 16) | (rw_data[2] << 8) | (rw_data[3]);
                        } else {
                            val = (rw_data[0] << 8) | (rw_data[1]);
                        }
                        dspWriteReg(addr, val, 0xffffffff, 0);
                    }

                    if(rw == 1) {
                        dspReadReg((uint8_t *)&val, addr, sizeof(val));
                    }

                    if(addr >= 0x3000) {
                        response_size = sizeof(uint32_t);
                    } else {
                        response_size = sizeof(uint16_t);
                    }

                    HID_data_response_prepare(response, (uint8_t *)&val, response_size);
                    return true;
                }

                case HID_CMD_GET_IO_MESSAGE: {
                    msg_t io_msg = ioGetMessage();

                    HID_data_response_prepare(response, (uint8_t *)&io_msg, sizeof(io_msg));
                    return true;
                }

                case HID_CMD_SET_IO_MESSAGE: {
                    uint8_t *io_msg_data = &(command->payload[0]);
                    uint32_t io_msg = (io_msg_data[2] << 24) | (io_msg_data[3] << 16) | (io_msg_data[1] << 8) | (io_msg_data[0]);

                    ioSetMessage((msg_t) io_msg);

                    HID_ack_response_prepare(response);
                    return true;
                }

                case HID_CMD_AVNERA_SET_NEXT_BOOT: {
                    uint8_t boot_app = command->payload[0];

                    if(MSG_OK != avneraSetNextBoot(boot_app)) {
                        HID_error_response_prepare(response, HID_ERROR_AVNERA_ERROR);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_AVNERA_GET_BOOT_INFO: {
                    avnera_boot_info_t boot_info = avneraGetBootInfo();

                    HID_data_response_prepare(response, (uint8_t *)&boot_info, sizeof(boot_info));
                    return true;
                }

                case HID_CMD_AVNERA_GET_FW_ADDR: {
                    uint8_t boot_app = command->payload[0];
                    uint32_t fw_addr = avneraGetAppFWAddr(boot_app);

                    HID_data_response_prepare(response, (uint8_t *)&fw_addr, sizeof(fw_addr));
                    return true;
                }

                case HID_CMD_AVNERA_AVBOOT_RESET: {
                    // Since Avboot reset will reset the chipset to next active set, we can not get
                    // reponse from chipset, so do not check the return value.
                    avneraAvbootReset();

                    HID_ack_response_prepare(response);
                    return true;
                }

                case HID_CMD_AVNERA_AVBOOT_AUTH: {
                    bool auth_sts = avneraAuthProcedure();

                    HID_data_response_prepare(response, (uint8_t *)&auth_sts, sizeof(auth_sts));
                    return true;
                }

                case HID_CMD_PGA2505_GET_PREAMP_GAIN: {
                    uint8_t pre_amp_gain;

                    if(PGA2505_OK != pga2505_get_preamp_gain(&pga2505_inst, &pre_amp_gain)) {
                        HID_error_response_prepare(response, HID_ERROR_PGA2505_FAILURE);
                    } else {
                        HID_data_response_prepare(response, (uint8_t *)&pre_amp_gain, sizeof(pre_amp_gain));
                    }
                    return true;
                }

                case HID_CMD_PGA2505_SET_PREAMP_GAIN: {
                    uint16_t pre_amp_gain = ((uint16_t)command->payload[0] << 8) | (uint16_t)command->payload[1];

                    if(PGA2505_OK != pga2505_set_preamp_gain(&pga2505_inst, (uint8_t)pre_amp_gain)) {
                        HID_error_response_prepare(response, HID_ERROR_PGA2505_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_RESET: {
                    if(MSG_OK != coProcessorReset()) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_ENTER_BLD: {
                    if(MSG_OK != coProcessorEnterBld()) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_ERASE_APP: {
                    if(MSG_OK != coProcessorEraseApp()) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_IS_READY: {
                    bool is_ready = isCoProcessorReadyForDfu();

                    HID_data_response_prepare(response, (uint8_t *)&is_ready, sizeof(is_ready));
                    return true;
                }

                case HID_CMD_COPROCESSOR_START_DFU: {
                    if(MSG_OK != coProcessorStartDFU()) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_END_DFU: {
                    if(MSG_OK != coProcessorEndDFU()) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_DFU_STS: {
                    coprocessor_dfu_sts_t dfu_sts = coProcessorGetDfuStatus();

                    HID_data_response_prepare(response, (uint8_t *)&dfu_sts, sizeof(dfu_sts));
                    return true;
                }

                case HID_CMD_COPROCESSOR_WR_APP: {
                    if(MSG_OK != coProcessorWriteAppData(command->payload, command->bytes)) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_RD_CHECKSUM: {
                    uint32_t check_sum = coProcessorGetChecksum();

                    HID_data_response_prepare(response, (uint8_t *)&check_sum, sizeof(check_sum));
                    return true;
                }

                case HID_CMD_QUALCOMM_ENTER_PAIRING: {
                    if(MSG_OK != qualcommEnterPairing()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_CANCEL_PAIRING: {
                    if(MSG_OK != qualcommCancelPairing()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_GET_BD_ADDR: {
                    bdaddr_t bd_addr = qualcommGetBdAddr();

                    HID_data_response_prepare(response, (uint8_t *)&bd_addr, sizeof(bdaddr_t));
                    return true;
                }

                case HID_CMD_QUALCOMM_SET_BD_ADDR: {
                    bdaddr_t bd_addr;
                    uint8_t *lap_data = &(command->payload[0]);
                    uint8_t *uap_data  = &(command->payload[4]);
                    uint8_t *nap_data  = &(command->payload[5]);

                    bd_addr.lap = (lap_data[0] << 24) | (lap_data[1] << 16) | (lap_data[2] << 8) | (lap_data[3]);
                    bd_addr.uap = uap_data[0];
                    bd_addr.nap = (nap_data[0] << 8) | (nap_data[1]);

                    if(MSG_OK != qualcommSetBdAddr(bd_addr)) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_SET_TX_PWR: {
                    uint16_t txpwr;
                    uint8_t *tx_pwr_data = &(command->payload[0]);

                    txpwr = (tx_pwr_data[0] << 8) | (tx_pwr_data[1]);

                    if(MSG_OK != qualcommSetTxPwr(txpwr)) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_START_CONT_TX: {
                    uint16_t lo_freq, level, mod_freq;
                    uint8_t *lo_freq_data = &(command->payload[0]);
                    uint8_t *level_data = &(command->payload[2]);
                    uint8_t *mod_freq_data = &(command->payload[4]);

                    lo_freq = (lo_freq_data[0] << 8) | (lo_freq_data[1]);
                    level = (level_data[0] << 8) | (level_data[1]);
                    mod_freq = (mod_freq_data[0] << 8) | (mod_freq_data[1]);

                    if(MSG_OK != qualcommStartContTx(lo_freq, level, mod_freq)) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_PAUSE_RADIO_TEST: {
                    if(MSG_OK != qualcommPauseRadioTest()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_UPGRADE_COMMAND: {
                    if(MSG_OK != qualcommSendUpgradeCmd(command->payload, command->bytes)) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_UPGRADE_DATA_TRANSFER: {
                    if(MSG_OK != qualcommSendUpgradeData(command->payload, command->bytes)) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_UPGRADE_RESPONSE_GET: {
                    uint8_t bytes;

                    if(MSG_OK != qualcommReceiveUpgradeResp(response->payload, &bytes)) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_data_response_prepare(response, NULL, bytes);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_UPGRADE_RESPONSE_CLR: {
                    if(MSG_OK != qualcommClearUpgradeResp()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_CLEAR_PDL: {
                    if(MSG_OK != qualcommClearPDL()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_SET_DUT: {
                    if(MSG_OK != qualcommSetDUT()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_QUALCOMM_START_BLE_BONDING: {
                    if(MSG_OK != qualcommStartBleBonding()) {
                        HID_error_response_prepare(response, HID_ERROR_QUALCOMM_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_RD_FTYPTN: {
                    uint32_t addr = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t len = command->payload[4];

                    if(len > sizeof(response->payload)) {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return true;
                    }

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    if(MSG_OK != coProcessorFtyPtnRead(addr, response->payload, len)) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_data_response_prepare(response, NULL, len);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_WR_FTYPTN: {
                    uint32_t addr = 0;
                    uint8_t *addr_data = &(command->payload[0]);
                    uint8_t len = command->payload[4];

                    if(len > sizeof(command->payload) - 5) {
                        HID_error_response_prepare(response, HID_ERROR_VALUE_OUT_OF_RANGE);
                        return TRUE;
                    }

                    addr = (addr_data[0] << 24) | (addr_data[1] << 16) | (addr_data[2] << 8) | (addr_data[3]);
                    if(MSG_OK != coProcessorFtyPtnWrite(addr, &command->payload[5], len)) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                case HID_CMD_COPROCESSOR_ER_FTYPTN: {
                    if(MSG_OK != coProcessorFtyPtnErase()) {
                        HID_error_response_prepare(response, HID_ERROR_COPROCESSOR_FAILURE);
                    } else {
                        HID_ack_response_prepare(response);
                    }
                    return true;
                }

                default:
                    return false;
            }
            break;

        default:
            return false;
    }
}
