# Settings
set(TARGET_NAME          carina-app)
set(STM32_LINKER_SCRIPT  ${CMAKE_CURRENT_LIST_DIR}/cfg/memcfg.ld)
set(STM32_DEVICE_FAMILY  H7)
set(STM32_USE_FPU        ON)
set(CARINA_ENABLE_GUI    ON)
set(STM32_GENERATE_BIN   ON)

# Include external dependencies
include(external.cmake)

# Get ChibiOS
find_package(
    ChibiOS 20.3.3
    COMPONENTS
        rt
        ext
        gpt
        pal
        adc
        usb
        sai
        serial
        uart
        sdram
        pwm
        spi
        wspi
        i2c
        chprintf
        memstreams
        serial_nor
    REQUIRED
)

# Get common
find_package(
    Common
    COMPONENTS
        crc
        debug
        dfu
        flash
        hid
        lfa_sign
        product
        stm32h750-app-jump-qspi
        usb
        usb-hid
        usb-audio
    REQUIRED
)

# Get drivers
find_package(
    Drivers
    REQUIRED
    COMPONENTS
        pga2505
)

# Get uGFX-GUI
find_package(
    CarinaGui
    REQUIRED
)

# Add uGFX driver
find_package(
    ugfx
    COMPONENTS
        drivers/gdisp/STM32LTDC
    REQUIRED
)

# Define target
set(TARGET ${TARGET_NAME}.elf)

# Create executable
add_executable(${TARGET})

# Generate build-version information
include(${LOGITECH_CMAKE_TOOLS}/build/build_version.cmake)
logitech_build_generate_version(${TARGET})

# Add checksum to end of generated binary
include(${LOGITECH_CMAKE_TOOLS}/app_checksum/app_checksum.cmake)
logitech_build_append_checksum(${TARGET})

# Generate J-Link debug utilities
include(${LOGITECH_CMAKE_TOOLS}/jlink/flash_external/stm32h750_qspi/stm32h750_qspi.cmake)
jlink_generate_app_debug(${TARGET} ${PROJECT_SOURCE_DIR}/resources/ramcode.elf)

# Setup ChibiOS stuff
target_setup_stm32(${TARGET})

# Add sub-directories
add_subdirectory(cfg)
add_subdirectory(usbh)

target_compile_features(
    ${TARGET}
    PRIVATE
        cxx_std_17
        c_std_11
)

target_link_directories(
    ${TARGET}
    PRIVATE
        ../../common/ld
)

target_include_directories(
    ${TARGET}
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}
)

target_compile_definitions(
    ${TARGET}
    PRIVATE
        CORTEX_USE_FPU  # Investigate - this shouldn't be necessary
        $<$<CONFIG:Debug>:DEBUG_BUILD>
        $<$<BOOL:${CARINA_ENABLE_GUI}>:CARINA_ENABLE_GUI>
)

target_compile_options(
    ${TARGET}
    PRIVATE
        -O2 -g
        $<$<COMPILE_LANGUAGE:CXX>:-fno-use-cxa-atexit>
        $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
        $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
)

target_link_options(
    ${TARGET}
    PRIVATE
        --specs=nano.specs
        --specs=nosys.specs
        "LINKER:-gc-sections"
        "LINKER:-flto"
)

target_link_libraries(
    ${TARGET}
    PRIVATE
        ChibiOS
        carina-common
        common
        drivers
        carina-gui
)

target_sources(
    ${TARGET}
    PRIVATE
        audio.c
        audio.h
        cs47l90_configs.c
        hid_proj.c
        hid_proj.h
        io.c
        io.h
        main.c
        project.h
        sai_proj.c
        sai_proj.h
        st7701.c
        st7701.h
        sys_proj.c
        sys_proj.h
        th_audio.h
        ui.cpp
        ui.h
        usbcfg.c
        usbcfg.h
        usb_proj_pc.c
        usb_proj_pc.h
        wireless.c
        wireless.h
        usbh_proj.c
        usbh_proj.h

        # Stuff from kepler-common
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/avnera.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/avnera.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/buttons.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/buttons.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/coprocessor.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/coprocessor.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/cs47l90.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/cs47l90.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/cs47l90_regdefs.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/dsp.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/dsp.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/encoders.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/encoders.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/endian.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/endian.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/proximity_snsr.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/proximity_snsr.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/qcc3024.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/qcc3024.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/qualcomm.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/qualcomm.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/sky76351_21.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/sky76351_21.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/stm32f446.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/stm32f446.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/stm32f446_imperium.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/stm32f446_imperium.h
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/xb1_imperium.c
        ${CMAKE_CURRENT_LIST_DIR}/../../common/kepler/xb1_imperium.h
)
