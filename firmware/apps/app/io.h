/**
 * @file    io.h
 * @brief   IO Management header.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */
#ifndef _IO_H_
#define _IO_H_

/*===========================================================================*/
/* App constants.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* App  pre-compile time settings.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* App  macros.                                                              */
/*===========================================================================*/

/*===========================================================================*/
/* App  exported variables.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/
#include "buttons.h"
#include "encoders.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    ENCODER_SELECT_BUTTON,
    MIC_MUTE_BUTTON,
    PHANTOM_POWER_BUTTON
} io_button_t;

typedef enum {
    LEFT_ENCODER,
    RIGHT_ENCODER
} io_encoder_t;

typedef enum {
    HP_JACK,
    AUX_JACK
} io_jack_t;

typedef void (*EncoderCb)(void);
typedef void (*ButtonCb)(button_event_t pressType);
typedef void (*JackCb)(bool present);

THD_FUNCTION(IO_Thread, arg);

msg_t ioGetMessage(void);
void ioSetMessage(msg_t message);

void ioRegisterEncoderCb(io_encoder_t encoder, encoder_event_t encEvent, EncoderCb pCB); // Only one function can be registered per encoder direction
void ioRegisterButtonCb(io_button_t btn, ButtonCb btnCB);
void ioRegisterJackCb(io_jack_t jack, JackCb jackCB);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* #ifndef _UI_H_ */
