/**
 * @file    wireless.h
 * @brief   Wireless Audio Module header.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */
#ifndef _WIRELESS_H_
#define _WIRELESS_H_

/*===========================================================================*/
/* Module constants.                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Module pre-compile time settings.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Module constants and error checks.                                       */
/*===========================================================================*/


/*===========================================================================*/
/* Module data structures and types.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Module macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Module exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief   Wireless Thread for wireless audio events.
 *
 * @param[out] arg              optional thread argument.
 *
 */
THD_FUNCTION(WIRELESS_Thread, arg);

/**
 * @brief   Send an event that to write the Qualcomm Centpp Server Data
 *
 * @param[in]  data     The pointer to the data associated with the event
 * @param[in]  len      The length to the data associated with the event
 *
 * @return              None
 *
 * @api
 */
void wirelessWriteQtilCentppServerDataEvent(const uint8_t *data, uint16_t len);

/**
 * @brief   Send an event that to send the Qualcomm Centpp Server read notification
 *
 * @param[in]  data     The pointer to the data associated with the event
 * @param[in]  len      The length to the data associated with the event
 *
 * @return              None
 *
 * @api
 */
void wirelessSendQtilCentppServerReadNotifyEvent(const uint8_t *data, uint16_t len);

/**
 * @brief   Send an event that to send the Qualcomm Centpp Server write notification
 *
 * @param[in]  data     The pointer to the data associated with the event
 * @param[in]  len      The length to the data associated with the event
 *
 * @return              None
 *
 * @api
 */
void wirelessSendQtilCentppServerWriteNotifyEvent(const uint8_t *data, uint16_t len);

#ifdef __cplusplus
}
#endif

#endif /* _WIRELESS_H_ */

/** @} */

