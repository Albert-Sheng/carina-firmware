/**
 * @file    sys_proj.c
 * @brief   The Carina system manager. Basically the central nervous system of Carina
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */

#include "sys_proj.h"
#include "debug.h"
#include "io.h"
#include "ui.h"
#include "audio.h"

/*===========================================================================*/
/* System local definitions.                                                    */
/*===========================================================================*/

#define MAILBOX_SIZE            4
#define POOL_SIZE               8

#define VOLUME_LEVEL_MIN     0
#define VOLUME_LEVEL_DEFAULT 50
#define VOLUME_LEVEL_MAX     100

#define MIX_LEVEL_MIN     0
#define MIX_LEVEL_DEFAULT 50
#define MIX_LEVEL_MAX     100

#define PHANTOM_LEVEL_MIN     0
#define PHANTOM_LEVEL_DEFAULT 50
#define PHANTOM_LEVEL_MAX     100

/*===========================================================================*/
/* System local structures                                                   */
/*===========================================================================*/
typedef struct {
    uint8_t volumeLevel;
    uint8_t mixLevel;
    uint8_t phantomPowerLevel;
    bool muted;
    bool hpJackPresent;
    bool auxJackPresent;
} sys_status_t;

typedef enum {
    SYS_EVENT_VOLUME_INCREMENT,
    SYS_EVENT_VOLUME_DECREMENT,
    SYS_EVENT_VOLUME_SET,
    SYS_EVENT_MIX_INCREMENT,
    SYS_EVENT_MIX_DECREMENT,
    SYS_EVENT_MIX_SET,
    SYS_EVENT_MUTE,
    SYS_EVENT_PHANTOM,
    SYS_EVENT_AUX_JACK,
    SYS_EVENT_HP_JACK
} sys_event_type_t;

typedef struct {
    sys_event_type_t event_type;
    uint32_t data; // This might grow...
} sys_event_t;

/*===========================================================================*/
/* Private data                                                              */
/*===========================================================================*/
static sys_status_t s_status = {
    .volumeLevel = VOLUME_LEVEL_DEFAULT,
    .mixLevel = MIX_LEVEL_DEFAULT,
    .phantomPowerLevel = PHANTOM_LEVEL_MIN,
    .muted = false,
    .hpJackPresent = false,
    .auxJackPresent = false
};

// Memory pool of events
static sys_event_t s_sys_events[POOL_SIZE];
static memory_pool_t s_sys_pool;
static msg_t s_mailbox_buffer[MAILBOX_SIZE];
static MAILBOX_DECL(s_mailbox, s_mailbox_buffer, MAILBOX_SIZE);

/*===========================================================================*/
/* App exported variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* App external variables.                                                   */
/*===========================================================================*/


/*===========================================================================*/
/* App local functions.                                                      */
/*===========================================================================*/
/**
 * Helper to send an event to our thread
 * @param evt  The event to send
 * @param data The data associated with the event
 */
static inline void sysSendMessage(sys_event_type_t evt, uint32_t data) {
    // Try to get an event from the memory pool
    sys_event_t *ev = chPoolAlloc(&s_sys_pool);
    if(ev) {
        ev->event_type = evt;
        ev->data = data;
        chMBPostTimeout(&s_mailbox, (msg_t)ev, 50);
    }
}
/**
 * Process an incoming event we sent to ourself via the API
 * @param evt The event object to process
 */
static void sysProcessEvent(sys_event_t const *evt) {

    switch(evt->event_type) {
        case SYS_EVENT_VOLUME_INCREMENT:
            if(VOLUME_LEVEL_MAX > s_status.volumeLevel) {
                ++s_status.volumeLevel;
                uiSetVolume(s_status.volumeLevel);
                dspSetGain(s_status.volumeLevel, HPOUT1_gain, HPOUT1_gain_regs, (sizeof(HPOUT1_gain_regs) / sizeof(uint32_t)));
                //Dbg_printf(DBG_IO, "volume percentage: %d", s_status.volumeLevel);
            }
            break;
        case SYS_EVENT_VOLUME_DECREMENT:
            if(VOLUME_LEVEL_MIN < s_status.volumeLevel) {
                --s_status.volumeLevel;
                uiSetVolume(s_status.volumeLevel);
                dspSetGain(s_status.volumeLevel, HPOUT1_gain, HPOUT1_gain_regs, (sizeof(HPOUT1_gain_regs) / sizeof(uint32_t)));
                //Dbg_printf(DBG_IO, "volume percentage: %d", s_status.volumeLevel);
            }
            break;
        case SYS_EVENT_VOLUME_SET:
            // Doing conversion here to prevent warning
            if(VOLUME_LEVEL_MIN <= (int32_t)evt->data && evt->data <= VOLUME_LEVEL_MAX) {
                s_status.volumeLevel = evt->data;
                uiSetVolume(s_status.volumeLevel);
                dspSetGain(s_status.volumeLevel, HPOUT1_gain, HPOUT1_gain_regs, (sizeof(HPOUT1_gain_regs) / sizeof(uint32_t)));
                Dbg_printf(DBG_IO, "volume percentage: %d", s_status.volumeLevel);
            }
            break;
        case SYS_EVENT_MIX_INCREMENT:
            if(MIX_LEVEL_MAX > s_status.mixLevel) {
                ++s_status.mixLevel;
                uiSetMixLevel(s_status.mixLevel);
                Dbg_printf(DBG_IO, "mix level: %d", s_status.mixLevel);
            }
            break;
        case SYS_EVENT_MIX_DECREMENT:
            if(MIX_LEVEL_MIN < s_status.mixLevel) {
                --s_status.mixLevel;
                uiSetMixLevel(s_status.mixLevel);
                Dbg_printf(DBG_IO, "mix level: %d", s_status.mixLevel);
            }
            break;
        case SYS_EVENT_MIX_SET:
            // Doing conversion here to prevent warning
            if(MIX_LEVEL_MIN <= (int32_t)evt->data && evt->data <= MIX_LEVEL_MAX) {
                s_status.mixLevel = evt->data;
                uiSetMixLevel(s_status.mixLevel);
                Dbg_printf(DBG_IO, "mix level: %d", s_status.mixLevel);
            }
            break;
        case SYS_EVENT_MUTE:
            // TODO - we receive data related to press type that we need to handle/use
            // switch(msg.io.event) {
            //     case BUTTON_SHORT_PRESS_EVENT:
            //         TODO("Perform mic mute short button press function here.")
            //         break;

            //     case BUTTON_LONG_PRESS_EVENT:
            //         TODO("Perform mic mute long button press function here.")
            //         break;

            //     case BUTTON_VERYLONG_PRESS_EVENT:
            //         TODO("Perform mic mute very long button press function here.")
            //         break;

            //     default:
            //         Dbg_printf(DBG_ERROR, "Uknown mic mute press event: %d", msg.io.event);
            //         break;
            // }

            s_status.muted = !s_status.muted;
            if(!s_status.muted) {
                uiSetMixLevel(s_status.mixLevel);
            } else {
                uiSetMuted(s_status.muted);
            }

            break;
        case SYS_EVENT_PHANTOM:
            // TODO - we need to implement proper behavior here for phantom power level
            // switch(msg.io.event) {
            //     case BUTTON_SHORT_PRESS_EVENT:
            //         TODO("Perform phantome power short button press function here.")
            //         break;

            //     case BUTTON_LONG_PRESS_EVENT:
            //         TODO("Perform phantome power long button press function here.")
            //         break;

            //     case BUTTON_VERYLONG_PRESS_EVENT:
            //         TODO("Perform phantome power very long button press function here.")
            //         break;

            //     default:
            //         Dbg_printf(DBG_ERROR, "Uknown phantom pwr press event: %d", msg.io.event);
            //         break;
            // }
            if(s_status.phantomPowerLevel < PHANTOM_LEVEL_MAX) {
                s_status.phantomPowerLevel += 5;
                if(s_status.phantomPowerLevel > PHANTOM_LEVEL_MAX) {
                    s_status.phantomPowerLevel = PHANTOM_LEVEL_MAX;
                }
            } else {
                s_status.phantomPowerLevel = PHANTOM_LEVEL_MIN;
            }

            uiPhantomPower(s_status.phantomPowerLevel);
            break;
        case SYS_EVENT_AUX_JACK:
            s_status.auxJackPresent = (bool)evt->data;
            if(!s_status.auxJackPresent) {
                uiSetMixLevel(s_status.mixLevel);
            } else {
                uiAuxJack(s_status.auxJackPresent);
            }


            break;
        case SYS_EVENT_HP_JACK:
            s_status.hpJackPresent = (bool)evt->data;
            if(!s_status.hpJackPresent) {
                uiSetMixLevel(s_status.mixLevel);
            } else {
                uiHPJack(s_status.hpJackPresent);
            }

            break;
        default:
            break;
    }
}

/*===========================================================================*/
/* App exported functions.                                                   */
/*===========================================================================*/

/**
 * @brief   UI Thread for events.
 *
 * @param[out] arg              optional thread argument.
 *
 */
THD_FUNCTION(SYS_Thread, arg) {
    (void)arg;

    chRegSetThreadName("System Manager");

    chMBObjectInit(&s_mailbox, (msg_t *)s_mailbox_buffer, MAILBOX_SIZE);
    chPoolObjectInit(&s_sys_pool, POOL_SIZE, NULL);

    for(int32_t i = 0; i < POOL_SIZE; i++) {
        chPoolAdd(&s_sys_pool, &s_sys_events[i]);
    }

    // Register callbacks
    ioRegisterButtonCb(MIC_MUTE_BUTTON, sysHandleMuteEvent);
    ioRegisterButtonCb(PHANTOM_POWER_BUTTON, sysHandlePhantomPowerEvent);
    ioRegisterEncoderCb(LEFT_ENCODER, ENCODER_CLOCKWISE_EVENT, sysVolumeIncrement);
    ioRegisterEncoderCb(LEFT_ENCODER, ENCODER_COUNTER_CLOCKWISE_EVENT, sysVolumeDecrement);
    ioRegisterJackCb(HP_JACK, sysHandleHPJackEvent);
    ioRegisterJackCb(AUX_JACK, sysHandleAuxJackEvent);

    Dbg_printf(DBG_INFO, "System Manager thread started.");

    while(!chThdShouldTerminateX()) {
        msg_t msg;
        if(MSG_OK == chMBFetchTimeout(&s_mailbox, &msg, TIME_INFINITE)) {
            // The message is always a system_event_t
            sys_event_t *ev = (sys_event_t *)msg;
            // Handle the event
            sysProcessEvent(ev);
            chPoolFree(&s_sys_pool, ev);    // Put the event back in the pool
        }
    }
    Dbg_printf(DBG_INFO, "System Manager thread finished.");
    chThdExit(MSG_OK);
}

void sysVolumeIncrement(void) {
    sysSendMessage(SYS_EVENT_VOLUME_INCREMENT, 0);

}
void sysVolumeDecrement(void) {
    sysSendMessage(SYS_EVENT_VOLUME_DECREMENT, 0);
}
void sysVolumeSet(uint8_t level) {
    sysSendMessage(SYS_EVENT_VOLUME_SET, level);
}
void sysMixerIncrement(void) {
    sysSendMessage(SYS_EVENT_MIX_INCREMENT, 0);
}
void sysMixerDecrement(void) {
    sysSendMessage(SYS_EVENT_MIX_DECREMENT, 0);
}
void sysMixerSet(uint8_t level) {
    sysSendMessage(SYS_EVENT_MIX_SET, level);
}
void sysHandleMuteEvent(button_event_t evt) {
    sysSendMessage(SYS_EVENT_MUTE, evt);
}
void sysHandlePhantomPowerEvent(button_event_t evt) {
    sysSendMessage(SYS_EVENT_PHANTOM, evt);
}
void sysHandleHPJackEvent(bool present) {
    sysSendMessage(SYS_EVENT_HP_JACK, present);
}
void sysHandleAuxJackEvent(bool present) {
    sysSendMessage(SYS_EVENT_AUX_JACK, present);
}

uint8_t sysGetBalanceMix(void) {
    return s_status.mixLevel;
}

uint8_t sysGetVolume(void) {
    return s_status.volumeLevel;
}

bool sysGetMute(void) {
    return s_status.muted;
}
