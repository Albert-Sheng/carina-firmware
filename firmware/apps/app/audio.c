#include "audio.h"
#include "cs47l90_regdefs.h"

const uint32_t HPOUT1_gain_regs[2] = {
    CS47L90_DAC_DIGITAL_VOLUME_1L,
    CS47L90_DAC_DIGITAL_VOLUME_1R,
};

const dspTable_t HPOUT1_gain[] = {
    {0,   0x0100}, // Mute
    {9,   0x0100}, // Mute
    {10,  0x0003},
    {20,  0x000f},
    {25,  0x0019},
    {30,  0x001f},
    {35,  0x0025},
    {40,  0x003a},
    {45,  0x0045},
    {50,  0x004e},
    {55,  0x005a},
    {60,  0x0061},
    {65,  0x0067},
    {70,  0x006e},
    {75,  0x0074},
    {80,  0x0076},
    {85,  0x007a},
    {90,  0x007d},
    {93,  0x007e},
    {95,  0x007f},
    {100, 0x0080},
    dspTableEnd
};
