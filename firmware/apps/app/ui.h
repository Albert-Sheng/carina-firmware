/**
 * @file    ui.h
 * @brief   UI Management header.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */
#ifndef _UI_H_
#define _UI_H_

#include <ch.h>
#include <hal.h>

#include "io.h"
#include "buttons.h"

/*===========================================================================*/
/* App constants.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* App  pre-compile time settings.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* App  macros.                                                              */
/*===========================================================================*/

/*===========================================================================*/
/* App  exported variables.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

THD_FUNCTION(UI_Thread, arg);

void uiShowColorView(void);
void uiShowVolumeView(void);
void uiShowNextView(void);
void uiShowPreviousView(void);
void uiSetColor(uint32_t color);
void uiSetBacklight(uint8_t percent);
void uiSetVolume(uint8_t volumeLevel);  /* Valid range is 0-100 */
void uiSetMixLevel(uint8_t mixerLevel);    /* Valid range is 0-100 */
void uiSetMuted(bool muted);
void uiNavigateRight(void);
void uiNavigateLeft(void);
void uiNavigateButtonEvent(button_event_t pressType);
void uiAuxJack(bool present);
void uiHPJack(bool present);
void uiPhantomPower(uint8_t level);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* #ifndef _UI_H_ */
