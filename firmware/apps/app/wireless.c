/**
 * @file    wireless.c
 * @brief   Wireless Audio Module code.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */

#include <string.h>
#include "hal.h"
#include "debug.h"
#include "avnera.h"
#include "qualcomm.h"
#include "wireless.h"
#ifdef ENABLE_CENTPP
#include "mbg_centpp_api.h"
#endif

/*===========================================================================*/
/* Module local definitions.                                                 */
/*===========================================================================*/
#define MAILBOX_SIZE                                                    (4)
#define EVENTS_NUMBER                                                   (2)
#define POOL_SIZE                                   (sizeof(wireless_event_t))
#define WIRELESS_MB_TIMEOUT                                     TIME_MS2I(20)  // 20 msec

#define WIRELESS_EVENT_DATA_MAX_LEN                                     (512)  // should be integer multiples of sizeof(uint32_t)

/*===========================================================================*/
/* Module local variables and types.                                         */
/*===========================================================================*/
typedef enum {
    WIRELESS_EVENT_QTIL_WRITE_CENTPP_SERVER_DATA,
    WIRELESS_EVENT_QTIL_SEND_CENTPP_SERVER_READ_NOTIFY,
    WIRELESS_EVENT_QTIL_SEND_CENTPP_SERVER_WRITE_NOTIFY,
} wireless_event_type_t;

typedef struct {
    wireless_event_type_t event_type;
    uint16_t length;
    uint32_t data[WIRELESS_EVENT_DATA_MAX_LEN / sizeof(uint32_t)];
} wireless_event_t;

// Memory pool of events
static wireless_event_t wireless_events[EVENTS_NUMBER];
static memory_pool_t wireless_pool;
static msg_t mailbox_buffer[MAILBOX_SIZE];
static MAILBOX_DECL(mailbox, mailbox_buffer, MAILBOX_SIZE);
#ifdef ENABLE_CENTPP
static uint8_t ble_gatt_idx = 0xFF;
#endif

/*===========================================================================*/
/* Module exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Module local functions.                                                   */
/*===========================================================================*/
#ifdef ENABLE_CENTPP
/**
 * @brief   Callback function for Centpp TX
 *
 * @param[in]  payload              The centpp data to send to host
 * @param[in]  payload_len          The length of the centpp data to send
 * @param[in]  transport_context    The context of Centpp Engine
 *
 * @return     tCentPPResult        The operation status
 * @retval     CENTPP_RES_SUCCESS   if the operation completed successfully.
 *
 * @api
 */
static tCentPPResult wirelessBleCentppTx(const uint8_t *payload, uint16_t payload_len, void *transport_context) {

    (void)transport_context;

    // data may be truncated if payload_len is greater than WIRELESS_EVENT_DATA_MAX_LEN
    if(payload_len > WIRELESS_EVENT_DATA_MAX_LEN) {
        payload_len = WIRELESS_EVENT_DATA_MAX_LEN;
    }

    // if data is less than 20, use write characteristics, if data is greater than 20
    // use read characteristics.
    if(payload_len <= 20) {
        wirelessSendQtilCentppServerWriteNotifyEvent(payload, payload_len);
    } else {
        // write data to Qualcomm and notify host to read
        wirelessWriteQtilCentppServerDataEvent(payload, payload_len);
    }

    return CENTPP_RES_SUCCESS;
}

/**
 * @brief   Register Centpp BLE transport
 *
 * @param[in]  void                 None
 *
 * @return     void                 None
 *
 * @api
 */
static void wirelessBleCentppRegister(void) {
    centpp_register_transport_t transport = {0};
    transport.context = NULL;
    transport.mtu_size = WIRELESS_EVENT_DATA_MAX_LEN;
    transport.tx_callback = wirelessBleCentppTx;
    ble_gatt_idx = centpp_register_transport(&transport);
}
#else
#define wirelessBleCentppRegister() ((void)(0))
#endif

/**
 * @brief   Helper to send an event to this thread
 *
 * @param[in]  evt      The event to send
 * @param[in]  data     The pointer to the data associated with the event
 * @param[in]  len      The length to the data associated with the event
 *
 * @return              None
 *
 * @api
 */
static inline void wirelessSendMessage(wireless_event_type_t evt,
                                       const uint8_t *data,
                                       uint16_t len) {
    // Try to get an event from the memory pool
    wireless_event_t *ev = chPoolAlloc(&wireless_pool);
    if(ev) {
        ev->event_type = evt;
        if(len > WIRELESS_EVENT_DATA_MAX_LEN) {
            len = WIRELESS_EVENT_DATA_MAX_LEN;
        }
        ev->length = len;
        memcpy((uint8_t *)ev->data, data, len);
        chMBPostTimeout(&mailbox, (msg_t)ev, 50);
    }
}

/**
 * @brief   Read Qualcomm Centpp Server data and send it to Centpp Engine
 *
 * @param[in]  void     None
 *
 * @return              None
 *
 * @api
 */
static void wirelessReadQtilCentppServerData(void) {
    uint16_t length = 0;
    uint8_t buf[WIRELESS_EVENT_DATA_MAX_LEN];

    if((MSG_OK == qualcommReadCentppServerData(buf, &length)) && (0 != length)) {
        Dbg_printf(DBG_WIRELESS, "Get %d bytes Centpp Server Data!", length);
#ifdef ENABLE_CENTPP
        cpl_receive_from_host(buf, length, ble_gatt_idx);
#endif
    }
}

/**
 * @brief   Process an incoming event
 *
 * @param[in]  evt      The event object to process
 *
 * @return              None
 *
 * @api
 */
static void wirelessProcessEvent(wireless_event_t const *evt) {

    switch(evt->event_type) {
        case WIRELESS_EVENT_QTIL_WRITE_CENTPP_SERVER_DATA: {
            int retry_cnt = 10;
            Dbg_printf(DBG_WIRELESS, "Process write_centpp_server_data here!");

            do {
                if(MSG_OK == qualcommWriteCentppServerData((uint8_t *)evt->data, evt->length)) {
                    break;
                }
            } while(retry_cnt--);

            if(retry_cnt <= 0) {
                Dbg_printf(DBG_ERROR, "Send %d bytes Centpp Server data to Qualcomm Fail!", evt->length);
            } else {
                uint8_t notify[2];
                // send notification to host to launch a read
                notify[0] = (uint8_t)((evt->length & 0xFF00) >> 8);
                notify[1] = (uint8_t)(evt->length & 0x00FF);
                wirelessSendQtilCentppServerReadNotifyEvent(notify, 2);
            }
            break;
        }
        case WIRELESS_EVENT_QTIL_SEND_CENTPP_SERVER_READ_NOTIFY: {
            int retry_cnt = 10;
            Dbg_printf(DBG_WIRELESS, "Process send_centpp_server_read_notify here!");
            do {
                if(MSG_OK == qualcommSendCentppServerReadNotification((uint8_t *)evt->data, evt->length)) {
                    break;
                }
            } while(retry_cnt--);

            if(retry_cnt <= 0) {
                Dbg_printf(DBG_ERROR, "Send %d bytes read notification to Qualcomm Fail!", evt->length);
            }
            break;
        }
        case WIRELESS_EVENT_QTIL_SEND_CENTPP_SERVER_WRITE_NOTIFY: {
            int retry_cnt = 10;
            Dbg_printf(DBG_WIRELESS, "Process send_centpp_server_write_notify here!");

            do {
                if(MSG_OK == qualcommSendCentppServerWriteNotification((uint8_t *)evt->data, evt->length)) {
                    break;
                }
            } while(retry_cnt--);

            if(retry_cnt <= 0) {
                Dbg_printf(DBG_ERROR, "Send %d bytes write notification to Qualcomm Fail!", evt->length);
            }
            break;
        }
        default:
            break;
    }
}

/*===========================================================================*/
/* Module exported functions.                                                */
/*===========================================================================*/
THD_FUNCTION(WIRELESS_Thread, arg) {
    (void)arg;

    chRegSetThreadName("Wireless Manager");

    chMBObjectInit(&mailbox, (msg_t *)mailbox_buffer, MAILBOX_SIZE);
    chPoolObjectInit(&wireless_pool, POOL_SIZE, NULL);

    for(int32_t i = 0; i < EVENTS_NUMBER; i++) {
        chPoolAdd(&wireless_pool, &wireless_events[i]);
    }

    Dbg_printf(DBG_INFO, "Wireless Manager thread started.");

    // register Centpp BLE transport
    wirelessBleCentppRegister();

    while(!chThdShouldTerminateX()) {
        msg_t msg;

        wirelessReadQtilCentppServerData();
        if(MSG_OK == chMBFetchTimeout(&mailbox, &msg, WIRELESS_MB_TIMEOUT)) {
            // The message is always a wireless_event_t
            wireless_event_t *ev = (wireless_event_t *)msg;
            // Handle the event
            wirelessProcessEvent(ev);
            chPoolFree(&wireless_pool, ev);  // Put the event back in the pool
        }

        chThdSleepMilliseconds(100);
    }
    Dbg_printf(DBG_INFO, "Wireless Manager thread finished.");
    chThdExit(MSG_OK);
}


void wirelessWriteQtilCentppServerDataEvent(const uint8_t *data, uint16_t len) {
    wirelessSendMessage(WIRELESS_EVENT_QTIL_WRITE_CENTPP_SERVER_DATA, data, len);
}


void wirelessSendQtilCentppServerReadNotifyEvent(const uint8_t *data, uint16_t len) {
    wirelessSendMessage(WIRELESS_EVENT_QTIL_SEND_CENTPP_SERVER_READ_NOTIFY, data, len);
}


void wirelessSendQtilCentppServerWriteNotifyEvent(const uint8_t *data, uint16_t len) {
    wirelessSendMessage(WIRELESS_EVENT_QTIL_SEND_CENTPP_SERVER_WRITE_NOTIFY, data, len);
}

/** @} */

