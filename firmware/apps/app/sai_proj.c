#include "sai_proj.h"
#include "hal.h"
#include "debug.h"
#include "rate_conv.h"
#include "usbcfg.h"
#include "th_audio.h"
#include "usb_proj_pc.h"
#include "usbh_proj.h"

#include <string.h>



/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define FRAME_BYTES(b, c)   ((b / 8) * c)

/*
 * Number of audio buffers in audio queues.
 */
#define SAI_BUFFER_MULT     8

/*
 * SAI1 audio configuration settings.
 */
#define SAI1_SAMPLE_RATE    AUDIO_FREQUENCY_48K
#define SAI1_BITS           AUDIO_RESOLUTION_32B
#define SAI1_CHANNELS       AUDIO_SLOTS_4

#define SAI1_FRAME_BYTES    FRAME_BYTES(SAI1_BITS, SAI1_CHANNELS)
#define SAI1_XFER_SIZE      (SAI1_FRAME_BYTES * SAI1_SAMPLE_RATE / 2000)    // .5ms
#define SAI1_BUF_SIZE       BQ_BUFFER_SIZE(SAI_BUFFER_MULT, SAI1_XFER_SIZE)

/*
 * SAI2 audio configuration settings.
 */
#define SAI2_SAMPLE_RATE    AUDIO_FREQUENCY_48K
#define SAI2_BITS           AUDIO_RESOLUTION_32B
#define SAI2_CHANNELS       AUDIO_SLOTS_2

#define SAI2_FRAME_BYTES    FRAME_BYTES(SAI2_BITS, SAI2_CHANNELS)
#define SAI2_XFER_SIZE      (SAI2_FRAME_BYTES * SAI2_SAMPLE_RATE / 2000)    // .5ms
#define SAI2_BUF_SIZE       BQ_BUFFER_SIZE(SAI_BUFFER_MULT, SAI2_XFER_SIZE)

/*
 * Ratio used for mixing SAI1 & SAI2 audio buffers.
 */
#define RC_MIX_RATIO        (SAI1_SAMPLE_RATE / SAI2_SAMPLE_RATE)

#define BYTES2WORDS(n)      (n / 4)

extern output_queue_t usbpc_q_game_mix;
extern output_queue_t usbpc_q_mic;
extern output_queue_t usbh_oq_game_mix;
extern input_queue_t usbh_iq_mic;

typedef struct {
    thread_t *thread_ptr;
    output_buffers_queue_t txqueue;
    input_buffers_queue_t rxqueue;
    size_t prev_tx_xfer_size;
    size_t prev_rx_xfer_size;
} thread_param_t;


/*
 ******************************************************************************
 * FUNCTION PROTOTYPES
 ******************************************************************************
 */

static void sai1_rx_cb(void **buf, size_t *words);
static void sai1_tx_cb(void **buf, size_t *words);
static void sai2_rx_cb(void **buf, size_t *words);
static void sai2_tx_cb(void **buf, size_t *words);


/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

static const SAIConfig sai1a_Config = {
    .mode = SAI_MODE_I2S_TX_SLAVE,
    .sample_rate = SAI1_SAMPLE_RATE,
    .num_slots = SAI1_CHANNELS,
    .bit_depth = SAI1_BITS,
    .cb = sai1_tx_cb,
    .cr1 = SAI_xCR1_CKSTR,
};

static const SAIConfig sai1b_Config = {
    .mode = SAI_MODE_I2S_RX_SLAVE,
    .sample_rate = SAI1_SAMPLE_RATE,
    .num_slots = SAI1_CHANNELS,
    .bit_depth = SAI1_BITS,
    .cb = sai1_rx_cb,
    .cr1 = SAI_xCR1_CKSTR | SAI_xCR1_SYNCEN_0,   // Sync B block to A
};

static const SAIConfig sai2a_Config = {
    .mode = SAI_MODE_I2S_TX_SLAVE,
    .sample_rate = SAI2_SAMPLE_RATE,
    .num_slots = SAI2_CHANNELS,
    .bit_depth = SAI2_BITS,
    .cb = sai2_tx_cb,
    .cr1 = SAI_xCR1_CKSTR,
};

static const SAIConfig sai2b_Config = {
    .mode = SAI_MODE_I2S_RX_SLAVE,
    .sample_rate = SAI2_SAMPLE_RATE,
    .num_slots = SAI2_CHANNELS,
    .bit_depth = SAI2_BITS,
    .cb = sai2_rx_cb,
    .cr1 = SAI_xCR1_CKSTR | SAI_xCR1_SYNCEN_0,   // Sync B block to A
};


static thread_param_t game_mic_thread = { 0 };
static thread_param_t chat_thread = { 0 };

static THD_WORKING_AREA(waGameMic, 8192);
static THD_WORKING_AREA(waChat, 4096);


/* Blank buffers to deal with underrun and overruns */
static uint8_t sai1a_zeros[SAI1_XFER_SIZE] __attribute__((section(".ram3_init"))) = { 0 };
static uint8_t sai1b_zeros[SAI1_XFER_SIZE] __attribute__((section(".ram3_init"))) = { 0 };
static uint8_t sai2a_zeros[SAI2_XFER_SIZE] __attribute__((section(".ram3_init"))) = { 0 };
static uint8_t sai2b_zeros[SAI2_XFER_SIZE] __attribute__((section(".ram3_init"))) = { 0 };


/*
 ******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************
 */

static inline void sai_rx(void **buf, size_t *words, size_t *prev_bytes, input_buffers_queue_t *rxqueue, size_t xfer_size) {
    if(*prev_bytes) {
        ibqPostFullBufferI(rxqueue, *prev_bytes);
    }

    *prev_bytes = *words = 0;
    *buf = ibqGetEmptyBufferI(rxqueue);

    if(*buf) {
        *prev_bytes = xfer_size;
        *words = BYTES2WORDS(xfer_size);
    }
}


static inline void sai_tx(void **buf, size_t *words, size_t *prev_bytes, output_buffers_queue_t *txqueue) {
    if(*prev_bytes) {
        obqReleaseEmptyBufferI(txqueue);
    }

    *prev_bytes = *words = 0;
    *buf = obqGetFullBufferI(txqueue, words);

    if(*buf) {
        *prev_bytes = *words;
        *words = BYTES2WORDS(*words);
    }
}

static void sai1_rx_cb(void **buf, size_t *words) {
    sai_rx(buf, words, &game_mic_thread.prev_rx_xfer_size, &game_mic_thread.rxqueue, SAI1_XFER_SIZE);

    /* Handle overrun case */
    if(!*buf) {
        *buf = sai1b_zeros;
        *words = BYTES2WORDS(sizeof(sai1b_zeros));
    }
}


static void sai1_tx_cb(void **buf, size_t *words) {
    sai_tx(buf, words, &chat_thread.prev_tx_xfer_size, &chat_thread.txqueue);

    /* Handle underrun case */
    if(!*buf) {
        *buf = sai1a_zeros;
        *words = BYTES2WORDS(sizeof(sai1a_zeros));
    }
}

static void sai2_rx_cb(void **buf, size_t *words) {
    sai_rx(buf, words, &chat_thread.prev_rx_xfer_size, &chat_thread.rxqueue, SAI2_XFER_SIZE);

    /* Handle overrun case */
    if(!*buf) {
        *buf = sai2b_zeros;
        *words = BYTES2WORDS(sizeof(sai2b_zeros));
    }
}


static void sai2_tx_cb(void **buf, size_t *words) {
    sai_tx(buf, words, &game_mic_thread.prev_tx_xfer_size, &game_mic_thread.txqueue);

    /* Handle underrun case */
    if(!*buf) {
        *buf = sai2a_zeros;
        *words = BYTES2WORDS(sizeof(sai2a_zeros));
    }
}

static bool stop_loop_thread(thread_param_t *params) {
    if(params->thread_ptr == NULL) {
        Dbg_printf(DBG_INFO, "SAI thread already stopped");
        return false;
    }

    chThdTerminate(params->thread_ptr);
    chThdWait(params->thread_ptr);

    params->thread_ptr = NULL;
    params->prev_tx_xfer_size = 0;
    params->prev_rx_xfer_size = 0;

    osalSysLock();
    obqResetI(&params->txqueue);
    ibqResetI(&params->rxqueue);
    osalSysUnlock();

    return true;
}

static size_t prepare_write(size_t samples, output_queue_t *oqp, void **oq_buf, size_t bytes_per_sample) {
    size_t upper_limit;
    size_t n = (samples * bytes_per_sample);

    osalDbgCheck(n > 0U);

    osalSysLock();

    if(oqIsFullI(oqp)) {
        osalThreadEnqueueTimeoutS(&oqp->q_waiting, TIME_IMMEDIATE);
        osalSysUnlock();
        *oq_buf = NULL;
        return 0;
    }

    if(n > oqGetEmptyI(oqp)) {
        n = oqGetEmptyI(oqp);
    }

    osalSysUnlock();

    upper_limit = (size_t)(oqp->q_top - oqp->q_wrptr);

    *oq_buf = oqp->q_wrptr;

    if(n <= upper_limit) {
        return (n / bytes_per_sample);
    } else {
        return (upper_limit / bytes_per_sample);
    }
}

static void complete_write(size_t samples, output_queue_t *oqp, size_t bytes_per_sample) {
    size_t n = samples * bytes_per_sample;

    osalSysLock();

    oqp->q_counter -= n;
    oqp->q_wrptr += n;

    if(oqp->q_wrptr >= oqp->q_top) {
        oqp->q_wrptr = oqp->q_buffer;
    }

    osalSysUnlock();
}

static void samples_copy(size_t samples, size_t sample_bytes, void *dst_buf, size_t dst_step_bytes, void *src_buf, size_t src_step_bytes) {
    size_t sample;
    for(sample = 0; sample < samples; sample++) {
        memcpy((uint8_t*)dst_buf + (sample * dst_step_bytes), (uint8_t*)src_buf + (sample * src_step_bytes), sample_bytes);
    }
}

static void usbh_samples_copy(size_t samples, size_t sample_bytes, void *dst_buf, size_t dst_step_bytes, void *src_buf, size_t src_step_bytes) {
    size_t sample;
    for(sample = 0; sample < samples; sample++) {
        memcpy((uint8_t*)dst_buf + (sample * dst_step_bytes), (uint8_t*)src_buf + (sample * src_step_bytes + src_step_bytes / 4), sample_bytes / 2);
        memcpy((uint8_t*)dst_buf + (sample * dst_step_bytes + dst_step_bytes / 2), (uint8_t*)src_buf + (sample * src_step_bytes + 3 * src_step_bytes / 4), sample_bytes / 2);
    }
}

/*
 * RX Game and Mic audio from Codec at 32-bit/192KHz/4ch
 * Route ch 1 and 2 Game audio to USB (TODO)
 * SRC ch 3 and 4 Mic audio from 192 to 48Khz and route to SAI2b
 */
static THD_FUNCTION(route_game_mic_thread, arg) {
    thread_param_t *params = (thread_param_t *)arg;
    const size_t xfer_size = bqBSizeX(&params->rxqueue);
    uint64_t sai1b_buf[xfer_size / sizeof(uint64_t)];
    uint64_t game[xfer_size / sizeof(uint64_t) / 2];
    uint64_t mic[xfer_size / sizeof(uint64_t) / 2];
    size_t num_frames;
    size_t bytes;
    uint8_t *usb_buf;
    uint8_t *usb_mic_buf;
    uint8_t *usbh_buf;
    size_t usb_samples;
    size_t usb_mic_samples;
    size_t usbh_samples;

    chRegSetThreadName("SAI_Game_Mic");

    memset(sai1b_buf, 0, sizeof(sai1b_buf));
    memset(game, 0, sizeof(game));
    memset(mic, 0, sizeof(mic));

    while(!chThdShouldTerminateX()) {
        bytes = ibqReadTimeout(&params->rxqueue, (uint8_t *)sai1b_buf, xfer_size, TIME_MS2I(200));

        if(!bytes) {
            continue;
        }

        if(bytes % SAI1_FRAME_BYTES) {
            Dbg_printf(DBG_ERROR, "%s() xfer size (%d) is not on a frame boundary of %d!", __func__, bytes, SAI1_FRAME_BYTES);
            continue;
        }

        /* Split game and mic audio */
        num_frames = bytes / SAI1_FRAME_BYTES;

        for(size_t i = 0; i < num_frames; i++) {
            game[i] = sai1b_buf[i * 2];
            mic[i] = sai1b_buf[(i * 2) + 1];
        }

        /* Perform SRC from 96 to 48 KHz on mic stream */
        if(RC_MIX_RATIO > 1) {
            rc_frame2x32_downmix((FRAME2x32 *)mic, (const FRAME2x32 *)mic, num_frames, RC_MIX_RATIO);
        }

        obqWriteTimeout(&params->txqueue, (uint8_t *)mic, num_frames * SAI2_FRAME_BYTES / RC_MIX_RATIO, TIME_MS2I(200));

        /* Game Mix Stream out */
        if(USB_PC_game_mix_isActive()) {
            usb_samples = prepare_write(num_frames / 2, &usbpc_q_game_mix, (void**)&usb_buf, SAMPLE_BYTES(GAME_MIX_BITS, GAME_MIX_CHANNELS_USB));
        } else {
            usb_samples = 0;
            usb_buf = NULL;
        }

        if(usb_buf != NULL) {
            samples_copy(
                usb_samples, SAMPLE_BYTES(GAME_MIX_BITS, GAME_MIX_CHANNELS_USB),
                usb_buf, SAMPLE_BYTES(GAME_MIX_BITS, GAME_MIX_CHANNELS_USB),
                game, SAMPLE_BYTES(GAME_MIX_BITS, GAME_MIX_CHANNELS_SAI));
        }

        complete_write(usb_samples, &usbpc_q_game_mix, SAMPLE_BYTES(GAME_MIX_BITS, GAME_MIX_CHANNELS_USB));

        /* Mic Stream out */
        if(USB_PC_mic_isActive()) {
            usb_mic_samples = prepare_write(num_frames / 2, &usbpc_q_mic, (void**)&usb_mic_buf, SAMPLE_BYTES(MIC_BITS, MIC_CHANNELS_USB));
        } else {
            usb_mic_samples = 0;
            usb_mic_buf = NULL;
        }

        if(usb_mic_buf != NULL) {
            samples_copy(
                usb_mic_samples, SAMPLE_BYTES(MIC_BITS, MIC_CHANNELS_USB),
                usb_mic_buf, SAMPLE_BYTES(MIC_BITS, MIC_CHANNELS_USB),
                mic, SAMPLE_BYTES(MIC_BITS, MIC_CHANNELS_SAI));
        }

        complete_write(usb_mic_samples, &usbpc_q_mic, SAMPLE_BYTES(MIC_BITS, MIC_CHANNELS_USB));

        /* Game Mix Stream out via USB host*/
        if(isUsbhGameMixActive()) {
            usbh_samples = prepare_write(num_frames, &usbh_oq_game_mix, (void**)&usbh_buf, SAMPLE_BYTES(USBH_GAME_MIX_BITS, USBH_GAME_MIX_CHANNELS));
        } else {
            usbh_samples = 0;
            usbh_buf = NULL;
        }

        if(usbh_buf != NULL) {
            rc_frame2x32_to_2x16_downmix((FRAME2x16 *)usbh_buf, (FRAME2x32 *)game, usbh_samples, SAI1_SAMPLE_RATE/USBH_GAME_MIX_SAMPLE_RATE);
            //usbh_samples_copy(
            //    usbh_samples, SAMPLE_BYTES(USBH_GAME_MIX_BITS, USBH_GAME_MIX_CHANNELS),
            //    usbh_buf, SAMPLE_BYTES(USBH_GAME_MIX_BITS, USBH_GAME_MIX_CHANNELS),
            //    game, SAMPLE_BYTES(GAME_MIX_BITS, GAME_MIX_CHANNELS_SAI));
        }

        complete_write(usbh_samples, &usbh_oq_game_mix, SAMPLE_BYTES(USBH_GAME_MIX_BITS, USBH_GAME_MIX_CHANNELS));
    }

    chThdExit(MSG_OK);
}


/*
 * RX Chat audio from F446 at 32-bit/48KHz/2ch
 * SRC 2ch Chat audio from 48 to 192Hz and route to ch 1 and 2 of SAI1a
 * TODO: add USB tones to ch 3 and 4 of SAI1a
 */
static THD_FUNCTION(route_chat_thread, arg) {
    thread_param_t *params = (thread_param_t *)arg;
    const size_t rx_xfer_size = bqBSizeX(&params->rxqueue);
    const size_t tx_xfer_size = bqBSizeX(&params->txqueue);
    uint8_t sai2b_buf[rx_xfer_size];
    uint64_t sai1a_buf[tx_xfer_size / sizeof(uint64_t)];
    size_t num_frames;
    size_t bytes;

    chRegSetThreadName("SAI_Chat");

    memset(sai2b_buf, 0, sizeof(sai2b_buf));
    memset(sai1a_buf, 0, sizeof(sai1a_buf));

    while(!chThdShouldTerminateX()) {
        bytes = ibqReadTimeout(&params->rxqueue, (uint8_t *)sai2b_buf, rx_xfer_size, TIME_MS2I(200));

        if(!bytes) {
            continue;
        }

        if(bytes % SAI2_FRAME_BYTES) {
            Dbg_printf(DBG_ERROR, "%s() xfer size (%d) is not on a frame boundary of %d!", __func__, bytes, SAI2_FRAME_BYTES);
            continue;
        }

        num_frames = bytes / SAI2_FRAME_BYTES;

        /* Perform SRC on 2ch chat audio from 48 to 96 KHz */
        rc_frame2x32_to_4x32_upmix((FRAME2x32 *)sai1a_buf, (const FRAME2x32 *)sai2b_buf,  num_frames, RC_MIX_RATIO);

        /* TODO: Add USB tones to ch 3 and 4 of SAI1a */
        /* Add USBH mic to ch 3 and 4 of SAI1a */
        if(isUsbhMicActive()) {
            size_t i;
            uint8_t usbh_mic_buf[num_frames * SAMPLE_BYTES(USBH_MIC_BITS, USBH_MIC_CHANNELS)];
            if(usbhReadAudioDataFromIq((uint32_t *)usbh_mic_buf, &usbh_iq_mic, num_frames * SAMPLE_BYTES(USBH_MIC_BITS, USBH_MIC_CHANNELS))) {
                // put the usbh mic audio data (48KHz16Bit2Channel) on slot 3 and slot 4 of sai1a (48KHz32Bit4Channel)
                rc_frame2x16_to_4x32_upmix((FRAME2x32 *)sai1a_buf + 1, (FRAME2x16 *)usbh_mic_buf, num_frames, SAI1_SAMPLE_RATE / USBH_MIC_SAMPLE_RATE);
                //for(i = 0; i < num_frames; i++) {
                //    *((uint8_t *)(sai1a_buf + 2 * i + 1) + 2) = usbh_mic_buf[i * SAMPLE_BYTES(USBH_MIC_BITS, USBH_MIC_CHANNELS)];
                //    *((uint8_t *)(sai1a_buf + 2 * i + 1) + 3) = usbh_mic_buf[i * SAMPLE_BYTES(USBH_MIC_BITS, USBH_MIC_CHANNELS) + 1];
                //    *((uint8_t *)(sai1a_buf + 2 * i + 1) + 6) = usbh_mic_buf[i * SAMPLE_BYTES(USBH_MIC_BITS, USBH_MIC_CHANNELS) + 2];
                //    *((uint8_t *)(sai1a_buf + 2 * i + 1) + 7) = usbh_mic_buf[i * SAMPLE_BYTES(USBH_MIC_BITS, USBH_MIC_CHANNELS) + 3];
                //}
            }
        }

        obqWriteTimeout(&params->txqueue, (uint8_t *)sai1a_buf, num_frames * SAI1_FRAME_BYTES * RC_MIX_RATIO, TIME_MS2I(200));
    }

    chThdExit(MSG_OK);
}


/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

bool SAI_Start(void) {
    static uint8_t sai1a_tx_buf[SAI1_BUF_SIZE] __attribute__((section(".ram3"))) = { 0 };
    static uint8_t sai1b_rx_buf[SAI1_BUF_SIZE] __attribute__((section(".ram3"))) = { 0 };
    static uint8_t sai2a_tx_buf[SAI2_BUF_SIZE] __attribute__((section(".ram3"))) = { 0 };
    static uint8_t sai2b_rx_buf[SAI2_BUF_SIZE] __attribute__((section(".ram3"))) = { 0 };

    if(game_mic_thread.thread_ptr != NULL) {
        Dbg_printf(DBG_INFO, "SAI1 thread already running");
        return false;
    }

    /* Game and Mic thread RX on SAI1B and TX Mic on SAI2A and TX Game on USB */
    ibqObjectInit(&game_mic_thread.rxqueue, false, sai1b_rx_buf, SAI1_XFER_SIZE, SAI_BUFFER_MULT, NULL, NULL);
    obqObjectInit(&game_mic_thread.txqueue, false, sai2a_tx_buf, SAI2_XFER_SIZE, SAI_BUFFER_MULT, NULL, NULL);

    game_mic_thread.thread_ptr = chThdCreateStatic(waGameMic, sizeof(waGameMic), HIGHPRIO, route_game_mic_thread, &game_mic_thread);

    if(!game_mic_thread.thread_ptr) {
        Dbg_printf(DBG_ERROR, "Failed to create SAI1 thread");
        return false;
    }

    if(chat_thread.thread_ptr != NULL) {
        Dbg_printf(DBG_INFO, "SAI2 thread already running");
        return false;
    }

    /* Chat thread RX on SAI2B and TX on SAI1A */
    ibqObjectInit(&chat_thread.rxqueue, false, sai2b_rx_buf, SAI2_XFER_SIZE, SAI_BUFFER_MULT, NULL, NULL);
    obqObjectInit(&chat_thread.txqueue, false, sai1a_tx_buf, SAI1_XFER_SIZE, SAI_BUFFER_MULT, NULL, NULL);

    chat_thread.thread_ptr = chThdCreateStatic(waChat, sizeof(waChat), HIGHPRIO, route_chat_thread, &chat_thread);

    if(!chat_thread.thread_ptr) {
        Dbg_printf(DBG_ERROR, "Failed to create SAI2 thread");
        return false;
    }

    saiStart(&SAID1, &sai1a_Config, &sai1b_Config);
    saiStart(&SAID2, &sai2a_Config, &sai2b_Config);

    return true;
}


bool SAI_Stop(void) {
    bool ret = false;

    Dbg_printf(DBG_INFO, "%s()", __func__);

    saiStop(&SAID1);
    ret = stop_loop_thread(&game_mic_thread);

    saiStop(&SAID2);
    ret |= stop_loop_thread(&chat_thread);

    return ret;
}
