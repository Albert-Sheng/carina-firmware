include(FetchContent)


########################################################################################################################
# uGFX-GUI
########################################################################################################################
FetchContent_Declare(
    carina_gui
    GIT_REPOSITORY git@gitlab.com:astro-firmware-team/carina-gui.git
    GIT_TAG        0448daea327f7c2ec5f4edcbe26f0d1d4b3e04ba
)
FetchContent_GetProperties(carina_gui)
if(NOT carina_gui_POPULATED)
    FetchContent_Populate(carina_gui)
endif()
list(APPEND CMAKE_MODULE_PATH ${carina_gui_SOURCE_DIR}/cmake)
