/**
  ******************************************************************************
  * @file    hal_usbh_cfg.h
  * @author  MCD Application Team
  * @brief   HAL configuration file.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HAL_USBH_CFG_H
#define __HAL_USBH_CFG_H

/* Includes ------------------------------------------------------------------*/
/**
  * @brief Include header file
  */
#include "hal.h"
#include "stm32h7xx.h"

/* Derived constants and error checks ---------------------------------------*/
/**
  * @brief Registry checks
  */
#if !defined(STM32_HAS_OTG1) || !defined(STM32_HAS_OTG2)
#error "STM32_HAS_OTGx not defined in registry"
#endif

#if STM32_HAS_OTG1 && !defined(STM32_OTG1_ENDPOINTS)
#error "STM32_OTG1_ENDPOINTS not defined in registry"
#endif

#if STM32_HAS_OTG2 && !defined(STM32_OTG2_ENDPOINTS)
#error "STM32_OTG2_ENDPOINTS not defined in registry"
#endif

#if STM32_HAS_OTG1 && !defined(STM32_OTG1_FIFO_MEM_SIZE)
#error "STM32_OTG1_FIFO_MEM_SIZE not defined in registry"
#endif

#if STM32_HAS_OTG2 && !defined(STM32_OTG2_FIFO_MEM_SIZE)
#error "STM32_OTG2_FIFO_MEM_SIZE not defined in registry"
#endif

#if (STM32_USBH_USE_OTG1 && !defined(STM32_OTG1_HANDLER)) ||                 \
    (STM32_USBH_USE_OTG2 && !defined(STM32_OTG2_HANDLER))
#error "STM32_OTGx_HANDLER not defined in registry"
#endif

#if (STM32_USBH_USE_OTG1 && !defined(STM32_OTG1_NUMBER)) ||                  \
    (STM32_USBH_USE_OTG2 && !defined(STM32_OTG2_NUMBER))
#error "STM32_OTGx_NUMBER not defined in registry"
#endif

#if (STM32_USBH_USE_OTG1 && STM32_USB_USE_OTG1) ||                          \
    (STM32_USBH_USE_OTG2 && STM32_USB_USE_OTG2)
#error "USB Host and Device can not use the same OTG device"
#endif

#if (STM32_USBH_USE_OTG1)
#define USE_USB_FS
#endif

#if (STM32_USBH_USE_OTG2)
#define USE_USB_HS
#endif

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/**
  * @brief This is the list of modules to be used in the HAL driver
  */
#define HAL_HCD_MODULE_ENABLED

#define USE_HAL_HCD_REGISTER_CALLBACKS                                      (1)

/* Exported macro ------------------------------------------------------------*/
/**
  * @brief USBH configuration related macro
  */
#define USBH_MAX_NUM_ENDPOINTS                      2
#define USBH_MAX_NUM_INTERFACES                     10
#define USBH_MAX_NUM_CONFIGURATION                  1
#define USBH_MAX_NUM_SUPPORTED_CLASS                1
#define USBH_KEEP_CFG_DESCRIPTOR                    1
#define USBH_MAX_SIZE_CONFIGURATION                 0x200
#define USBH_MAX_DATA_BUFFER                        0x200
#define USBH_DEBUG_LEVEL                            3
#define USBH_DRIVER_USE_OS                          0
#define USBH_MAILBOX_SIZE                           4

/**
  * @brief CMSIS OS macros
  */
#if (USBH_DRIVER_USE_OS == 1)
#include "ch.h"
#define   USBH_PROCESS_PRIO                         NORMALPRIO
#endif

/**
  * @brief Memory management macros
  */
#include <string.h>
#include "chibios_malloc.h"
#define USBH_malloc               malloc
#define USBH_free                 free
#define USBH_memset               memset
#define USBH_memcpy               memcpy

#endif /* __HAL_USBH_CFG_H */


