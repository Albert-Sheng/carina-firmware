/**
  ******************************************************************************
  * @file    hal_usbh_dbg.h
  * @author  MCD Application Team
  * @brief   USB Host Low Level Debug Header file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HAL_USBH_DBG_H
#define __HAL_USBH_DBG_H

/* Includes ------------------------------------------------------------------*/
#include "debug.h"
#include "hal_usbh_cfg.h"

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* DEBUG macros */
#if (USBH_DEBUG_LEVEL > 0)
#define USBH_UsrLog(...)    Dbg_printf(DBG_INFO, __VA_ARGS__)
#else
#define USBH_UsrLog(...)
#endif

#if (USBH_DEBUG_LEVEL > 1)
#define USBH_ErrLog(...)    Dbg_printf(DBG_ERROR, __VA_ARGS__)
#else
#define USBH_ErrLog(...)
#endif

#if (USBH_DEBUG_LEVEL > 2)
#define USBH_DbgLog(...)    Dbg_printf(DBG_INFO, __VA_ARGS__)
#else
#define USBH_DbgLog(...)
#endif

/* Exported functions ------------------------------------------------------- */

#endif /* __HAL_USBH_DBG_H */

