/**
  ******************************************************************************
  * @file    hal_usbh_irq.c
  * @author  MCD Application Team
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------ */
#include "hal_usbh_hcd.h"
#include "hal_usbh_irq.h"

/* Private typedef ----------------------------------------------------------- */
/* Private define ------------------------------------------------------------ */
/* Private macro ------------------------------------------------------------- */
/* Private variables --------------------------------------------------------- */
extern HCD_HandleTypeDef hhcd;

/* Private function prototypes ----------------------------------------------- */
/* Private functions --------------------------------------------------------- */

/******************************************************************************/
/* Cortex-M7 Processor Exceptions Handlers */
/******************************************************************************/


/******************************************************************************/
/* stm32h7xx Peripherals Interrupt Handlers */
/* Add here the Interrupt Handler for the used peripheral(s) (PPP), for the */
/* available peripheral interrupt handler's name please refer to the startup */
/* file (startup_stm32h7xx.s).  */
/******************************************************************************/

/**
  * @brief  This function handles USB-On-The-Go FS/HS global interrupt request.
  * @param  None
  * @retval None
  */
#if STM32_USBH_USE_OTG1 || defined(__DOXYGEN__)
/**
 * @brief   OTG1 interrupt handler.
 *
 * @isr
 */
OSAL_IRQ_HANDLER(STM32_OTG1_HANDLER) {

    OSAL_IRQ_PROLOGUE();

    HAL_HCD_IRQHandler(&hhcd);

    OSAL_IRQ_EPILOGUE();
}
#endif

#if STM32_USBH_USE_OTG2 || defined(__DOXYGEN__)
/**
 * @brief   OTG2 interrupt handler.
 *
 * @isr
 */
OSAL_IRQ_HANDLER(STM32_OTG2_HANDLER) {

    OSAL_IRQ_PROLOGUE();

    HAL_HCD_IRQHandler(&hhcd);

    OSAL_IRQ_EPILOGUE();
}
#endif

