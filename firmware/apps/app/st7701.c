#include "st7701.h"

#include <hal.h>

/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define LINE_LCD_RESET          PAL_LINE(GPIOC, 15U)
#define LINE_LCD_BACK_LIGHT     PAL_LINE(GPIOI,  8U)


/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

static const SPIConfig st7701_spi_cfg = {
    .circular = false,
    .end_cb = NULL,
    .ssport = GPIOC,
    .sspad = 13,
    .cfg1 = SPI_CFG1_MBR_DIV8 | SPI_CFG1_DSIZE_VALUE(9 - 1),  // 9-bit
};

static const uint8_t init_cmd2_bk0[] = {
    3,  0xC0, 0x3B, 0x00,
    3,  0xC1, 0x12, 0x00,       // VBP = 18, VFP = 16
    3,  0xC2, 0x30, 0x02,
    2,  0xCC, 0x10,
    2,  0xCD, 0x08,
    17, 0xB0, 0x00, 0x1D, 0x29, 0x12, 0x17, 0x0B, 0x18, 0x09, 0x08, 0x2A, 0x06, 0x14, 0x11, 0x19, 0x1C, 0x18,     // Positive Voltage Gamma Control
    17, 0xB1, 0x00, 0x1D, 0x29, 0x12, 0x16, 0x0A, 0x18, 0x08, 0x09, 0x2A, 0x07, 0x13, 0x12, 0x19, 0x1D, 0x18,     // Negative Voltage Gamma Control
    0   // indicates the end
};

static const uint8_t init_cmd2_bk1[] = {
    2,  0xB0, 0x5D,
    2,  0xB1, 0x43, // VCOM amplitude setting
    2,  0xB2, 0x81, // VGH Voltage setting 12V
    2,  0xB3, 0x80,
    2,  0xB5, 0x43, // VGL Voltage setting -8.3V
    2,  0xB7, 0x85,
    2,  0xB8, 0x20,
    2,  0xC1, 0x78,
    2,  0xC2, 0x78,
    2,  0xD0, 0x88,
    4,  0xE0, 0x00, 0x00, 0x02,
    12, 0xE1, 0x03, 0xA0, 0x00, 0x00, 0x04, 0xA0, 0x00, 0x00, 0x00, 0x20, 0x20,
    14, 0xE2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    5,  0xE3, 0x00, 0x00, 0x11, 0x00,
    3,  0xE4, 0x22, 0x00,
    17, 0xE5, 0x05, 0xEC, 0xA0, 0xA0, 0x07, 0xEE, 0xA0, 0xA0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    5,  0xE6, 0x00, 0x00, 0x11, 0x00,
    3,  0xE7, 0x22, 0x00,
    17, 0xE8, 0x06, 0xED, 0xA0, 0xA0, 0x08, 0xEF, 0xA0, 0xA0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    8,  0xEB, 0x00, 0x00, 0x40, 0x40, 0x00, 0x00, 0x00,
    17, 0xED, 0xFF, 0xFF, 0xFF, 0xBA, 0x0A, 0xBF, 0x45, 0xFF, 0xFF, 0x54, 0xFB, 0xA0, 0xAB, 0xFF, 0xFF, 0xFF,
    7,  0xEF, 0x10, 0x0D, 0x04, 0x08, 0x3F, 0x1F,
    0   // indicates the end
};

static const uint8_t init_cmd2_bk3[] = {
    2, 0xEF, 0x08,
    0   // indicates the end
};

static const uint8_t sleep_out_cmd = 0x11;
static const uint8_t disp_on_cmd = 0x29;
static const uint8_t madctl_cmd[] = { 0x36, 0x00 };
static const uint8_t colmod_cmd[] = { 0x3A, 0x60 };  // 18-bit


/*
 ******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************
 */

void write_reg(SPIDriver *spip, int count, const uint8_t *data) {
    static uint16_t spi_data __attribute__((section(".ram8"))); // DMA is enabled for SPI. Buffer must be accessible by DMA.

    for(int i = 0; i < count; i++) {
        spi_data = *data++;

        if(i != 0) {
            spi_data |= 0x100;  // set bit 8 to indicate it's a register param
        }

        spiSelect(spip);
        spiSend(spip, 1, &spi_data);
        spiUnselect(spip);
    }
}


static bool select_cmd2_block(SPIDriver *spip, uint8_t block_num) {
    uint8_t spi_buf[6] = { 0xFF, 0x77, 0x01, 0x00, 0x00, block_num };

    if(((block_num & 0xF) == 2) || ((block_num & 0xF) > 3)) {
        return false;
    }

    write_reg(spip, 6, spi_buf);

    return true;
}


static void write_cmd2_block_params(SPIDriver *spip, uint8_t block_num, const uint8_t *data_ptr) {
    uint8_t count = *data_ptr++;

    if(!select_cmd2_block(spip, 0x10 | block_num)) {
        return;
    }

    while(count) {
        write_reg(spip, count, data_ptr);
        data_ptr += count;
        count = *data_ptr++;
    }
}


/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

void ST7701_Init(SPIDriver *spip) {
    if(spip == NULL) {
        return;
    }

    spiStart(spip, &st7701_spi_cfg);
    spiAcquireBus(spip);

    /*
     * Power on sequence
     */

    palSetLine(LINE_LCD_RESET);
    chThdSleepMilliseconds(10);

    /*
     * Init sequence
     */

    write_cmd2_block_params(spip, 0, init_cmd2_bk0);
    write_cmd2_block_params(spip, 1, init_cmd2_bk1);
    write_cmd2_block_params(spip, 3, init_cmd2_bk3);

    select_cmd2_block(spip, 0);                 // Deselect Command 2

    write_reg(spip, 2, madctl_cmd);
    write_reg(spip, 2, colmod_cmd);

    write_reg(spip, 1, &sleep_out_cmd);  // Sleep Out

    chThdSleepMilliseconds(120);

    write_reg(spip, 1, &disp_on_cmd);    // Display On

    spiReleaseBus(spip);
    spiStop(spip);

    palSetLine(LINE_LCD_BACK_LIGHT);            // Enable backlight
}


void ST7701_Deinit(void) {
    /*
     * Power off sequence
     */
    palClearLine(LINE_LCD_RESET);
    chThdSleepMilliseconds(120);
}
