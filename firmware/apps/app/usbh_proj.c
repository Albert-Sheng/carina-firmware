/**
 * @file    usbh_proj.c
 * @brief   USB Host Module code.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */
#include "debug.h"
#include "usbh_core.h"
#include "usbh_audio.h"
#include "usbh_proj.h"

/*===========================================================================*/
/* Module local definitions.                                                 */
/*===========================================================================*/
// USB host driver ID
#define USBH_DRIVER_ID                                                  (0x00)

/*===========================================================================*/
/* Module local variables and types.                                         */
/*===========================================================================*/
static bool usbh_game_mix_active = false;
static bool usbh_mic_active = false;

#if (USBH_DRIVER_USE_OS == 0U)
static thread_t *usbh_thread = NULL;
static THD_WORKING_AREA(waUsbHost, 4096);
#endif

/*===========================================================================*/
/* Module exported variables.                                                */
/*===========================================================================*/
USBH_HandleTypeDef hUSBHost;
AUDIO_ApplicationTypeDef app_state = APPLICATION_IDLE;
AUDIO_PLAYBACK_StateTypeDef audio_play_state = AUDIO_PLAYBACK_STATE_IDLE;
AUDIO_RECORD_StateTypeDef audio_record_state = AUDIO_RECORD_STATE_IDLE;

uint16_t usbh_playbuf_length;
uint8_t usbh_playbuf_game_mix[USBH_GAME_MIX_PACKET_SIZE_MAX];
uint8_t usbh_recordbuf_mic[USBH_MIC_PACKET_SIZE_MAX];

static uint8_t usbh_qbuf_game_mix[USBH_GAME_MIX_BUFFER_SIZE];
static uint8_t usbh_qbuf_mic[USBH_MIC_BUFFER_SIZE];
OUTPUTQUEUE_DECL(usbh_oq_game_mix, usbh_qbuf_game_mix, sizeof(usbh_qbuf_game_mix), NULL, NULL);
INPUTQUEUE_DECL(usbh_iq_mic, usbh_qbuf_mic, sizeof(usbh_qbuf_mic), NULL, NULL);

/*===========================================================================*/
/* Module local functions.                                                   */
/*===========================================================================*/
/**
 * @brief   read the USB host audio data from an output queue.
 *
 * @param[in] buf       pointer to the memory to store the audio data
 * @param[in] oqp       the output queue where to copy the audio data
 * @param[in] n         number of bytes to copy
 *
 * @notapi
 */
static void usbhReadAudioDataFromOq(uint32_t *buf,
                                   output_queue_t *oqp,
                                   size_t n) {
    size_t nwords = 0;
    size_t btogo = 0;
    size_t wrap = 0;
    uint32_t w = 0;
    uint8_t i = 0;

    chSysLock();

    osalDbgAssert(n > 0, "is zero");

    if(n > oqGetFullI(oqp)) {
        n = oqGetFullI(oqp);
    }

    nwords = (n / 4);

    while (true) {
        wrap = oqp->q_top - oqp->q_rdptr;
        if(wrap < 4) {
            btogo = 4;
            for(i=0;i<4;i++) {
                oqp->q_counter++;
                w |= *oqp->q_rdptr++;
                if(oqp->q_rdptr >= oqp->q_top) {
                    oqp->q_rdptr = oqp->q_buffer;
                }
                btogo--;
                if(btogo) {
                    w <<= 8;
                } else {
                    *buf++ = w;
                    nwords--;
                    w = 0;
                }
            }
        } else {
            // This relies on the Cortex-M7 being able to perform un-alligned memory access.
            oqp->q_counter += 4;
            *buf++ = *(uint32_t *)oqp->q_rdptr;
            oqp->q_rdptr += 4;
            if(oqp->q_rdptr >= oqp->q_top) {
                oqp->q_rdptr = oqp->q_buffer;
            }
            nwords--;
        }

        if(nwords == 0) {
            break;
        }
    }

    chSysUnlock();
}

/**
 * @brief   Write the USB host audio data to an input queue.
 *
 * @param[in] buf       pointer to the memory where to copy the audio data
 * @param[in] iqp       the input queue where to store the audio data
 * @param[in] n         number of bytes to copy
 *
 * @notapi
 */
static void usbhWriteAudioDataToIq(uint32_t *buf,
                                   input_queue_t *iqp,
                                   size_t n) {
    size_t nwords = 0;
    size_t btogo = 0;
    size_t wrap = 0;
    uint32_t w = 0;
    uint8_t i = 0;

    chSysLock();

    // iq is full, so ignore the audio data this time
    if(iqIsFullI(iqp)) {
        osalThreadEnqueueTimeoutS(&iqp->q_waiting, TIME_IMMEDIATE);
        chSysUnlock();
        return;
    }

    // buffer has no more space to store the audio data
    if(n > iqGetEmptyI(iqp)) {
        n = iqGetEmptyI(iqp);
    }

    // check the space again, return immediately if there is no space
    if(n == 0) {
        chSysUnlock();
        return;
    }

    nwords = (n / 4);

    while (true) {
        wrap = iqp->q_top - iqp->q_wrptr;
        if(wrap < 4) {
            btogo = 4;
            w = *buf++;
            for(i = 0; i < 4; i++) {
                iqp->q_counter++;
                *iqp->q_wrptr++ = (uint8_t)w;
                if(iqp->q_wrptr >= iqp->q_top) {
                    iqp->q_wrptr = iqp->q_buffer;
                }
                btogo--;
                if(btogo) {
                    w >>= 8;
                } else {
                    nwords--;
                    w = 0;
                }
            }
        } else {
            // This relies on the Cortex-M7 being able to perform un-alligned memory access.
            iqp->q_counter += 4;
            *(uint32_t *)iqp->q_wrptr = *buf++;
            iqp->q_wrptr += 4;
            if(iqp->q_wrptr >= iqp->q_top) {
                iqp->q_wrptr = iqp->q_buffer;
            }
            nwords--;
        }

        if(nwords == 0) {
            break;
        }
    }
    chSysUnlock();
}

/**
  * @brief  User Process
  * @param  phost: Host Handle
  * @param  id: Host Library user message ID
  * @retval None
  */
static void usbhUserProc(USBH_HandleTypeDef * phost, uint8_t id) {
    UNUSED(phost);

    switch(id) {
        case HOST_USER_SELECT_CONFIGURATION:
            break;

        case HOST_USER_DISCONNECTION:
            app_state = APPLICATION_DISCONNECT;
            usbh_game_mix_active = false;
            usbh_mic_active = false;
            audio_play_state = AUDIO_PLAYBACK_STATE_IDLE;
            break;

        case HOST_USER_CLASS_ACTIVE:
            app_state = APPLICATION_READY;

            break;

        case HOST_USER_CONNECTION:
            app_state = APPLICATION_START;

            break;

        default:
            break;
    }
}

#if (USBH_DRIVER_USE_OS == 0U)
void USBH_Thread(void *arg);
#endif

/*===========================================================================*/
/* Module exported functions.                                                */
/*===========================================================================*/
/**
 * @brief   Read the USB host audio data from an input queue.
 *
 * @param[in] buf       pointer to the memory where to store the audio data
 * @param[in] iqp       the input queue where to copy the audio data
 * @param[in] n         number of bytes to copy
 *
 * @notapi
 */
bool usbhReadAudioDataFromIq(uint32_t *buf,
                                   input_queue_t *iqp,
                                   size_t n) {
    size_t nwords = 0;
    size_t btogo = 0;
    size_t wrap = 0;
    uint32_t w = 0;
    uint8_t i = 0;

    osalSysLock();

    osalDbgAssert(n > 0, "is zero");

    if (n > iqGetFullI(iqp)) {
      osalSysUnlock();
      return false;
    }

    if(n > iqGetFullI(iqp)) {
        n = iqGetFullI(iqp);
    }

    nwords = (n / 4);
    while (true) {
        wrap = iqp->q_top - iqp->q_rdptr;
        if(wrap < 4) {
            btogo = 4;
            for(i = 0; i < 4; i++) {
                iqp->q_counter--;
                w |= *iqp->q_rdptr++;
                if(iqp->q_rdptr >= iqp->q_top) {
                    iqp->q_rdptr = iqp->q_buffer;
                }
                btogo--;
                if(btogo) {
                    w <<= 8;
                } else {
                    *buf++ = w;
                    nwords--;
                    w = 0;
                }
            }
        } else {
            // This relies on the Cortex-M7 being able to perform un-alligned memory access.
            iqp->q_counter -= 4;
            *buf++ = *(uint32_t *)iqp->q_rdptr;
            iqp->q_rdptr += 4;
            if(iqp->q_rdptr >= iqp->q_top) {
                iqp->q_rdptr = iqp->q_buffer;
            }
            nwords--;
        }

        if(nwords == 0) {
            break;
        }
    }

    osalSysUnlock();
    return true;
}

void usbhInit(void) {
    /* Init Host Library */
    if(USBH_OK != USBH_Init(&hUSBHost, usbhUserProc, USBH_DRIVER_ID)) {
        Dbg_printf(DBG_ERROR, "USBH_Init fail");
        return;
    }

    /* Add Supported Class */
    if(USBH_OK != USBH_RegisterClass(&hUSBHost, USBH_AUDIO_CLASS)) {
        Dbg_printf(DBG_ERROR, "USBH_RegisterClass fail");
        return;
    }
}

void usbhDeInit(void) {
    if(USBH_OK != USBH_DeInit(&hUSBHost)) {
        Dbg_printf(DBG_ERROR, "USBH_DeInit fail");
        return;
    }
}

void usbhStart(void) {
#if (USBH_DRIVER_USE_OS == 0U)
    if(usbh_thread != NULL) {
        Dbg_printf(DBG_ERROR, "USB host thread is already running");
        return;
    }

    usbh_thread = chThdCreateStatic(waUsbHost, sizeof(waUsbHost), NORMALPRIO, USBH_Thread, &hUSBHost);
    if(NULL == usbh_thread) {
        Dbg_printf(DBG_ERROR, "Failed to create USB host thread");
        return;
    }
#endif // (USBH_DRIVER_USE_OS == 0U)

    /* Start Host Process */
    USBH_Start(&hUSBHost);
}

void usbhStop(void) {
#if (USBH_DRIVER_USE_OS == 0U)
    // Stop the USB host thread first
    if(usbh_thread == NULL) {
        Dbg_printf(DBG_ERROR, "SAI thread already stopped");
        return;
    }

    chThdTerminate(usbh_thread);
    chThdWait(usbh_thread);
    usbh_thread = NULL;
#endif // (USBH_DRIVER_USE_OS == 0U)

    /* Stop Host Process */
    USBH_Stop(&hUSBHost);
}

bool isUsbhGameMixActive(void) {
    return usbh_game_mix_active;
}

bool isUsbhMicActive(void) {
    return usbh_mic_active;
}

bool usbhPlayBufferReload(void) {
    uint16_t packet_size = USBH_GAME_MIX_PACKET_SIZE;
    chSysLock();
    int full = oqGetFullI(&usbh_oq_game_mix);
    chSysUnlock();

    if(full >= USBH_GAME_MIX_BUFFER_HIGH_TH) {
        packet_size = USBH_GAME_MIX_PACKET_SIZE_MAX;
    } else if(full >= USBH_GAME_MIX_BUFFER_MID_TH) {
        packet_size = USBH_GAME_MIX_PACKET_SIZE;
    } else if(full >= USBH_GAME_MIX_BUFFER_LOW_TH) {
        packet_size = USBH_GAME_MIX_PACKET_SIZE_MIN;
    } else {
        packet_size = 0;
    }

    if(0 == packet_size) {
        return false;
    }

    usbhReadAudioDataFromOq((uint32_t *)usbh_playbuf_game_mix, &usbh_oq_game_mix, packet_size);
    usbh_playbuf_length = packet_size;
    return true;
}

void usbhPlayInit(USBH_HandleTypeDef *phost) {
    USBH_AUDIO_SetFrequency(phost, USBH_GAME_MIX_SAMPLE_RATE, USBH_GAME_MIX_CHANNELS, USBH_GAME_MIX_BITS);
}

bool usbhPlayCfg(USBH_HandleTypeDef *phost) {
    AUDIO_HandleTypeDef *AUDIO_Handle;

    if(phost->gState == HOST_CLASS) {
        AUDIO_Handle = (AUDIO_HandleTypeDef *) phost->pActiveClass->pData;
        if(AUDIO_Handle->play_state == AUDIO_PLAYBACK_IDLE) {
            if(usbhPlayBufferReload()) {
                return true;
            }
        }
    }

    return false;
}

void usbhPlayFrame(USBH_HandleTypeDef *phost) {
    AUDIO_HandleTypeDef *AUDIO_Handle;

    if(phost->gState == HOST_CLASS) {
        AUDIO_Handle = (AUDIO_HandleTypeDef *) phost->pActiveClass->pData;
        if(AUDIO_Handle->play_state == AUDIO_PLAYBACK_IDLE) {
            AUDIO_Handle->headphone.buf = usbh_playbuf_game_mix;
            AUDIO_Handle->headphone.total_length = usbh_playbuf_length;
            AUDIO_Handle->headphone.frame_length = usbh_playbuf_length;
            AUDIO_Handle->headphone.global_ptr = 0U;
            AUDIO_Handle->headphone.partial_ptr = 0U;

            AUDIO_Handle->headphone.sample_size = (USBH_GAME_MIX_BITS * USBH_GAME_MIX_CHANNELS) / 8U;
            AUDIO_Handle->headphone.frame_remain_pers = ((USBH_GAME_MIX_SAMPLE_RATE * USBH_GAME_MIX_BITS * USBH_GAME_MIX_CHANNELS) / 8U) % 1000U;
            AUDIO_Handle->headphone.frame_remain_total = 0U;
            AUDIO_Handle->headphone.added_frame_length = 0U;

            AUDIO_Handle->play_state = AUDIO_PLAYBACK_PLAY_FRAME;
            AUDIO_Handle->control_state = AUDIO_CONTROL_INIT;
            AUDIO_Handle->play_processing_state = AUDIO_DATA_START_OUT;
        }
    }
}

void usbhPlaybackProc(void) {
    switch(audio_play_state) {
        case AUDIO_PLAYBACK_STATE_IDLE: {
            audio_play_state = AUDIO_PLAYBACK_STATE_INIT;
            break;
        }
        case AUDIO_PLAYBACK_STATE_INIT: {
            usbhPlayInit(&hUSBHost);
            audio_play_state = AUDIO_PLAYBACK_STATE_CONFIG;
            break;
        }
        case AUDIO_PLAYBACK_STATE_CONFIG: {
            if(usbhPlayCfg(&hUSBHost)) {
                audio_play_state = AUDIO_PLAYBACK_STATE_PLAY;

            }
            break;
        }
        case AUDIO_PLAYBACK_STATE_PLAY: {
            usbhPlayFrame(&hUSBHost);
            audio_play_state = AUDIO_PLAYBACK_STATE_PLAYING;
            break;
        }
        case AUDIO_PLAYBACK_STATE_PLAYING:
        case AUDIO_PLAYBACK_STATE_WAIT:
        case AUDIO_PLAYBACK_STATE_PAUSE:
        case AUDIO_PLAYBACK_STATE_RESUME:
        case AUDIO_PLAYBACK_STATE_ERROR:
        default:
            break;
    }
}

void usbhRecordFrame(USBH_HandleTypeDef *phost) {
    AUDIO_HandleTypeDef *AUDIO_Handle;

    if(phost->gState == HOST_CLASS) {
        AUDIO_Handle = (AUDIO_HandleTypeDef *) phost->pActiveClass->pData;
        if(AUDIO_Handle->record_state == AUDIO_RECORD_IDLE) {
            AUDIO_Handle->microphone.buf = usbh_recordbuf_mic;
            AUDIO_Handle->microphone.cbuf = usbh_recordbuf_mic;
            AUDIO_Handle->microphone.total_length = 0U;
            AUDIO_Handle->microphone.frame_length = 0;
            AUDIO_Handle->microphone.global_ptr = 0U;
            AUDIO_Handle->microphone.partial_ptr = 0U;

            AUDIO_Handle->microphone.sample_size = (USBH_MIC_BITS * USBH_MIC_CHANNELS) / 8U;
            AUDIO_Handle->microphone.frame_remain_pers = ((USBH_MIC_SAMPLE_RATE * USBH_MIC_BITS * USBH_MIC_CHANNELS) / 8U) % 1000U;
            AUDIO_Handle->microphone.frame_remain_total = 0U;
            AUDIO_Handle->microphone.added_frame_length = 0U;

            AUDIO_Handle->record_state = AUDIO_RECORD_RECORD_FRAME;
            AUDIO_Handle->record_processing_state = AUDIO_DATA_IN_START;
        }
    }
}

void usbhRecordProc(void) {
    switch(audio_record_state) {
        case AUDIO_RECORD_STATE_IDLE: {
            audio_record_state = AUDIO_RECORD_STATE_INIT;
            break;
        }
        case AUDIO_RECORD_STATE_INIT: {
            audio_record_state = AUDIO_RECORD_STATE_CONFIG;
            break;
        }
        case AUDIO_RECORD_STATE_CONFIG: {
            audio_record_state = AUDIO_RECORD_STATE_RECORD;
            break;
        }
        case AUDIO_RECORD_STATE_RECORD: {
            usbhRecordFrame(&hUSBHost);
            audio_record_state = AUDIO_RECORD_STATE_RECORDING;
            break;
        }
        case AUDIO_RECORD_STATE_RECORDING:
        case AUDIO_RECORD_STATE_WAIT:
        case AUDIO_RECORD_STATE_PAUSE:
        case AUDIO_RECORD_STATE_RESUME:
        case AUDIO_RECORD_STATE_ERROR:
        default:
            break;
    }
}



#if (USBH_DRIVER_USE_OS == 0U)
/**
  * @brief  USB Host Thread task
  * @param  arg USB Host handle type definition
  * @retval None
  */
THD_FUNCTION(USBH_Thread, arg) {
    USBH_HandleTypeDef *phost = (USBH_HandleTypeDef *)arg;

    chRegSetThreadName("USB Host Thread");

    Dbg_printf(DBG_INFO, "USB host thread started.");

    while(!chThdShouldTerminateX()) {

        USBH_Process(phost);

        if(isUsbhGameMixActive()) {
            usbhPlaybackProc();
        }

        if(isUsbhMicActive()) {
            usbhRecordProc();
        }

        chThdSleepMicroseconds(10);
    }

    Dbg_printf(DBG_INFO, "USB host thread finished.");
    chThdExit(MSG_OK);
}
#endif /* (USBH_DRIVER_USE_OS == 0U) */

/**
  * @brief  Informs user that settings have been changed.
  * @param  phost: Host Handle
  * @retval None
  */
void USBH_AUDIO_FrequencySet(USBH_HandleTypeDef * phost) {
    AUDIO_HandleTypeDef *AUDIO_Handle;

    if(phost->gState == HOST_CLASS) {
        AUDIO_Handle = (AUDIO_HandleTypeDef *) phost->pActiveClass->pData;
        if(AUDIO_Handle->play_state == AUDIO_PLAYBACK_IDLE) {
            usbh_game_mix_active = true;
        }

        if(AUDIO_Handle->record_state == AUDIO_RECORD_IDLE) {
            usbh_mic_active = true;
        }
    }
}

/**
  * @brief  The function informs user that User data are processed
  *  @param  phost: Selected device
  * @retval None
  */
void  USBH_AUDIO_BufferEmptyCallback(USBH_HandleTypeDef *phost) {
    AUDIO_HandleTypeDef *AUDIO_Handle;

    if(phost->gState == HOST_CLASS) {
        AUDIO_Handle = (AUDIO_HandleTypeDef *) phost->pActiveClass->pData;
        if(usbhPlayBufferReload()) {
            AUDIO_Handle->headphone.cbuf = usbh_playbuf_game_mix;
            AUDIO_Handle->headphone.total_length = usbh_playbuf_length;
            AUDIO_Handle->headphone.frame_length = usbh_playbuf_length;
            AUDIO_Handle->headphone.global_ptr = 0U;
            AUDIO_Handle->headphone.partial_ptr = 0U;

            AUDIO_Handle->headphone.sample_size = (USBH_GAME_MIX_BITS * USBH_GAME_MIX_CHANNELS) / 8U;
            AUDIO_Handle->headphone.frame_remain_pers = ((USBH_GAME_MIX_SAMPLE_RATE * USBH_GAME_MIX_BITS * USBH_GAME_MIX_CHANNELS) / 8U) % 1000U;
            AUDIO_Handle->headphone.frame_remain_total = 0U;
            AUDIO_Handle->headphone.added_frame_length = 0U;

            AUDIO_Handle->play_state = AUDIO_PLAYBACK_PLAY_FRAME;
            AUDIO_Handle->control_state = AUDIO_CONTROL_INIT;
            AUDIO_Handle->play_processing_state = AUDIO_DATA_OUT;
        }
    }
}

/**
  * @brief  The function informs user that audio data are received
  *  @param  phost: Selected device
  * @retval None
  */
void  USBH_AUDIO_BufferFullCallback(USBH_HandleTypeDef *phost) {
    AUDIO_HandleTypeDef *AUDIO_Handle;

    if(phost->gState == HOST_CLASS) {
        AUDIO_Handle = (AUDIO_HandleTypeDef *) phost->pActiveClass->pData;
        // make sure the audio data is frame size aligned
        if(0 == (AUDIO_Handle->microphone.total_length % USBH_MIC_FRAME_SIZE)) {
            usbhWriteAudioDataToIq((uint32_t *)AUDIO_Handle->microphone.buf, &usbh_iq_mic, AUDIO_Handle->microphone.total_length);
            AUDIO_Handle->microphone.total_length = 0;
        }
    }
}

/** @} */

