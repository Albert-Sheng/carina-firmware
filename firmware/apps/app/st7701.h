#pragma once

#include <hal.h>

void ST7701_Init(SPIDriver *spip);
void ST7701_Deinit(void);
void ST7701_Write_Cmd(SPIDriver *spip, int count, const uint8_t *data);
