#include "ui.h"
#include "manager.hpp"
#include "events.hpp"
#include "debug.h"

#define GUI_EVENT_POSTING_TIMEOUT       10
#define GUI_EVENT_DISPATCHING_TIMEOUT   1

static gui::manager gm;

// Currently displayed view type
// This has to be removed prior to any release. This merely exists for debugging purposes to switch through
// the different views easily until the actual navigation tree is defined & implemented.
static gui::view_type current_view{ gui::view_type::first };

template<typename T>
static
inline
void
send_event(const enum gui::event::type type, T&& data)
{
#if CARINA_ENABLE_GUI
    if (type == gui::event::type::show_view)
        current_view = static_cast<gui::view_type>(data);

    gm.post_event(
        { type, std::forward<T>(data) },
        GUI_EVENT_POSTING_TIMEOUT
    );
#else
    (void)type;
    (void)data;
#endif
}

/**
 * @brief   UI Thread for events.
 *
 * @param[out] arg optional thread argument.
 *
 */
THD_FUNCTION(UI_Thread, arg)
{
    (void)arg;

    chRegSetThreadName("UI Handler");

    // Register callbacks from I/O thread
    ioRegisterButtonCb(ENCODER_SELECT_BUTTON, uiNavigateButtonEvent);
    ioRegisterEncoderCb(RIGHT_ENCODER, ENCODER_COUNTER_CLOCKWISE_EVENT, uiNavigateRight);
    ioRegisterEncoderCb(RIGHT_ENCODER, ENCODER_CLOCKWISE_EVENT, uiNavigateLeft);

    Dbg_printf(DBG_INFO, "UI event thread started.");

    if (!gm.init())
        gfxHalt("GUI initialization failed.");

    // Show default view
    uiShowVolumeView();

    while(!chThdShouldTerminateX()) {
        #if CARINA_ENABLE_GUI
            gm.dispatch_event(GUI_EVENT_DISPATCHING_TIMEOUT);
            chThdYield();
        #else
            chThdSleepMilliseconds(1000);
        #endif
    }

    Dbg_printf(DBG_INFO, "UI event thread finished.");
    chThdExit(MSG_OK);
}

void uiShowColorView(void)
{
    //uiSendMessage(GUI_EVENT_SHOW_VIEW, GUI_VIEW_COLOR);
}

void uiShowVolumeView(void)
{
    send_event(gui::event::type::show_view, gui::view_type::volume);
}

void uiShowNextView(void)
{
    current_view = static_cast<gui::view_type>(static_cast<uint32_t>(current_view) + 1);
    if (current_view == gui::view_type::last)
        current_view = gui::view_type::first;

    send_event(gui::event::type::show_view, current_view);
}

void uiShowPreviousView(void)
{
}

void uiSetColor(uint32_t color)
{
    //uiSendMessage(GUI_EVENT_COLOR, color);
}

void uiSetBacklight(uint8_t percent)
{
    gdispSetBacklight(percent);
}

void uiSetVolume(uint8_t volumeLevel)
{
    send_event(gui::event::type::set_level, volumeLevel);
}

void uiSetMixLevel(uint8_t mixerLevel)
{
    send_event(gui::event::type::set_level, mixerLevel);
}

void uiSetMuted(bool muted)
{
    //uiSendMessage(GUI_EVENT_MUTE, (gBool)muted);
}

void uiNavigateRight()
{
    // ToDo
    uiShowNextView();
}

void uiNavigateLeft()
{
    // ToDo
    uiShowNextView();
}

void uiPhantomPower(uint8_t level)
{
    //uiSendMessage(GUI_EVENT_PHANTOM_POWER, level);
}

void uiNavigateButtonEvent(button_event_t pressType)
{
    // TODO
    switch(pressType) {
        case BUTTON_SHORT_PRESS_EVENT:
            //uiSendMessage(GUI_EVENT_NAV_SHORT_PRESS, 0);
            break;
        case BUTTON_LONG_PRESS_EVENT:
            //uiSendMessage(GUI_EVENT_NAV_LONG_PRESS, 0);
            break;
        case BUTTON_VERYLONG_PRESS_EVENT:
            //uiSendMessage(GUI_EVENT_NAV_VERYLONG_PRESS, 0);
            break;
        case BUTTON_COMBO_PRESS_EVENT:
        default:
            Dbg_printf(DBG_ERROR, "Unknown encoder press type: %d", pressType);
            break;
    }
}

void uiAuxJack(bool present)
{
    //uiSendMessage(GUI_EVENT_AUX_JACK, (gBool)present);
}

void uiHPJack(bool present)
{
    //uiSendMessage(GUI_EVENT_HP_JACK, (gBool)present);
}
