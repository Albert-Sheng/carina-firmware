/**
 * @file    cs47l90_configs.c
 * @brief   CS47L90 register configurations.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */
#include "cs47l90.h"

const cs47l90_sequence_t CS47L90_Sequence_Init[] = {
    {0x08, 0x0308, 0x0000ffff, 0, 0x0000ffff},       // Ctrl_IF_CFG_1(08H):      0308  CIF1MISO_DRV_STR=8mA, CIF1MISO_PD=0
    {0x09, 0x0200, 0x0000ffff, 0, 0x0000ffff},       // Ctrl_IF_CFG_2(09H):      0200  CIF2SDA_DRV_STR=8mA
    {0x0A, 0x0308, 0x0000ffff, 0, 0x0000ffff},       // Ctrl_IF_CFG_3(0AH):      0308  CIF3MISO_DRV_STR=8mA
    {0x20, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Tone_Generator_1(20H):   0000  TONE_RATE=SAMPLE_RATE_1, TONE_OFFSET=0 Degrees (in phase), TONE2_OVD=Disabled (1kHz tone output), TONE1_OVD=Disabled (1kHz tone output), TONE2_ENA=0, TONE1_ENA=0
    {0x21, 0x1000, 0x0000ffff, 0, 0x0000ffff},       // Tone_Generator_2(21H):   1000  TONE1_LVL=0001_0000_0000_0000
    {0x22, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Tone_Generator_3(22H):   0000  TONE1_LVL=0000_0000
    {0x23, 0x1000, 0x0000ffff, 0, 0x0000ffff},       // Tone_Generator_4(23H):   1000  TONE2_LVL=0001_0000_0000_0000
    {0x24, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Tone_Generator_5(24H):   0000  TONE2_LVL=0000_0000
    {0x30, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // PWM_Drive_1(30H):        0000  PWM_RATE=SAMPLE_RATE_1, PWM_CLK_SEL=000, PWM2_OVD=0, PWM1_OVD=0, PWM2_ENA=0, PWM1_ENA=0
    {0x31, 0x0100, 0x0000ffff, 0, 0x0000ffff},       // PWM_Drive_2(31H):        0100  PWM1_LVL=75% Duty cycle
    {0x32, 0x0100, 0x0000ffff, 0, 0x0000ffff},       // PWM_Drive_3(32H):        0100  PWM2_LVL=75% Duty cycle
    {0x41, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Sequence_control(41H):   0000  WSEQ_ENA_MICD_CLAMP_FALL=0, WSEQ_ENA_MICD_CLAMP_RISE=0
    {0x42, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Spare_Triggers(42H):     0000  WSEQ_TRG16=0, WSEQ_TRG15=0, WSEQ_TRG14=0, WSEQ_TRG13=0, WSEQ_TRG12=0, WSEQ_TRG11=0, WSEQ_TRG10=0, WSEQ_TRG9=0, WSEQ_TRG8=0, WSEQ_TRG7=0, WSEQ_TRG6=0, WSEQ_TRG5=0, WSEQ_TRG4=0, WSEQ_TRG3=0, WSEQ_TRG2=0, WSEQ_TRG1=0
    {0x4B, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_1(4BH): 01FF  WSEQ_TRG1_INDEX=1_1111_1111
    {0x4C, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_2(4CH): 01FF  WSEQ_TRG2_INDEX=1_1111_1111
    {0x4D, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_3(4DH): 01FF  WSEQ_TRG3_INDEX=1_1111_1111
    {0x4E, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_4(4EH): 01FF  WSEQ_TRG4_INDEX=1_1111_1111
    {0x4F, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_5(4FH): 01FF  WSEQ_TRG5_INDEX=1_1111_1111
    {0x50, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_6(50H): 01FF  WSEQ_TRG6_INDEX=1_1111_1111
    {0x59, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_7(59H): 01FF  WSEQ_TRG7_INDEX=1_1111_1111
    {0x5A, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_8(5AH): 01FF  WSEQ_TRG8_INDEX=1_1111_1111
    {0x5B, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_9(5BH): 01FF  WSEQ_TRG9_INDEX=1_1111_1111
    {0x5C, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_10(5CH): 01FF  WSEQ_TRG10_INDEX=1_1111_1111
    {0x5D, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_11(5DH): 01FF  WSEQ_TRG11_INDEX=1_1111_1111
    {0x5E, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_12(5EH): 01FF  WSEQ_TRG12_INDEX=1_1111_1111
    {0x61, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Sample_Rate_Sequence_Select_1(61H): 01FF  WSEQ_SAMPLE_RATE_DETECT_A_INDEX=1_1111_1111
    {0x62, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Sample_Rate_Sequence_Select_2(62H): 01FF  WSEQ_SAMPLE_RATE_DETECT_B_INDEX=1_1111_1111
    {0x63, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Sample_Rate_Sequence_Select_3(63H): 01FF  WSEQ_SAMPLE_RATE_DETECT_C_INDEX=1_1111_1111
    {0x64, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Sample_Rate_Sequence_Select_4(64H): 01FF  WSEQ_SAMPLE_RATE_DETECT_D_INDEX=1_1111_1111
    {0x66, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Always_On_Triggers_Sequence_Select_1(66H): 01FF  WSEQ_MICD_CLAMP_RISE_INDEX=1_1111_1111
    {0x67, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Always_On_Triggers_Sequence_Select_2(67H): 01FF  WSEQ_MICD_CLAMP_FALL_INDEX=1_1111_1111
    {0x68, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_13(68H): 01FF  WSEQ_TRG13_INDEX=1_1111_1111
    {0x69, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_14(69H): 01FF  WSEQ_TRG14_INDEX=1_1111_1111
    {0x6A, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_15(6AH): 01FF  WSEQ_TRG15_INDEX=1_1111_1111
    {0x6B, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Spare_Sequence_Select_16(6BH): 01FF  WSEQ_TRG16_INDEX=1_1111_1111
    {0x6E, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Trigger_Sequence_Select_32(6EH): 01FF  WSEQ_DRC1_SIG_DET_RISE_INDEX=1_1111_1111
    {0x6F, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Trigger_Sequence_Select_33(6FH): 01FF  WSEQ_DRC1_SIG_DET_FALL_INDEX=1_1111_1111
    {0x78, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_1(78H): 01FF  WSEQ_EVENTLOG1_INDEX=1_1111_1111
    {0x79, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_2(79H): 01FF  WSEQ_EVENTLOG2_INDEX=1_1111_1111
    {0x7A, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_3(7AH): 01FF  WSEQ_EVENTLOG3_INDEX=1_1111_1111
    {0x7B, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_4(7BH): 01FF  WSEQ_EVENTLOG4_INDEX=1_1111_1111
    {0x7C, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_5(7CH): 01FF  WSEQ_EVENTLOG5_INDEX=1_1111_1111
    {0x7D, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_6(7DH): 01FF  WSEQ_EVENTLOG6_INDEX=1_1111_1111
    {0x7E, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_7(7EH): 01FF  WSEQ_EVENTLOG7_INDEX=1_1111_1111
    {0x7F, 0x01FF, 0x0000ffff, 0, 0x0000ffff},       // Eventlog_Sequence_Select_8(7FH): 01FF  WSEQ_EVENTLOG8_INDEX=1_1111_1111
    {0x8C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // User_Key_Ctrl(8CH):      0000  USER_KEY_CTRL=0000_0000_0000_0000
    {0x90, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_Control_1(90H):  0000  HAP_RATE=SAMPLE_RATE_1, ONESHOT_TRIG=0, HAP_CTRL=Disabled, HAP_ACT=Eccentric Rotating Mass (ERM)
    {0x91, 0x7FFF, 0x0000ffff, 0, 0x0000ffff},       // Haptics_Control_2(91H):  7FFF  LRA_FREQ=Reserved
    {0x92, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_phase_1_intensity(92H): 0000  PHASE1_INTENSITY=0%
    {0x93, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_phase_1_duration(93H): 0000  PHASE1_DURATION=0ms
    {0x94, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_phase_2_intensity(94H): 0000  PHASE2_INTENSITY=0%
    {0x95, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_phase_2_duration(95H): 0000  PHASE2_DURATION=0ms
    {0x96, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_phase_3_intensity(96H): 0000  PHASE3_INTENSITY=0%
    {0x97, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Haptics_phase_3_duration(97H): 0000  PHASE3_DURATION=0ms
    {0x98, 0x0000, 0x0000fffe, 0, 0x0000fffe},       // Haptics_Status(98H):     0000  ONESHOT_STS=0
    {0xA0, 0x002B, 0x0000ffff, 0, 0x0000ffff},       // Comfort_Noise_Generator(A0H): 002B  NOISE_GEN_RATE=SAMPLE_RATE_1, NOISE_GEN_ENA=1, NOISE_GEN_GAIN=-48dB
    {0x100, 0x0042, 0x0000ffff, 0, 0x0000ffff},       // Clock_32k_1(100H):       0042  CLK_32K_ENA=1, CLK_32K_SRC=SYSCLK (automatically divided)
    {0x101, 0x0404, 0x0000ffff, 0, 0x0000ffff},       // System_Clock_1(101H):    0444  SYSCLK_FRAC=SYSCLK is a multiple of 6.144MHz, SYSCLK_FREQ=98.304MHz (90.3168MHz), SYSCLK_ENA=0, SYSCLK_SRC=FLL1
    {0x102, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // Sample_rate_1(102H):     0003  SAMPLE_RATE_1=48kHz
    {0x103, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // Sample_rate_2(103H):     0005  SAMPLE_RATE_2=192kHz
    {0x104, 0x0011, 0x0000ffff, 0, 0x0000ffff},       // Sample_rate_3(104H):     0011  SAMPLE_RATE_3=8kHz
    {0x112, 0x0445, 0x0000ffff, 0, 0x0000ffff},       // Async_clock_1(112H):     0445  ASYNC_CLK_FREQ=98.304MHz (90.3168MHz), ASYNC_CLK_ENA=1, ASYNC_CLK_SRC=FLL2
    {0x113, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // Async_sample_rate_1(113H): 0003  ASYNC_SAMPLE_RATE_1=48kHz
    {0x114, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // Async_sample_rate_2(114H): 0005  ASYNC_SAMPLE_RATE_2=192kHz
    {0x120, 0x0304, 0x0000ffff, 0, 0x0000ffff},       // DSP_Clock_1(120H):       0304  DSP_CLK_ENA=0, DSP_CLK_SRC=FLL1
    {0x122, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DSP_Clock_2(122H):       0000  DSP_CLK_FREQ=Reserved
    {0x124, 0x0C49, 0x0000ffff, 0, 0x0000ffff},       // DSP_Clock_3(124H):       0C49  DSP_FLL_AO_FREQ=49.141MHz
    {0x149, 0x0013, 0x0000ffff, 0, 0x0000ffff},       // Output_system_clock(149H): 0013  OPCLK_ENA=0, OPCLK_DIV=Divide by 2, OPCLK_SEL=49.152MHz (45.1584MHz)
    {0x14A, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Output_async_clock(14AH): 0000  OPCLK_ASYNC_ENA=0, OPCLK_ASYNC_DIV=Reserved, OPCLK_ASYNC_SEL=6.144MHz (5.6448MHz)
    {0x14E, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // Clock_Gen_Pad_Ctrl(14EH): 0180  MCLK2_PD=1, MCLK1_PD=1
    {0x152, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Rate_Estimator_1(152H):  0000  TRIG_ON_STARTUP=0, LRCLK_SRC=AIF1LRCLK, RATE_EST_ENA=0
    {0x153, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Rate_Estimator_2(153H):  0000  SAMPLE_RATE_DETECT_A=None
    {0x154, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Rate_Estimator_3(154H):  0000  SAMPLE_RATE_DETECT_B=None
    {0x155, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Rate_Estimator_4(155H):  0000  SAMPLE_RATE_DETECT_C=None
    {0x156, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Rate_Estimator_5(156H):  0000  SAMPLE_RATE_DETECT_D=None
    {0x171, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_1(171H):    0001  FLL1_FREERUN=0, FLL1_ENA=0
    {0x172, 0x000C, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_2(172H):    000C  FLL1_CTRL_UPD=0, FLL1_N=12
    {0x173, 0x0024, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_3(173H):    0024  FLL1_THETA=36
    {0x174, 0x007D, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_4(174H):    007D  FLL1_LAMBDA=125
    {0x175, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_5(175H):    0000  FLL1_FRATIO=1
    {0x176, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_6(176H):    0000  FLL1_REFCLK_DIV=1, FLL1_REFCLK_SRC=MCLK1
    {0x177, 0x0281, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Loop_Filter_Test_1(177H): 0281  FLL1_FRC_INTEG_UPD=0, FLL1_FRC_INTEG_VAL=0010_1000_0001
    {0x179, 0x0008, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Control_7(179H):    0008  FLL1_GAIN=4
    {0x17A, 0x2106, 0x0000ffff, 0, 0x0000ffff},       // FLL1_EFS_2(17AH):        2106  FLL1_PHASE_ENA=0
    {0x181, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_1(181H): 0000  FLL1_SYNC_ENA=0
    {0x182, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_2(182H): 0000  FLL1_SYNC_N=0
    {0x183, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_3(183H): 0000  FLL1_SYNC_THETA=0
    {0x184, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_4(184H): 0000  FLL1_SYNC_LAMBDA=0
    {0x185, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_5(185H): 0000  FLL1_SYNC_FRATIO=1
    {0x186, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_6(186H): 0000  FLL1_SYNCCLK_DIV=1, FLL1_SYNCCLK_SRC=MCLK1
    {0x187, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Synchroniser_7(187H): 0001  FLL1_SYNC_GAIN=1, FLL1_SYNC_DFSAT=1
    {0x189, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL1_Spread_Spectrum(189H): 0000  FLL1_SS_AMPL=0.7% (triangle), 0.7% (ZMFM, dither), FLL1_SS_FREQ=439kHz, FLL1_SS_SEL=Disabled
    {0x18A, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // FLL1_GPIO_Clock(18AH):   0005  FLL1_GPCLK_DIV=2, FLL1_GPCLK_ENA=1
    {0x191, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_1(191H):    0000  FLL2_FREERUN=0, FLL2_ENA=0
    {0x192, 0x000C, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_2(192H):    000C  FLL2_CTRL_UPD=0, FLL2_N=12
    {0x193, 0x0024, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_3(193H):    0024  FLL2_THETA=36
    {0x194, 0x007D, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_4(194H):    007D  FLL2_LAMBDA=125
    {0x195, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_5(195H):    0000  FLL2_FRATIO=1
    {0x196, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_6(196H):    0001  FLL2_REFCLK_DIV=1, FLL2_REFCLK_SRC=MCLK2
    {0x197, 0x0281, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Loop_Filter_Test_1(197H): 0281  FLL2_FRC_INTEG_UPD=0, FLL2_FRC_INTEG_VAL=0010_1000_0001
    {0x199, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Control_7(199H):    0000  FLL2_GAIN=1
    {0x19A, 0x2906, 0x0000ffff, 0, 0x0000ffff},       // FLL2_EFS_2(19AH):        2906  FLL2_PHASE_ENA=1
    {0x1A1, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_1(1A1H): 0000  FLL2_SYNC_ENA=0
    {0x1A2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_2(1A2H): 0000  FLL2_SYNC_N=0
    {0x1A3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_3(1A3H): 0000  FLL2_SYNC_THETA=0
    {0x1A4, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_4(1A4H): 0000  FLL2_SYNC_LAMBDA=0
    {0x1A5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_5(1A5H): 0000  FLL2_SYNC_FRATIO=1
    {0x1A6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_6(1A6H): 0000  FLL2_SYNCCLK_DIV=1, FLL2_SYNCCLK_SRC=MCLK1
    {0x1A7, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Synchroniser_7(1A7H): 0001  FLL2_SYNC_GAIN=1, FLL2_SYNC_DFSAT=1
    {0x1A9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL2_Spread_Spectrum(1A9H): 0000  FLL2_SS_AMPL=0.7% (triangle), 0.7% (ZMFM, dither), FLL2_SS_FREQ=439kHz, FLL2_SS_SEL=Disabled
    {0x1AA, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FLL2_GPIO_Clock(1AAH):   0004  FLL2_GPCLK_DIV=2, FLL2_GPCLK_ENA=0
    {0x1D1, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_1(1D1H):  0004  FLL_AO_HOLD=1, FLL_AO_ENA=0
    {0x1D2, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_2(1D2H):  0004  FLL_AO_CTRL_UPD=0, FLL_AO_N=4
    {0x1D3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_3(1D3H):  0000  FLL_AO_THETA=0
    {0x1D4, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_4(1D4H):  0000  FLL_AO_LAMBDA=0
    {0x1D5, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_5(1D5H):  0001  FLL_AO_FB_DIV=1
    {0x1D6, 0x8004, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_6(1D6H):  8004  FLL_AO_REFDET_ENA=1, FLL_AO_REFCLK_DIV=1, FLL_AO_REFCLK_SRC=Reserved
    {0x1D8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_7(1D8H):  0000  FLL_AO_GAIN=0
    {0x1DA, 0x0070, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_8(1DAH):  0070  FLL_AO_HS_DITH_TUNE=0, FLL_AO_LS_DITH_TUNE_SHAPED=7, FLL_AO_LS_DITH_TUNE_NONSHAPED=0
    {0x1DB, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_9(1DBH):  0000  FLL_AO_TR_RATE=0000
    {0x1DC, 0x06DA, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_10(1DCH): 06DA  FLL_AO_PHASEDET_ENA=0, FLL_AO_WLR_SDM_FRC_ENA=0, FLL_AO_WLR_SDM_ENA=1, FLL_AO_SYNC_EFS_ENA=1, FLL_AO_LS_DITH_ENA=1, FLL_AO_HS_DITH_ENA=1, FLL_AO_OSF_FRC_ENA=1, FLL_AO_OSF_ENA=1, FLL_AO_AUTO_DFSAT_ENA=1, FLL_AO_DFSAT_ENA=0
    {0x1DD, 0x0011, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Control_11(1DDH): 0011  FLL_AO_LOCKDET_PHASE_MASK=0, FLL_AO_LOCKDET_THR=1000, FLL_AO_LOCKDET_ENA=1
    {0x1DF, 0x8002, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_Digital_Test_1(1DFH): 8002  FLL_AO_FREERUN_TRIM=1_0000_0000_0000, FLL_AO_WLR_DEBUG_ENA=0
    {0x1EA, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // FLL_AO_GPIO_Clock(1EAH): 0002  FLL_AO_GPCLK_DIV=1, FLL_AO_GPCLK_ENA=0
    {0x200, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // Mic_Charge_Pump_1(200H): 0005  CP2_DISCH=1, CP2_BYPASS=0, CP2_ENA=1
    {0x213, 0x03E4, 0x0000ffff, 0, 0x0000ffff},       // LDO2_Control_1(213H):    03E4  LDO2_VSEL=2.5V, LDO2_DISCH=MICVDD discharged when disabled
    {0x218, 0x00E7, 0x0000ffff, 0, 0x0000ffff},       // Mic_Bias_Ctrl_1(218H):   00E7  MICB1_EXT_CAP=0, MICB1_LVL=2.2V, MICB1_RATE=Fast start-up / shut-down, MICB1_DISCH=MICBIAS1 discharged when disabled, MICB1_BYPASS=1, MICB1_ENA=1
    {0x219, 0x00E4, 0x0000ffff, 0, 0x0000ffff},       // Mic_Bias_Ctrl_2(219H):   00E4  MICB2_EXT_CAP=0, MICB2_LVL=2.2V, MICB2_RATE=Fast start-up / shut-down, MICB2_DISCH=MICBIAS2 discharged when disabled, MICB2_BYPASS=0, MICB2_ENA=0
    {0x21C, 0x2223, 0x0000ffff, 0, 0x0000ffff},       // Mic_Bias_Ctrl_5(21CH):   2223  MICB1D_BYP=0, MICB1D_DISCH=1, MICB1D_ENA=0, MICB1C_BYP=0, MICB1C_DISCH=1, MICB1C_ENA=0, MICB1B_BYP=0, MICB1B_DISCH=1, MICB1B_ENA=0, MICB1A_BYP=0, MICB1A_DISCH=1, MICB1A_ENA=1
    {0x21E, 0x2222, 0x0000ffff, 0, 0x0000ffff},       // Mic_Bias_Ctrl_6(21EH):   2222  MICB2D_BYP=0, MICB2D_DISCH=1, MICB2D_ENA=0, MICB2C_BYP=0, MICB2C_DISCH=1, MICB2C_ENA=0, MICB2B_BYP=0, MICB2B_DISCH=1, MICB2B_ENA=0, MICB2A_BYP=0, MICB2A_DISCH=1, MICB2A_ENA=0
    {0x299, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // Headphone_Detect_0(299H): 0002  HPD_OVD_ENA=0, HPD_OUT_SEL=HPOUT1L, HPD_FRC_SEL=MICDET1, HPD_SENSE_SEL=MICDET1, HPD_GND_SEL=MICDET3/HPOUTFB3
    {0x29B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Headphone_Detect_1(29BH): 0000  HPD_IMPEDANCE_RANGE=4 ohms to 30 ohms, HPD_CLK_DIV=32kHz, HPD_RATE=1, HPD_POLL=0
    {0x2A2, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // Mic_Detect_1_Control_0(2A2H): 0002  MICD1_ADC_MODE=0, MICD1_SENSE_SEL=MICDET1, MICD1_GND_SEL=MICDET3/HPOUTFB3
    {0x2A3, 0x1103, 0x0000ffff, 0, 0x0000ffff},       // Mic_Detect_1_Control_1(2A3H): 1103  MICD1_BIAS_STARTTIME=0.25ms, MICD1_RATE=0.25ms, MICD1_BIAS_SRC=MICBIAS1A, MICD1_DBTIME=1, MICD1_ENA=1
    {0x2A4, 0x009F, 0x0000ffff, 0, 0x0000ffff},       // Mic_Detect_1_Control_2(2A4H): 009F  MICD1_LVL_SEL=1001_1111
    {0x2B2, 0x0010, 0x0000ffff, 0, 0x0000ffff},       // Mic_Detect_2_Control_0(2B2H): 0010  MICD2_ADC_MODE=0, MICD2_SENSE_SEL=MICDET2, MICD2_GND_SEL=MICDET1/HPOUTFB1
    {0x2B3, 0x1102, 0x0000ffff, 0, 0x0000ffff},       // Mic_Detect_2_Control_1(2B3H): 1102  MICD2_BIAS_STARTTIME=0.25ms, MICD2_RATE=0.25ms, MICD2_BIAS_SRC=MICBIAS1A, MICD2_DBTIME=1, MICD2_ENA=0
    {0x2B4, 0x009F, 0x0000ffff, 0, 0x0000ffff},       // Mic_Detect_2_Control_2(2B4H): 009F  MICD2_LVL_SEL=1001_1111
    {0x2C6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Micd_Clamp_control(2C6H): 0000  MICD_CLAMP_OVD=0, MICD_CLAMP_MODE=0000
    {0x2C8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // GP_Switch_1(2C8H):       0000  SW2_MODE=Disabled (open), SW1_MODE=Disabled (open)
    {0x2D3, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // Jack_detect_analogue(2D3H): 0003  JD2_ENA=1, JD1_ENA=1
    {0x300, 0x000F, 0x0000ffff, 0, 0x0000ffff},       // Input_Enables(300H):     000F  IN5L_ENA=0, IN5R_ENA=0, IN4L_ENA=0, IN4R_ENA=0, IN3L_ENA=0, IN3R_ENA=0, IN2L_ENA=1, IN2R_ENA=1, IN1L_ENA=1, IN1R_ENA=1
    {0x308, 0x0400, 0x0000ffff, 0, 0x0000ffff},       // Input_Rate(308H):        0400  IN_RATE=SAMPLE_RATE_1, IN_RATE_MODE=Each channel uses independent sample rate, defined by INx[L|R]_RATE
    {0x309, 0x0022, 0x0000ffff, 0, 0x0000ffff},       // Input_Volume_Ramp(309H): 0022  IN_VD_RAMP=1ms, IN_VI_RAMP=1ms
    {0x30C, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // HPF_Control(30CH):       0002  IN_HPF_CUT=10Hz
    {0x310, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // IN1L_Control(310H):      0080  IN1L_HPF=0, IN1_DMIC_SUP=MICVDD, IN1_MODE=Analogue input, IN1L_PGA_VOL=0dB
    {0x311, 0x2080, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_1L(311H): 2080  IN1L_SRC=Single-ended (IN1ALP), IN1L_LP_MODE=HiFi mode, IN_VU=0, IN1L_MUTE=0, IN1L_VOL=0dB
    {0x312, 0x0500, 0x0000ffff, 0, 0x0000ffff},       // DMIC1L_Control(312H):    0500  IN1L_SIG_DET_ENA=Disabled, IN1_OSR=3.072MHz
    {0x313, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN1L_Rate_Control(313H): 0000  IN1L_RATE=SAMPLE_RATE_1
    {0x314, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // IN1R_Control(314H):      0080  IN1R_HPF=0, IN1_DMICCLK_SRC=Internally generated (within ADC Sub-system), IN1R_PGA_VOL=0dB
    {0x315, 0x2080, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_1R(315H): 2080  IN1R_SRC=Single-ended (IN1ARP), IN1R_LP_MODE=HiFi mode, IN_VU=0, IN1R_MUTE=0, IN1R_VOL=0dB
    {0x316, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DMIC1R_Control(316H):    0000  IN1R_SIG_DET_ENA=Disabled
    {0x317, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN1R_Rate_Control(317H): 0000  IN1R_RATE=SAMPLE_RATE_1
    {0x318, 0x0880, 0x0000ffff, 0, 0x0000ffff},       // IN2L_Control(318H):      0880  IN2L_HPF=0, IN2_DMIC_SUP=MICBIAS1, IN2_MODE=Analogue input, IN2L_PGA_VOL=0dB
    {0x319, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_2L(319H): 0180  IN2L_SRC=Differential (IN2ALP - IN2ALN), IN2L_LP_MODE=HiFi mode, IN_VU=0, IN2L_MUTE=1, IN2L_VOL=0dB
    {0x31A, 0x0500, 0x0000ffff, 0, 0x0000ffff},       // DMIC2L_Control(31AH):    0500  IN2L_SIG_DET_ENA=Disabled, IN2_OSR=3.072MHz
    {0x31B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN2L_Rate_Control(31BH): 0000  IN2L_RATE=SAMPLE_RATE_1
    {0x31C, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // IN2R_Control(31CH):      0080  IN2R_HPF=0, IN2_DMICCLK_SRC=Internally generated (within ADC Sub-system), IN2R_PGA_VOL=0dB
    {0x31D, 0x2080, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_2R(31DH): 20s80  IN2R_SRC=Single-ended (IN2RP), IN2R_LP_MODE=HiFi mode, IN_VU=0, IN2R_MUTE=0, IN2R_VOL=0dB
    {0x31E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DMIC2R_Control(31EH):    0000  IN2R_SIG_DET_ENA=Disabled
    {0x31F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN2R_Rate_Control(31FH): 0000  IN2R_RATE=SAMPLE_RATE_1
    {0x320, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN3L_Control(320H):      0000  IN3L_HPF=0
    {0x321, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_3L(321H): 0180  IN3L_LP_MODE=HiFi mode, IN_VU=0, IN3L_MUTE=1, IN3L_VOL=0dB
    {0x322, 0x0500, 0x0000ffff, 0, 0x0000ffff},       // DMIC3L_Control(322H):    0500  IN3L_SIG_DET_ENA=Disabled, IN3_OSR=3.072MHz
    {0x323, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN3L_Rate_Control(323H): 0000  IN3L_RATE=SAMPLE_RATE_1
    {0x324, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN3R_Control(324H):      0000  IN3R_HPF=0, IN3_DMICCLK_SRC=Internally generated (within ADC Sub-system)
    {0x325, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_3R(325H): 0180  IN3R_LP_MODE=HiFi mode, IN_VU=0, IN3R_MUTE=1, IN3R_VOL=0dB
    {0x326, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DMIC3R_Control(326H):    0000  IN3R_SIG_DET_ENA=Disabled
    {0x327, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN3R_Rate_Control(327H): 0000  IN3R_RATE=SAMPLE_RATE_1
    {0x328, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN4L_Control(328H):      0000  IN4L_HPF=0
    {0x329, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_4L(329H): 0180  IN4L_LP_MODE=HiFi mode, IN_VU=0, IN4L_MUTE=1, IN4L_VOL=0dB
    {0x32A, 0x0500, 0x0000ffff, 0, 0x0000ffff},       // DMIC4L_Control(32AH):    0500  IN4L_SIG_DET_ENA=Disabled, IN4_OSR=3.072MHz
    {0x32B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN4L_Rate_Control(32BH): 0000  IN4L_RATE=SAMPLE_RATE_1
    {0x32C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN4R_Control(32CH):      0000  IN4R_HPF=0, IN4_DMICCLK_SRC=Internally generated (within ADC Sub-system)
    {0x32D, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_4R(32DH): 0180  IN4R_LP_MODE=HiFi mode, IN_VU=0, IN4R_MUTE=1, IN4R_VOL=0dB
    {0x32E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DMIC4R_Control(32EH):    0000  IN4R_SIG_DET_ENA=Disabled
    {0x32F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN4R_Rate_Control(32FH): 0000  IN4R_RATE=SAMPLE_RATE_1
    {0x330, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN5L_Control(330H):      0000  IN5L_HPF=0
    {0x331, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_5L(331H): 0180  IN5L_LP_MODE=HiFi mode, IN_VU=0, IN5L_MUTE=1, IN5L_VOL=0dB
    {0x332, 0x0500, 0x0000ffff, 0, 0x0000ffff},       // DMIC5L_Control(332H):    0500  IN5L_SIG_DET_ENA=Disabled, IN5_OSR=3.072MHz
    {0x333, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN5L_Rate_Control(333H): 0000  IN5L_RATE=SAMPLE_RATE_1
    {0x334, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN5R_Control(334H):      0000  IN5R_HPF=0, IN5_DMICCLK_SRC=Internally generated (within ADC Sub-system)
    {0x335, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // ADC_Digital_Volume_5R(335H): 0180  IN5R_LP_MODE=HiFi mode, IN_VU=0, IN5R_MUTE=1, IN5R_VOL=0dB
    {0x336, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DMIC5R_Control(336H):    0000  IN5R_SIG_DET_ENA=Disabled
    {0x337, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IN5R_Rate_Control(337H): 0000  IN5R_RATE=SAMPLE_RATE_1
    {0x340, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // Signal_Detect_Globals(340H): 0001  IN_SIG_DET_THR=-30.1dB, IN_SIG_DET_HOLD=4-8ms
    {0x348, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Dig_Mic_Pad_Ctrl(348H):  0000  DMICDAT2_PD=0, DMICDAT1_PD=0
    {0x400, 0x0037, 0x0000ffff, 0, 0x0000ffff},       // Output_Enables_1(400H):  0037  OUT5L_ENA=0, OUT5R_ENA=0, HP3L_ENA=1, HP3R_ENA=1, HP2L_ENA=0, HP2R_ENA=1, HP1L_ENA=1, HP1R_ENA=1
    {0x408, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Output_Rate_1(408H):     0000  OUT_RATE=SAMPLE_RATE_1
    {0x409, 0x0022, 0x0000ffff, 0, 0x0000ffff},       // Output_Volume_Ramp(409H): 0022  OUT_VD_RAMP=1ms, OUT_VI_RAMP=1ms
    {0x410, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_1L(410H): 0080  OUT1L_HIFI=0, OUT1_MONO=0, OUT1L_ANC_SRC=Disabled
    {0x411, 0x008E, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_1L(411H): 008E  OUT_VU=0, OUT1L_MUTE=0, OUT1L_VOL=7dB
    {0x412, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_1(412H): 0002  HP1_GND_SEL=MICDET3/HPOUTFB3
    {0x413, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_1L(413H): 0001  OUT1L_NGATE_SRC=0000_0000_0001
    {0x414, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_1R(414H): 0080  OUT1R_HIFI=0, OUT1R_ANC_SRC=Disabled
    {0x415, 0x008E, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_1R(415H): 008E  OUT_VU=0, OUT1R_MUTE=0, OUT1R_VOL=7dB
    {0x417, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_1R(417H): 0002  OUT1R_NGATE_SRC=0000_0000_0010
    {0x418, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_2L(418H): 0080  OUT2L_HIFI=0, OUT2_MONO=0, OUT2L_ANC_SRC=Disabled
    {0x419, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_2L(419H): 0080  OUT_VU=0, OUT2L_MUTE=0, OUT2L_VOL=0dB
    {0x41A, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_2(41AH): 0002  HP2_GND_SEL=MICDET3/HPOUTFB3
    {0x41B, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_2L(41BH): 0004  OUT2L_NGATE_SRC=0000_0000_0100
    {0x41C, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_2R(41CH): 0080  OUT2R_HIFI=0, OUT2R_ANC_SRC=Disabled
    {0x41D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_2R(41DH): 0080  OUT_VU=0, OUT2R_MUTE=0, OUT2R_VOL=0dB
    {0x41F, 0x0008, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_2R(41FH): 0008  OUT2R_NGATE_SRC=0000_0000_1000
    {0x420, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_3L(420H): 0080  OUT3L_HIFI=0, OUT3_MONO=0, OUT3L_ANC_SRC=Disabled
    {0x421, 0x008E, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_3L(421H): 008E  OUT_VU=0, OUT3L_MUTE=0, OUT3L_VOL=7dB
    {0x423, 0x0010, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_3L(423H): 0010  OUT3L_NGATE_SRC=0000_0001_0000
    {0x424, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_3R(424H): 0080  OUT3R_HIFI=0, OUT3R_ANC_SRC=Disabled
    {0x425, 0x008E, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_3R(425H): 008E  OUT_VU=0, OUT3R_MUTE=0, OUT3R_VOL=7dB
    {0x427, 0x0020, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_3R(427H): 0020  OUT3R_NGATE_SRC=0000_0010_0000
    {0x430, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_5L(430H): 0000  OUT5L_HIFI=0, OUT5_OSR=Normal mode, OUT5L_ANC_SRC=Disabled
    {0x431, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_5L(431H): 0180  OUT_VU=0, OUT5L_MUTE=1, OUT5L_VOL=0dB
    {0x433, 0x0100, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_5L(433H): 0100  OUT5L_NGATE_SRC=0001_0000_0000
    {0x434, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Output_Path_Config_5R(434H): 0000  OUT5R_HIFI=0, OUT5R_ANC_SRC=Disabled
    {0x435, 0x0180, 0x0000ffff, 0, 0x0000ffff},       // DAC_Digital_Volume_5R(435H): 0180  OUT_VU=0, OUT5R_MUTE=1, OUT5R_VOL=0dB
    {0x437, 0x0200, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Select_5R(437H): 0200  OUT5R_NGATE_SRC=0010_0000_0000
    {0x44E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Filter_Control(44EH):    0000  HIFI_FIR_TYPE=48kHz Deep stopband, Linear phase
    {0x450, 0x0000, 0x0000fffd, 0, 0x0000fffd},       // DAC_AEC_Control_1(450H): 0000  AEC1_LOOPBACK_SRC=OUT1L, AEC1_ENA_STS=0, AEC1_LOOPBACK_ENA=0
    {0x451, 0x0000, 0x0000fffd, 0, 0x0000fffd},       // DAC_AEC_Control_2(451H): 0000  AEC2_LOOPBACK_SRC=OUT1L, AEC2_ENA_STS=0, AEC2_LOOPBACK_ENA=0
    {0x458, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Noise_Gate_Control(458H): 0000  NGATE_HOLD=30ms, NGATE_THR=-78dB, NGATE_ENA=0
    {0x490, 0x0069, 0x0000ffff, 0, 0x0000ffff},       // PDM_SPK1_CTRL_1(490H):   0069  SPK1R_MUTE=0, SPK1L_MUTE=0, SPK1_MUTE_ENDIAN=LSB first, SPK1_MUTE_SEQ=0110_1001
    {0x491, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // PDM_SPK1_CTRL_2(491H):   0000  SPK1_FMT=Mode A
    {0x500, 0x002F, 0x0000ffff, 0, 0x0000ffff},       // AIF1_BCLK_Ctrl(500H):    002F  AIF1_BCLK_INV=AIF1BCLK not inverted, AIF1_BCLK_FRC=Normal, AIF1_BCLK_MSTR=AIF1BCLK Master mode, AIF1_BCLK_FREQ=6.144MHz (5.6448MHz)
    {0x501, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Tx_Pin_Ctrl(501H):  0000  AIF1TX_DAT_TRI=Logic 0 during unused timeslots
    {0x502, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Rx_Pin_Ctrl(502H):  0001  AIF1_LRCLK_MODE=0, AIF1_LRCLK_INV=AIF1LRCLK not inverted, AIF1_LRCLK_FRC=Normal, AIF1_LRCLK_MSTR=AIF1LRCLK Master mode
    {0x503, 0x0800, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Rate_Ctrl(503H):    0800  AIF1_RATE=SAMPLE_RATE_2, AIF1_TRI=Normal
    {0x504, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Format(504H):       0002  AIF1_FMT=I2S mode
    {0x506, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Rx_BCLK_Rate(506H): 0080  AIF1_BCPF=128 cycles
    {0x507, 0x1820, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_1(507H): 1820  AIF1TX_WL=24 bits per slot, AIF1TX_SLOT_LEN=32 cycles per slot
    {0x508, 0x1820, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_2(508H): 1820  AIF1RX_WL=24 bits per slot, AIF1RX_SLOT_LEN=32 cycles per slot
    {0x509, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_3(509H): 0000  AIF1TX1_SLOT=0
    {0x50A, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_4(50AH): 0002  AIF1TX2_SLOT=2
    {0x50B, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_5(50BH): 0001  AIF1TX3_SLOT=1
    {0x50C, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_6(50CH): 0003  AIF1TX4_SLOT=3
    {0x50D, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_7(50DH): 0004  AIF1TX5_SLOT=4
    {0x50E, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_8(50EH): 0005  AIF1TX6_SLOT=5
    {0x50F, 0x0006, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_9(50FH): 0006  AIF1TX7_SLOT=6
    {0x510, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_10(510H): 0007  AIF1TX8_SLOT=7
    {0x511, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_11(511H): 0000  AIF1RX1_SLOT=0
    {0x512, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_12(512H): 0002  AIF1RX2_SLOT=2
    {0x513, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_13(513H): 0001  AIF1RX3_SLOT=1
    {0x514, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_14(514H): 0003  AIF1RX4_SLOT=3
    {0x515, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_15(515H): 0004  AIF1RX5_SLOT=4
    {0x516, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_16(516H): 0005  AIF1RX6_SLOT=5
    {0x517, 0x0006, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_17(517H): 0006  AIF1RX7_SLOT=6
    {0x518, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Frame_Ctrl_18(518H): 0007  AIF1RX8_SLOT=7
    {0x519, 0x000F, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Tx_Enables(519H):   000F  AIF1TX8_ENA=0, AIF1TX7_ENA=0, AIF1TX6_ENA=0, AIF1TX5_ENA=0, AIF1TX4_ENA=1, AIF1TX3_ENA=1, AIF1TX2_ENA=1, AIF1TX1_ENA=1
    {0x51A, 0x000F, 0x0000ffff, 0, 0x0000ffff},       // AIF1_Rx_Enables(51AH):   000F  AIF1RX8_ENA=0, AIF1RX7_ENA=0, AIF1RX6_ENA=0, AIF1RX5_ENA=0, AIF1RX4_ENA=1, AIF1RX3_ENA=1, AIF1RX2_ENA=1, AIF1RX1_ENA=1
    {0x540, 0x006D, 0x0000ffff, 0, 0x0000ffff},       // AIF2_BCLK_Ctrl(540H):    006D  AIF2_BCLK_INV=AIF2BCLK not inverted, AIF2_BCLK_FRC=AIF2BCLK always enabled in Master mode, AIF2_BCLK_MSTR=AIF2BCLK Master mode, AIF2_BCLK_FREQ=3.072MHz (2.8824MHz)
    {0x541, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Tx_Pin_Ctrl(541H):  0000  AIF2TX_DAT_TRI=Logic 0 during unused timeslots
    {0x542, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Rx_Pin_Ctrl(542H):  0001  AIF2_LRCLK_MODE=0, AIF2_LRCLK_INV=AIF2LRCLK not inverted, AIF2_LRCLK_FRC=Normal, AIF2_LRCLK_MSTR=AIF2LRCLK Master mode
    {0x543, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Rate_Ctrl(543H):    0000  AIF2_RATE=SAMPLE_RATE_1, AIF2_TRI=Normal
    {0x544, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Format(544H):       0002  AIF2_FMT=I2S mode
    {0x546, 0x0040, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Rx_BCLK_Rate(546H): 0040  AIF2_BCPF=64 cycles
    {0x547, 0x1818, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_1(547H): 1818  AIF2TX_WL=24 bits per slot, AIF2TX_SLOT_LEN=24 cycles per slot
    {0x548, 0x1818, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_2(548H): 1818  AIF2RX_WL=24 bits per slot, AIF2RX_SLOT_LEN=24 cycles per slot
    {0x549, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_3(549H): 0000  AIF2TX1_SLOT=0
    {0x54A, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_4(54AH): 0001  AIF2TX2_SLOT=1
    {0x54B, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_5(54BH): 0002  AIF2TX3_SLOT=2
    {0x54C, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_6(54CH): 0003  AIF2TX4_SLOT=3
    {0x54D, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_7(54DH): 0004  AIF2TX5_SLOT=4
    {0x54E, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_8(54EH): 0005  AIF2TX6_SLOT=5
    {0x54F, 0x0006, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_9(54FH): 0006  AIF2TX7_SLOT=6
    {0x550, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_10(550H): 0007  AIF2TX8_SLOT=7
    {0x551, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_11(551H): 0000  AIF2RX1_SLOT=0
    {0x552, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_12(552H): 0001  AIF2RX2_SLOT=1
    {0x553, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_13(553H): 0002  AIF2RX3_SLOT=2
    {0x554, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_14(554H): 0003  AIF2RX4_SLOT=3
    {0x555, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_15(555H): 0004  AIF2RX5_SLOT=4
    {0x556, 0x0005, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_16(556H): 0005  AIF2RX6_SLOT=5
    {0x557, 0x0006, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_17(557H): 0006  AIF2RX7_SLOT=6
    {0x558, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Frame_Ctrl_18(558H): 0007  AIF2RX8_SLOT=7
    {0x559, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Tx_Enables(559H):   0003  AIF2TX8_ENA=0, AIF2TX7_ENA=0, AIF2TX6_ENA=0, AIF2TX5_ENA=0, AIF2TX4_ENA=0, AIF2TX3_ENA=0, AIF2TX2_ENA=1, AIF2TX1_ENA=1
    {0x55A, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF2_Rx_Enables(55AH):   0003  AIF2RX8_ENA=0, AIF2RX7_ENA=0, AIF2RX6_ENA=0, AIF2RX5_ENA=0, AIF2RX4_ENA=0, AIF2RX3_ENA=0, AIF2RX2_ENA=1, AIF2RX1_ENA=1
    {0x580, 0x000D, 0x0000ffff, 0, 0x0000ffff},       // AIF3_BCLK_Ctrl(580H):    000D  AIF3_BCLK_INV=AIF3BCLK not inverted, AIF3_BCLK_FRC=Normal, AIF3_BCLK_MSTR=AIF3BCLK Slave mode, AIF3_BCLK_FREQ=3.072MHz (2.8824MHz)
    {0x581, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Tx_Pin_Ctrl(581H):  0000  AIF3TX_DAT_TRI=Logic 0 during unused timeslots
    {0x582, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Rx_Pin_Ctrl(582H):  0000  AIF3_LRCLK_MODE=0, AIF3_LRCLK_INV=AIF3LRCLK not inverted, AIF3_LRCLK_FRC=Normal, AIF3_LRCLK_MSTR=AIF3LRCLK Slave mode
    {0x583, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Rate_Ctrl(583H):    0000  AIF3_RATE=SAMPLE_RATE_1, AIF3_TRI=Normal
    {0x584, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Format(584H):       0002  AIF3_FMT=I2S mode
    {0x586, 0x0040, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Rx_BCLK_Rate(586H): 0040  AIF3_BCPF=64 cycles
    {0x587, 0x1818, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Frame_Ctrl_1(587H): 1818  AIF3TX_WL=24 bits per slot, AIF3TX_SLOT_LEN=24 cycles per slot
    {0x588, 0x1818, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Frame_Ctrl_2(588H): 1818  AIF3RX_WL=24 bits per slot, AIF3RX_SLOT_LEN=24 cycles per slot
    {0x589, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Frame_Ctrl_3(589H): 0000  AIF3TX1_SLOT=0
    {0x58A, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Frame_Ctrl_4(58AH): 0001  AIF3TX2_SLOT=1
    {0x591, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Frame_Ctrl_11(591H): 0000  AIF3RX1_SLOT=0
    {0x592, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Frame_Ctrl_12(592H): 0001  AIF3RX2_SLOT=1
    {0x599, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Tx_Enables(599H):   0003  AIF3TX2_ENA=1, AIF3TX1_ENA=1
    {0x59A, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF3_Rx_Enables(59AH):   0003  AIF3RX2_ENA=1, AIF3RX1_ENA=1
    {0x5A0, 0x000D, 0x0000ffff, 0, 0x0000ffff},       // AIF4_BCLK_Ctrl(5A0H):    000D  AIF4_BCLK_INV=AIF4BCLK not inverted, AIF4_BCLK_FRC=Normal, AIF4_BCLK_MSTR=AIF4BCLK Slave mode, AIF4_BCLK_FREQ=3.072MHz (2.8824MHz)
    {0x5A1, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Tx_Pin_Ctrl(5A1H):  0000  AIF4TX_DAT_TRI=Logic 0 during unused timeslots
    {0x5A2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Rx_Pin_Ctrl(5A2H):  0000  AIF4_LRCLK_MODE=0, AIF4_LRCLK_INV=AIF4LRCLK not inverted, AIF4_LRCLK_FRC=Normal, AIF4_LRCLK_MSTR=AIF4LRCLK Slave mode
    {0x5A3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Rate_Ctrl(5A3H):    0000  AIF4_RATE=SAMPLE_RATE_1, AIF4_TRI=Normal
    {0x5A4, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Format(5A4H):       0002  AIF4_FMT=I2S mode
    {0x5A6, 0x0040, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Rx_BCLK_Rate(5A6H): 0040  AIF4_BCPF=64 cycles
    {0x5A7, 0x1020, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Frame_Ctrl_1(5A7H): 1020  AIF4TX_WL=16 bits per slot, AIF4TX_SLOT_LEN=32 cycles per slot
    {0x5A8, 0x1020, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Frame_Ctrl_2(5A8H): 1020  AIF4RX_WL=16 bits per slot, AIF4RX_SLOT_LEN=32 cycles per slot
    {0x5A9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Frame_Ctrl_3(5A9H): 0000  AIF4TX1_SLOT=0
    {0x5AA, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Frame_Ctrl_4(5AAH): 0001  AIF4TX2_SLOT=1
    {0x5B1, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Frame_Ctrl_11(5B1H): 0000  AIF4RX1_SLOT=0
    {0x5B2, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Frame_Ctrl_12(5B2H): 0001  AIF4RX2_SLOT=1
    {0x5B9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Tx_Enables(5B9H):   0000  AIF4TX2_ENA=0, AIF4TX1_ENA=0
    {0x5BA, 0x0003, 0x0000ffff, 0, 0x0000ffff},       // AIF4_Rx_Enables(5BAH):   0003  AIF4RX2_ENA=1, AIF4RX1_ENA=1
    {0x5C2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SPD1_TX_Control(5C2H):   0000  SPD1_VAL2=0, SPD1_VAL1=0, SPD1_RATE=SAMPLE_RATE_1, SPD1_ENA=0
    {0x5C3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SPD1_TX_Channel_Status_1(5C3H): 0000  SPD1_CATCODE=0000_0000, SPD1_CHSTMODE=00, SPD1_PREEMPH=000, SPD1_NOCOPY=0, SPD1_NOAUDIO=0, SPD1_PRO=0
    {0x5C4, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // SPD1_TX_Channel_Status_2(5C4H): 0001  SPD1_FREQ=0000, SPD1_CHNUM2=0000, SPD1_CHNUM1=0000, SPD1_SRCNUM=0001
    {0x5C5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SPD1_TX_Channel_Status_3(5C5H): 0000  SPD1_ORGSAMP=0000, SPD1_TXWL=000, SPD1_MAXWL=0, SPD1_CS31_30=00, SPD1_CLKACU=00
    {0x5D2, 0x0100, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_RX_Ports0(5D2H): 0100  SLIMRX2_PORT_ADDR=1, SLIMRX1_PORT_ADDR=0
    {0x5D3, 0x0302, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_RX_Ports1(5D3H): 0302  SLIMRX4_PORT_ADDR=3, SLIMRX3_PORT_ADDR=2
    {0x5D4, 0x0504, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_RX_Ports2(5D4H): 0504  SLIMRX6_PORT_ADDR=5, SLIMRX5_PORT_ADDR=4
    {0x5D5, 0x0706, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_RX_Ports3(5D5H): 0706  SLIMRX8_PORT_ADDR=7, SLIMRX7_PORT_ADDR=6
    {0x5D6, 0x0908, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_TX_Ports0(5D6H): 0908  SLIMTX2_PORT_ADDR=9, SLIMTX1_PORT_ADDR=8
    {0x5D7, 0x0B0A, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_TX_Ports1(5D7H): 0B0A  SLIMTX4_PORT_ADDR=11, SLIMTX3_PORT_ADDR=10
    {0x5D8, 0x0D0C, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_TX_Ports2(5D8H): 0D0C  SLIMTX6_PORT_ADDR=13, SLIMTX5_PORT_ADDR=12
    {0x5D9, 0x0F0E, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_TX_Ports3(5D9H): 0F0E  SLIMTX8_PORT_ADDR=15, SLIMTX7_PORT_ADDR=14
    {0x5E3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Framer_Ref_Gear(5E3H): 0000  SLIMCLK_SRC=0, SLIMCLK_REF_GEAR=Clock stopped
    {0x5E5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_1(5E5H):   0000  SLIMRX2_RATE=SAMPLE_RATE_1, SLIMRX1_RATE=SAMPLE_RATE_1
    {0x5E6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_2(5E6H):   0000  SLIMRX4_RATE=SAMPLE_RATE_1, SLIMRX3_RATE=SAMPLE_RATE_1
    {0x5E7, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_3(5E7H):   0000  SLIMRX6_RATE=SAMPLE_RATE_1, SLIMRX5_RATE=SAMPLE_RATE_1
    {0x5E8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_4(5E8H):   0000  SLIMRX8_RATE=SAMPLE_RATE_1, SLIMRX7_RATE=SAMPLE_RATE_1
    {0x5E9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_5(5E9H):   0000  SLIMTX2_RATE=SAMPLE_RATE_1, SLIMTX1_RATE=SAMPLE_RATE_1
    {0x5EA, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_6(5EAH):   0000  SLIMTX4_RATE=SAMPLE_RATE_1, SLIMTX3_RATE=SAMPLE_RATE_1
    {0x5EB, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_7(5EBH):   0000  SLIMTX6_RATE=SAMPLE_RATE_1, SLIMTX5_RATE=SAMPLE_RATE_1
    {0x5EC, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_Rates_8(5ECH):   0000  SLIMTX8_RATE=SAMPLE_RATE_1, SLIMTX7_RATE=SAMPLE_RATE_1
    {0x5F0, 0x000F, 0x0000ffff, 0, 0x0000ffff},       // Slimbus_Pad_Ctrl(5F0H):  000F  SLIMDAT3_DRV_STR=12mA, SLIMDAT2_DRV_STR=12mA, SLIMDAT1_DRV_STR=12mA, SLIMCLK_DRV_STR=4mA
    {0x5F5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_RX_Channel_Enable(5F5H): 0000  SLIMRX8_ENA=0, SLIMRX7_ENA=0, SLIMRX6_ENA=0, SLIMRX5_ENA=0, SLIMRX4_ENA=0, SLIMRX3_ENA=0, SLIMRX2_ENA=0, SLIMRX1_ENA=0
    {0x5F6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // SLIMbus_TX_Channel_Enable(5F6H): 0000  SLIMTX8_ENA=0, SLIMTX7_ENA=0, SLIMTX6_ENA=0, SLIMTX5_ENA=0, SLIMTX4_ENA=0, SLIMTX3_ENA=0, SLIMTX2_ENA=0, SLIMTX1_ENA=0
    {0x601, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // Mixer_Bypass(601H):      0001  MIXER_AIF_BYP=0, MIXER_DSP_BYP=1
    {0x640, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM1MIX_Input_1_Source(640H): 0000  PWM1MIX_STS1=0, PWM1MIX_SRC1=Silence (mute)
    {0x641, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM1MIX_Input_1_Volume(641H): 0080  PWM1MIX_VOL1=0dB
    {0x642, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM1MIX_Input_2_Source(642H): 0000  PWM1MIX_STS2=0, PWM1MIX_SRC2=Silence (mute)
    {0x643, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM1MIX_Input_2_Volume(643H): 0080  PWM1MIX_VOL2=0dB
    {0x644, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM1MIX_Input_3_Source(644H): 0000  PWM1MIX_STS3=0, PWM1MIX_SRC3=Silence (mute)
    {0x645, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM1MIX_Input_3_Volume(645H): 0080  PWM1MIX_VOL3=0dB
    {0x646, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM1MIX_Input_4_Source(646H): 0000  PWM1MIX_STS4=0, PWM1MIX_SRC4=Silence (mute)
    {0x647, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM1MIX_Input_4_Volume(647H): 0080  PWM1MIX_VOL4=0dB
    {0x648, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM2MIX_Input_1_Source(648H): 0000  PWM2MIX_STS1=0, PWM2MIX_SRC1=Silence (mute)
    {0x649, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM2MIX_Input_1_Volume(649H): 0080  PWM2MIX_VOL1=0dB
    {0x64A, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM2MIX_Input_2_Source(64AH): 0000  PWM2MIX_STS2=0, PWM2MIX_SRC2=Silence (mute)
    {0x64B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM2MIX_Input_2_Volume(64BH): 0080  PWM2MIX_VOL2=0dB
    {0x64C, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM2MIX_Input_3_Source(64CH): 0000  PWM2MIX_STS3=0, PWM2MIX_SRC3=Silence (mute)
    {0x64D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM2MIX_Input_3_Volume(64DH): 0080  PWM2MIX_VOL3=0dB
    {0x64E, 0x0000, 0x00007fff, 0, 0x00007fff},       // PWM2MIX_Input_4_Source(64EH): 0000  PWM2MIX_STS4=0, PWM2MIX_SRC4=Silence (mute)
    {0x64F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // PWM2MIX_Input_4_Volume(64FH): 0080  PWM2MIX_VOL4=0dB
    {0x680, 0x8012, 0x00007fff, 0, 0x00007fff},       // OUT1LMIX_Input_1_Source(680H): 8012  OUT1LMIX_STS1=1, OUT1LMIX_SRC1=IN2L signal path
    {0x681, 0x00FE, 0x0000ffff, 0, 0x0000ffff},       // OUT1LMIX_Input_1_Volume(681H): 00FE  OUT1LMIX_VOL1=16dB
    {0x682, 0x8010, 0x00007fff, 0, 0x00007fff},       // OUT1LMIX_Input_2_Source(682H): 8010  OUT1LMIX_STS2=1, OUT1LMIX_SRC2=IN1L signal path
    {0x683, 0x005C, 0x0000ffff, 0, 0x0000ffff},       // OUT1LMIX_Input_2_Volume(683H): 005C  OUT1LMIX_VOL2=-18dB
    {0x684, 0x8022, 0x00007fff, 0, 0x00007fff},       // OUT1LMIX_Input_3_Source(684H): 8022  OUT1LMIX_STS3=1, OUT1LMIX_SRC3=AIF1 RX3
    {0x685, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT1LMIX_Input_3_Volume(685H): 0080  OUT1LMIX_VOL3=0dB
    {0x686, 0x0030, 0x00007fff, 0, 0x00007fff},       // OUT1LMIX_Input_4_Source(686H): 0030  OUT1LMIX_STS4=0, OUT1LMIX_SRC4=AIF3 RX1
    {0x687, 0x00FE, 0x0000ffff, 0, 0x0000ffff},       // OUT1LMIX_Input_4_Volume(687H): 00FE  OUT1LMIX_VOL4=16dB
    {0x688, 0x8012, 0x00007fff, 0, 0x00007fff},       // OUT1RMIX_Input_1_Source(688H): 8012  OUT1RMIX_STS1=1, OUT1RMIX_SRC1=IN2L signal path
    {0x689, 0x00FE, 0x0000ffff, 0, 0x0000ffff},       // OUT1RMIX_Input_1_Volume(689H): 00FE  OUT1RMIX_VOL1=16dB
    {0x68A, 0x8011, 0x00007fff, 0, 0x00007fff},       // OUT1RMIX_Input_2_Source(68AH): 8011  OUT1RMIX_STS2=1, OUT1RMIX_SRC2=IN1R signal path
    {0x68B, 0x005C, 0x0000ffff, 0, 0x0000ffff},       // OUT1RMIX_Input_2_Volume(68BH): 005C  OUT1RMIX_VOL2=-18dB
    {0x68C, 0x8023, 0x00007fff, 0, 0x00007fff},       // OUT1RMIX_Input_3_Source(68CH): 8023  OUT1RMIX_STS3=1, OUT1RMIX_SRC3=AIF1 RX4
    {0x68D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT1RMIX_Input_3_Volume(68DH): 0080  OUT1RMIX_VOL3=0dB
    {0x68E, 0x0031, 0x00007fff, 0, 0x00007fff},       // OUT1RMIX_Input_4_Source(68EH): 0031  OUT1RMIX_STS4=0, OUT1RMIX_SRC4=AIF3 RX2
    {0x68F, 0x00FE, 0x0000ffff, 0, 0x0000ffff},       // OUT1RMIX_Input_4_Volume(68FH): 00FE  OUT1RMIX_VOL4=16dB
    {0x690, 0x8013, 0x00007fff, 0, 0x00007fff},       // OUT2LMIX_Input_1_Source(690H): 8013  OUT2LMIX_STS1=1, OUT2LMIX_SRC1=IN2R signal path
    {0x691, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2LMIX_Input_1_Volume(691H): 0080  OUT2LMIX_VOL1=0dB
    {0x692, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2LMIX_Input_2_Source(692H): 0000  OUT2LMIX_STS2=0, OUT2LMIX_SRC2=Silence (mute)
    {0x693, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2LMIX_Input_2_Volume(693H): 0080  OUT2LMIX_VOL2=0dB
    {0x694, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2LMIX_Input_3_Source(694H): 0000  OUT2LMIX_STS3=0, OUT2LMIX_SRC3=Silence (mute)
    {0x695, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2LMIX_Input_3_Volume(695H): 0080  OUT2LMIX_VOL3=0dB
    {0x696, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2LMIX_Input_4_Source(696H): 0000  OUT2LMIX_STS4=0, OUT2LMIX_SRC4=Silence (mute)
    {0x697, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2LMIX_Input_4_Volume(697H): 0080  OUT2LMIX_VOL4=0dB
    {0x698, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2RMIX_Input_1_Source(698H): 0000  OUT2RMIX_STS1=0, OUT2RMIX_SRC1=Silence (mute)
    {0x699, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2RMIX_Input_1_Volume(699H): 0080  OUT2RMIX_VOL1=0dB
    {0x69A, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2RMIX_Input_2_Source(69AH): 0000  OUT2RMIX_STS2=0, OUT2RMIX_SRC2=Silence (mute)
    {0x69B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2RMIX_Input_2_Volume(69BH): 0080  OUT2RMIX_VOL2=0dB
    {0x69C, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2RMIX_Input_3_Source(69CH): 0000  OUT2RMIX_STS3=0, OUT2RMIX_SRC3=Silence (mute)
    {0x69D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2RMIX_Input_3_Volume(69DH): 0080  OUT2RMIX_VOL3=0dB
    {0x69E, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT2RMIX_Input_4_Source(69EH): 0000  OUT2RMIX_STS4=0, OUT2RMIX_SRC4=Silence (mute)
    {0x69F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT2RMIX_Input_4_Volume(69FH): 0080  OUT2RMIX_VOL4=0dB
    {0x6A0, 0x8092, 0x00007fff, 0, 0x00007fff},       // OUT3LMIX_Input_1_Source(6A0H): 8092  OUT3LMIX_STS1=1, OUT3LMIX_SRC1=ASRC1 IN2 Left
    {0x6A1, 0x005C, 0x0000ffff, 0, 0x0000ffff},       // OUT3LMIX_Input_1_Volume(6A1H): 005C  OUT3LMIX_VOL1=-18dB
    {0x6A2, 0x8010, 0x00007fff, 0, 0x00007fff},       // OUT3LMIX_Input_2_Source(6A2H): 8010  OUT3LMIX_STS2=1, OUT3LMIX_SRC2=IN1L signal path
    {0x6A3, 0x005C, 0x0000ffff, 0, 0x0000ffff},       // OUT3LMIX_Input_2_Volume(6A3H): 005C  OUT3LMIX_VOL2=-18dB
    {0x6A4, 0x8020, 0x00007fff, 0, 0x00007fff},       // OUT3LMIX_Input_3_Source(6A4H): 8020  OUT3LMIX_STS3=1, OUT3LMIX_SRC3=AIF1 RX1
    {0x6A5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT3LMIX_Input_3_Volume(6A5H): 0080  OUT3LMIX_VOL3=0dB
    {0x6A6, 0x8028, 0x00007fff, 0, 0x00007fff},       // OUT3LMIX_Input_4_Source(6A6H): 8028  OUT3LMIX_STS4=1, OUT3LMIX_SRC4=AIF2 RX1
    {0x6A7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT3LMIX_Input_4_Volume(6A7H): 0080  OUT3LMIX_VOL4=0dB
    {0x6A8, 0x8093, 0x00007fff, 0, 0x00007fff},       // OUT3RMIX_Input_1_Source(6A8H): 8093  OUT3RMIX_STS1=1, OUT3RMIX_SRC1=ASRC1 IN2 Right
    {0x6A9, 0x005C, 0x0000ffff, 0, 0x0000ffff},       // OUT3RMIX_Input_1_Volume(6A9H): 005C  OUT3RMIX_VOL1=-18dB
    {0x6AA, 0x8011, 0x00007fff, 0, 0x00007fff},       // OUT3RMIX_Input_2_Source(6AAH): 8011  OUT3RMIX_STS2=1, OUT3RMIX_SRC2=IN1R signal path
    {0x6AB, 0x005C, 0x0000ffff, 0, 0x0000ffff},       // OUT3RMIX_Input_2_Volume(6ABH): 005C  OUT3RMIX_VOL2=-18dB
    {0x6AC, 0x8021, 0x00007fff, 0, 0x00007fff},       // OUT3RMIX_Input_3_Source(6ACH): 8021  OUT3RMIX_STS3=1, OUT3RMIX_SRC3=AIF1 RX2
    {0x6AD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT3RMIX_Input_3_Volume(6ADH): 0080  OUT3RMIX_VOL3=0dB
    {0x6AE, 0x8029, 0x00007fff, 0, 0x00007fff},       // OUT3RMIX_Input_4_Source(6AEH): 8029  OUT3RMIX_STS4=1, OUT3RMIX_SRC4=AIF2 RX2
    {0x6AF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT3RMIX_Input_4_Volume(6AFH): 0080  OUT3RMIX_VOL4=0dB
    {0x6C0, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5LMIX_Input_1_Source(6C0H): 0000  OUT5LMIX_STS1=0, OUT5LMIX_SRC1=Silence (mute)
    {0x6C1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5LMIX_Input_1_Volume(6C1H): 0080  OUT5LMIX_VOL1=0dB
    {0x6C2, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5LMIX_Input_2_Source(6C2H): 0000  OUT5LMIX_STS2=0, OUT5LMIX_SRC2=Silence (mute)
    {0x6C3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5LMIX_Input_2_Volume(6C3H): 0080  OUT5LMIX_VOL2=0dB
    {0x6C4, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5LMIX_Input_3_Source(6C4H): 0000  OUT5LMIX_STS3=0, OUT5LMIX_SRC3=Silence (mute)
    {0x6C5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5LMIX_Input_3_Volume(6C5H): 0080  OUT5LMIX_VOL3=0dB
    {0x6C6, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5LMIX_Input_4_Source(6C6H): 0000  OUT5LMIX_STS4=0, OUT5LMIX_SRC4=Silence (mute)
    {0x6C7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5LMIX_Input_4_Volume(6C7H): 0080  OUT5LMIX_VOL4=0dB
    {0x6C8, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5RMIX_Input_1_Source(6C8H): 0000  OUT5RMIX_STS1=0, OUT5RMIX_SRC1=Silence (mute)
    {0x6C9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5RMIX_Input_1_Volume(6C9H): 0080  OUT5RMIX_VOL1=0dB
    {0x6CA, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5RMIX_Input_2_Source(6CAH): 0000  OUT5RMIX_STS2=0, OUT5RMIX_SRC2=Silence (mute)
    {0x6CB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5RMIX_Input_2_Volume(6CBH): 0080  OUT5RMIX_VOL2=0dB
    {0x6CC, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5RMIX_Input_3_Source(6CCH): 0000  OUT5RMIX_STS3=0, OUT5RMIX_SRC3=Silence (mute)
    {0x6CD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5RMIX_Input_3_Volume(6CDH): 0080  OUT5RMIX_VOL3=0dB
    {0x6CE, 0x0000, 0x00007fff, 0, 0x00007fff},       // OUT5RMIX_Input_4_Source(6CEH): 0000  OUT5RMIX_STS4=0, OUT5RMIX_SRC4=Silence (mute)
    {0x6CF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // OUT5RMIX_Input_4_Volume(6CFH): 0080  OUT5RMIX_VOL4=0dB
    {0x700, 0x8092, 0x00007fff, 0, 0x00007fff},       // AIF1TX1MIX_Input_1_Source(700H): 8092  AIF1TX1MIX_STS1=1, AIF1TX1MIX_SRC1=ASRC1 IN2 Left
    {0x701, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX1MIX_Input_1_Volume(701H): 0080  AIF1TX1MIX_VOL1=0dB
    {0x702, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX1MIX_Input_2_Source(702H): 0000  AIF1TX1MIX_STS2=0, AIF1TX1MIX_SRC2=Silence (mute)
    {0x703, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX1MIX_Input_2_Volume(703H): 0080  AIF1TX1MIX_VOL2=0dB
    {0x704, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX1MIX_Input_3_Source(704H): 0000  AIF1TX1MIX_STS3=0, AIF1TX1MIX_SRC3=Silence (mute)
    {0x705, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX1MIX_Input_3_Volume(705H): 0080  AIF1TX1MIX_VOL3=0dB
    {0x706, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX1MIX_Input_4_Source(706H): 0000  AIF1TX1MIX_STS4=0, AIF1TX1MIX_SRC4=Silence (mute)
    {0x707, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX1MIX_Input_4_Volume(707H): 0080  AIF1TX1MIX_VOL4=0dB
    {0x708, 0x8093, 0x00007fff, 0, 0x00007fff},       // AIF1TX2MIX_Input_1_Source(708H): 8093  AIF1TX2MIX_STS1=1, AIF1TX2MIX_SRC1=ASRC1 IN2 Right
    {0x709, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX2MIX_Input_1_Volume(709H): 0080  AIF1TX2MIX_VOL1=0dB
    {0x70A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX2MIX_Input_2_Source(70AH): 0000  AIF1TX2MIX_STS2=0, AIF1TX2MIX_SRC2=Silence (mute)
    {0x70B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX2MIX_Input_2_Volume(70BH): 0080  AIF1TX2MIX_VOL2=0dB
    {0x70C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX2MIX_Input_3_Source(70CH): 0000  AIF1TX2MIX_STS3=0, AIF1TX2MIX_SRC3=Silence (mute)
    {0x70D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX2MIX_Input_3_Volume(70DH): 0080  AIF1TX2MIX_VOL3=0dB
    {0x70E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX2MIX_Input_4_Source(70EH): 0000  AIF1TX2MIX_STS4=0, AIF1TX2MIX_SRC4=Silence (mute)
    {0x70F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX2MIX_Input_4_Volume(70FH): 0080  AIF1TX2MIX_VOL4=0dB
    {0x710, 0x8013, 0x00007fff, 0, 0x00007fff},       // AIF1TX3MIX_Input_1_Source(710H): 8013  AIF1TX3MIX_STS1=1, AIF1TX3MIX_SRC1=IN2R signal path
    {0x711, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX3MIX_Input_1_Volume(711H): 0080  AIF1TX3MIX_VOL1=0dB
    {0x712, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX3MIX_Input_2_Source(712H): 0000  AIF1TX3MIX_STS2=0, AIF1TX3MIX_SRC2=Silence (mute)
    {0x713, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX3MIX_Input_2_Volume(713H): 0080  AIF1TX3MIX_VOL2=0dB
    {0x714, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX3MIX_Input_3_Source(714H): 0000  AIF1TX3MIX_STS3=0, AIF1TX3MIX_SRC3=Silence (mute)
    {0x715, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX3MIX_Input_3_Volume(715H): 0080  AIF1TX3MIX_VOL3=0dB
    {0x716, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX3MIX_Input_4_Source(716H): 0000  AIF1TX3MIX_STS4=0, AIF1TX3MIX_SRC4=Silence (mute)
    {0x717, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX3MIX_Input_4_Volume(717H): 0080  AIF1TX3MIX_VOL4=0dB
    {0x718, 0x8013, 0x00007fff, 0, 0x00007fff},       // AIF1TX4MIX_Input_1_Source(718H): 8013  AIF1TX4MIX_STS1=1, AIF1TX4MIX_SRC1=IN2R signal path
    {0x719, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX4MIX_Input_1_Volume(719H): 0080  AIF1TX4MIX_VOL1=0dB
    {0x71A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX4MIX_Input_2_Source(71AH): 0000  AIF1TX4MIX_STS2=0, AIF1TX4MIX_SRC2=Silence (mute)
    {0x71B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX4MIX_Input_2_Volume(71BH): 0080  AIF1TX4MIX_VOL2=0dB
    {0x71C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX4MIX_Input_3_Source(71CH): 0000  AIF1TX4MIX_STS3=0, AIF1TX4MIX_SRC3=Silence (mute)
    {0x71D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX4MIX_Input_3_Volume(71DH): 0080  AIF1TX4MIX_VOL3=0dB
    {0x71E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX4MIX_Input_4_Source(71EH): 0000  AIF1TX4MIX_STS4=0, AIF1TX4MIX_SRC4=Silence (mute)
    {0x71F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX4MIX_Input_4_Volume(71FH): 0080  AIF1TX4MIX_VOL4=0dB
    {0x720, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX5MIX_Input_1_Source(720H): 0000  AIF1TX5MIX_STS1=0, AIF1TX5MIX_SRC1=Silence (mute)
    {0x721, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX5MIX_Input_1_Volume(721H): 0080  AIF1TX5MIX_VOL1=0dB
    {0x722, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX5MIX_Input_2_Source(722H): 0000  AIF1TX5MIX_STS2=0, AIF1TX5MIX_SRC2=Silence (mute)
    {0x723, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX5MIX_Input_2_Volume(723H): 0080  AIF1TX5MIX_VOL2=0dB
    {0x724, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX5MIX_Input_3_Source(724H): 0000  AIF1TX5MIX_STS3=0, AIF1TX5MIX_SRC3=Silence (mute)
    {0x725, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX5MIX_Input_3_Volume(725H): 0080  AIF1TX5MIX_VOL3=0dB
    {0x726, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX5MIX_Input_4_Source(726H): 0000  AIF1TX5MIX_STS4=0, AIF1TX5MIX_SRC4=Silence (mute)
    {0x727, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX5MIX_Input_4_Volume(727H): 0080  AIF1TX5MIX_VOL4=0dB
    {0x728, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX6MIX_Input_1_Source(728H): 0000  AIF1TX6MIX_STS1=0, AIF1TX6MIX_SRC1=Silence (mute)
    {0x729, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX6MIX_Input_1_Volume(729H): 0080  AIF1TX6MIX_VOL1=0dB
    {0x72A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX6MIX_Input_2_Source(72AH): 0000  AIF1TX6MIX_STS2=0, AIF1TX6MIX_SRC2=Silence (mute)
    {0x72B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX6MIX_Input_2_Volume(72BH): 0080  AIF1TX6MIX_VOL2=0dB
    {0x72C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX6MIX_Input_3_Source(72CH): 0000  AIF1TX6MIX_STS3=0, AIF1TX6MIX_SRC3=Silence (mute)
    {0x72D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX6MIX_Input_3_Volume(72DH): 0080  AIF1TX6MIX_VOL3=0dB
    {0x72E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX6MIX_Input_4_Source(72EH): 0000  AIF1TX6MIX_STS4=0, AIF1TX6MIX_SRC4=Silence (mute)
    {0x72F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX6MIX_Input_4_Volume(72FH): 0080  AIF1TX6MIX_VOL4=0dB
    {0x730, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX7MIX_Input_1_Source(730H): 0000  AIF1TX7MIX_STS1=0, AIF1TX7MIX_SRC1=Silence (mute)
    {0x731, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX7MIX_Input_1_Volume(731H): 0080  AIF1TX7MIX_VOL1=0dB
    {0x732, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX7MIX_Input_2_Source(732H): 0000  AIF1TX7MIX_STS2=0, AIF1TX7MIX_SRC2=Silence (mute)
    {0x733, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX7MIX_Input_2_Volume(733H): 0080  AIF1TX7MIX_VOL2=0dB
    {0x734, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX7MIX_Input_3_Source(734H): 0000  AIF1TX7MIX_STS3=0, AIF1TX7MIX_SRC3=Silence (mute)
    {0x735, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX7MIX_Input_3_Volume(735H): 0080  AIF1TX7MIX_VOL3=0dB
    {0x736, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX7MIX_Input_4_Source(736H): 0000  AIF1TX7MIX_STS4=0, AIF1TX7MIX_SRC4=Silence (mute)
    {0x737, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX7MIX_Input_4_Volume(737H): 0080  AIF1TX7MIX_VOL4=0dB
    {0x738, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX8MIX_Input_1_Source(738H): 0000  AIF1TX8MIX_STS1=0, AIF1TX8MIX_SRC1=Silence (mute)
    {0x739, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX8MIX_Input_1_Volume(739H): 0080  AIF1TX8MIX_VOL1=0dB
    {0x73A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX8MIX_Input_2_Source(73AH): 0000  AIF1TX8MIX_STS2=0, AIF1TX8MIX_SRC2=Silence (mute)
    {0x73B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX8MIX_Input_2_Volume(73BH): 0080  AIF1TX8MIX_VOL2=0dB
    {0x73C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX8MIX_Input_3_Source(73CH): 0000  AIF1TX8MIX_STS3=0, AIF1TX8MIX_SRC3=Silence (mute)
    {0x73D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX8MIX_Input_3_Volume(73DH): 0080  AIF1TX8MIX_VOL3=0dB
    {0x73E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF1TX8MIX_Input_4_Source(73EH): 0000  AIF1TX8MIX_STS4=0, AIF1TX8MIX_SRC4=Silence (mute)
    {0x73F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF1TX8MIX_Input_4_Volume(73FH): 0080  AIF1TX8MIX_VOL4=0dB
    {0x740, 0x0013, 0x00007fff, 0, 0x00007fff},       // AIF2TX1MIX_Input_1_Source(740H): 0013  AIF2TX1MIX_STS1=0, AIF2TX1MIX_SRC1=IN2R signal path
    {0x741, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX1MIX_Input_1_Volume(741H): 0080  AIF2TX1MIX_VOL1=0dB
    {0x742, 0x0012, 0x00007fff, 0, 0x00007fff},       // AIF2TX1MIX_Input_2_Source(742H): 0012  AIF2TX1MIX_STS2=0, AIF2TX1MIX_SRC2=IN2L signal path
    {0x743, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX1MIX_Input_2_Volume(743H): 0080  AIF2TX1MIX_VOL2=0dB
    {0x744, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX1MIX_Input_3_Source(744H): 0000  AIF2TX1MIX_STS3=0, AIF2TX1MIX_SRC3=Silence (mute)
    {0x745, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX1MIX_Input_3_Volume(745H): 0080  AIF2TX1MIX_VOL3=0dB
    {0x746, 0x0011, 0x00007fff, 0, 0x00007fff},       // AIF2TX1MIX_Input_4_Source(746H): 0011  AIF2TX1MIX_STS4=0, AIF2TX1MIX_SRC4=IN1R signal path
    {0x747, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX1MIX_Input_4_Volume(747H): 0080  AIF2TX1MIX_VOL4=0dB
    {0x748, 0x0013, 0x00007fff, 0, 0x00007fff},       // AIF2TX2MIX_Input_1_Source(748H): 0013  AIF2TX2MIX_STS1=0, AIF2TX2MIX_SRC1=IN2R signal path
    {0x749, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX2MIX_Input_1_Volume(749H): 0080  AIF2TX2MIX_VOL1=0dB
    {0x74A, 0x0012, 0x00007fff, 0, 0x00007fff},       // AIF2TX2MIX_Input_2_Source(74AH): 0012  AIF2TX2MIX_STS2=0, AIF2TX2MIX_SRC2=IN2L signal path
    {0x74B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX2MIX_Input_2_Volume(74BH): 0080  AIF2TX2MIX_VOL2=0dB
    {0x74C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX2MIX_Input_3_Source(74CH): 0000  AIF2TX2MIX_STS3=0, AIF2TX2MIX_SRC3=Silence (mute)
    {0x74D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX2MIX_Input_3_Volume(74DH): 0080  AIF2TX2MIX_VOL3=0dB
    {0x74E, 0x0010, 0x00007fff, 0, 0x00007fff},       // AIF2TX2MIX_Input_4_Source(74EH): 0010  AIF2TX2MIX_STS4=0, AIF2TX2MIX_SRC4=IN1L signal path
    {0x74F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX2MIX_Input_4_Volume(74FH): 0080  AIF2TX2MIX_VOL4=0dB
    {0x750, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX3MIX_Input_1_Source(750H): 0000  AIF2TX3MIX_STS1=0, AIF2TX3MIX_SRC1=Silence (mute)
    {0x751, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX3MIX_Input_1_Volume(751H): 0080  AIF2TX3MIX_VOL1=0dB
    {0x752, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX3MIX_Input_2_Source(752H): 0000  AIF2TX3MIX_STS2=0, AIF2TX3MIX_SRC2=Silence (mute)
    {0x753, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX3MIX_Input_2_Volume(753H): 0080  AIF2TX3MIX_VOL2=0dB
    {0x754, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX3MIX_Input_3_Source(754H): 0000  AIF2TX3MIX_STS3=0, AIF2TX3MIX_SRC3=Silence (mute)
    {0x755, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX3MIX_Input_3_Volume(755H): 0080  AIF2TX3MIX_VOL3=0dB
    {0x756, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX3MIX_Input_4_Source(756H): 0000  AIF2TX3MIX_STS4=0, AIF2TX3MIX_SRC4=Silence (mute)
    {0x757, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX3MIX_Input_4_Volume(757H): 0080  AIF2TX3MIX_VOL4=0dB
    {0x758, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX4MIX_Input_1_Source(758H): 0000  AIF2TX4MIX_STS1=0, AIF2TX4MIX_SRC1=Silence (mute)
    {0x759, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX4MIX_Input_1_Volume(759H): 0080  AIF2TX4MIX_VOL1=0dB
    {0x75A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX4MIX_Input_2_Source(75AH): 0000  AIF2TX4MIX_STS2=0, AIF2TX4MIX_SRC2=Silence (mute)
    {0x75B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX4MIX_Input_2_Volume(75BH): 0080  AIF2TX4MIX_VOL2=0dB
    {0x75C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX4MIX_Input_3_Source(75CH): 0000  AIF2TX4MIX_STS3=0, AIF2TX4MIX_SRC3=Silence (mute)
    {0x75D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX4MIX_Input_3_Volume(75DH): 0080  AIF2TX4MIX_VOL3=0dB
    {0x75E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX4MIX_Input_4_Source(75EH): 0000  AIF2TX4MIX_STS4=0, AIF2TX4MIX_SRC4=Silence (mute)
    {0x75F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX4MIX_Input_4_Volume(75FH): 0080  AIF2TX4MIX_VOL4=0dB
    {0x760, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX5MIX_Input_1_Source(760H): 0000  AIF2TX5MIX_STS1=0, AIF2TX5MIX_SRC1=Silence (mute)
    {0x761, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX5MIX_Input_1_Volume(761H): 0080  AIF2TX5MIX_VOL1=0dB
    {0x762, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX5MIX_Input_2_Source(762H): 0000  AIF2TX5MIX_STS2=0, AIF2TX5MIX_SRC2=Silence (mute)
    {0x763, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX5MIX_Input_2_Volume(763H): 0080  AIF2TX5MIX_VOL2=0dB
    {0x764, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX5MIX_Input_3_Source(764H): 0000  AIF2TX5MIX_STS3=0, AIF2TX5MIX_SRC3=Silence (mute)
    {0x765, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX5MIX_Input_3_Volume(765H): 0080  AIF2TX5MIX_VOL3=0dB
    {0x766, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX5MIX_Input_4_Source(766H): 0000  AIF2TX5MIX_STS4=0, AIF2TX5MIX_SRC4=Silence (mute)
    {0x767, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX5MIX_Input_4_Volume(767H): 0080  AIF2TX5MIX_VOL4=0dB
    {0x768, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX6MIX_Input_1_Source(768H): 0000  AIF2TX6MIX_STS1=0, AIF2TX6MIX_SRC1=Silence (mute)
    {0x769, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX6MIX_Input_1_Volume(769H): 0080  AIF2TX6MIX_VOL1=0dB
    {0x76A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX6MIX_Input_2_Source(76AH): 0000  AIF2TX6MIX_STS2=0, AIF2TX6MIX_SRC2=Silence (mute)
    {0x76B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX6MIX_Input_2_Volume(76BH): 0080  AIF2TX6MIX_VOL2=0dB
    {0x76C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX6MIX_Input_3_Source(76CH): 0000  AIF2TX6MIX_STS3=0, AIF2TX6MIX_SRC3=Silence (mute)
    {0x76D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX6MIX_Input_3_Volume(76DH): 0080  AIF2TX6MIX_VOL3=0dB
    {0x76E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX6MIX_Input_4_Source(76EH): 0000  AIF2TX6MIX_STS4=0, AIF2TX6MIX_SRC4=Silence (mute)
    {0x76F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX6MIX_Input_4_Volume(76FH): 0080  AIF2TX6MIX_VOL4=0dB
    {0x770, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX7MIX_Input_1_Source(770H): 0000  AIF2TX7MIX_STS1=0, AIF2TX7MIX_SRC1=Silence (mute)
    {0x771, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX7MIX_Input_1_Volume(771H): 0080  AIF2TX7MIX_VOL1=0dB
    {0x772, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX7MIX_Input_2_Source(772H): 0000  AIF2TX7MIX_STS2=0, AIF2TX7MIX_SRC2=Silence (mute)
    {0x773, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX7MIX_Input_2_Volume(773H): 0080  AIF2TX7MIX_VOL2=0dB
    {0x774, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX7MIX_Input_3_Source(774H): 0000  AIF2TX7MIX_STS3=0, AIF2TX7MIX_SRC3=Silence (mute)
    {0x775, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX7MIX_Input_3_Volume(775H): 0080  AIF2TX7MIX_VOL3=0dB
    {0x776, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX7MIX_Input_4_Source(776H): 0000  AIF2TX7MIX_STS4=0, AIF2TX7MIX_SRC4=Silence (mute)
    {0x777, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX7MIX_Input_4_Volume(777H): 0080  AIF2TX7MIX_VOL4=0dB
    {0x778, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX8MIX_Input_1_Source(778H): 0000  AIF2TX8MIX_STS1=0, AIF2TX8MIX_SRC1=Silence (mute)
    {0x779, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX8MIX_Input_1_Volume(779H): 0080  AIF2TX8MIX_VOL1=0dB
    {0x77A, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX8MIX_Input_2_Source(77AH): 0000  AIF2TX8MIX_STS2=0, AIF2TX8MIX_SRC2=Silence (mute)
    {0x77B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX8MIX_Input_2_Volume(77BH): 0080  AIF2TX8MIX_VOL2=0dB
    {0x77C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX8MIX_Input_3_Source(77CH): 0000  AIF2TX8MIX_STS3=0, AIF2TX8MIX_SRC3=Silence (mute)
    {0x77D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX8MIX_Input_3_Volume(77DH): 0080  AIF2TX8MIX_VOL3=0dB
    {0x77E, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF2TX8MIX_Input_4_Source(77EH): 0000  AIF2TX8MIX_STS4=0, AIF2TX8MIX_SRC4=Silence (mute)
    {0x77F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF2TX8MIX_Input_4_Volume(77FH): 0080  AIF2TX8MIX_VOL4=0dB
    {0x780, 0x0013, 0x00007fff, 0, 0x00007fff},       // AIF3TX1MIX_Input_1_Source(780H): 0013  AIF3TX1MIX_STS1=0, AIF3TX1MIX_SRC1=IN2R signal path
    {0x781, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX1MIX_Input_1_Volume(781H): 0080  AIF3TX1MIX_VOL1=0dB
    {0x782, 0x0012, 0x00007fff, 0, 0x00007fff},       // AIF3TX1MIX_Input_2_Source(782H): 0012  AIF3TX1MIX_STS2=0, AIF3TX1MIX_SRC2=IN2L signal path
    {0x783, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX1MIX_Input_2_Volume(783H): 0080  AIF3TX1MIX_VOL2=0dB
    {0x784, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF3TX1MIX_Input_3_Source(784H): 0000  AIF3TX1MIX_STS3=0, AIF3TX1MIX_SRC3=Silence (mute)
    {0x785, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX1MIX_Input_3_Volume(785H): 0080  AIF3TX1MIX_VOL3=0dB
    {0x786, 0x0011, 0x00007fff, 0, 0x00007fff},       // AIF3TX1MIX_Input_4_Source(786H): 0011  AIF3TX1MIX_STS4=0, AIF3TX1MIX_SRC4=IN1R signal path
    {0x787, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX1MIX_Input_4_Volume(787H): 0080  AIF3TX1MIX_VOL4=0dB
    {0x788, 0x0013, 0x00007fff, 0, 0x00007fff},       // AIF3TX2MIX_Input_1_Source(788H): 0013  AIF3TX2MIX_STS1=0, AIF3TX2MIX_SRC1=IN2R signal path
    {0x789, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX2MIX_Input_1_Volume(789H): 0080  AIF3TX2MIX_VOL1=0dB
    {0x78A, 0x0012, 0x00007fff, 0, 0x00007fff},       // AIF3TX2MIX_Input_2_Source(78AH): 0012  AIF3TX2MIX_STS2=0, AIF3TX2MIX_SRC2=IN2L signal path
    {0x78B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX2MIX_Input_2_Volume(78BH): 0080  AIF3TX2MIX_VOL2=0dB
    {0x78C, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF3TX2MIX_Input_3_Source(78CH): 0000  AIF3TX2MIX_STS3=0, AIF3TX2MIX_SRC3=Silence (mute)
    {0x78D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX2MIX_Input_3_Volume(78DH): 0080  AIF3TX2MIX_VOL3=0dB
    {0x78E, 0x0010, 0x00007fff, 0, 0x00007fff},       // AIF3TX2MIX_Input_4_Source(78EH):  0010  AIF3TX2MIX_STS4=0, AIF3TX2MIX_SRC4=IN1L signal path
    {0x78F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF3TX2MIX_Input_4_Volume(78FH): 0080  AIF3TX2MIX_VOL4=0dB
    {0x7A0, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX1MIX_Input_1_Source(7A0H): 0000  AIF4TX1MIX_STS1=0, AIF4TX1MIX_SRC1=Silence (mute)
    {0x7A1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX1MIX_Input_1_Volume(7A1H): 0080  AIF4TX1MIX_VOL1=0dB
    {0x7A2, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX1MIX_Input_2_Source(7A2H): 0000  AIF4TX1MIX_STS2=0, AIF4TX1MIX_SRC2=Silence (mute)
    {0x7A3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX1MIX_Input_2_Volume(7A3H): 0080  AIF4TX1MIX_VOL2=0dB
    {0x7A4, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX1MIX_Input_3_Source(7A4H): 0000  AIF4TX1MIX_STS3=0, AIF4TX1MIX_SRC3=Silence (mute)
    {0x7A5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX1MIX_Input_3_Volume(7A5H): 0080  AIF4TX1MIX_VOL3=0dB
    {0x7A6, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX1MIX_Input_4_Source(7A6H): 0000  AIF4TX1MIX_STS4=0, AIF4TX1MIX_SRC4=Silence (mute)
    {0x7A7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX1MIX_Input_4_Volume(7A7H): 0080  AIF4TX1MIX_VOL4=0dB
    {0x7A8, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX2MIX_Input_1_Source(7A8H): 0000  AIF4TX2MIX_STS1=0, AIF4TX2MIX_SRC1=Silence (mute)
    {0x7A9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX2MIX_Input_1_Volume(7A9H): 0080  AIF4TX2MIX_VOL1=0dB
    {0x7AA, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX2MIX_Input_2_Source(7AAH): 0000  AIF4TX2MIX_STS2=0, AIF4TX2MIX_SRC2=Silence (mute)
    {0x7AB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX2MIX_Input_2_Volume(7ABH): 0080  AIF4TX2MIX_VOL2=0dB
    {0x7AC, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX2MIX_Input_3_Source(7ACH): 0000  AIF4TX2MIX_STS3=0, AIF4TX2MIX_SRC3=Silence (mute)
    {0x7AD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX2MIX_Input_3_Volume(7ADH): 0080  AIF4TX2MIX_VOL3=0dB
    {0x7AE, 0x0000, 0x00007fff, 0, 0x00007fff},       // AIF4TX2MIX_Input_4_Source(7AEH): 0000  AIF4TX2MIX_STS4=0, AIF4TX2MIX_SRC4=Silence (mute)
    {0x7AF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // AIF4TX2MIX_Input_4_Volume(7AFH): 0080  AIF4TX2MIX_VOL4=0dB
    {0x7C0, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX1MIX_Input_1_Source(7C0H): 0000  SLIMTX1MIX_STS1=0, SLIMTX1MIX_SRC1=Silence (mute)
    {0x7C1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX1MIX_Input_1_Volume(7C1H): 0080  SLIMTX1MIX_VOL1=0dB
    {0x7C2, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX1MIX_Input_2_Source(7C2H): 0000  SLIMTX1MIX_STS2=0, SLIMTX1MIX_SRC2=Silence (mute)
    {0x7C3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX1MIX_Input_2_Volume(7C3H): 0080  SLIMTX1MIX_VOL2=0dB
    {0x7C4, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX1MIX_Input_3_Source(7C4H): 0000  SLIMTX1MIX_STS3=0, SLIMTX1MIX_SRC3=Silence (mute)
    {0x7C5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX1MIX_Input_3_Volume(7C5H): 0080  SLIMTX1MIX_VOL3=0dB
    {0x7C6, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX1MIX_Input_4_Source(7C6H): 0000  SLIMTX1MIX_STS4=0, SLIMTX1MIX_SRC4=Silence (mute)
    {0x7C7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX1MIX_Input_4_Volume(7C7H): 0080  SLIMTX1MIX_VOL4=0dB
    {0x7C8, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX2MIX_Input_1_Source(7C8H): 0000  SLIMTX2MIX_STS1=0, SLIMTX2MIX_SRC1=Silence (mute)
    {0x7C9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX2MIX_Input_1_Volume(7C9H): 0080  SLIMTX2MIX_VOL1=0dB
    {0x7CA, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX2MIX_Input_2_Source(7CAH): 0000  SLIMTX2MIX_STS2=0, SLIMTX2MIX_SRC2=Silence (mute)
    {0x7CB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX2MIX_Input_2_Volume(7CBH): 0080  SLIMTX2MIX_VOL2=0dB
    {0x7CC, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX2MIX_Input_3_Source(7CCH): 0000  SLIMTX2MIX_STS3=0, SLIMTX2MIX_SRC3=Silence (mute)
    {0x7CD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX2MIX_Input_3_Volume(7CDH): 0080  SLIMTX2MIX_VOL3=0dB
    {0x7CE, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX2MIX_Input_4_Source(7CEH): 0000  SLIMTX2MIX_STS4=0, SLIMTX2MIX_SRC4=Silence (mute)
    {0x7CF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX2MIX_Input_4_Volume(7CFH): 0080  SLIMTX2MIX_VOL4=0dB
    {0x7D0, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX3MIX_Input_1_Source(7D0H): 0000  SLIMTX3MIX_STS1=0, SLIMTX3MIX_SRC1=Silence (mute)
    {0x7D1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX3MIX_Input_1_Volume(7D1H): 0080  SLIMTX3MIX_VOL1=0dB
    {0x7D2, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX3MIX_Input_2_Source(7D2H): 0000  SLIMTX3MIX_STS2=0, SLIMTX3MIX_SRC2=Silence (mute)
    {0x7D3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX3MIX_Input_2_Volume(7D3H): 0080  SLIMTX3MIX_VOL2=0dB
    {0x7D4, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX3MIX_Input_3_Source(7D4H): 0000  SLIMTX3MIX_STS3=0, SLIMTX3MIX_SRC3=Silence (mute)
    {0x7D5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX3MIX_Input_3_Volume(7D5H): 0080  SLIMTX3MIX_VOL3=0dB
    {0x7D6, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX3MIX_Input_4_Source(7D6H): 0000  SLIMTX3MIX_STS4=0, SLIMTX3MIX_SRC4=Silence (mute)
    {0x7D7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX3MIX_Input_4_Volume(7D7H): 0080  SLIMTX3MIX_VOL4=0dB
    {0x7D8, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX4MIX_Input_1_Source(7D8H): 0000  SLIMTX4MIX_STS1=0, SLIMTX4MIX_SRC1=Silence (mute)
    {0x7D9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX4MIX_Input_1_Volume(7D9H): 0080  SLIMTX4MIX_VOL1=0dB
    {0x7DA, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX4MIX_Input_2_Source(7DAH): 0000  SLIMTX4MIX_STS2=0, SLIMTX4MIX_SRC2=Silence (mute)
    {0x7DB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX4MIX_Input_2_Volume(7DBH): 0080  SLIMTX4MIX_VOL2=0dB
    {0x7DC, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX4MIX_Input_3_Source(7DCH): 0000  SLIMTX4MIX_STS3=0, SLIMTX4MIX_SRC3=Silence (mute)
    {0x7DD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX4MIX_Input_3_Volume(7DDH): 0080  SLIMTX4MIX_VOL3=0dB
    {0x7DE, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX4MIX_Input_4_Source(7DEH): 0000  SLIMTX4MIX_STS4=0, SLIMTX4MIX_SRC4=Silence (mute)
    {0x7DF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX4MIX_Input_4_Volume(7DFH): 0080  SLIMTX4MIX_VOL4=0dB
    {0x7E0, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX5MIX_Input_1_Source(7E0H): 0000  SLIMTX5MIX_STS1=0, SLIMTX5MIX_SRC1=Silence (mute)
    {0x7E1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX5MIX_Input_1_Volume(7E1H): 0080  SLIMTX5MIX_VOL1=0dB
    {0x7E2, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX5MIX_Input_2_Source(7E2H): 0000  SLIMTX5MIX_STS2=0, SLIMTX5MIX_SRC2=Silence (mute)
    {0x7E3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX5MIX_Input_2_Volume(7E3H): 0080  SLIMTX5MIX_VOL2=0dB
    {0x7E4, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX5MIX_Input_3_Source(7E4H): 0000  SLIMTX5MIX_STS3=0, SLIMTX5MIX_SRC3=Silence (mute)
    {0x7E5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX5MIX_Input_3_Volume(7E5H): 0080  SLIMTX5MIX_VOL3=0dB
    {0x7E6, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX5MIX_Input_4_Source(7E6H): 0000  SLIMTX5MIX_STS4=0, SLIMTX5MIX_SRC4=Silence (mute)
    {0x7E7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX5MIX_Input_4_Volume(7E7H): 0080  SLIMTX5MIX_VOL4=0dB
    {0x7E8, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX6MIX_Input_1_Source(7E8H): 0000  SLIMTX6MIX_STS1=0, SLIMTX6MIX_SRC1=Silence (mute)
    {0x7E9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX6MIX_Input_1_Volume(7E9H): 0080  SLIMTX6MIX_VOL1=0dB
    {0x7EA, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX6MIX_Input_2_Source(7EAH): 0000  SLIMTX6MIX_STS2=0, SLIMTX6MIX_SRC2=Silence (mute)
    {0x7EB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX6MIX_Input_2_Volume(7EBH): 0080  SLIMTX6MIX_VOL2=0dB
    {0x7EC, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX6MIX_Input_3_Source(7ECH): 0000  SLIMTX6MIX_STS3=0, SLIMTX6MIX_SRC3=Silence (mute)
    {0x7ED, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX6MIX_Input_3_Volume(7EDH): 0080  SLIMTX6MIX_VOL3=0dB
    {0x7EE, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX6MIX_Input_4_Source(7EEH): 0000  SLIMTX6MIX_STS4=0, SLIMTX6MIX_SRC4=Silence (mute)
    {0x7EF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX6MIX_Input_4_Volume(7EFH): 0080  SLIMTX6MIX_VOL4=0dB
    {0x7F0, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX7MIX_Input_1_Source(7F0H): 0000  SLIMTX7MIX_STS1=0, SLIMTX7MIX_SRC1=Silence (mute)
    {0x7F1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX7MIX_Input_1_Volume(7F1H): 0080  SLIMTX7MIX_VOL1=0dB
    {0x7F2, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX7MIX_Input_2_Source(7F2H): 0000  SLIMTX7MIX_STS2=0, SLIMTX7MIX_SRC2=Silence (mute)
    {0x7F3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX7MIX_Input_2_Volume(7F3H): 0080  SLIMTX7MIX_VOL2=0dB
    {0x7F4, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX7MIX_Input_3_Source(7F4H): 0000  SLIMTX7MIX_STS3=0, SLIMTX7MIX_SRC3=Silence (mute)
    {0x7F5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX7MIX_Input_3_Volume(7F5H): 0080  SLIMTX7MIX_VOL3=0dB
    {0x7F6, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX7MIX_Input_4_Source(7F6H): 0000  SLIMTX7MIX_STS4=0, SLIMTX7MIX_SRC4=Silence (mute)
    {0x7F7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX7MIX_Input_4_Volume(7F7H): 0080  SLIMTX7MIX_VOL4=0dB
    {0x7F8, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX8MIX_Input_1_Source(7F8H): 0000  SLIMTX8MIX_STS1=0, SLIMTX8MIX_SRC1=Silence (mute)
    {0x7F9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX8MIX_Input_1_Volume(7F9H): 0080  SLIMTX8MIX_VOL1=0dB
    {0x7FA, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX8MIX_Input_2_Source(7FAH): 0000  SLIMTX8MIX_STS2=0, SLIMTX8MIX_SRC2=Silence (mute)
    {0x7FB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX8MIX_Input_2_Volume(7FBH): 0080  SLIMTX8MIX_VOL2=0dB
    {0x7FC, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX8MIX_Input_3_Source(7FCH): 0000  SLIMTX8MIX_STS3=0, SLIMTX8MIX_SRC3=Silence (mute)
    {0x7FD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX8MIX_Input_3_Volume(7FDH): 0080  SLIMTX8MIX_VOL3=0dB
    {0x7FE, 0x0000, 0x00007fff, 0, 0x00007fff},       // SLIMTX8MIX_Input_4_Source(7FEH): 0000  SLIMTX8MIX_STS4=0, SLIMTX8MIX_SRC4=Silence (mute)
    {0x7FF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SLIMTX8MIX_Input_4_Volume(7FFH): 0080  SLIMTX8MIX_VOL4=0dB
    {0x800, 0x0000, 0x00007fff, 0, 0x00007fff},       // SPDIF1TX1MIX_Input_1_Source(800H): 0000  SPDIF1TX1_STS=0, SPDIF1TX1_SRC=Silence (mute)
    {0x801, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SPDIF1TX1MIX_Input_1_Volume(801H): 0080  SPDIF1TX1_VOL=0dB
    {0x808, 0x0000, 0x00007fff, 0, 0x00007fff},       // SPDIF1TX2MIX_Input_1_Source(808H): 0000  SPDIF1TX2_STS=0, SPDIF1TX2_SRC=Silence (mute)
    {0x809, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // SPDIF1TX2MIX_Input_1_Volume(809H): 0080  SPDIF1TX2_VOL=0dB
    {0x880, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ1MIX_Input_1_Source(880H): 0000  EQ1MIX_STS1=0, EQ1MIX_SRC1=Silence (mute)
    {0x881, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ1MIX_Input_1_Volume(881H): 0080  EQ1MIX_VOL1=0dB
    {0x882, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ1MIX_Input_2_Source(882H): 0000  EQ1MIX_STS2=0, EQ1MIX_SRC2=Silence (mute)
    {0x883, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ1MIX_Input_2_Volume(883H): 0080  EQ1MIX_VOL2=0dB
    {0x884, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ1MIX_Input_3_Source(884H): 0000  EQ1MIX_STS3=0, EQ1MIX_SRC3=Silence (mute)
    {0x885, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ1MIX_Input_3_Volume(885H): 0080  EQ1MIX_VOL3=0dB
    {0x886, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ1MIX_Input_4_Source(886H): 0000  EQ1MIX_STS4=0, EQ1MIX_SRC4=Silence (mute)
    {0x887, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ1MIX_Input_4_Volume(887H): 0080  EQ1MIX_VOL4=0dB
    {0x888, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ2MIX_Input_1_Source(888H): 0000  EQ2MIX_STS1=0, EQ2MIX_SRC1=Silence (mute)
    {0x889, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ2MIX_Input_1_Volume(889H): 0080  EQ2MIX_VOL1=0dB
    {0x88A, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ2MIX_Input_2_Source(88AH): 0000  EQ2MIX_STS2=0, EQ2MIX_SRC2=Silence (mute)
    {0x88B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ2MIX_Input_2_Volume(88BH): 0080  EQ2MIX_VOL2=0dB
    {0x88C, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ2MIX_Input_3_Source(88CH): 0000  EQ2MIX_STS3=0, EQ2MIX_SRC3=Silence (mute)
    {0x88D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ2MIX_Input_3_Volume(88DH): 0080  EQ2MIX_VOL3=0dB
    {0x88E, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ2MIX_Input_4_Source(88EH): 0000  EQ2MIX_STS4=0, EQ2MIX_SRC4=Silence (mute)
    {0x88F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ2MIX_Input_4_Volume(88FH): 0080  EQ2MIX_VOL4=0dB
    {0x890, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ3MIX_Input_1_Source(890H): 0000  EQ3MIX_STS1=0, EQ3MIX_SRC1=Silence (mute)
    {0x891, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ3MIX_Input_1_Volume(891H): 0080  EQ3MIX_VOL1=0dB
    {0x892, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ3MIX_Input_2_Source(892H): 0000  EQ3MIX_STS2=0, EQ3MIX_SRC2=Silence (mute)
    {0x893, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ3MIX_Input_2_Volume(893H): 0080  EQ3MIX_VOL2=0dB
    {0x894, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ3MIX_Input_3_Source(894H): 0000  EQ3MIX_STS3=0, EQ3MIX_SRC3=Silence (mute)
    {0x895, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ3MIX_Input_3_Volume(895H): 0080  EQ3MIX_VOL3=0dB
    {0x896, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ3MIX_Input_4_Source(896H): 0000  EQ3MIX_STS4=0, EQ3MIX_SRC4=Silence (mute)
    {0x897, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ3MIX_Input_4_Volume(897H): 0080  EQ3MIX_VOL4=0dB
    {0x898, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ4MIX_Input_1_Source(898H): 0000  EQ4MIX_STS1=0, EQ4MIX_SRC1=Silence (mute)
    {0x899, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ4MIX_Input_1_Volume(899H): 0080  EQ4MIX_VOL1=0dB
    {0x89A, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ4MIX_Input_2_Source(89AH): 0000  EQ4MIX_STS2=0, EQ4MIX_SRC2=Silence (mute)
    {0x89B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ4MIX_Input_2_Volume(89BH): 0080  EQ4MIX_VOL2=0dB
    {0x89C, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ4MIX_Input_3_Source(89CH): 0000  EQ4MIX_STS3=0, EQ4MIX_SRC3=Silence (mute)
    {0x89D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ4MIX_Input_3_Volume(89DH): 0080  EQ4MIX_VOL3=0dB
    {0x89E, 0x0000, 0x00007fff, 0, 0x00007fff},       // EQ4MIX_Input_4_Source(89EH): 0000  EQ4MIX_STS4=0, EQ4MIX_SRC4=Silence (mute)
    {0x89F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // EQ4MIX_Input_4_Volume(89FH): 0080  EQ4MIX_VOL4=0dB
    {0x8C0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1LMIX_Input_1_Source(8C0H): 0000  DRC1LMIX_STS1=0, DRC1LMIX_SRC1=Silence (mute)
    {0x8C1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1LMIX_Input_1_Volume(8C1H): 0080  DRC1LMIX_VOL1=0dB
    {0x8C2, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1LMIX_Input_2_Source(8C2H): 0000  DRC1LMIX_STS2=0, DRC1LMIX_SRC2=Silence (mute)
    {0x8C3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1LMIX_Input_2_Volume(8C3H): 0080  DRC1LMIX_VOL2=0dB
    {0x8C4, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1LMIX_Input_3_Source(8C4H): 0000  DRC1LMIX_STS3=0, DRC1LMIX_SRC3=Silence (mute)
    {0x8C5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1LMIX_Input_3_Volume(8C5H): 0080  DRC1LMIX_VOL3=0dB
    {0x8C6, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1LMIX_Input_4_Source(8C6H): 0000  DRC1LMIX_STS4=0, DRC1LMIX_SRC4=Silence (mute)
    {0x8C7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1LMIX_Input_4_Volume(8C7H): 0080  DRC1LMIX_VOL4=0dB
    {0x8C8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1RMIX_Input_1_Source(8C8H): 0000  DRC1RMIX_STS1=0, DRC1RMIX_SRC1=Silence (mute)
    {0x8C9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1RMIX_Input_1_Volume(8C9H): 0080  DRC1RMIX_VOL1=0dB
    {0x8CA, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1RMIX_Input_2_Source(8CAH): 0000  DRC1RMIX_STS2=0, DRC1RMIX_SRC2=Silence (mute)
    {0x8CB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1RMIX_Input_2_Volume(8CBH): 0080  DRC1RMIX_VOL2=0dB
    {0x8CC, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1RMIX_Input_3_Source(8CCH): 0000  DRC1RMIX_STS3=0, DRC1RMIX_SRC3=Silence (mute)
    {0x8CD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1RMIX_Input_3_Volume(8CDH): 0080  DRC1RMIX_VOL3=0dB
    {0x8CE, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC1RMIX_Input_4_Source(8CEH): 0000  DRC1RMIX_STS4=0, DRC1RMIX_SRC4=Silence (mute)
    {0x8CF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC1RMIX_Input_4_Volume(8CFH): 0080  DRC1RMIX_VOL4=0dB
    {0x8D0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2LMIX_Input_1_Source(8D0H): 0000  DRC2LMIX_STS1=0, DRC2LMIX_SRC1=Silence (mute)
    {0x8D1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2LMIX_Input_1_Volume(8D1H): 0080  DRC2LMIX_VOL1=0dB
    {0x8D2, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2LMIX_Input_2_Source(8D2H): 0000  DRC2LMIX_STS2=0, DRC2LMIX_SRC2=Silence (mute)
    {0x8D3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2LMIX_Input_2_Volume(8D3H): 0080  DRC2LMIX_VOL2=0dB
    {0x8D4, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2LMIX_Input_3_Source(8D4H): 0000  DRC2LMIX_STS3=0, DRC2LMIX_SRC3=Silence (mute)
    {0x8D5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2LMIX_Input_3_Volume(8D5H): 0080  DRC2LMIX_VOL3=0dB
    {0x8D6, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2LMIX_Input_4_Source(8D6H): 0000  DRC2LMIX_STS4=0, DRC2LMIX_SRC4=Silence (mute)
    {0x8D7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2LMIX_Input_4_Volume(8D7H): 0080  DRC2LMIX_VOL4=0dB
    {0x8D8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2RMIX_Input_1_Source(8D8H): 0000  DRC2RMIX_STS1=0, DRC2RMIX_SRC1=Silence (mute)
    {0x8D9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2RMIX_Input_1_Volume(8D9H): 0080  DRC2RMIX_VOL1=0dB
    {0x8DA, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2RMIX_Input_2_Source(8DAH): 0000  DRC2RMIX_STS2=0, DRC2RMIX_SRC2=Silence (mute)
    {0x8DB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2RMIX_Input_2_Volume(8DBH): 0080  DRC2RMIX_VOL2=0dB
    {0x8DC, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2RMIX_Input_3_Source(8DCH): 0000  DRC2RMIX_STS3=0, DRC2RMIX_SRC3=Silence (mute)
    {0x8DD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2RMIX_Input_3_Volume(8DDH): 0080  DRC2RMIX_VOL3=0dB
    {0x8DE, 0x0000, 0x00007fff, 0, 0x00007fff},       // DRC2RMIX_Input_4_Source(8DEH): 0000  DRC2RMIX_STS4=0, DRC2RMIX_SRC4=Silence (mute)
    {0x8DF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DRC2RMIX_Input_4_Volume(8DFH): 0080  DRC2RMIX_VOL4=0dB
    {0x900, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP1MIX_Input_1_Source(900H): 0000  LHPF1MIX_STS1=0, LHPF1MIX_SRC1=Silence (mute)
    {0x901, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP1MIX_Input_1_Volume(901H): 0080  LHPF1MIX_VOL1=0dB
    {0x902, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP1MIX_Input_2_Source(902H): 0000  LHPF1MIX_STS2=0, LHPF1MIX_SRC2=Silence (mute)
    {0x903, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP1MIX_Input_2_Volume(903H): 0080  LHPF1MIX_VOL2=0dB
    {0x904, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP1MIX_Input_3_Source(904H): 0000  LHPF1MIX_STS3=0, LHPF1MIX_SRC3=Silence (mute)
    {0x905, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP1MIX_Input_3_Volume(905H): 0080  LHPF1MIX_VOL3=0dB
    {0x906, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP1MIX_Input_4_Source(906H): 0000  LHPF1MIX_STS4=0, LHPF1MIX_SRC4=Silence (mute)
    {0x907, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP1MIX_Input_4_Volume(907H): 0080  LHPF1MIX_VOL4=0dB
    {0x908, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP2MIX_Input_1_Source(908H): 0000  LHPF2MIX_STS1=0, LHPF2MIX_SRC1=Silence (mute)
    {0x909, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP2MIX_Input_1_Volume(909H): 0080  LHPF2MIX_VOL1=0dB
    {0x90A, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP2MIX_Input_2_Source(90AH): 0000  LHPF2MIX_STS2=0, LHPF2MIX_SRC2=Silence (mute)
    {0x90B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP2MIX_Input_2_Volume(90BH): 0080  LHPF2MIX_VOL2=0dB
    {0x90C, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP2MIX_Input_3_Source(90CH): 0000  LHPF2MIX_STS3=0, LHPF2MIX_SRC3=Silence (mute)
    {0x90D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP2MIX_Input_3_Volume(90DH): 0080  LHPF2MIX_VOL3=0dB
    {0x90E, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP2MIX_Input_4_Source(90EH): 0000  LHPF2MIX_STS4=0, LHPF2MIX_SRC4=Silence (mute)
    {0x90F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP2MIX_Input_4_Volume(90FH): 0080  LHPF2MIX_VOL4=0dB
    {0x910, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP3MIX_Input_1_Source(910H): 0000  LHPF3MIX_STS1=0, LHPF3MIX_SRC1=Silence (mute)
    {0x911, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP3MIX_Input_1_Volume(911H): 0080  LHPF3MIX_VOL1=0dB
    {0x912, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP3MIX_Input_2_Source(912H): 0000  LHPF3MIX_STS2=0, LHPF3MIX_SRC2=Silence (mute)
    {0x913, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP3MIX_Input_2_Volume(913H): 0080  LHPF3MIX_VOL2=0dB
    {0x914, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP3MIX_Input_3_Source(914H): 0000  LHPF3MIX_STS3=0, LHPF3MIX_SRC3=Silence (mute)
    {0x915, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP3MIX_Input_3_Volume(915H): 0080  LHPF3MIX_VOL3=0dB
    {0x916, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP3MIX_Input_4_Source(916H): 0000  LHPF3MIX_STS4=0, LHPF3MIX_SRC4=Silence (mute)
    {0x917, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP3MIX_Input_4_Volume(917H): 0080  LHPF3MIX_VOL4=0dB
    {0x918, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP4MIX_Input_1_Source(918H): 0000  LHPF4MIX_STS1=0, LHPF4MIX_SRC1=Silence (mute)
    {0x919, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP4MIX_Input_1_Volume(919H): 0080  LHPF4MIX_VOL1=0dB
    {0x91A, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP4MIX_Input_2_Source(91AH): 0000  LHPF4MIX_STS2=0, LHPF4MIX_SRC2=Silence (mute)
    {0x91B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP4MIX_Input_2_Volume(91BH): 0080  LHPF4MIX_VOL2=0dB
    {0x91C, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP4MIX_Input_3_Source(91CH): 0000  LHPF4MIX_STS3=0, LHPF4MIX_SRC3=Silence (mute)
    {0x91D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP4MIX_Input_3_Volume(91DH): 0080  LHPF4MIX_VOL3=0dB
    {0x91E, 0x0000, 0x00007fff, 0, 0x00007fff},       // HPLP4MIX_Input_4_Source(91EH): 0000  LHPF4MIX_STS4=0, LHPF4MIX_SRC4=Silence (mute)
    {0x91F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // HPLP4MIX_Input_4_Volume(91FH): 0080  LHPF4MIX_VOL4=0dB
    {0x940, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1LMIX_Input_1_Source(940H): 0000  DSP1LMIX_STS1=0, DSP1LMIX_SRC1=Silence (mute)
    {0x941, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1LMIX_Input_1_Volume(941H): 0080  DSP1LMIX_VOL1=0dB
    {0x942, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1LMIX_Input_2_Source(942H): 0000  DSP1LMIX_STS2=0, DSP1LMIX_SRC2=Silence (mute)
    {0x943, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1LMIX_Input_2_Volume(943H): 0080  DSP1LMIX_VOL2=0dB
    {0x944, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1LMIX_Input_3_Source(944H): 0000  DSP1LMIX_STS3=0, DSP1LMIX_SRC3=Silence (mute)
    {0x945, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1LMIX_Input_3_Volume(945H): 0080  DSP1LMIX_VOL3=0dB
    {0x946, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1LMIX_Input_4_Source(946H): 0000  DSP1LMIX_STS4=0, DSP1LMIX_SRC4=Silence (mute)
    {0x947, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1LMIX_Input_4_Volume(947H): 0080  DSP1LMIX_VOL4=0dB
    {0x948, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1RMIX_Input_1_Source(948H): 0000  DSP1RMIX_STS1=0, DSP1RMIX_SRC1=Silence (mute)
    {0x949, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1RMIX_Input_1_Volume(949H): 0080  DSP1RMIX_VOL1=0dB
    {0x94A, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1RMIX_Input_2_Source(94AH): 0000  DSP1RMIX_STS2=0, DSP1RMIX_SRC2=Silence (mute)
    {0x94B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1RMIX_Input_2_Volume(94BH): 0080  DSP1RMIX_VOL2=0dB
    {0x94C, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1RMIX_Input_3_Source(94CH): 0000  DSP1RMIX_STS3=0, DSP1RMIX_SRC3=Silence (mute)
    {0x94D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1RMIX_Input_3_Volume(94DH): 0080  DSP1RMIX_VOL3=0dB
    {0x94E, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1RMIX_Input_4_Source(94EH): 0000  DSP1RMIX_STS4=0, DSP1RMIX_SRC4=Silence (mute)
    {0x94F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP1RMIX_Input_4_Volume(94FH): 0080  DSP1RMIX_VOL4=0dB
    {0x950, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1AUX1MIX_Input_1_Source(950H): 0000  DSP1AUX1_STS=0, DSP1AUX1_SRC=Silence (mute)
    {0x958, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1AUX2MIX_Input_1_Source(958H): 0000  DSP1AUX2_STS=0, DSP1AUX2_SRC=Silence (mute)
    {0x960, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1AUX3MIX_Input_1_Source(960H): 0000  DSP1AUX3_STS=0, DSP1AUX3_SRC=Silence (mute)
    {0x968, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1AUX4MIX_Input_1_Source(968H): 0000  DSP1AUX4_STS=0, DSP1AUX4_SRC=Silence (mute)
    {0x970, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1AUX5MIX_Input_1_Source(970H): 0000  DSP1AUX5_STS=0, DSP1AUX5_SRC=Silence (mute)
    {0x978, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP1AUX6MIX_Input_1_Source(978H): 0000  DSP1AUX6_STS=0, DSP1AUX6_SRC=Silence (mute)
    {0x980, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2LMIX_Input_1_Source(980H): 0000  DSP2LMIX_STS1=0, DSP2LMIX_SRC1=Silence (mute)
    {0x981, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2LMIX_Input_1_Volume(981H): 0080  DSP2LMIX_VOL1=0dB
    {0x982, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2LMIX_Input_2_Source(982H): 0000  DSP2LMIX_STS2=0, DSP2LMIX_SRC2=Silence (mute)
    {0x983, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2LMIX_Input_2_Volume(983H): 0080  DSP2LMIX_VOL2=0dB
    {0x984, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2LMIX_Input_3_Source(984H): 0000  DSP2LMIX_STS3=0, DSP2LMIX_SRC3=Silence (mute)
    {0x985, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2LMIX_Input_3_Volume(985H): 0080  DSP2LMIX_VOL3=0dB
    {0x986, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2LMIX_Input_4_Source(986H): 0000  DSP2LMIX_STS4=0, DSP2LMIX_SRC4=Silence (mute)
    {0x987, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2LMIX_Input_4_Volume(987H): 0080  DSP2LMIX_VOL4=0dB
    {0x988, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2RMIX_Input_1_Source(988H): 0000  DSP2RMIX_STS1=0, DSP2RMIX_SRC1=Silence (mute)
    {0x989, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2RMIX_Input_1_Volume(989H): 0080  DSP2RMIX_VOL1=0dB
    {0x98A, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2RMIX_Input_2_Source(98AH): 0000  DSP2RMIX_STS2=0, DSP2RMIX_SRC2=Silence (mute)
    {0x98B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2RMIX_Input_2_Volume(98BH): 0080  DSP2RMIX_VOL2=0dB
    {0x98C, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2RMIX_Input_3_Source(98CH): 0000  DSP2RMIX_STS3=0, DSP2RMIX_SRC3=Silence (mute)
    {0x98D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2RMIX_Input_3_Volume(98DH): 0080  DSP2RMIX_VOL3=0dB
    {0x98E, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2RMIX_Input_4_Source(98EH): 0000  DSP2RMIX_STS4=0, DSP2RMIX_SRC4=Silence (mute)
    {0x98F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP2RMIX_Input_4_Volume(98FH): 0080  DSP2RMIX_VOL4=0dB
    {0x990, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2AUX1MIX_Input_1_Source(990H): 0000  DSP2AUX1_STS=0, DSP2AUX1_SRC=Silence (mute)
    {0x998, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2AUX2MIX_Input_1_Source(998H): 0000  DSP2AUX2_STS=0, DSP2AUX2_SRC=Silence (mute)
    {0x9A0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2AUX3MIX_Input_1_Source(9A0H): 0000  DSP2AUX3_STS=0, DSP2AUX3_SRC=Silence (mute)
    {0x9A8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2AUX4MIX_Input_1_Source(9A8H): 0000  DSP2AUX4_STS=0, DSP2AUX4_SRC=Silence (mute)
    {0x9B0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2AUX5MIX_Input_1_Source(9B0H): 0000  DSP2AUX5_STS=0, DSP2AUX5_SRC=Silence (mute)
    {0x9B8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP2AUX6MIX_Input_1_Source(9B8H): 0000  DSP2AUX6_STS=0, DSP2AUX6_SRC=Silence (mute)
    {0x9C0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3LMIX_Input_1_Source(9C0H): 0000  DSP3LMIX_STS1=0, DSP3LMIX_SRC1=Silence (mute)
    {0x9C1, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3LMIX_Input_1_Volume(9C1H): 0080  DSP3LMIX_VOL1=0dB
    {0x9C2, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3LMIX_Input_2_Source(9C2H): 0000  DSP3LMIX_STS2=0, DSP3LMIX_SRC2=Silence (mute)
    {0x9C3, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3LMIX_Input_2_Volume(9C3H): 0080  DSP3LMIX_VOL2=0dB
    {0x9C4, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3LMIX_Input_3_Source(9C4H): 0000  DSP3LMIX_STS3=0, DSP3LMIX_SRC3=Silence (mute)
    {0x9C5, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3LMIX_Input_3_Volume(9C5H): 0080  DSP3LMIX_VOL3=0dB
    {0x9C6, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3LMIX_Input_4_Source(9C6H): 0000  DSP3LMIX_STS4=0, DSP3LMIX_SRC4=Silence (mute)
    {0x9C7, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3LMIX_Input_4_Volume(9C7H): 0080  DSP3LMIX_VOL4=0dB
    {0x9C8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3RMIX_Input_1_Source(9C8H): 0000  DSP3RMIX_STS1=0, DSP3RMIX_SRC1=Silence (mute)
    {0x9C9, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3RMIX_Input_1_Volume(9C9H): 0080  DSP3RMIX_VOL1=0dB
    {0x9CA, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3RMIX_Input_2_Source(9CAH): 0000  DSP3RMIX_STS2=0, DSP3RMIX_SRC2=Silence (mute)
    {0x9CB, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3RMIX_Input_2_Volume(9CBH): 0080  DSP3RMIX_VOL2=0dB
    {0x9CC, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3RMIX_Input_3_Source(9CCH): 0000  DSP3RMIX_STS3=0, DSP3RMIX_SRC3=Silence (mute)
    {0x9CD, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3RMIX_Input_3_Volume(9CDH): 0080  DSP3RMIX_VOL3=0dB
    {0x9CE, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3RMIX_Input_4_Source(9CEH): 0000  DSP3RMIX_STS4=0, DSP3RMIX_SRC4=Silence (mute)
    {0x9CF, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP3RMIX_Input_4_Volume(9CFH): 0080  DSP3RMIX_VOL4=0dB
    {0x9D0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3AUX1MIX_Input_1_Source(9D0H): 0000  DSP3AUX1_STS=0, DSP3AUX1_SRC=Silence (mute)
    {0x9D8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3AUX2MIX_Input_1_Source(9D8H): 0000  DSP3AUX2_STS=0, DSP3AUX2_SRC=Silence (mute)
    {0x9E0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3AUX3MIX_Input_1_Source(9E0H): 0000  DSP3AUX3_STS=0, DSP3AUX3_SRC=Silence (mute)
    {0x9E8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3AUX4MIX_Input_1_Source(9E8H): 0000  DSP3AUX4_STS=0, DSP3AUX4_SRC=Silence (mute)
    {0x9F0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3AUX5MIX_Input_1_Source(9F0H): 0000  DSP3AUX5_STS=0, DSP3AUX5_SRC=Silence (mute)
    {0x9F8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP3AUX6MIX_Input_1_Source(9F8H): 0000  DSP3AUX6_STS=0, DSP3AUX6_SRC=Silence (mute)
    {0xA00, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4LMIX_Input_1_Source(A00H): 0000  DSP4LMIX_STS1=0, DSP4LMIX_SRC1=Silence (mute)
    {0xA01, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4LMIX_Input_1_Volume(A01H): 0080  DSP4LMIX_VOL1=0dB
    {0xA02, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4LMIX_Input_2_Source(A02H): 0000  DSP4LMIX_STS2=0, DSP4LMIX_SRC2=Silence (mute)
    {0xA03, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4LMIX_Input_2_Volume(A03H): 0080  DSP4LMIX_VOL2=0dB
    {0xA04, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4LMIX_Input_3_Source(A04H): 0000  DSP4LMIX_STS3=0, DSP4LMIX_SRC3=Silence (mute)
    {0xA05, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4LMIX_Input_3_Volume(A05H): 0080  DSP4LMIX_VOL3=0dB
    {0xA06, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4LMIX_Input_4_Source(A06H): 0000  DSP4LMIX_STS4=0, DSP4LMIX_SRC4=Silence (mute)
    {0xA07, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4LMIX_Input_4_Volume(A07H): 0080  DSP4LMIX_VOL4=0dB
    {0xA08, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4RMIX_Input_1_Source(A08H): 0000  DSP4RMIX_STS1=0, DSP4RMIX_SRC1=Silence (mute)
    {0xA09, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4RMIX_Input_1_Volume(A09H): 0080  DSP4RMIX_VOL1=0dB
    {0xA0A, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4RMIX_Input_2_Source(A0AH): 0000  DSP4RMIX_STS2=0, DSP4RMIX_SRC2=Silence (mute)
    {0xA0B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4RMIX_Input_2_Volume(A0BH): 0080  DSP4RMIX_VOL2=0dB
    {0xA0C, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4RMIX_Input_3_Source(A0CH): 0000  DSP4RMIX_STS3=0, DSP4RMIX_SRC3=Silence (mute)
    {0xA0D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4RMIX_Input_3_Volume(A0DH): 0080  DSP4RMIX_VOL3=0dB
    {0xA0E, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4RMIX_Input_4_Source(A0EH): 0000  DSP4RMIX_STS4=0, DSP4RMIX_SRC4=Silence (mute)
    {0xA0F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP4RMIX_Input_4_Volume(A0FH): 0080  DSP4RMIX_VOL4=0dB
    {0xA10, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4AUX1MIX_Input_1_Source(A10H): 0000  DSP4AUX1_STS=0, DSP4AUX1_SRC=Silence (mute)
    {0xA18, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4AUX2MIX_Input_1_Source(A18H): 0000  DSP4AUX2_STS=0, DSP4AUX2_SRC=Silence (mute)
    {0xA20, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4AUX3MIX_Input_1_Source(A20H): 0000  DSP4AUX3_STS=0, DSP4AUX3_SRC=Silence (mute)
    {0xA28, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4AUX4MIX_Input_1_Source(A28H): 0000  DSP4AUX4_STS=0, DSP4AUX4_SRC=Silence (mute)
    {0xA30, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4AUX5MIX_Input_1_Source(A30H): 0000  DSP4AUX5_STS=0, DSP4AUX5_SRC=Silence (mute)
    {0xA38, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP4AUX6MIX_Input_1_Source(A38H): 0000  DSP4AUX6_STS=0, DSP4AUX6_SRC=Silence (mute)
    {0xA40, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5LMIX_Input_1_Source(A40H): 0000  DSP5LMIX_STS1=0, DSP5LMIX_SRC1=Silence (mute)
    {0xA41, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5LMIX_Input_1_Volume(A41H): 0080  DSP5LMIX_VOL1=0dB
    {0xA42, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5LMIX_Input_2_Source(A42H): 0000  DSP5LMIX_STS2=0, DSP5LMIX_SRC2=Silence (mute)
    {0xA43, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5LMIX_Input_2_Volume(A43H): 0080  DSP5LMIX_VOL2=0dB
    {0xA44, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5LMIX_Input_3_Source(A44H): 0000  DSP5LMIX_STS3=0, DSP5LMIX_SRC3=Silence (mute)
    {0xA45, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5LMIX_Input_3_Volume(A45H): 0080  DSP5LMIX_VOL3=0dB
    {0xA46, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5LMIX_Input_4_Source(A46H): 0000  DSP5LMIX_STS4=0, DSP5LMIX_SRC4=Silence (mute)
    {0xA47, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5LMIX_Input_4_Volume(A47H): 0080  DSP5LMIX_VOL4=0dB
    {0xA48, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5RMIX_Input_1_Source(A48H): 0000  DSP5RMIX_STS1=0, DSP5RMIX_SRC1=Silence (mute)
    {0xA49, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5RMIX_Input_1_Volume(A49H): 0080  DSP5RMIX_VOL1=0dB
    {0xA4A, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5RMIX_Input_2_Source(A4AH): 0000  DSP5RMIX_STS2=0, DSP5RMIX_SRC2=Silence (mute)
    {0xA4B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5RMIX_Input_2_Volume(A4BH): 0080  DSP5RMIX_VOL2=0dB
    {0xA4C, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5RMIX_Input_3_Source(A4CH): 0000  DSP5RMIX_STS3=0, DSP5RMIX_SRC3=Silence (mute)
    {0xA4D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5RMIX_Input_3_Volume(A4DH): 0080  DSP5RMIX_VOL3=0dB
    {0xA4E, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5RMIX_Input_4_Source(A4EH): 0000  DSP5RMIX_STS4=0, DSP5RMIX_SRC4=Silence (mute)
    {0xA4F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP5RMIX_Input_4_Volume(A4FH): 0080  DSP5RMIX_VOL4=0dB
    {0xA50, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5AUX1MIX_Input_1_Source(A50H): 0000  DSP5AUX1_STS=0, DSP5AUX1_SRC=Silence (mute)
    {0xA58, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5AUX2MIX_Input_1_Source(A58H): 0000  DSP5AUX2_STS=0, DSP5AUX2_SRC=Silence (mute)
    {0xA60, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5AUX3MIX_Input_1_Source(A60H): 0000  DSP5AUX3_STS=0, DSP5AUX3_SRC=Silence (mute)
    {0xA68, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5AUX4MIX_Input_1_Source(A68H): 0000  DSP5AUX4_STS=0, DSP5AUX4_SRC=Silence (mute)
    {0xA70, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5AUX5MIX_Input_1_Source(A70H): 0000  DSP5AUX5_STS=0, DSP5AUX5_SRC=Silence (mute)
    {0xA78, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP5AUX6MIX_Input_1_Source(A78H): 0000  DSP5AUX6_STS=0, DSP5AUX6_SRC=Silence (mute)
    {0xA80, 0x0000, 0x00007fff, 0, 0x00007fff},       // ASRC1_1LMIX_Input_1_Source(A80H): 0000  ASRC1_IN1L_STS=0, ASRC1_IN1L_SRC=Silence (mute)
    {0xA88, 0x0000, 0x00007fff, 0, 0x00007fff},       // ASRC1_1RMIX_Input_1_Source(A88H): 0000  ASRC1_IN1R_STS=0, ASRC1_IN1R_SRC=Silence (mute)
    {0xA90, 0x8034, 0x00007fff, 0, 0x00007fff},       // ASRC1_2LMIX_Input_1_Source(A90H): 8034  ASRC1_IN2L_STS=1, ASRC1_IN2L_SRC=AIF4 RX1
    {0xA98, 0x8035, 0x00007fff, 0, 0x00007fff},       // ASRC1_2RMIX_Input_1_Source(A98H): 8035  ASRC1_IN2R_STS=1, ASRC1_IN2R_SRC=AIF4 RX2
    {0xAA0, 0x0000, 0x00007fff, 0, 0x00007fff},       // ASRC2_1LMIX_Input_1_Source(AA0H): 0000  ASRC2_IN1L_STS=0, ASRC2_IN1L_SRC=Silence (mute)
    {0xAA8, 0x0000, 0x00007fff, 0, 0x00007fff},       // ASRC2_1RMIX_Input_1_Source(AA8H): 0000  ASRC2_IN1R_STS=0, ASRC2_IN1R_SRC=Silence (mute)
    {0xAB0, 0x0000, 0x00007fff, 0, 0x00007fff},       // ASRC2_2LMIX_Input_1_Source(AB0H): 0000  ASRC2_IN2L_STS=0, ASRC2_IN2L_SRC=Silence (mute)
    {0xAB8, 0x0000, 0x00007fff, 0, 0x00007fff},       // ASRC2_2RMIX_Input_1_Source(AB8H): 0000  ASRC2_IN2R_STS=0, ASRC2_IN2R_SRC=Silence (mute)
    {0xB00, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1DEC1MIX_Input_1_Source(B00H): 0000  ISRC1DEC1_STS=0, ISRC1DEC1_SRC=Silence (mute)
    {0xB08, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1DEC2MIX_Input_1_Source(B08H): 0000  ISRC1DEC2_STS=0, ISRC1DEC2_SRC=Silence (mute)
    {0xB10, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1DEC3MIX_Input_1_Source(B10H): 0000  ISRC1DEC3_STS=0, ISRC1DEC3_SRC=Silence (mute)
    {0xB18, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1DEC4MIX_Input_1_Source(B18H): 0000  ISRC1DEC4_STS=0, ISRC1DEC4_SRC=Silence (mute)
    {0xB20, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1INT1MIX_Input_1_Source(B20H): 0000  ISRC1INT1_STS=0, ISRC1INT1_SRC=Silence (mute)
    {0xB28, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1INT2MIX_Input_1_Source(B28H): 0000  ISRC1INT2_STS=0, ISRC1INT2_SRC=Silence (mute)
    {0xB30, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1INT3MIX_Input_1_Source(B30H): 0000  ISRC1INT3_STS=0, ISRC1INT3_SRC=Silence (mute)
    {0xB38, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC1INT4MIX_Input_1_Source(B38H): 0000  ISRC1INT4_STS=0, ISRC1INT4_SRC=Silence (mute)
    {0xB40, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2DEC1MIX_Input_1_Source(B40H): 0000  ISRC2DEC1_STS=0, ISRC2DEC1_SRC=Silence (mute)
    {0xB48, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2DEC2MIX_Input_1_Source(B48H): 0000  ISRC2DEC2_STS=0, ISRC2DEC2_SRC=Silence (mute)
    {0xB50, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2DEC3MIX_Input_1_Source(B50H): 0000  ISRC2DEC3_STS=0, ISRC2DEC3_SRC=Silence (mute)
    {0xB58, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2DEC4MIX_Input_1_Source(B58H): 0000  ISRC2DEC4_STS=0, ISRC2DEC4_SRC=Silence (mute)
    {0xB60, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2INT1MIX_Input_1_Source(B60H): 0000  ISRC2INT1_STS=0, ISRC2INT1_SRC=Silence (mute)
    {0xB68, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2INT2MIX_Input_1_Source(B68H): 0000  ISRC2INT2_STS=0, ISRC2INT2_SRC=Silence (mute)
    {0xB70, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2INT3MIX_Input_1_Source(B70H): 0000  ISRC2INT3_STS=0, ISRC2INT3_SRC=Silence (mute)
    {0xB78, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC2INT4MIX_Input_1_Source(B78H): 0000  ISRC2INT4_STS=0, ISRC2INT4_SRC=Silence (mute)
    {0xB80, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC3DEC1MIX_Input_1_Source(B80H): 0000  ISRC3DEC1_STS=0, ISRC3DEC1_SRC=Silence (mute)
    {0xB88, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC3DEC2MIX_Input_1_Source(B88H): 0000  ISRC3DEC2_STS=0, ISRC3DEC2_SRC=Silence (mute)
    {0xBA0, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC3INT1MIX_Input_1_Source(BA0H): 0000  ISRC3INT1_STS=0, ISRC3INT1_SRC=Silence (mute)
    {0xBA8, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC3INT2MIX_Input_1_Source(BA8H): 0000  ISRC3INT2_STS=0, ISRC3INT2_SRC=Silence (mute)
    {0xBC0, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC4DEC1MIX_Input_1_Source(BC0H): 0000  ISRC4DEC1_STS=0, ISRC4DEC1_SRC=Silence (mute)
    {0xBC8, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC4DEC2MIX_Input_1_Source(BC8H): 0000  ISRC4DEC2_STS=0, ISRC4DEC2_SRC=Silence (mute)
    {0xBE0, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC4INT1MIX_Input_1_Source(BE0H): 0000  ISRC4INT1_STS=0, ISRC4INT1_SRC=Silence (mute)
    {0xBE8, 0x0000, 0x00007fff, 0, 0x00007fff},       // ISRC4INT2MIX_Input_1_Source(BE8H): 0000  ISRC4INT2_STS=0, ISRC4INT2_SRC=Silence (mute)
    {0xC00, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6LMIX_Input_1_Source(C00H): 0000  DSP6LMIX_STS1=0, DSP6LMIX_SRC1=Silence (mute)
    {0xC01, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6LMIX_Input_1_Volume(C01H): 0080  DSP6LMIX_VOL1=0dB
    {0xC02, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6LMIX_Input_2_Source(C02H): 0000  DSP6LMIX_STS2=0, DSP6LMIX_SRC2=Silence (mute)
    {0xC03, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6LMIX_Input_2_Volume(C03H): 0080  DSP6LMIX_VOL2=0dB
    {0xC04, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6LMIX_Input_3_Source(C04H): 0000  DSP6LMIX_STS3=0, DSP6LMIX_SRC3=Silence (mute)
    {0xC05, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6LMIX_Input_3_Volume(C05H): 0080  DSP6LMIX_VOL3=0dB
    {0xC06, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6LMIX_Input_4_Source(C06H): 0000  DSP6LMIX_STS4=0, DSP6LMIX_SRC4=Silence (mute)
    {0xC07, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6LMIX_Input_4_Volume(C07H): 0080  DSP6LMIX_VOL4=0dB
    {0xC08, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6RMIX_Input_1_Source(C08H): 0000  DSP6RMIX_STS1=0, DSP6RMIX_SRC1=Silence (mute)
    {0xC09, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6RMIX_Input_1_Volume(C09H): 0080  DSP6RMIX_VOL1=0dB
    {0xC0A, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6RMIX_Input_2_Source(C0AH): 0000  DSP6RMIX_STS2=0, DSP6RMIX_SRC2=Silence (mute)
    {0xC0B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6RMIX_Input_2_Volume(C0BH): 0080  DSP6RMIX_VOL2=0dB
    {0xC0C, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6RMIX_Input_3_Source(C0CH): 0000  DSP6RMIX_STS3=0, DSP6RMIX_SRC3=Silence (mute)
    {0xC0D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6RMIX_Input_3_Volume(C0DH): 0080  DSP6RMIX_VOL3=0dB
    {0xC0E, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6RMIX_Input_4_Source(C0EH): 0000  DSP6RMIX_STS4=0, DSP6RMIX_SRC4=Silence (mute)
    {0xC0F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP6RMIX_Input_4_Volume(C0FH): 0080  DSP6RMIX_VOL4=0dB
    {0xC10, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6AUX1MIX_Input_1_Source(C10H): 0000  DSP6AUX1_STS=0, DSP6AUX1_SRC=Silence (mute)
    {0xC18, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6AUX2MIX_Input_1_Source(C18H): 0000  DSP6AUX2_STS=0, DSP6AUX2_SRC=Silence (mute)
    {0xC20, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6AUX3MIX_Input_1_Source(C20H): 0000  DSP6AUX3_STS=0, DSP6AUX3_SRC=Silence (mute)
    {0xC28, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6AUX4MIX_Input_1_Source(C28H): 0000  DSP6AUX4_STS=0, DSP6AUX4_SRC=Silence (mute)
    {0xC30, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6AUX5MIX_Input_1_Source(C30H): 0000  DSP6AUX5_STS=0, DSP6AUX5_SRC=Silence (mute)
    {0xC38, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP6AUX6MIX_Input_1_Source(C38H): 0000  DSP6AUX6_STS=0, DSP6AUX6_SRC=Silence (mute)
    {0xC40, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7LMIX_Input_1_Source(C40H): 0000  DSP7LMIX_STS1=0, DSP7LMIX_SRC1=Silence (mute)
    {0xC41, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7LMIX_Input_1_Volume(C41H): 0080  DSP7LMIX_VOL1=0dB
    {0xC42, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7LMIX_Input_2_Source(C42H): 0000  DSP7LMIX_STS2=0, DSP7LMIX_SRC2=Silence (mute)
    {0xC43, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7LMIX_Input_2_Volume(C43H): 0080  DSP7LMIX_VOL2=0dB
    {0xC44, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7LMIX_Input_3_Source(C44H): 0000  DSP7LMIX_STS3=0, DSP7LMIX_SRC3=Silence (mute)
    {0xC45, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7LMIX_Input_3_Volume(C45H): 0080  DSP7LMIX_VOL3=0dB
    {0xC46, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7LMIX_Input_4_Source(C46H): 0000  DSP7LMIX_STS4=0, DSP7LMIX_SRC4=Silence (mute)
    {0xC47, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7LMIX_Input_4_Volume(C47H): 0080  DSP7LMIX_VOL4=0dB
    {0xC48, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7RMIX_Input_1_Source(C48H): 0000  DSP7RMIX_STS1=0, DSP7RMIX_SRC1=Silence (mute)
    {0xC49, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7RMIX_Input_1_Volume(C49H): 0080  DSP7RMIX_VOL1=0dB
    {0xC4A, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7RMIX_Input_2_Source(C4AH): 0000  DSP7RMIX_STS2=0, DSP7RMIX_SRC2=Silence (mute)
    {0xC4B, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7RMIX_Input_2_Volume(C4BH): 0080  DSP7RMIX_VOL2=0dB
    {0xC4C, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7RMIX_Input_3_Source(C4CH): 0000  DSP7RMIX_STS3=0, DSP7RMIX_SRC3=Silence (mute)
    {0xC4D, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7RMIX_Input_3_Volume(C4DH): 0080  DSP7RMIX_VOL3=0dB
    {0xC4E, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7RMIX_Input_4_Source(C4EH): 0000  DSP7RMIX_STS4=0, DSP7RMIX_SRC4=Silence (mute)
    {0xC4F, 0x0080, 0x0000ffff, 0, 0x0000ffff},       // DSP7RMIX_Input_4_Volume(C4FH): 0080  DSP7RMIX_VOL4=0dB
    {0xC50, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7AUX1MIX_Input_1_Source(C50H): 0000  DSP7AUX1_STS=0, DSP7AUX1_SRC=Silence (mute)
    {0xC58, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7AUX2MIX_Input_1_Source(C58H): 0000  DSP7AUX2_STS=0, DSP7AUX2_SRC=Silence (mute)
    {0xC60, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7AUX3MIX_Input_1_Source(C60H): 0000  DSP7AUX3_STS=0, DSP7AUX3_SRC=Silence (mute)
    {0xC68, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7AUX4MIX_Input_1_Source(C68H): 0000  DSP7AUX4_STS=0, DSP7AUX4_SRC=Silence (mute)
    {0xC70, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7AUX5MIX_Input_1_Source(C70H): 0000  DSP7AUX5_STS=0, DSP7AUX5_SRC=Silence (mute)
    {0xC78, 0x0000, 0x00007fff, 0, 0x00007fff},       // DSP7AUX6MIX_Input_1_Source(C78H): 0000  DSP7AUX6_STS=0, DSP7AUX6_SRC=Silence (mute)
    {0xDC0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC1MIX_Input_1_Source(DC0H): 0000  DFC1_STS=0, DFC1_SRC=Silence (mute)
    {0xDC8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC2MIX_Input_1_Source(DC8H): 0000  DFC2_STS=0, DFC2_SRC=Silence (mute)
    {0xDD0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC3MIX_Input_1_Source(DD0H): 0000  DFC3_STS=0, DFC3_SRC=Silence (mute)
    {0xDD8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC4MIX_Input_1_Source(DD8H): 0000  DFC4_STS=0, DFC4_SRC=Silence (mute)
    {0xDE0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC5MIX_Input_1_Source(DE0H): 0000  DFC5_STS=0, DFC5_SRC=Silence (mute)
    {0xDE8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC6MIX_Input_1_Source(DE8H): 0000  DFC6_STS=0, DFC6_SRC=Silence (mute)
    {0xDF0, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC7MIX_Input_1_Source(DF0H): 0000  DFC7_STS=0, DFC7_SRC=Silence (mute)
    {0xDF8, 0x0000, 0x00007fff, 0, 0x00007fff},       // DFC8MIX_Input_1_Source(DF8H): 0000  DFC8_STS=0, DFC8_SRC=Silence (mute)
    {0xE00, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FX_Ctrl1(E00H):          0000  FX_RATE=SAMPLE_RATE_1
    {0xE01, 0x0002, 0x0000000f, 0, 0x0000f000},       // FX_Ctrl2(E01H):          0002  FX_STS=0000_0000_0000
    {0xE10, 0x6318, 0x0000ffff, 0, 0x0000ffff},       // EQ1_1(E10H):             6318  EQ1_B1_GAIN=0_1100, EQ1_B2_GAIN=0_1100, EQ1_B3_GAIN=0_1100, EQ1_ENA=0
    {0xE11, 0x6300, 0x0000ffff, 0, 0x0000ffff},       // EQ1_2(E11H):             6300  EQ1_B4_GAIN=0_1100, EQ1_B5_GAIN=0_1100, EQ1_B1_MODE=0
    {0xE12, 0x0FC8, 0x0000ffff, 0, 0x0000ffff},       // EQ1_3(E12H):             0FC8  EQ1_B1_A=0000_1111_1100_1000
    {0xE13, 0x03FE, 0x0000ffff, 0, 0x0000ffff},       // EQ1_4(E13H):             03FE  EQ1_B1_B=0000_0011_1111_1110
    {0xE14, 0x00E0, 0x0000ffff, 0, 0x0000ffff},       // EQ1_5(E14H):             00E0  EQ1_B1_PG=0000_0000_1110_0000
    {0xE15, 0x1EC4, 0x0000ffff, 0, 0x0000ffff},       // EQ1_6(E15H):             1EC4  EQ1_B2_A=0001_1110_1100_0100
    {0xE16, 0xF136, 0x0000ffff, 0, 0x0000ffff},       // EQ1_7(E16H):             F136  EQ1_B2_B=1111_0001_0011_0110
    {0xE17, 0x0409, 0x0000ffff, 0, 0x0000ffff},       // EQ1_8(E17H):             0409  EQ1_B2_C=0000_0100_0000_1001
    {0xE18, 0x04CC, 0x0000ffff, 0, 0x0000ffff},       // EQ1_9(E18H):             04CC  EQ1_B2_PG=0000_0100_1100_1100
    {0xE19, 0x1C9B, 0x0000ffff, 0, 0x0000ffff},       // EQ1_10(E19H):            1C9B  EQ1_B3_A=0001_1100_1001_1011
    {0xE1A, 0xF337, 0x0000ffff, 0, 0x0000ffff},       // EQ1_11(E1AH):            F337  EQ1_B3_B=1111_0011_0011_0111
    {0xE1B, 0x040B, 0x0000ffff, 0, 0x0000ffff},       // EQ1_12(E1BH):            040B  EQ1_B3_C=0000_0100_0000_1011
    {0xE1C, 0x0CBB, 0x0000ffff, 0, 0x0000ffff},       // EQ1_13(E1CH):            0CBB  EQ1_B3_PG=0000_1100_1011_1011
    {0xE1D, 0x16F8, 0x0000ffff, 0, 0x0000ffff},       // EQ1_14(E1DH):            16F8  EQ1_B4_A=0001_0110_1111_1000
    {0xE1E, 0xF7D9, 0x0000ffff, 0, 0x0000ffff},       // EQ1_15(E1EH):            F7D9  EQ1_B4_B=1111_0111_1101_1001
    {0xE1F, 0x040A, 0x0000ffff, 0, 0x0000ffff},       // EQ1_16(E1FH):            040A  EQ1_B4_C=0000_0100_0000_1010
    {0xE20, 0x1F14, 0x0000ffff, 0, 0x0000ffff},       // EQ1_17(E20H):            1F14  EQ1_B4_PG=0001_1111_0001_0100
    {0xE21, 0x058C, 0x0000ffff, 0, 0x0000ffff},       // EQ1_18(E21H):            058C  EQ1_B5_A=0000_0101_1000_1100
    {0xE22, 0x0563, 0x0000ffff, 0, 0x0000ffff},       // EQ1_19(E22H):            0563  EQ1_B5_B=0000_0101_0110_0011
    {0xE23, 0x4000, 0x0000ffff, 0, 0x0000ffff},       // EQ1_20(E23H):            4000  EQ1_B5_PG=0100_0000_0000_0000
    {0xE24, 0x0B75, 0x0000ffff, 0, 0x0000ffff},       // EQ1_21(E24H):            0B75  EQ1_B1_C=0000_1011_0111_0101
    {0xE26, 0x6318, 0x0000ffff, 0, 0x0000ffff},       // EQ2_1(E26H):             6318  EQ2_B1_GAIN=0_1100, EQ2_B2_GAIN=0_1100, EQ2_B3_GAIN=0_1100, EQ2_ENA=0
    {0xE27, 0x6300, 0x0000ffff, 0, 0x0000ffff},       // EQ2_2(E27H):             6300  EQ2_B4_GAIN=0_1100, EQ2_B5_GAIN=0_1100, EQ2_B1_MODE=0
    {0xE28, 0x0FC8, 0x0000ffff, 0, 0x0000ffff},       // EQ2_3(E28H):             0FC8  EQ2_B1_A=0000_1111_1100_1000
    {0xE29, 0x03FE, 0x0000ffff, 0, 0x0000ffff},       // EQ2_4(E29H):             03FE  EQ2_B1_B=0000_0011_1111_1110
    {0xE2A, 0x00E0, 0x0000ffff, 0, 0x0000ffff},       // EQ2_5(E2AH):             00E0  EQ2_B1_PG=0000_0000_1110_0000
    {0xE2B, 0x1EC4, 0x0000ffff, 0, 0x0000ffff},       // EQ2_6(E2BH):             1EC4  EQ2_B2_A=0001_1110_1100_0100
    {0xE2C, 0xF136, 0x0000ffff, 0, 0x0000ffff},       // EQ2_7(E2CH):             F136  EQ2_B2_B=1111_0001_0011_0110
    {0xE2D, 0x0409, 0x0000ffff, 0, 0x0000ffff},       // EQ2_8(E2DH):             0409  EQ2_B2_C=0000_0100_0000_1001
    {0xE2E, 0x04CC, 0x0000ffff, 0, 0x0000ffff},       // EQ2_9(E2EH):             04CC  EQ2_B2_PG=0000_0100_1100_1100
    {0xE2F, 0x1C9B, 0x0000ffff, 0, 0x0000ffff},       // EQ2_10(E2FH):            1C9B  EQ2_B3_A=0001_1100_1001_1011
    {0xE30, 0xF337, 0x0000ffff, 0, 0x0000ffff},       // EQ2_11(E30H):            F337  EQ2_B3_B=1111_0011_0011_0111
    {0xE31, 0x040B, 0x0000ffff, 0, 0x0000ffff},       // EQ2_12(E31H):            040B  EQ2_B3_C=0000_0100_0000_1011
    {0xE32, 0x0CBB, 0x0000ffff, 0, 0x0000ffff},       // EQ2_13(E32H):            0CBB  EQ2_B3_PG=0000_1100_1011_1011
    {0xE33, 0x16F8, 0x0000ffff, 0, 0x0000ffff},       // EQ2_14(E33H):            16F8  EQ2_B4_A=0001_0110_1111_1000
    {0xE34, 0xF7D9, 0x0000ffff, 0, 0x0000ffff},       // EQ2_15(E34H):            F7D9  EQ2_B4_B=1111_0111_1101_1001
    {0xE35, 0x040A, 0x0000ffff, 0, 0x0000ffff},       // EQ2_16(E35H):            040A  EQ2_B4_C=0000_0100_0000_1010
    {0xE36, 0x1F14, 0x0000ffff, 0, 0x0000ffff},       // EQ2_17(E36H):            1F14  EQ2_B4_PG=0001_1111_0001_0100
    {0xE37, 0x058C, 0x0000ffff, 0, 0x0000ffff},       // EQ2_18(E37H):            058C  EQ2_B5_A=0000_0101_1000_1100
    {0xE38, 0x0563, 0x0000ffff, 0, 0x0000ffff},       // EQ2_19(E38H):            0563  EQ2_B5_B=0000_0101_0110_0011
    {0xE39, 0x4000, 0x0000ffff, 0, 0x0000ffff},       // EQ2_20(E39H):            4000  EQ2_B5_PG=0100_0000_0000_0000
    {0xE3A, 0x0B75, 0x0000ffff, 0, 0x0000ffff},       // EQ2_21(E3AH):            0B75  EQ2_B1_C=0000_1011_0111_0101
    {0xE3C, 0x6318, 0x0000ffff, 0, 0x0000ffff},       // EQ3_1(E3CH):             6318  EQ3_B1_GAIN=0_1100, EQ3_B2_GAIN=0_1100, EQ3_B3_GAIN=0_1100, EQ3_ENA=0
    {0xE3D, 0x6300, 0x0000ffff, 0, 0x0000ffff},       // EQ3_2(E3DH):             6300  EQ3_B4_GAIN=0_1100, EQ3_B5_GAIN=0_1100, EQ3_B1_MODE=0
    {0xE3E, 0x0FC8, 0x0000ffff, 0, 0x0000ffff},       // EQ3_3(E3EH):             0FC8  EQ3_B1_A=0000_1111_1100_1000
    {0xE3F, 0x03FE, 0x0000ffff, 0, 0x0000ffff},       // EQ3_4(E3FH):             03FE  EQ3_B1_B=0000_0011_1111_1110
    {0xE40, 0x00E0, 0x0000ffff, 0, 0x0000ffff},       // EQ3_5(E40H):             00E0  EQ3_B1_PG=0000_0000_1110_0000
    {0xE41, 0x1EC4, 0x0000ffff, 0, 0x0000ffff},       // EQ3_6(E41H):             1EC4  EQ3_B2_A=0001_1110_1100_0100
    {0xE42, 0xF136, 0x0000ffff, 0, 0x0000ffff},       // EQ3_7(E42H):             F136  EQ3_B2_B=1111_0001_0011_0110
    {0xE43, 0x0409, 0x0000ffff, 0, 0x0000ffff},       // EQ3_8(E43H):             0409  EQ3_B2_C=0000_0100_0000_1001
    {0xE44, 0x04CC, 0x0000ffff, 0, 0x0000ffff},       // EQ3_9(E44H):             04CC  EQ3_B2_PG=0000_0100_1100_1100
    {0xE45, 0x1C9B, 0x0000ffff, 0, 0x0000ffff},       // EQ3_10(E45H):            1C9B  EQ3_B3_A=0001_1100_1001_1011
    {0xE46, 0xF337, 0x0000ffff, 0, 0x0000ffff},       // EQ3_11(E46H):            F337  EQ3_B3_B=1111_0011_0011_0111
    {0xE47, 0x040B, 0x0000ffff, 0, 0x0000ffff},       // EQ3_12(E47H):            040B  EQ3_B3_C=0000_0100_0000_1011
    {0xE48, 0x0CBB, 0x0000ffff, 0, 0x0000ffff},       // EQ3_13(E48H):            0CBB  EQ3_B3_PG=0000_1100_1011_1011
    {0xE49, 0x16F8, 0x0000ffff, 0, 0x0000ffff},       // EQ3_14(E49H):            16F8  EQ3_B4_A=0001_0110_1111_1000
    {0xE4A, 0xF7D9, 0x0000ffff, 0, 0x0000ffff},       // EQ3_15(E4AH):            F7D9  EQ3_B4_B=1111_0111_1101_1001
    {0xE4B, 0x040A, 0x0000ffff, 0, 0x0000ffff},       // EQ3_16(E4BH):            040A  EQ3_B4_C=0000_0100_0000_1010
    {0xE4C, 0x1F14, 0x0000ffff, 0, 0x0000ffff},       // EQ3_17(E4CH):            1F14  EQ3_B4_PG=0001_1111_0001_0100
    {0xE4D, 0x058C, 0x0000ffff, 0, 0x0000ffff},       // EQ3_18(E4DH):            058C  EQ3_B5_A=0000_0101_1000_1100
    {0xE4E, 0x0563, 0x0000ffff, 0, 0x0000ffff},       // EQ3_19(E4EH):            0563  EQ3_B5_B=0000_0101_0110_0011
    {0xE4F, 0x4000, 0x0000ffff, 0, 0x0000ffff},       // EQ3_20(E4FH):            4000  EQ3_B5_PG=0100_0000_0000_0000
    {0xE50, 0x0B75, 0x0000ffff, 0, 0x0000ffff},       // EQ3_21(E50H):            0B75  EQ3_B1_C=0000_1011_0111_0101
    {0xE52, 0x6318, 0x0000ffff, 0, 0x0000ffff},       // EQ4_1(E52H):             6318  EQ4_B1_GAIN=0_1100, EQ4_B2_GAIN=0_1100, EQ4_B3_GAIN=0_1100, EQ4_ENA=0
    {0xE53, 0x6300, 0x0000ffff, 0, 0x0000ffff},       // EQ4_2(E53H):             6300  EQ4_B4_GAIN=0_1100, EQ4_B5_GAIN=0_1100, EQ4_B1_MODE=0
    {0xE54, 0x0FC8, 0x0000ffff, 0, 0x0000ffff},       // EQ4_3(E54H):             0FC8  EQ4_B1_A=0000_1111_1100_1000
    {0xE55, 0x03FE, 0x0000ffff, 0, 0x0000ffff},       // EQ4_4(E55H):             03FE  EQ4_B1_B=0000_0011_1111_1110
    {0xE56, 0x00E0, 0x0000ffff, 0, 0x0000ffff},       // EQ4_5(E56H):             00E0  EQ4_B1_PG=0000_0000_1110_0000
    {0xE57, 0x1EC4, 0x0000ffff, 0, 0x0000ffff},       // EQ4_6(E57H):             1EC4  EQ4_B2_A=0001_1110_1100_0100
    {0xE58, 0xF136, 0x0000ffff, 0, 0x0000ffff},       // EQ4_7(E58H):             F136  EQ4_B2_B=1111_0001_0011_0110
    {0xE59, 0x0409, 0x0000ffff, 0, 0x0000ffff},       // EQ4_8(E59H):             0409  EQ4_B2_C=0000_0100_0000_1001
    {0xE5A, 0x04CC, 0x0000ffff, 0, 0x0000ffff},       // EQ4_9(E5AH):             04CC  EQ4_B2_PG=0000_0100_1100_1100
    {0xE5B, 0x1C9B, 0x0000ffff, 0, 0x0000ffff},       // EQ4_10(E5BH):            1C9B  EQ4_B3_A=0001_1100_1001_1011
    {0xE5C, 0xF337, 0x0000ffff, 0, 0x0000ffff},       // EQ4_11(E5CH):            F337  EQ4_B3_B=1111_0011_0011_0111
    {0xE5D, 0x040B, 0x0000ffff, 0, 0x0000ffff},       // EQ4_12(E5DH):            040B  EQ4_B3_C=0000_0100_0000_1011
    {0xE5E, 0x0CBB, 0x0000ffff, 0, 0x0000ffff},       // EQ4_13(E5EH):            0CBB  EQ4_B3_PG=0000_1100_1011_1011
    {0xE5F, 0x16F8, 0x0000ffff, 0, 0x0000ffff},       // EQ4_14(E5FH):            16F8  EQ4_B4_A=0001_0110_1111_1000
    {0xE60, 0xF7D9, 0x0000ffff, 0, 0x0000ffff},       // EQ4_15(E60H):            F7D9  EQ4_B4_B=1111_0111_1101_1001
    {0xE61, 0x040A, 0x0000ffff, 0, 0x0000ffff},       // EQ4_16(E61H):            040A  EQ4_B4_C=0000_0100_0000_1010
    {0xE62, 0x1F14, 0x0000ffff, 0, 0x0000ffff},       // EQ4_17(E62H):            1F14  EQ4_B4_PG=0001_1111_0001_0100
    {0xE63, 0x058C, 0x0000ffff, 0, 0x0000ffff},       // EQ4_18(E63H):            058C  EQ4_B5_A=0000_0101_1000_1100
    {0xE64, 0x0563, 0x0000ffff, 0, 0x0000ffff},       // EQ4_19(E64H):            0563  EQ4_B5_B=0000_0101_0110_0011
    {0xE65, 0x4000, 0x0000ffff, 0, 0x0000ffff},       // EQ4_20(E65H):            4000  EQ4_B5_PG=0100_0000_0000_0000
    {0xE66, 0x0B75, 0x0000ffff, 0, 0x0000ffff},       // EQ4_21(E66H):            0B75  EQ4_B1_C=0000_1011_0111_0101
    {0xE80, 0x0018, 0x0000ffff, 0, 0x0000ffff},       // DRC1_ctrl1(E80H):        0018  DRC1_SIG_DET_RMS=0_0000, DRC1_SIG_DET_PK=00, DRC1_NG_ENA=0, DRC1_SIG_DET_MODE=0, DRC1_SIG_DET=0, DRC1_KNEE2_OP_ENA=0, DRC1_QR=1, DRC1_ANTICLIP=1, DRC1_WSEQ_SIG_DET_ENA=0, DRC1L_ENA=0, DRC1R_ENA=0
    {0xE81, 0x0933, 0x0000ffff, 0, 0x0000ffff},       // DRC1_ctrl2(E81H):        0933  DRC1_ATK=0100, DRC1_DCY=1001, DRC1_MINGAIN=100, DRC1_MAXGAIN=11
    {0xE82, 0x0018, 0x0000ffff, 0, 0x0000ffff},       // DRC1_ctrl3(E82H):        0018  DRC1_NG_MINGAIN=0000, DRC1_NG_EXP=00, DRC1_QR_THR=00, DRC1_QR_DCY=00, DRC1_HI_COMP=011, DRC1_LO_COMP=000
    {0xE83, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DRC1_ctrl4(E83H):        0000  DRC1_KNEE_IP=00_0000, DRC1_KNEE_OP=0_0000
    {0xE84, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DRC1_ctrl5(E84H):        0000  DRC1_KNEE2_IP=0_0000, DRC1_KNEE2_OP=0_0000
    {0xE88, 0x0018, 0x0000ffff, 0, 0x0000ffff},       // DRC2_ctrl1(E88H):        0018  DRC2_SIG_DET_RMS=0_0000, DRC2_SIG_DET_PK=00, DRC2_NG_ENA=0, DRC2_SIG_DET_MODE=0, DRC2_SIG_DET=0, DRC2_KNEE2_OP_ENA=0, DRC2_QR=1, DRC2_ANTICLIP=1, DRC2L_ENA=0, DRC2R_ENA=0
    {0xE89, 0x0933, 0x0000ffff, 0, 0x0000ffff},       // DRC2_ctrl2(E89H):        0933  DRC2_ATK=0100, DRC2_DCY=1001, DRC2_MINGAIN=100, DRC2_MAXGAIN=11
    {0xE8A, 0x0018, 0x0000ffff, 0, 0x0000ffff},       // DRC2_ctrl3(E8AH):        0018  DRC2_NG_MINGAIN=0000, DRC2_NG_EXP=00, DRC2_QR_THR=00, DRC2_QR_DCY=00, DRC2_HI_COMP=011, DRC2_LO_COMP=000
    {0xE8B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DRC2_ctrl4(E8BH):        0000  DRC2_KNEE_IP=00_0000, DRC2_KNEE_OP=0_0000
    {0xE8C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DRC2_ctrl5(E8CH):        0000  DRC2_KNEE2_IP=0_0000, DRC2_KNEE2_OP=0_0000
    {0xEC0, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF1_1(EC0H):          0000  LHPF1_MODE=0, LHPF1_ENA=0
    {0xEC1, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF1_2(EC1H):          0000  LHPF1_COEFF=0000_0000_0000_0000
    {0xEC4, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF2_1(EC4H):          0000  LHPF2_MODE=0, LHPF2_ENA=0
    {0xEC5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF2_2(EC5H):          0000  LHPF2_COEFF=0000_0000_0000_0000
    {0xEC8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF3_1(EC8H):          0000  LHPF3_MODE=0, LHPF3_ENA=0
    {0xEC9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF3_2(EC9H):          0000  LHPF3_COEFF=0000_0000_0000_0000
    {0xECC, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF4_1(ECCH):          0000  LHPF4_MODE=0, LHPF4_ENA=0
    {0xECD, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // HPLPF4_2(ECDH):          0000  LHPF4_COEFF=0000_0000_0000_0000
    {0xED0, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ASRC2_ENABLE(ED0H):      0000  ASRC2_IN2L_ENA=0, ASRC2_IN2R_ENA=0, ASRC2_IN1L_ENA=0, ASRC2_IN1R_ENA=0
    {0xED2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ASRC2_RATE1(ED2H):       0000  ASRC2_RATE1=SAMPLE_RATE_1
    {0xED3, 0x4000, 0x0000ffff, 0, 0x0000ffff},       // ASRC2_RATE2(ED3H):       4000  ASRC2_RATE2=ASYNC_SAMPLE_RATE_1
    {0xEE0, 0x000C, 0x0000ffff, 0, 0x0000ffff},       // ASRC1_ENABLE(EE0H):      000C  ASRC1_IN2L_ENA=1, ASRC1_IN2R_ENA=1, ASRC1_IN1L_ENA=0, ASRC1_IN1R_ENA=0
    {0xEE2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ASRC1_RATE1(EE2H):       0000  ASRC1_RATE1=SAMPLE_RATE_1
    {0xEE3, 0x4000, 0x0000ffff, 0, 0x0000ffff},       // ASRC1_RATE2(EE3H):       4000  ASRC1_RATE2=ASYNC_SAMPLE_RATE_1
    {0xEF0, 0x0800, 0x0000ffff, 0, 0x0000ffff},       // ISRC1_CTRL_1(EF0H):      0800  ISRC1_FSH=SAMPLE_RATE_2
    {0xEF1, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // ISRC1_CTRL_2(EF1H):      0001  ISRC1_FSL=SAMPLE_RATE_1
    {0xEF2, 0xC000, 0x0000ffff, 0, 0x0000ffff},       // ISRC1_CTRL_3(EF2H):      C000  ISRC1_INT1_ENA=1, ISRC1_INT2_ENA=1, ISRC1_INT3_ENA=0, ISRC1_INT4_ENA=0, ISRC1_DEC1_ENA=0, ISRC1_DEC2_ENA=0, ISRC1_DEC3_ENA=0, ISRC1_DEC4_ENA=0
    {0xEF3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ISRC2_CTRL_1(EF3H):      0000  ISRC2_FSH=SAMPLE_RATE_1
    {0xEF4, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // ISRC2_CTRL_2(EF4H):      0001  ISRC2_FSL=SAMPLE_RATE_1
    {0xEF5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ISRC2_CTRL_3(EF5H):      0000  ISRC2_INT1_ENA=0, ISRC2_INT2_ENA=0, ISRC2_INT3_ENA=0, ISRC2_INT4_ENA=0, ISRC2_DEC1_ENA=0, ISRC2_DEC2_ENA=0, ISRC2_DEC3_ENA=0, ISRC2_DEC4_ENA=0
    {0xEF6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ISRC3_CTRL_1(EF6H):      0000  ISRC3_FSH=SAMPLE_RATE_1
    {0xEF7, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // ISRC3_CTRL_2(EF7H):      0001  ISRC3_FSL=SAMPLE_RATE_1
    {0xEF8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ISRC3_CTRL_3(EF8H):      0000  ISRC3_INT1_ENA=0, ISRC3_INT2_ENA=0, ISRC3_DEC1_ENA=0, ISRC3_DEC2_ENA=0
    {0xEF9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ISRC4_CTRL_1(EF9H):      0000  ISRC4_FSH=SAMPLE_RATE_1
    {0xEFA, 0x0001, 0x0000ffff, 0, 0x0000ffff},       // ISRC4_CTRL_2(EFAH):      0001  ISRC4_FSL=SAMPLE_RATE_1
    {0xEFB, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ISRC4_CTRL_3(EFBH):      0000  ISRC4_INT1_ENA=0, ISRC4_INT2_ENA=0, ISRC4_DEC1_ENA=0, ISRC4_DEC2_ENA=0
    {0xF00, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Clock_Control(F00H):     0000  EXT_NG_SEL_CLR=0, EXT_NG_SEL_SET=0, CLK_R_ENA_CLR=0, CLK_R_ENA_SET=0, CLK_NG_ENA_CLR=0, CLK_NG_ENA_SET=0, CLK_L_ENA_CLR=0, CLK_L_ENA_SET=0
    {0xF01, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ANC_SRC(F01H):           0000  IN_RXANCR_SEL=None, IN_RXANCL_SEL=None
    {0xF08, 0x001C, 0x0000ffff, 0, 0x0000ffff},       // CIC_shift_coeff(F08H):   001C  CIC_SHIFT=01_1100
    {0xF09, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // CIC_R_coeff(F09H):       0000  CIC_COUNT_LOAD=0000_0000_0000_0000
    {0xF0B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // WF_shift_coeff(F0BH):    0000  WF_SHIFT=000
    {0xF0C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // NG_attack_shift(F0CH):   0000  NG_ATTACK_SHIFT=0_0000
    {0xF0D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // NG_decay_shift(F0DH):    0000  NG_DECAY_SHIFT=0_0000
    {0xF0E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // NG_threshold_index(F0EH): 0000  NG_THRESH_INDEX=0_0000
    {0xF0F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // NG_slope_select(F0FH):   0000  NG_SLOPE_SEL=0
    {0xF11, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // NG_Internal_Gain(F11H):  0000  INT_NG_GAIN=0_0000_0000
    {0xF12, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // NG_External_Gain(F12H):  0000  EXT_NG_GAIN=0_0000_0000
    {0xF15, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_Filter_control(F15H): 0000  FCL_SLIM_ENA_CLR=0, FCL_SLIM_ENA_SET=0, FCL_LIMITER_HPLP_ENA_CLR=0, FCL_LIMITER_HPLP_ENA_SET=0, FCL_LIMITER_BYPASS_CLR=0, FCL_LIMITER_BYPASS_SET=0, FCL_FILTER_ENA_CLR=0, FCL_FILTER_ENA_SET=0
    {0xF17, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FCL_ADC_reformatter_control(F17H): 0004  FCL_MIC_MODE_SEL=Left
    {0xF18, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FCL_NC_CIC_shift_coeff(F18H): 0004  FCL_NC_CIC_SHIFT=0_0100
    {0xF19, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // FCL_NC_CIC_load_coeff(F19H): 0002  FCL_NC_CIC_COUNT_LOAD=0_0010
    {0xF1A, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_HPF_coeff(F1AH):     0000  FCL_HPF_SHIFT=0000, FCL_HPF_MUL=0_0000
    {0xF1B, 0x0010, 0x0000ffff, 0, 0x0000ffff},       // FCL_CAL_GAIN_Coeff_1(F1BH): 0010  FCL_CAL_GAIN_SHIFT=1_0000
    {0xF1C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_CAL_GAIN_Coeff_2(F1CH): 0000  FCL_CAL_GAIN_MUL=00_0000
    {0xF1D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_Gain_Coeff(F1DH): 0000  FCL_IIR_GAIN_SHIFT=0000
    {0xF1E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_Gain_Update(F1EH):   0000  FCL_IIR_GAIN_UPDATE=0, FCL_CAL_GAIN_UPDATE=0
    {0xF1F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_Mask_Register_1(F1FH): 0000  FCL_MASK_CAL=0000
    {0xF20, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_Mask_Register_2(F20H): 0000  FCL_MASK_IIR=0000
    {0xF21, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_Mask_Register_3(F21H): 0000  FCL_MASK_LFL=0000
    {0xF24, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF_1(F24H):      0000  FCL_IIR_PF_BP=0_0000_0000
    {0xF25, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF_2(F25H):      0000  FCL_IIR_PF_BPS=00_0000
    {0xF28, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_1_1(F28H):   0000  FCL_IIR_PF4_B1=0_0000_0000
    {0xF29, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_1_2(F29H):   0000  FCL_IIR_PF4_BS1=00_0000
    {0xF2B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_2_1(F2BH):   0000  FCL_IIR_PF4_B0=0_0000_0000
    {0xF2C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_2_2(F2CH):   0000  FCL_IIR_PF4_BS0=00_0000
    {0xF2D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_3_1(F2DH):   0000  FCL_IIR_PF4_H1=0000_0000
    {0xF2E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_3_2(F2EH):   0000  FCL_IIR_PF4_HS1=00_0000
    {0xF2F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_4_1(F2FH):   0000  FCL_IIR_PF4_H0=0000_0000
    {0xF30, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF4_4_2(F30H):   0000  FCL_IIR_PF4_HS0=00_0000
    {0xF32, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_1_1(F32H):   0000  FCL_IIR_PF3_B1=0_0000_0000
    {0xF33, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_1_2(F33H):   0000  FCL_IIR_PF3_BS1=00_0000
    {0xF35, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_2_1(F35H):   0000  FCL_IIR_PF3_B0=0_0000_0000
    {0xF36, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_2_2(F36H):   0000  FCL_IIR_PF3_BS0=00_0000
    {0xF37, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_3_1(F37H):   0000  FCL_IIR_PF3_H1=0000_0000
    {0xF38, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_3_2(F38H):   0000  FCL_IIR_PF3_HS1=00_0000
    {0xF39, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_4_1(F39H):   0000  FCL_IIR_PF3_H0=0000_0000
    {0xF3A, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF3_4_2(F3AH):   0000  FCL_IIR_PF3_HS0=00_0000
    {0xF3C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_1_1(F3CH):   0000  FCL_IIR_PF2_B1=0_0000_0000
    {0xF3D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_1_2(F3DH):   0000  FCL_IIR_PF2_BS1=00_0000
    {0xF3F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_2_1(F3FH):   0000  FCL_IIR_PF2_B0=0_0000_0000
    {0xF40, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_2_2(F40H):   0000  FCL_IIR_PF2_BS0=00_0000
    {0xF41, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_3_1(F41H):   0000  FCL_IIR_PF2_H1=0000_0000
    {0xF42, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_3_2(F42H):   0000  FCL_IIR_PF2_HS1=00_0000
    {0xF43, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_4_1(F43H):   0000  FCL_IIR_PF2_H0=0000_0000
    {0xF44, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF2_4_2(F44H):   0000  FCL_IIR_PF2_HS0=00_0000
    {0xF46, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_1_1(F46H):   0000  FCL_IIR_PF1_B1=0_0000_0000
    {0xF47, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_1_2(F47H):   0000  FCL_IIR_PF1_BS1=00_0000
    {0xF49, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_2_1(F49H):   0000  FCL_IIR_PF1_B0=0_0000_0000
    {0xF4A, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_2_2(F4AH):   0000  FCL_IIR_PF1_BS0=00_0000
    {0xF4B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_3_1(F4BH):   0000  FCL_IIR_PF1_H1=0000_0000
    {0xF4C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_3_2(F4CH):   0000  FCL_IIR_PF1_HS1=00_0000
    {0xF4D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_4_1(F4DH):   0000  FCL_IIR_PF1_H0=0000_0000
    {0xF4E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF1_4_2(F4EH):   0000  FCL_IIR_PF1_HS0=00_0000
    {0xF50, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_1_1(F50H):   0000  FCL_IIR_PF0_B1=0_0000_0000
    {0xF51, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_1_2(F51H):   0000  FCL_IIR_PF0_BS1=00_0000
    {0xF53, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_2_1(F53H):   0000  FCL_IIR_PF0_B0=0_0000_0000
    {0xF54, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_2_2(F54H):   0000  FCL_IIR_PF0_BS0=00_0000
    {0xF55, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_3_1(F55H):   0000  FCL_IIR_PF0_H1=0000_0000
    {0xF56, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_3_2(F56H):   0000  FCL_IIR_PF0_HS1=00_0000
    {0xF57, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_4_1(F57H):   0000  FCL_IIR_PF0_H0=0000_0000
    {0xF58, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_IIR_PF0_4_2(F58H):   0000  FCL_IIR_PF0_HS0=00_0000
    {0xF5B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_FB_MASK_1(F5BH):     0000  FCL_IIR_PF_ENA=0_0000
    {0xF5C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_FB_MASK_2(F5CH):     0000  FCL_IIR_FB_MASK=0000_0000
    {0xF5F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_LFL_HP_LP_config(F5FH): 0000  FCL_LFL_HP_LP_COEFF_SHIFT=0000, FCL_LFL_HP_LP_COEFF_MUL=000
    {0xF60, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_LFL_HP_config(F60H): 0000  FCL_LFL_HP_COEFF_SHIFT=0000, FCL_LFL_HP_COEFF_MUL=000
    {0xF61, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_LFL_LP_config(F61H): 0000  FCL_LFL_LP_COEFF_SHIFT=0000, FCL_LFL_LP_COEFF_MUL=000
    {0xF62, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_LFL_threshold_1(F62H): 0000  FCL_LFL_THRESHOLD=0000_0000
    {0xF63, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_LFL_threshold_2(F63H): 0000  FCL_LFL_GAIN=0000_0000
    {0xF64, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_LFL_debug(F64H):     0000  FCL_T_LFL_LP_MOFFSET_BYP=0, FCL_T_LFL_LP_SOFFSET_BYP=0, FCL_T_LFL_HP_MOFFSET_BYP=0, FCL_T_LFL_HP_SOFFSET_BYP=0, FCL_T_LFL_HP_LP_MOFFSET_BYP=0, FCL_T_LFL_HP_LP_SOFFSET_BYP=0
    {0xF65, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_SLIM_Linear_Region(F65H): 0000  FCL_SLIM_LINEAR=0000_0000
    {0xF66, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_SLIM_Node_0(F66H):   0000  FCL_SLIM_NODE0=0000_0000
    {0xF67, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_SLIM_Node_1(F67H):   0000  FCL_SLIM_NODE1=0000_0000
    {0xF68, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_SLIM_Node_2(F68H):   0000  FCL_SLIM_NODE2=0000_0000
    {0xF69, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCL_SLIM_Node_3(F69H):   0000  FCL_SLIM_NODE3=0000_0000
    {0xF71, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_Filter_control(F71H): 0000  FCR_SLIM_ENA_CLR=0, FCR_SLIM_ENA_SET=0, FCR_LIMITER_HPLP_ENA_CLR=0, FCR_LIMITER_HPLP_ENA_SET=0, FCR_LIMITER_BYPASS_CLR=0, FCR_LIMITER_BYPASS_SET=0, FCR_FILTER_ENA_CLR=0, FCR_FILTER_ENA_SET=0
    {0xF73, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FCR_ADC_reformatter_control(F73H): 0004  FCR_MIC_MODE_SEL=Left
    {0xF74, 0x0004, 0x0000ffff, 0, 0x0000ffff},       // FCR_NC_CIC_shift_coeff(F74H): 0004  FCR_NC_CIC_SHIFT=0_0100
    {0xF75, 0x0002, 0x0000ffff, 0, 0x0000ffff},       // FCR_NC_CIC_load_coeff(F75H): 0002  FCR_NC_CIC_COUNT_LOAD=0_0010
    {0xF76, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_HPF_coeff(F76H):     0000  FCR_HPF_SHIFT=0000, FCR_HPF_MUL=0_0000
    {0xF77, 0x0010, 0x0000ffff, 0, 0x0000ffff},       // FCR_CAL_GAIN_Coeff_1(F77H): 0010  FCR_CAL_GAIN_SHIFT=1_0000
    {0xF78, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_CAL_GAIN_Coeff_2(F78H): 0000  FCR_CAL_GAIN_MUL=00_0000
    {0xF79, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_Gain_Coeff(F79H): 0000  FCR_IIR_GAIN_SHIFT=0000
    {0xF7A, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_Gain_Update(F7AH):   0000  FCR_IIR_GAIN_UPDATE=0, FCR_CAL_GAIN_UPDATE=0
    {0xF7B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_Mask_Register_1(F7BH): 0000  FCR_MASK_CAL=0000
    {0xF7C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_Mask_Register_2(F7CH): 0000  FCR_MASK_IIR=0000
    {0xF7D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_Mask_Register_3(F7DH): 0000  FCR_MASK_LFL=0000
    {0xF80, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF_1(F80H):      0000  FCR_IIR_PF_BP=0_0000_0000
    {0xF81, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF_2(F81H):      0000  FCR_IIR_PF_BPS=00_0000
    {0xF84, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_1_1(F84H):   0000  FCR_IIR_PF4_B1=0_0000_0000
    {0xF85, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_1_2(F85H):   0000  FCR_IIR_PF4_BS1=00_0000
    {0xF87, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_2_1(F87H):   0000  FCR_IIR_PF4_B0=0_0000_0000
    {0xF88, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_2_2(F88H):   0000  FCR_IIR_PF4_BS0=00_0000
    {0xF89, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_3_1(F89H):   0000  FCR_IIR_PF4_H1=0000_0000
    {0xF8A, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_3_2(F8AH):   0000  FCR_IIR_PF4_HS1=00_0000
    {0xF8B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_4_1(F8BH):   0000  FCR_IIR_PF4_H0=0000_0000
    {0xF8C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF4_4_2(F8CH):   0000  FCR_IIR_PF4_HS0=00_0000
    {0xF8E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_1_1(F8EH):   0000  FCR_IIR_PF3_B1=0_0000_0000
    {0xF8F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_1_2(F8FH):   0000  FCR_IIR_PF3_BS1=00_0000
    {0xF91, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_2_1(F91H):   0000  FCR_IIR_PF3_B0=0_0000_0000
    {0xF92, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_2_2(F92H):   0000  FCR_IIR_PF3_BS0=00_0000
    {0xF93, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_3_1(F93H):   0000  FCR_IIR_PF3_H1=0000_0000
    {0xF94, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_3_2(F94H):   0000  FCR_IIR_PF3_HS1=00_0000
    {0xF95, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_4_1(F95H):   0000  FCR_IIR_PF3_H0=0000_0000
    {0xF96, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF3_4_2(F96H):   0000  FCR_IIR_PF3_HS0=00_0000
    {0xF98, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_1_1(F98H):   0000  FCR_IIR_PF2_B1=0_0000_0000
    {0xF99, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_1_2(F99H):   0000  FCR_IIR_PF2_BS1=00_0000
    {0xF9B, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_2_1(F9BH):   0000  FCR_IIR_PF2_B0=0_0000_0000
    {0xF9C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_2_2(F9CH):   0000  FCR_IIR_PF2_BS0=00_0000
    {0xF9D, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_3_1(F9DH):   0000  FCR_IIR_PF2_H1=0000_0000
    {0xF9E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_3_2(F9EH):   0000  FCR_IIR_PF2_HS1=00_0000
    {0xF9F, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_4_1(F9FH):   0000  FCR_IIR_PF2_H0=0000_0000
    {0xFA0, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF2_4_2(FA0H):   0000  FCR_IIR_PF2_HS0=00_0000
    {0xFA2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_1_1(FA2H):   0000  FCR_IIR_PF1_B1=0_0000_0000
    {0xFA3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_1_2(FA3H):   0000  FCR_IIR_PF1_BS1=00_0000
    {0xFA5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_2_1(FA5H):   0000  FCR_IIR_PF1_B0=0_0000_0000
    {0xFA6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_2_2(FA6H):   0000  FCR_IIR_PF1_BS0=00_0000
    {0xFA7, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_3_1(FA7H):   0000  FCR_IIR_PF1_H1=0000_0000
    {0xFA8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_3_2(FA8H):   0000  FCR_IIR_PF1_HS1=00_0000
    {0xFA9, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_4_1(FA9H):   0000  FCR_IIR_PF1_H0=0000_0000
    {0xFAA, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF1_4_2(FAAH):   0000  FCR_IIR_PF1_HS0=00_0000
    {0xFAC, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_1_1(FACH):   0000  FCR_IIR_PF0_B1=0_0000_0000
    {0xFAD, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_1_2(FADH):   0000  FCR_IIR_PF0_BS1=00_0000
    {0xFAF, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_2_1(FAFH):   0000  FCR_IIR_PF0_B0=0_0000_0000
    {0xFB0, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_2_2(FB0H):   0000  FCR_IIR_PF0_BS0=00_0000
    {0xFB1, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_3_1(FB1H):   0000  FCR_IIR_PF0_H1=0000_0000
    {0xFB2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_3_2(FB2H):   0000  FCR_IIR_PF0_HS1=00_0000
    {0xFB3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_4_1(FB3H):   0000  FCR_IIR_PF0_H0=0000_0000
    {0xFB4, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_IIR_PF0_4_2(FB4H):   0000  FCR_IIR_PF0_HS0=00_0000
    {0xFB7, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_FB_MASK_1(FB7H):     0000  FCR_IIR_PF_ENA=0_0000
    {0xFB8, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_FB_MASK_2(FB8H):     0000  FCR_IIR_FB_MASK=0000_0000
    {0xFBB, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_LFL_HP_LP_config(FBBH): 0000  FCR_LFL_HP_LP_COEFF_SHIFT=0000, FCR_LFL_HP_LP_COEFF_MUL=000
    {0xFBC, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_LFL_HP_config(FBCH): 0000  FCR_LFL_HP_COEFF_SHIFT=0000, FCR_LFL_HP_COEFF_MUL=000
    {0xFBD, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_LFL_LP_config(FBDH): 0000  FCR_LFL_LP_COEFF_SHIFT=0000, FCR_LFL_LP_COEFF_MUL=000
    {0xFBE, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_LFL_threshold_1(FBEH): 0000  FCR_LFL_THRESHOLD=0000_0000
    {0xFBF, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_LFL_threshold_2(FBFH): 0000  FCR_LFL_GAIN=0000_0000
    {0xFC0, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_LFL_debug(FC0H):     0000  FCR_T_LFL_LP_MOFFSET_BYP=0, FCR_T_LFL_LP_SOFFSET_BYP=0, FCR_T_LFL_HP_MOFFSET_BYP=0, FCR_T_LFL_HP_SOFFSET_BYP=0, FCR_T_LFL_HP_LP_MOFFSET_BYP=0, FCR_T_LFL_HP_LP_SOFFSET_BYP=0
    {0xFC1, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_SLIM_Linear_Region(FC1H): 0000  FCR_SLIM_LINEAR=0000_0000
    {0xFC2, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_SLIM_Node_0(FC2H):   0000  FCR_SLIM_NODE0=0000_0000
    {0xFC3, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_SLIM_Node_1(FC3H):   0000  FCR_SLIM_NODE1=0000_0000
    {0xFC4, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_SLIM_Node_2(FC4H):   0000  FCR_SLIM_NODE2=0000_0000
    {0xFC5, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // FCR_SLIM_Node_3(FC5H):   0000  FCR_SLIM_NODE3=0000_0000
    {0x1480, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC1_CTRL_W0(1480H):     0000  DFC1_RATE=0000, DFC1_DITH_ENA=0, DFC1_ENA=0
    {0x1482, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC1_RX_W0(1482H):       1F00  DFC1_RX_DATA_WIDTH=1_1111, DFC1_RX_DATA_TYPE=000
    {0x1484, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC1_TX_W0(1484H):       1F00  DFC1_TX_DATA_WIDTH=1_1111, DFC1_TX_DATA_TYPE=000
    {0x1486, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC2_CTRL_W0(1486H):     0000  DFC2_RATE=0000, DFC2_DITH_ENA=0, DFC2_ENA=0
    {0x1488, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC2_RX_W0(1488H):       1F00  DFC2_RX_DATA_WIDTH=1_1111, DFC2_RX_DATA_TYPE=000
    {0x148A, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC2_TX_W0(148AH):       1F00  DFC2_TX_DATA_WIDTH=1_1111, DFC2_TX_DATA_TYPE=000
    {0x148C, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC3_CTRL_W0(148CH):     0000  DFC3_RATE=0000, DFC3_DITH_ENA=0, DFC3_ENA=0
    {0x148E, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC3_RX_W0(148EH):       1F00  DFC3_RX_DATA_WIDTH=1_1111, DFC3_RX_DATA_TYPE=000
    {0x1490, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC3_TX_W0(1490H):       1F00  DFC3_TX_DATA_WIDTH=1_1111, DFC3_TX_DATA_TYPE=000
    {0x1492, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC4_CTRL_W0(1492H):     0000  DFC4_RATE=0000, DFC4_DITH_ENA=0, DFC4_ENA=0
    {0x1494, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC4_RX_W0(1494H):       1F00  DFC4_RX_DATA_WIDTH=1_1111, DFC4_RX_DATA_TYPE=000
    {0x1496, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC4_TX_W0(1496H):       1F00  DFC4_TX_DATA_WIDTH=1_1111, DFC4_TX_DATA_TYPE=000
    {0x1498, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC5_CTRL_W0(1498H):     0000  DFC5_RATE=0000, DFC5_DITH_ENA=0, DFC5_ENA=0
    {0x149A, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC5_RX_W0(149AH):       1F00  DFC5_RX_DATA_WIDTH=1_1111, DFC5_RX_DATA_TYPE=000
    {0x149C, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC5_TX_W0(149CH):       1F00  DFC5_TX_DATA_WIDTH=1_1111, DFC5_TX_DATA_TYPE=000
    {0x149E, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC6_CTRL_W0(149EH):     0000  DFC6_RATE=0000, DFC6_DITH_ENA=0, DFC6_ENA=0
    {0x14A0, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC6_RX_W0(14A0H):       1F00  DFC6_RX_DATA_WIDTH=1_1111, DFC6_RX_DATA_TYPE=000
    {0x14A2, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC6_TX_W0(14A2H):       1F00  DFC6_TX_DATA_WIDTH=1_1111, DFC6_TX_DATA_TYPE=000
    {0x14A4, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC7_CTRL_W0(14A4H):     0000  DFC7_RATE=0000, DFC7_DITH_ENA=0, DFC7_ENA=0
    {0x14A6, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC7_RX_W0(14A6H):       1F00  DFC7_RX_DATA_WIDTH=1_1111, DFC7_RX_DATA_TYPE=000
    {0x14A8, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC7_TX_W0(14A8H):       1F00  DFC7_TX_DATA_WIDTH=1_1111, DFC7_TX_DATA_TYPE=000
    {0x14AA, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC8_CTRL_W0(14AAH):     0000  DFC8_RATE=0000, DFC8_DITH_ENA=0, DFC8_ENA=0
    {0x14AC, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC8_RX_W0(14ACH):       1F00  DFC8_RX_DATA_WIDTH=1_1111, DFC8_RX_DATA_TYPE=000
    {0x14AE, 0x1F00, 0x0000ffff, 0, 0x0000ffff},       // DFC8_TX_W0(14AEH):       1F00  DFC8_TX_DATA_WIDTH=1_1111, DFC8_TX_DATA_TYPE=000
    {0x14B6, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // DFC_STATUS_W0(14B6H):    0000  DFC_ERR_CHAN=0000_0000
    {0x1600, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ0(1600H):       0000  DSP_IRQ2=0, DSP_IRQ1=0
    {0x1601, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ1(1601H):       0000  DSP_IRQ4=0, DSP_IRQ3=0
    {0x1602, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ2(1602H):       0000  DSP_IRQ6=0, DSP_IRQ5=0
    {0x1603, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ3(1603H):       0000  DSP_IRQ8=0, DSP_IRQ7=0
    {0x1604, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ4(1604H):       0000  DSP_IRQ10=0, DSP_IRQ9=0
    {0x1605, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ5(1605H):       0000  DSP_IRQ12=0, DSP_IRQ11=0
    {0x1606, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ6(1606H):       0000  DSP_IRQ14=0, DSP_IRQ13=0
    {0x1607, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // ADSP2_IRQ7(1607H):       0000  DSP_IRQ16=0, DSP_IRQ15=0
    {0x1700, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO1_CTRL_1(1700H):     2001  GP1_LVL=Low, GP1_OP_CFG=CMOS, GP1_DB=Enabled, GP1_POL=Non-inverted (Active High), GP1_FN=Button detect input / Logic level output
    {0x1701, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO1_CTRL_2(1701H):     F000  GP1_DIR=Input, GP1_PU=1, GP1_PD=1, GP1_DRV_STR=1
    {0x1702, 0x2010, 0x00007fff, 0, 0x00007fff},       // GPIO2_CTRL_1(1702H):     2010  GP2_LVL=Low, GP2_OP_CFG=CMOS, GP2_DB=Enabled, GP2_POL=Non-inverted (Active High), GP2_FN=FLL1 Clock
    {0x1703, 0x7000, 0x0000ffff, 0, 0x0000ffff},       // GPIO2_CTRL_2(1703H):     7000  GP2_DIR=Output, GP2_PU=1, GP2_PD=1, GP2_DRV_STR=1
    {0x1704, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO3_CTRL_1(1704H):     2001  GP3_LVL=Low, GP3_OP_CFG=CMOS, GP3_DB=Enabled, GP3_POL=Non-inverted (Active High), GP3_FN=Button detect input / Logic level output
    {0x1705, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO3_CTRL_2(1705H):     F000  GP3_DIR=Input, GP3_PU=1, GP3_PD=1, GP3_DRV_STR=1
    {0x1706, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO4_CTRL_1(1706H):     2001  GP4_LVL=Low, GP4_OP_CFG=CMOS, GP4_DB=Enabled, GP4_POL=Non-inverted (Active High), GP4_FN=Button detect input / Logic level output
    {0x1707, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO4_CTRL_2(1707H):     F000  GP4_DIR=Input, GP4_PU=1, GP4_PD=1, GP4_DRV_STR=1
    {0x1708, 0xA001, 0x00007fff, 0, 0x00007fff},       // GPIO5_CTRL_1(1708H):     A001  GP5_LVL=High, GP5_OP_CFG=CMOS, GP5_DB=Enabled, GP5_POL=Non-inverted (Active High), GP5_FN=Button detect input / Logic level output
    {0x1709, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO5_CTRL_2(1709H):     F000  GP5_DIR=Input, GP5_PU=1, GP5_PD=1, GP5_DRV_STR=1
    {0x170A, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO6_CTRL_1(170AH):     2001  GP6_LVL=Low, GP6_OP_CFG=CMOS, GP6_DB=Enabled, GP6_POL=Non-inverted (Active High), GP6_FN=Button detect input / Logic level output
    {0x170B, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO6_CTRL_2(170BH):     F000  GP6_DIR=Input, GP6_PU=1, GP6_PD=1, GP6_DRV_STR=1
    {0x170C, 0xA001, 0x00007fff, 0, 0x00007fff},       // GPIO7_CTRL_1(170CH):     A001  GP7_LVL=High, GP7_OP_CFG=CMOS, GP7_DB=Enabled, GP7_POL=Non-inverted (Active High), GP7_FN=Button detect input / Logic level output
    {0x170D, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO7_CTRL_2(170DH):     F000  GP7_DIR=Input, GP7_PU=1, GP7_PD=1, GP7_DRV_STR=1
    {0x170E, 0xA001, 0x00007fff, 0, 0x00007fff},       // GPIO8_CTRL_1(170EH):     A001  GP8_LVL=High, GP8_OP_CFG=CMOS, GP8_DB=Enabled, GP8_POL=Non-inverted (Active High), GP8_FN=Button detect input / Logic level output
    {0x170F, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO8_CTRL_2(170FH):     F000  GP8_DIR=Input, GP8_PU=1, GP8_PD=1, GP8_DRV_STR=1
    {0x1710, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO9_CTRL_1(1710H):     2001  GP9_LVL=Low, GP9_OP_CFG=CMOS, GP9_DB=Enabled, GP9_POL=Non-inverted (Active High), GP9_FN=Button detect input / Logic level output
    {0x1711, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO9_CTRL_2(1711H):     F000  GP9_DIR=Input, GP9_PU=1, GP9_PD=1, GP9_DRV_STR=1
    {0x1712, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO10_CTRL_1(1712H):    2001  GP10_LVL=Low, GP10_OP_CFG=CMOS, GP10_DB=Enabled, GP10_POL=Non-inverted (Active High), GP10_FN=Button detect input / Logic level output
    {0x1713, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO10_CTRL_2(1713H):    F000  GP10_DIR=Input, GP10_PU=1, GP10_PD=1, GP10_DRV_STR=1
    {0x1714, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO11_CTRL_1(1714H):    2001  GP11_LVL=Low, GP11_OP_CFG=CMOS, GP11_DB=Enabled, GP11_POL=Non-inverted (Active High), GP11_FN=Button detect input / Logic level output
    {0x1715, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO11_CTRL_2(1715H):    F000  GP11_DIR=Input, GP11_PU=1, GP11_PD=1, GP11_DRV_STR=1
    {0x1716, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO12_CTRL_1(1716H):    2001  GP12_LVL=Low, GP12_OP_CFG=CMOS, GP12_DB=Enabled, GP12_POL=Non-inverted (Active High), GP12_FN=Button detect input / Logic level output
    {0x1717, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO12_CTRL_2(1717H):    F000  GP12_DIR=Input, GP12_PU=1, GP12_PD=1, GP12_DRV_STR=1
    {0x1718, 0xA010, 0x00007fff, 0, 0x00007fff},       // GPIO13_CTRL_1(1718H):    A010  GP13_LVL=High, GP13_OP_CFG=CMOS, GP13_DB=Enabled, GP13_POL=Non-inverted (Active High), GP13_FN=FLL1 Clock
    {0x1719, 0x7000, 0x0000ffff, 0, 0x0000ffff},       // GPIO13_CTRL_2(1719H):    7000  GP13_DIR=Output, GP13_PU=1, GP13_PD=1, GP13_DRV_STR=1
    {0x171A, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO14_CTRL_1(171AH):    2001  GP14_LVL=Low, GP14_OP_CFG=CMOS, GP14_DB=Enabled, GP14_POL=Non-inverted (Active High), GP14_FN=Button detect input / Logic level output
    {0x171B, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO14_CTRL_2(171BH):    F000  GP14_DIR=Input, GP14_PU=1, GP14_PD=1, GP14_DRV_STR=1
    {0x171C, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO15_CTRL_1(171CH):    2000  GP15_LVL=Low, GP15_OP_CFG=CMOS, GP15_DB=Enabled, GP15_POL=Non-inverted (Active High), GP15_FN=AIF1TXDAT
    {0x171D, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO15_CTRL_2(171DH):    F000  GP15_DIR=Input, GP15_PU=1, GP15_PD=1, GP15_DRV_STR=1
    {0x171E, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO16_CTRL_1(171EH):    2000  GP16_LVL=Low, GP16_OP_CFG=CMOS, GP16_DB=Enabled, GP16_POL=Non-inverted (Active High), GP16_FN=AIF1BCLK
    {0x171F, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO16_CTRL_2(171FH):    F000  GP16_DIR=Input, GP16_PU=1, GP16_PD=1, GP16_DRV_STR=1
    {0x1720, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO17_CTRL_1(1720H):    2000  GP17_LVL=Low, GP17_OP_CFG=CMOS, GP17_DB=Enabled, GP17_POL=Non-inverted (Active High), GP17_FN=AIF1RXDAT
    {0x1721, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO17_CTRL_2(1721H):    F000  GP17_DIR=Input, GP17_PU=1, GP17_PD=1, GP17_DRV_STR=1
    {0x1722, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO18_CTRL_1(1722H):    2000  GP18_LVL=Low, GP18_OP_CFG=CMOS, GP18_DB=Enabled, GP18_POL=Non-inverted (Active High), GP18_FN=AIF1LRCLK
    {0x1723, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO18_CTRL_2(1723H):    F000  GP18_DIR=Input, GP18_PU=1, GP18_PD=1, GP18_DRV_STR=1
    {0x1724, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO19_CTRL_1(1724H):    2000  GP19_LVL=Low, GP19_OP_CFG=CMOS, GP19_DB=Enabled, GP19_POL=Non-inverted (Active High), GP19_FN=AIF2TXDAT
    {0x1725, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO19_CTRL_2(1725H):    F000  GP19_DIR=Input, GP19_PU=1, GP19_PD=1, GP19_DRV_STR=1
    {0x1726, 0xA000, 0x00007fff, 0, 0x00007fff},       // GPIO20_CTRL_1(1726H):    A000  GP20_LVL=High, GP20_OP_CFG=CMOS, GP20_DB=Enabled, GP20_POL=Non-inverted (Active High), GP20_FN=AIF2BCLK
    {0x1727, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO20_CTRL_2(1727H):    F000  GP20_DIR=Input, GP20_PU=1, GP20_PD=1, GP20_DRV_STR=1
    {0x1728, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO21_CTRL_1(1728H):    2000  GP21_LVL=Low, GP21_OP_CFG=CMOS, GP21_DB=Enabled, GP21_POL=Non-inverted (Active High), GP21_FN=AIF2RXDAT
    {0x1729, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO21_CTRL_2(1729H):    F000  GP21_DIR=Input, GP21_PU=1, GP21_PD=1, GP21_DRV_STR=1
    {0x172A, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO22_CTRL_1(172AH):    2000  GP22_LVL=Low, GP22_OP_CFG=CMOS, GP22_DB=Enabled, GP22_POL=Non-inverted (Active High), GP22_FN=AIF2LRCLK
    {0x172B, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO22_CTRL_2(172BH):    F000  GP22_DIR=Input, GP22_PU=1, GP22_PD=1, GP22_DRV_STR=1
    {0x172C, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO23_CTRL_1(172CH):    2000  GP23_LVL=Low, GP23_OP_CFG=CMOS, GP23_DB=Enabled, GP23_POL=Non-inverted (Active High), GP23_FN=AIF3TXDAT
    {0x172D, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO23_CTRL_2(172DH):    F000  GP23_DIR=Input, GP23_PU=1, GP23_PD=1, GP23_DRV_STR=1
    {0x172E, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO24_CTRL_1(172EH):    2000  GP24_LVL=Low, GP24_OP_CFG=CMOS, GP24_DB=Enabled, GP24_POL=Non-inverted (Active High), GP24_FN=AIF3BCLK
    {0x172F, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO24_CTRL_2(172FH):    F000  GP24_DIR=Input, GP24_PU=1, GP24_PD=1, GP24_DRV_STR=1
    {0x1730, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO25_CTRL_1(1730H):    2000  GP25_LVL=Low, GP25_OP_CFG=CMOS, GP25_DB=Enabled, GP25_POL=Non-inverted (Active High), GP25_FN=AIF3RXDAT
    {0x1731, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO25_CTRL_2(1731H):    F000  GP25_DIR=Input, GP25_PU=1, GP25_PD=1, GP25_DRV_STR=1
    {0x1732, 0xA000, 0x00007fff, 0, 0x00007fff},       // GPIO26_CTRL_1(1732H):    A000  GP26_LVL=High, GP26_OP_CFG=CMOS, GP26_DB=Enabled, GP26_POL=Non-inverted (Active High), GP26_FN=AIF3LRCLK
    {0x1733, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO26_CTRL_2(1733H):    F000  GP26_DIR=Input, GP26_PU=1, GP26_PD=1, GP26_DRV_STR=1
    {0x1734, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO27_CTRL_1(1734H):    2000  GP27_LVL=Low, GP27_OP_CFG=CMOS, GP27_DB=Enabled, GP27_POL=Non-inverted (Active High), GP27_FN=AIF4TXDAT
    {0x1735, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO27_CTRL_2(1735H):    F000  GP27_DIR=Input, GP27_PU=1, GP27_PD=1, GP27_DRV_STR=1
    {0x1736, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO28_CTRL_1(1736H):    2000  GP28_LVL=Low, GP28_OP_CFG=CMOS, GP28_DB=Enabled, GP28_POL=Non-inverted (Active High), GP28_FN=AIF4BCLK
    {0x1737, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO28_CTRL_2(1737H):    F000  GP28_DIR=Input, GP28_PU=1, GP28_PD=1, GP28_DRV_STR=1
    {0x1738, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO29_CTRL_1(1738H):    2000  GP29_LVL=Low, GP29_OP_CFG=CMOS, GP29_DB=Enabled, GP29_POL=Non-inverted (Active High), GP29_FN=AIF4RXDAT
    {0x1739, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO29_CTRL_2(1739H):    F000  GP29_DIR=Input, GP29_PU=1, GP29_PD=1, GP29_DRV_STR=1
    {0x173A, 0x2000, 0x00007fff, 0, 0x00007fff},       // GPIO30_CTRL_1(173AH):    2000  GP30_LVL=Low, GP30_OP_CFG=CMOS, GP30_DB=Enabled, GP30_POL=Non-inverted (Active High), GP30_FN=AIF4LRCLK
    {0x173B, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO30_CTRL_2(173BH):    F000  GP30_DIR=Input, GP30_PU=1, GP30_PD=1, GP30_DRV_STR=1
    {0x173C, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO31_CTRL_1(173CH):    2001  GP31_LVL=Low, GP31_OP_CFG=CMOS, GP31_DB=Enabled, GP31_POL=Non-inverted (Active High), GP31_FN=Button detect input / Logic level output
    {0x173D, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO31_CTRL_2(173DH):    F000  GP31_DIR=Input, GP31_PU=1, GP31_PD=1, GP31_DRV_STR=1
    {0x173E, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO32_CTRL_1(173EH):    2001  GP32_LVL=Low, GP32_OP_CFG=CMOS, GP32_DB=Enabled, GP32_POL=Non-inverted (Active High), GP32_FN=Button detect input / Logic level output
    {0x173F, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO32_CTRL_2(173FH):    F000  GP32_DIR=Input, GP32_PU=1, GP32_PD=1, GP32_DRV_STR=1
    {0x1740, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO33_CTRL_1(1740H):    2001  GP33_LVL=Low, GP33_OP_CFG=CMOS, GP33_DB=Enabled, GP33_POL=Non-inverted (Active High), GP33_FN=Button detect input / Logic level output
    {0x1741, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO33_CTRL_2(1741H):    F000  GP33_DIR=Input, GP33_PU=1, GP33_PD=1, GP33_DRV_STR=1
    {0x1742, 0xA001, 0x00007fff, 0, 0x00007fff},       // GPIO34_CTRL_1(1742H):    A001  GP34_LVL=High, GP34_OP_CFG=CMOS, GP34_DB=Enabled, GP34_POL=Non-inverted (Active High), GP34_FN=Button detect input / Logic level output
    {0x1743, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO34_CTRL_2(1743H):    F000  GP34_DIR=Input, GP34_PU=1, GP34_PD=1, GP34_DRV_STR=1
    {0x1744, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO35_CTRL_1(1744H):    2001  GP35_LVL=Low, GP35_OP_CFG=CMOS, GP35_DB=Enabled, GP35_POL=Non-inverted (Active High), GP35_FN=Button detect input / Logic level output
    {0x1745, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO35_CTRL_2(1745H):    F000  GP35_DIR=Input, GP35_PU=1, GP35_PD=1, GP35_DRV_STR=1
    {0x1746, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO36_CTRL_1(1746H):    2001  GP36_LVL=Low, GP36_OP_CFG=CMOS, GP36_DB=Enabled, GP36_POL=Non-inverted (Active High), GP36_FN=Button detect input / Logic level output
    {0x1747, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO36_CTRL_2(1747H):    F000  GP36_DIR=Input, GP36_PU=1, GP36_PD=1, GP36_DRV_STR=1
    {0x1748, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO37_CTRL_1(1748H):    2001  GP37_LVL=Low, GP37_OP_CFG=CMOS, GP37_DB=Enabled, GP37_POL=Non-inverted (Active High), GP37_FN=Button detect input / Logic level output
    {0x1749, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO37_CTRL_2(1749H):    F000  GP37_DIR=Input, GP37_PU=1, GP37_PD=1, GP37_DRV_STR=1
    {0x174A, 0x2001, 0x00007fff, 0, 0x00007fff},       // GPIO38_CTRL_1(174AH):    2001  GP38_LVL=Low, GP38_OP_CFG=CMOS, GP38_DB=Enabled, GP38_POL=Non-inverted (Active High), GP38_FN=Button detect input / Logic level output
    {0x174B, 0xF000, 0x0000ffff, 0, 0x0000ffff},       // GPIO38_CTRL_2(174BH):    F000  GP38_DIR=Input, GP38_PU=1, GP38_PD=1, GP38_DRV_STR=1
    {0x1840, 0x9200, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_1(1840H):      9200  IM_DSP_SHARED_WR_COLL_EINT1=1, IM_CTRLIF_ERR_EINT1=1, IM_SYSCLK_FAIL_EINT1=1, IM_BOOT_DONE_EINT1=0
    {0x1841, 0xFB00, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_2(1841H):      FB00  IM_FLL_AO_LOCK_EINT1=1, IM_FLL2_LOCK_EINT1=1, IM_FLL1_LOCK_EINT1=1
    {0x1845, 0x0301, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_6(1845H):      0301  IM_MICDET2_EINT1=1, IM_MICDET1_EINT1=1, IM_HPDET_EINT1=1
    {0x1846, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_7(1846H):      003F  IM_MICD_CLAMP_FALL_EINT1=1, IM_MICD_CLAMP_RISE_EINT1=1, IM_JD2_FALL_EINT1=1, IM_JD2_RISE_EINT1=1, IM_JD1_FALL_EINT1=1, IM_JD1_RISE_EINT1=1
    {0x1848, 0x0F07, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_9(1848H):      0F07  IM_ASRC2_IN2_LOCK_EINT1=1, IM_ASRC2_IN1_LOCK_EINT1=1, IM_ASRC1_IN2_LOCK_EINT1=1, IM_ASRC1_IN1_LOCK_EINT1=1, IM_DRC2_SIG_DET_EINT1=1, IM_DRC1_SIG_DET_EINT1=1
    {0x184A, 0xFFFF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_11(184AH):     FFFF  IM_DSP_IRQ16_EINT1=1, IM_DSP_IRQ15_EINT1=1, IM_DSP_IRQ14_EINT1=1, IM_DSP_IRQ13_EINT1=1, IM_DSP_IRQ12_EINT1=1, IM_DSP_IRQ11_EINT1=1, IM_DSP_IRQ10_EINT1=1, IM_DSP_IRQ9_EINT1=1, IM_DSP_IRQ8_EINT1=1, IM_DSP_IRQ7_EINT1=1, IM_DSP_IRQ6_EINT1=1, IM_DSP_IRQ5_EINT1=1, IM_DSP_IRQ4_EINT1=1, IM_DSP_IRQ3_EINT1=1, IM_DSP_IRQ2_EINT1=1, IM_DSP_IRQ1_EINT1=1
    {0x184B, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_12(184BH):     003F  IM_HP3R_SC_EINT1=1, IM_HP3L_SC_EINT1=1, IM_HP2R_SC_EINT1=1, IM_HP2L_SC_EINT1=1, IM_HP1R_SC_EINT1=1, IM_HP1L_SC_EINT1=1
    {0x184C, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_13(184CH):     003F  IM_HP3R_ENABLE_DONE_EINT1=1, IM_HP3L_ENABLE_DONE_EINT1=1, IM_HP2R_ENABLE_DONE_EINT1=1, IM_HP2L_ENABLE_DONE_EINT1=1, IM_HP1R_ENABLE_DONE_EINT1=1, IM_HP1L_ENABLE_DONE_EINT1=1
    {0x184D, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_14(184DH):     003F  IM_HP3R_DISABLE_DONE_EINT1=1, IM_HP3L_DISABLE_DONE_EINT1=1, IM_HP2R_DISABLE_DONE_EINT1=1, IM_HP2L_DISABLE_DONE_EINT1=1, IM_HP1R_DISABLE_DONE_EINT1=1, IM_HP1L_DISABLE_DONE_EINT1=1
    {0x1850, 0xFFFF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_17(1850H):     FFFF  IM_GP16_EINT1=1, IM_GP15_EINT1=1, IM_GP14_EINT1=1, IM_GP13_EINT1=1, IM_GP12_EINT1=1, IM_GP11_EINT1=1, IM_GP10_EINT1=1, IM_GP9_EINT1=1, IM_GP8_EINT1=1, IM_GP7_EINT1=1, IM_GP6_EINT1=1, IM_GP5_EINT1=1, IM_GP4_EINT1=1, IM_GP3_EINT1=1, IM_GP2_EINT1=1, IM_GP1_EINT1=1
    {0x1851, 0xFFFF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_18(1851H):     FFFF  IM_GP32_EINT1=1, IM_GP31_EINT1=1, IM_GP30_EINT1=1, IM_GP29_EINT1=1, IM_GP28_EINT1=1, IM_GP27_EINT1=1, IM_GP26_EINT1=1, IM_GP25_EINT1=1, IM_GP24_EINT1=1, IM_GP23_EINT1=1, IM_GP22_EINT1=1, IM_GP21_EINT1=1, IM_GP20_EINT1=1, IM_GP19_EINT1=1, IM_GP18_EINT1=1, IM_GP17_EINT1=1
    {0x1852, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_19(1852H):     003F  IM_GP38_EINT1=1, IM_GP37_EINT1=1, IM_GP36_EINT1=1, IM_GP35_EINT1=1, IM_GP34_EINT1=1, IM_GP33_EINT1=1
    {0x1854, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_21(1854H):     00FF  IM_TIMER8_EINT1=1, IM_TIMER7_EINT1=1, IM_TIMER6_EINT1=1, IM_TIMER5_EINT1=1, IM_TIMER4_EINT1=1, IM_TIMER3_EINT1=1, IM_TIMER2_EINT1=1, IM_TIMER1_EINT1=1
    {0x1855, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_22(1855H):     00FF  IM_EVENT8_NOT_EMPTY_EINT1=1, IM_EVENT7_NOT_EMPTY_EINT1=1, IM_EVENT6_NOT_EMPTY_EINT1=1, IM_EVENT5_NOT_EMPTY_EINT1=1, IM_EVENT4_NOT_EMPTY_EINT1=1, IM_EVENT3_NOT_EMPTY_EINT1=1, IM_EVENT2_NOT_EMPTY_EINT1=1, IM_EVENT1_NOT_EMPTY_EINT1=1
    {0x1856, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_23(1856H):     00FF  IM_EVENT8_FULL_EINT1=1, IM_EVENT7_FULL_EINT1=1, IM_EVENT6_FULL_EINT1=1, IM_EVENT5_FULL_EINT1=1, IM_EVENT4_FULL_EINT1=1, IM_EVENT3_FULL_EINT1=1, IM_EVENT2_FULL_EINT1=1, IM_EVENT1_FULL_EINT1=1
    {0x1857, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_24(1857H):     00FF  IM_EVENT8_WMARK_EINT1=1, IM_EVENT7_WMARK_EINT1=1, IM_EVENT6_WMARK_EINT1=1, IM_EVENT5_WMARK_EINT1=1, IM_EVENT4_WMARK_EINT1=1, IM_EVENT3_WMARK_EINT1=1, IM_EVENT2_WMARK_EINT1=1, IM_EVENT1_WMARK_EINT1=1
    {0x1858, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_25(1858H):     007F  IM_DSP7_DMA_EINT1=1, IM_DSP6_DMA_EINT1=1, IM_DSP5_DMA_EINT1=1, IM_DSP4_DMA_EINT1=1, IM_DSP3_DMA_EINT1=1, IM_DSP2_DMA_EINT1=1, IM_DSP1_DMA_EINT1=1
    {0x185A, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_27(185AH):     007F  IM_DSP7_START1_EINT1=1, IM_DSP6_START1_EINT1=1, IM_DSP5_START1_EINT1=1, IM_DSP4_START1_EINT1=1, IM_DSP3_START1_EINT1=1, IM_DSP2_START1_EINT1=1, IM_DSP1_START1_EINT1=1
    {0x185B, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_28(185BH):     007F  IM_DSP7_START2_EINT1=1, IM_DSP6_START2_EINT1=1, IM_DSP5_START2_EINT1=1, IM_DSP4_START2_EINT1=1, IM_DSP3_START2_EINT1=1, IM_DSP2_START2_EINT1=1, IM_DSP1_START2_EINT1=1
    {0x185D, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_30(185DH):     007F  IM_DSP7_BUSY_EINT1=1, IM_DSP6_BUSY_EINT1=1, IM_DSP5_BUSY_EINT1=1, IM_DSP4_BUSY_EINT1=1, IM_DSP3_BUSY_EINT1=1, IM_DSP2_BUSY_EINT1=1, IM_DSP1_BUSY_EINT1=1
    {0x185E, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_31(185EH):     0007  IM_MIF3_DONE_EINT1=1, IM_MIF2_DONE_EINT1=1, IM_MIF1_DONE_EINT1=1
    {0x185F, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_Mask_32(185FH):     0007  IM_MIF3_BLOCK_EINT1=1, IM_MIF2_BLOCK_EINT1=1, IM_MIF1_BLOCK_EINT1=1
    {0x1940, 0x9280, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_1(1940H):      9280  IM_DSP_SHARED_WR_COLL_EINT2=1, IM_CTRLIF_ERR_EINT2=1, IM_SYSCLK_FAIL_EINT2=1, IM_BOOT_DONE_EINT2=1
    {0x1941, 0xFB00, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_2(1941H):      FB00  IM_FLL_AO_LOCK_EINT2=1, IM_FLL2_LOCK_EINT2=1, IM_FLL1_LOCK_EINT2=1
    {0x1945, 0x0301, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_6(1945H):      0301  IM_MICDET2_EINT2=1, IM_MICDET1_EINT2=1, IM_HPDET_EINT2=1
    {0x1946, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_7(1946H):      003F  IM_MICD_CLAMP_FALL_EINT2=1, IM_MICD_CLAMP_RISE_EINT2=1, IM_JD2_FALL_EINT2=1, IM_JD2_RISE_EINT2=1, IM_JD1_FALL_EINT2=1, IM_JD1_RISE_EINT2=1
    {0x1948, 0x0F07, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_9(1948H):      0F07  IM_ASRC2_IN2_LOCK_EINT2=1, IM_ASRC2_IN1_LOCK_EINT2=1, IM_ASRC1_IN2_LOCK_EINT2=1, IM_ASRC1_IN1_LOCK_EINT2=1, IM_DRC2_SIG_DET_EINT2=1, IM_DRC1_SIG_DET_EINT2=1
    {0x194A, 0xFFFF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_11(194AH):     FFFF  IM_DSP_IRQ16_EINT2=1, IM_DSP_IRQ15_EINT2=1, IM_DSP_IRQ14_EINT2=1, IM_DSP_IRQ13_EINT2=1, IM_DSP_IRQ12_EINT2=1, IM_DSP_IRQ11_EINT2=1, IM_DSP_IRQ10_EINT2=1, IM_DSP_IRQ9_EINT2=1, IM_DSP_IRQ8_EINT2=1, IM_DSP_IRQ7_EINT2=1, IM_DSP_IRQ6_EINT2=1, IM_DSP_IRQ5_EINT2=1, IM_DSP_IRQ4_EINT2=1, IM_DSP_IRQ3_EINT2=1, IM_DSP_IRQ2_EINT2=1, IM_DSP_IRQ1_EINT2=1
    {0x194B, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_12(194BH):     003F  IM_HP3R_SC_EINT2=1, IM_HP3L_SC_EINT2=1, IM_HP2R_SC_EINT2=1, IM_HP2L_SC_EINT2=1, IM_HP1R_SC_EINT2=1, IM_HP1L_SC_EINT2=1
    {0x194C, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_13(194CH):     003F  IM_HP3R_ENABLE_DONE_EINT2=1, IM_HP3L_ENABLE_DONE_EINT2=1, IM_HP2R_ENABLE_DONE_EINT2=1, IM_HP2L_ENABLE_DONE_EINT2=1, IM_HP1R_ENABLE_DONE_EINT2=1, IM_HP1L_ENABLE_DONE_EINT2=1
    {0x194D, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_14(194DH):     003F  IM_HP3R_DISABLE_DONE_EINT2=1, IM_HP3L_DISABLE_DONE_EINT2=1, IM_HP2R_DISABLE_DONE_EINT2=1, IM_HP2L_DISABLE_DONE_EINT2=1, IM_HP1R_DISABLE_DONE_EINT2=1, IM_HP1L_DISABLE_DONE_EINT2=1
    {0x1950, 0xFFFF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_17(1950H):     FFFF  IM_GP16_EINT2=1, IM_GP15_EINT2=1, IM_GP14_EINT2=1, IM_GP13_EINT2=1, IM_GP12_EINT2=1, IM_GP11_EINT2=1, IM_GP10_EINT2=1, IM_GP9_EINT2=1, IM_GP8_EINT2=1, IM_GP7_EINT2=1, IM_GP6_EINT2=1, IM_GP5_EINT2=1, IM_GP4_EINT2=1, IM_GP3_EINT2=1, IM_GP2_EINT2=1, IM_GP1_EINT2=1
    {0x1951, 0xFFFF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_18(1951H):     FFFF  IM_GP32_EINT2=1, IM_GP31_EINT2=1, IM_GP30_EINT2=1, IM_GP29_EINT2=1, IM_GP28_EINT2=1, IM_GP27_EINT2=1, IM_GP26_EINT2=1, IM_GP25_EINT2=1, IM_GP24_EINT2=1, IM_GP23_EINT2=1, IM_GP22_EINT2=1, IM_GP21_EINT2=1, IM_GP20_EINT2=1, IM_GP19_EINT2=1, IM_GP18_EINT2=1, IM_GP17_EINT2=1
    {0x1952, 0x003F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_19(1952H):     003F  IM_GP38_EINT2=1, IM_GP37_EINT2=1, IM_GP36_EINT2=1, IM_GP35_EINT2=1, IM_GP34_EINT2=1, IM_GP33_EINT2=1
    {0x1954, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_21(1954H):     00FF  IM_TIMER8_EINT2=1, IM_TIMER7_EINT2=1, IM_TIMER6_EINT2=1, IM_TIMER5_EINT2=1, IM_TIMER4_EINT2=1, IM_TIMER3_EINT2=1, IM_TIMER2_EINT2=1, IM_TIMER1_EINT2=1
    {0x1955, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_22(1955H):     00FF  IM_EVENT8_NOT_EMPTY_EINT2=1, IM_EVENT7_NOT_EMPTY_EINT2=1, IM_EVENT6_NOT_EMPTY_EINT2=1, IM_EVENT5_NOT_EMPTY_EINT2=1, IM_EVENT4_NOT_EMPTY_EINT2=1, IM_EVENT3_NOT_EMPTY_EINT2=1, IM_EVENT2_NOT_EMPTY_EINT2=1, IM_EVENT1_NOT_EMPTY_EINT2=1
    {0x1956, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_23(1956H):     00FF  IM_EVENT8_FULL_EINT2=1, IM_EVENT7_FULL_EINT2=1, IM_EVENT6_FULL_EINT2=1, IM_EVENT5_FULL_EINT2=1, IM_EVENT4_FULL_EINT2=1, IM_EVENT3_FULL_EINT2=1, IM_EVENT2_FULL_EINT2=1, IM_EVENT1_FULL_EINT2=1
    {0x1957, 0x00FF, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_24(1957H):     00FF  IM_EVENT8_WMARK_EINT2=1, IM_EVENT7_WMARK_EINT2=1, IM_EVENT6_WMARK_EINT2=1, IM_EVENT5_WMARK_EINT2=1, IM_EVENT4_WMARK_EINT2=1, IM_EVENT3_WMARK_EINT2=1, IM_EVENT2_WMARK_EINT2=1, IM_EVENT1_WMARK_EINT2=1
    {0x1958, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_25(1958H):     007F  IM_DSP7_DMA_EINT2=1, IM_DSP6_DMA_EINT2=1, IM_DSP5_DMA_EINT2=1, IM_DSP4_DMA_EINT2=1, IM_DSP3_DMA_EINT2=1, IM_DSP2_DMA_EINT2=1, IM_DSP1_DMA_EINT2=1
    {0x195A, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_27(195AH):     007F  IM_DSP7_START1_EINT2=1, IM_DSP6_START1_EINT2=1, IM_DSP5_START1_EINT2=1, IM_DSP4_START1_EINT2=1, IM_DSP3_START1_EINT2=1, IM_DSP2_START1_EINT2=1, IM_DSP1_START1_EINT2=1
    {0x195B, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_28(195BH):     007F  IM_DSP7_START2_EINT2=1, IM_DSP6_START2_EINT2=1, IM_DSP5_START2_EINT2=1, IM_DSP4_START2_EINT2=1, IM_DSP3_START2_EINT2=1, IM_DSP2_START2_EINT2=1, IM_DSP1_START2_EINT2=1
    {0x195D, 0x007F, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_30(195DH):     007F  IM_DSP7_BUSY_EINT2=1, IM_DSP6_BUSY_EINT2=1, IM_DSP5_BUSY_EINT2=1, IM_DSP4_BUSY_EINT2=1, IM_DSP3_BUSY_EINT2=1, IM_DSP2_BUSY_EINT2=1, IM_DSP1_BUSY_EINT2=1
    {0x195E, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_31(195EH):     0007  IM_MIF3_DONE_EINT2=1, IM_MIF2_DONE_EINT2=1, IM_MIF1_DONE_EINT2=1
    {0x195F, 0x0007, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_Mask_32(195FH):     0007  IM_MIF3_BLOCK_EINT2=1, IM_MIF2_BLOCK_EINT2=1, IM_MIF1_BLOCK_EINT2=1
    {0x1A06, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // Interrupt_Debounce_7(1A06H): 0000  MICD_CLAMP_DB=0, JD2_DB=0, JD1_DB=0
    {0x1A80, 0x4400, 0x0000ffff, 0, 0x0000ffff},       // IRQ1_CTRL(1A80H):        4400  IM_IRQ1=0, IRQ_POL=1, IRQ_OP_CFG=0
    {0x1A82, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // IRQ2_CTRL(1A82H):        0000  IM_IRQ2=0
    {0x1AC0, 0x0000, 0x0000ffff, 0, 0x0000ffff},       // GPIO_Debounce_Config(1AC0H): 0000  GP_DBTIME=100us
    {0x1AD0, 0x4002, 0x0000ffff, 0, 0x0000ffff},       // AOD_Pad_Ctrl(1AD0H):     4002  RESET_PU=1, RESET_PD=0

    cs47l90_sequence_end,
};

