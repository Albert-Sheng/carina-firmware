/**
 * @file    hid_proj.h
 * @brief   Project Carina related HID command header.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */
#ifndef _HID_PROJ_H_
#define _HID_PROJ_H_

#include "hid.h"
/*===========================================================================*/
/* HID Commands constants.                                                   */
/*===========================================================================*/
/**
 * @brief   The identifier of USB HID commands
 * @details List all of the command definitions used in USB HID commands
 * @note
 */
#define HID_CMD_IMPERIUM_EXIST                                          (0x60)
#define HID_CMD_LED_CTRL                                                (0x61)
#define HID_CMD_PHANTOM_PWR_CTRL                                        (0x62)
#define HID_CMD_AVNERA_MAC_CTRL                                         (0x63)
#define HID_CMD_AVNERA_PAIRING_CTRL                                     (0x64)
#define HID_CMD_AVNERA_RF_CTRL                                          (0x65)
#define HID_CMD_AVNERA_MAC_STATS_CTRL                                   (0x66)
#define HID_CMD_AVNERA_READ_MEM                                         (0x67)
#define HID_CMD_AVNERA_WRITE_MEM                                        (0x68)
#define HID_CMD_AVNERA_GET_FLASH_OPSTS                                  (0x69)
#define HID_CMD_AVNERA_READ_FLASH                                       (0x6A)
#define HID_CMD_AVNERA_WRITE_FLASH                                      (0x6B)
#define HID_CMD_AVNERA_ERASE_FLASH                                      (0x6C)
#define HID_CMD_AVNERA_READ_MAC_PER                                     (0x6D)
#define HID_CMD_PROXIMITY_SENSOR_STS                                    (0x6E)
#define HID_CMD_CS47L90_ACCESS                                          (0x6F)
#define HID_CMD_HDMI_PASSTHROUGH_CTRL                                   (0x70)
#define HID_CMD_GET_IO_MESSAGE                                          (0x71)
#define HID_CMD_SET_IO_MESSAGE                                          (0x72)
#define HID_CMD_AVNERA_SET_NEXT_BOOT                                    (0x73)
#define HID_CMD_AVNERA_GET_BOOT_INFO                                    (0x74)
#define HID_CMD_AVNERA_GET_FW_ADDR                                      (0x75)
#define HID_CMD_AVNERA_AVBOOT_RESET                                     (0x76)
#define HID_CMD_AVNERA_AVBOOT_AUTH                                      (0x77)
#define HID_CMD_LCD_BACK_LIGHT_CTRL                                     (0x78)
#define HID_CMD_LCD_SET_CLEAR_COLOR                                     (0x79)
#define HID_CMD_PGA2505_GET_PREAMP_GAIN                                 (0x7A)
#define HID_CMD_PGA2505_SET_PREAMP_GAIN                                 (0x7B)

// For Coprocessor
#define HID_CMD_COPROCESSOR_RESET                                       (0x80)
#define HID_CMD_COPROCESSOR_ENTER_BLD                                   (0x81)
#define HID_CMD_COPROCESSOR_ERASE_APP                                   (0x82)
#define HID_CMD_COPROCESSOR_IS_READY                                    (0x83)
#define HID_CMD_COPROCESSOR_START_DFU                                   (0x84)
#define HID_CMD_COPROCESSOR_END_DFU                                     (0x85)
#define HID_CMD_COPROCESSOR_DFU_STS                                     (0x86)
#define HID_CMD_COPROCESSOR_WR_APP                                      (0x87)
#define HID_CMD_COPROCESSOR_RD_CHECKSUM                                 (0x88)
#define HID_CMD_COPROCESSOR_RD_FTYPTN                                   (0x89)
#define HID_CMD_COPROCESSOR_WR_FTYPTN                                   (0x8A)
#define HID_CMD_COPROCESSOR_ER_FTYPTN                                   (0x8B)

// For Qualcomm Bluetooth Chipset
#define HID_CMD_QUALCOMM_ENTER_PAIRING                                  (0x90)
#define HID_CMD_QUALCOMM_CANCEL_PAIRING                                 (0x91)
#define HID_CMD_QUALCOMM_GET_BD_ADDR                                    (0x92)
#define HID_CMD_QUALCOMM_SET_BD_ADDR                                    (0x93)
/* 0x94 ~ 0x96 reserved */
#define HID_CMD_QUALCOMM_SET_TX_PWR                                     (0x97)
#define HID_CMD_QUALCOMM_START_CONT_TX                                  (0x98)
#define HID_CMD_QUALCOMM_PAUSE_RADIO_TEST                               (0x99)
#define HID_CMD_QUALCOMM_UPGRADE_COMMAND                                (0x9A)
#define HID_CMD_QUALCOMM_UPGRADE_DATA_TRANSFER                          (0x9B)
#define HID_CMD_QUALCOMM_UPGRADE_RESPONSE_GET                           (0x9C)
#define HID_CMD_QUALCOMM_UPGRADE_RESPONSE_CLR                           (0x9D)
/* 0x9E ~ 0x9F Reserved */
#define HID_CMD_QUALCOMM_CLEAR_PDL                                      (0xA0)
#define HID_CMD_QUALCOMM_SET_DUT                                        (0xA1)
/* 0xA2 ~ 0xAF Reserved */
#define HID_CMD_QUALCOMM_START_BLE_BONDING                              (0xB0)

/**
 * @brief   The identifier of USB HID sub commands
 * @details List all of the sub command definitions used in USB HID commands
 * @note
 */
#define HID_SUBCMD_SET_AVNERA_ARBITER_ID                                (0x00)
#define HID_SUBCMD_GET_AVNERA_ARBITER_ID                                (0x01)
#define HID_SUBCMD_SAVE_AVNERA_ARBITER_ID                               (0x02)
#define HID_SUBCMD_GET_AVNERA_LINK_STATE                                (0x03)

#define HID_SUBCMD_SET_AVNERA_MODULATION_MODE                           (0x00)
#define HID_SUBCMD_SET_AVNERA_CARRIER                                   (0x01)
#define HID_SUBCMD_SET_AVNERA_CHANNEL                                   (0x02)
#define HID_SUBCMD_SET_AVNERA_ANTENNA                                   (0x03)
#define HID_SUBCMD_SET_AVNERA_TXPWR                                     (0x04)
#define HID_SUBCMD_SET_AVNERA_FIXED_FREQ                                (0x05)
#define HID_SUBCMD_ENABLE_AVNERA_CONT_TX                                (0x06)
#define HID_SUBCMD_ENABLE_AVNERA_CONT_RX                                (0x07)

#define HID_SUBCMD_START_AVNERA_MAC_STATS                               (0x00)
#define HID_SUBCMD_STOP_AVNERA_MAC_STATS                                (0x01)
#define HID_SUBCMD_CLEAR_AVNERA_MAC_STATS                               (0x02)

bool hid_command_handler_proj(hid_message_t *command, hid_message_t *response);

#endif /* _HID_PROJ_H_ */

/** @} */

