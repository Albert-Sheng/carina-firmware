#pragma once

#define DBG_SERIAL                              LPSD1

#define USB_VID                                 0x9886
#define USB_PID_PC                              0x0049

// Used as PID for APP_INFO struct
#define USB_PID                                 USB_PID_PC

#define NUM_SWITCHES                            0
#define NUM_BUTTONS                             3
#define NUM_ENCODERS                            2
#define NUM_COMBO_BUTTONS                       0

// HID
#define HID_PACKET_SIZE                         64
#define HID_THREAD_STACK_SIZE                   2048

// DFU
#define DFU_MAX_ERASE_DURATION_SECS             (30)        // 30 secs
#define DFU_MAX_VERIFY_DURATION_SECS            (5)         // 5 secs
#define DFU_MAX_INSTALL_DURATION_SECS           (4 * 60)    // 4 mins
#define DFU_THREAD_STACK_SIZE                   1024
#define DFU_THREAD_PRIORITY                     LOWPRIO

// CentPP
#define CENTPP_MAX_TRANSPORT_BUFFER_LENGTH      512
#define CENTPP_THREAD_STACK_SIZE                1024
#define CENTPP_MAX_TRANSPORT_COUNT              2

// Misc.
#define AVNERA_CHIP_ID                          SKY7635121_CHIP_ID
#define QUALCOMM_CHIP_ID                        QCC3024_CHIP_ID
#define DSP_CHIP_ID                             CS47L90_CHIP_ID
