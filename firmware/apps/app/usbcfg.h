/**
 * @file    usbcfg.h
 * @brief   USB config header.
 *
 * @addtogroup USB
 * @{
 */

#ifndef USBCFG_H
#define USBCFG_H

#include "usb.h"
#include "hid_proj.h"
#include "usb_hid_transport.h"
#include "usb_audio_transport.h"

extern USBHIDDriver UHD1;
extern USBAUDIODriver UAD2;
extern USBAUDIODriver UAD3;

#define USB_STREAM_CHANNELS             2U
#define USB_STREAM_BITS                 32U
#define USB_STREAM_FREQ_1               48000U

#define USB_TONES_CHANNELS              2U
#define USB_TONES_BITS                  32U
#define USB_TONES_FREQ_1                48000U

/*
 * USB endpoint packet sizes
 */
#define STREAM_PACKET_SIZE          (AUDIO_PACKET_SIZE(USB_STREAM_FREQ_1, USB_STREAM_CHANNELS, USB_STREAM_BITS))
#define STREAM_PACKET_SIZE_MAX      STREAM_PACKET_SIZE

#define TONES_PACKET_SIZE           (AUDIO_PACKET_SIZE(USB_TONES_FREQ_1, USB_TONES_CHANNELS, USB_TONES_BITS))
#define TONES_PACKET_SIZE_MAX       TONES_PACKET_SIZE
#define TONES_FB_PACKET_SIZE        4
#define TONES_FB_PACKET_SIZE_MAX    TONES_FB_PACKET_SIZE

#ifdef __cplusplus
extern "C" {
#endif

const USBModeConfig_t *usbGetModePC(void);
size_t hidGetReport(uint8_t id, uint8_t *bp, size_t n);
msg_t hidSetReport(uint8_t id, uint8_t *bp, size_t n);

#ifdef __cplusplus
}
#endif

#endif  /* USBCFG_H */

/** @} */
