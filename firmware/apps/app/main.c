#include "led.h"
#include "coprocessor.h"
#include "user_config.h"
#include "gfx.h"
#if ENABLE_MEMFAULT
#include "memfault/components.h"
#include "memfault/core/trace_event.h"
#endif
#include "avnera.h"
#include "cs47l90.h"
#include "dsp.h"
#include "qualcomm.h"
#include "ui.h"
#include "io.h"
#include "sys_proj.h"
#include "dfu.h"
#include "app.h"
#include "sai_proj.h"
#include "usbcfg.h"
#include "usb.h"
#include "common/centpp/centpp.h"
#include "pga2505/pga2505.h"
#include "wireless.h"
#include "usbh_proj.h"

static const SerialConfig serial_Config = {
    .speed = 115200,
    .cr1 = 0,
    .cr2 = USART_CR2_STOP1_BITS | USART_CR2_LINEN,
    .cr3 = 0,
};

static const ADCConversionGroup hwrev_adc_config = {
    .circular       = false,
    .num_channels   = 1,
    .end_cb         = NULL,
    .error_cb       = NULL,
    .cfgr           = ADC_CFGR_CONT | ADC_CFGR_RES_12BITS | ADC_CFGR_OVRMOD,
    .pcsel          = ADC_SELMASK_IN4,
    .smpr[0]        = ADC_SMPR1_SMP_AN4(ADC_SMPR_SMP_2P5),
    .sqr[0]         = ADC_SQR1_SQ1_N(ADC_CHANNEL_IN4),
};

/*
 * I2C config for 100KHz clock
 * I2C Clock = 32Mhz / Prescaler of 8 = 4Mhz
 */
static const I2CConfig i2cconfig = {
    STM32_TIMINGR_PRESC(8U)  |
    STM32_TIMINGR_SCLDEL(4U) | STM32_TIMINGR_SDADEL(2U) |
    STM32_TIMINGR_SCLH(15U)  | STM32_TIMINGR_SCLL(19U),
    0,
    0
};

// Device driver instances
pga2505 pga2505_inst;

/*
 * SDRAM driver configuration structure.
 */
static const SDRAMConfig sdram_cfg = {
    .sdcr = (uint32_t)(FMC_ColumnBits_Number_8b |
                       FMC_RowBits_Number_12b |
                       FMC_SDMemory_Width_16b |
                       FMC_InternalBank_Number_4 |
                       FMC_CAS_Latency_3 |
                       FMC_Write_Protection_Disable |
                       FMC_SDClock_Period_2 |
                       FMC_Read_Burst_Enable |
                       FMC_ReadPipe_Delay_0),

    .sdtr = (uint32_t)((3   -  1) |  // FMC_LoadToActiveDelay = 3 (TMRD: 3 Clock cycles)
                       (12 <<  4) |  // FMC_ExitSelfRefreshDelay = 12 (TXSR: min=67ns (12x6=72))
                       (10 <<  8) |  // FMC_SelfRefreshTime = 10 (TRAS: min=60ns)
                       (12 << 12) |  // FMC_RowCycleDelay = 7 (TRC: min=67 (12x=72))
                       (2 << 16) |   // FMC_WriteRecoveryTime = 2 (TWR:  min=1+6ns (12ns))
                       (2 << 20) |   // FMC_RPDelay = 2 (TRP)
                       (2 << 24)),   // FMC_RCDDelay = 2 (TRCD)

    .sdcmr = (uint32_t)((FMC_SDCMR_NRFS_3) |
                        ((FMC_SDCMR_MRD_BURST_LENGTH_1 |
                          FMC_SDCMR_MRD_BURST_TYPE_SEQUENTIAL |
                          FMC_SDCMR_MRD_CAS_LATENCY_3 |
                          FMC_SDCMR_MRD_OPERATING_MODE_STANDARD |
                          FMC_SDCMR_MRD_WRITEBURST_MODE_SINGLE) << 9)),

    /* if (STM32_SYSCLK == 200000000) ->
       64ms / 4096 = 15.625us
       15.625us * 100MHz = 1563 - 20 = 1543  = 0x607*/
    .sdrtr = (uint32_t)(1855 << 1),
};

User_Config_t userConfig __attribute__((section(".ram0")));

static THD_WORKING_AREA(waHID, 1024);
static THD_WORKING_AREA(waIO,  1024);
static THD_WORKING_AREA(waUI,  1024);
static THD_WORKING_AREA(waSys, 1024);
static THD_WORKING_AREA(waWireless, 1024);

static void centpp_features_register(void) {
    // ToDo: Implement centpp features currently commented
    //CentPPRegister_iDeviceInfo();
    //CentPPRegister_iDeviceName(&s_centpp_cfg.iname_cfg);
    //CentPPRegister_iMixer(&s_centpp_cfg.imixer_cfg);
    //CentPPRegister_iVolumeSet(&s_centpp_cfg.ivolume_cfg);
    //CentPPRegister_iHeadsetMix(&s_centpp_cfg.iheadsetmix_cfg);
    //CentPPRegister_iEqSet(&s_centpp_cfg.ieqset_cfg);
    CentPPRegister_iGenericDFU(&s_centpp_cfg.igenericdfu_cfg);
}

static void app_soft_reset(void)
{
    //USB_stop();
    __NVIC_SystemReset();
}

/*
 * Application entry point.
 */
int main(void) {
    static const dfu_callback_t s_dfu_cb = {
        .soft_reset_cb = app_soft_reset
    };

    /*
    * System initializations.
    * - HAL initialization, this also initializes the configured device drivers
    *   and performs the board-specific initializations.
    * - Kernel initialization, the main() function becomes a thread and the
    *   RTOS is active.
    */
    halInit();
    chSysInit();

    /*
     * Debug UART Initialization.
     * - LPUART module is initialized.
     * - Hardware revision and header information is printed out.
     */
    sdStart(&DBG_SERIAL, &serial_Config);
    Dbg_hwrev_init(&ADCD1, &hwrev_adc_config, GPIOG, 3, GPIOC, 4);
    Dbg_print_header();

    i2cStart(&I2CD2, &i2cconfig);

    /*
     * Avnera Reset is asserted as early as possible.
     */
    avneraReset();
    chThdSleepMilliseconds(800);

    /*
     * USB initializations.
     * - USB HS PHY reset is released.
     * - Delay placed for the PHY 60MHz clock to stabilize.
     * - USBD2 is started and bus is connected.
     */
    const USBModeConfig_t * const usb_mode = usbGetModePC();
    palSetPad(GPIOD, 2);
    chThdSleepMilliseconds(1);
    usbModeStart(&USBD2, usb_mode);

    /*
     * Initialize SDRAM
     */
    sdramInit();
    sdramStart(&SDRAMD1, &sdram_cfg);

    /*
     * Initialize coProcessor
     */
    coProcessorInit();
    coProcessorStart();

    /*
     * Reset the CS47L90.
     */
    dspReset();

    // PGA2505
    {
        // Power on
        // This happens via the co-processor on the main board
        if (MSG_OK != coProcessorEnablePGA2505Power(TRUE))
            chSysHalt("PGA2505 power-on failed.");

        // Initalize driver
        if (PGA2505_OK != pga2505_init(&pga2505_inst))
            chSysHalt("PGA2505 initialization failed.");

        // Set initial gain
        if (PGA2505_OK != pga2505_set_preamp_gain(&pga2505_inst, 0))
            Dbg_printf(DBG_WARN, "setting initial gain on pga2505 failed.");
    }

    ledInit();

    /*
     * DSP Initialization.
     */
    dspInit();
    dspStart();

    /*
     * Avnera Initialization.
     */
    avneraInit();
    avneraStart();

    /*
     * Qualcomm Initialization.
     */
    qualcommInit();
    qualcommStart();

    /*
     * Imperium Initialization.
     */
    imperiumReset();
    imperiumStart();

    /*
     * Initialize uGFX library.
     * The GDISP initialization will initialize the display infrastructure.
     */
    gfxInit();

    chThdCreateStatic(waHID, sizeof(waHID), LOWPRIO+2, HID_Thread, NULL);
    chThdCreateStatic(waWireless, sizeof(waWireless), LOWPRIO+1, WIRELESS_Thread, NULL);
    chThdCreateStatic(waIO, sizeof(waIO), LOWPRIO, IO_Thread, NULL);
    chThdCreateStatic(waUI,  sizeof(waUI),  NORMALPRIO+2, UI_Thread, NULL);
    chThdCreateStatic(waSys, sizeof(waSys), NORMALPRIO+1, SYS_Thread, NULL);

    Dbg_printf(DBG_INFO, "Startup done.");

    DFU_init(&s_dfu_cb);

#if ENABLE_CENTPP
    /*
     * CentPP Initialization.
     * - Start CentPP engine.
     * - Check if Avnera external flash partition for CentPP is available and load contents into memory.
     */
    s_centpp_cfg.register_features_cb = centpp_features_register;
    CentPP_Start(&s_centpp_cfg);
    if(!avneraGetFlashOpSts()) {
        avneraReadFlash((uint8_t*)&userConfig, AVNERA_EXT_FLASH_CENTPP_ADDR, sizeof(User_Config_t));
    }
#endif

    /*
     * SAI Initialization.
     */
    SAI_Start();

    /*
     * USB Host Initialization.
     */
    usbhInit();
    usbhStart();

#if ENABLE_MEMFAULT
    /*
     * Memfault Initialization.
     */
    memfault_platform_boot();

    /**********test_trace****************/
    void test_trace(void) {
        MEMFAULT_TRACE_EVENT_WITH_LOG(critical_error,
                                      "A test error trace!");
    }
    test_trace();
    /************************************/


    /****** test_assert *****************/
    int test_assert(void) {
        MEMFAULT_ASSERT(0);
        return -1; // should never get here
    }
    test_assert();
    /************************************/


    /*********test_faut*****************/
    void test_fault(void) {
        void (*bad_func)(void) = (void *)0xEEEEDEAD;
        bad_func();
        //return -1; // should never get here
    }
    test_fault();
    /***********************************/



    /************test_reboot*****************/
    int test_reboot(void) {
        memfault_reboot_tracking_mark_reset_imminent(kMfltRebootReason_UserReset, NULL);
        memfault_platform_reboot();
    }
    test_reboot();
    /*****************************************/


    /**********Heart beat**********************/

    void memfault_metrics_heartbeat_collect_data(void) {
        // NOTE: When using FreeRTOS we can just call
        // "uxTaskGetStackHighWaterMark(s_main_task_tcb)"
        const uint32_t stack_high_water_mark = 1;// TODO: code to get high water mark
        memfault_metrics_heartbeat_set_unsigned(
            MEMFAULT_METRICS_KEY(MainTaskWakeups), stack_high_water_mark);
    }
    memfault_metrics_heartbeat_collect_data();
    memfault_metrics_heartbeat_debug_trigger();
    /********************************************/





    /*************test coredump storage************/
    int test_coredump_storage(void) {

        // Note: Coredump saving runs from an ISR prior to reboot so should
        // be safe to call with interrupts disabled.
        //your_platform_disable_interrupts();
        memfault_coredump_storage_debug_test_begin();
        //your_platform_enable_interrupts();

        memfault_coredump_storage_debug_test_finish();
        return 0;
    }
    test_coredump_storage();
    /**********************************************/


    memfault_data_export_dump_chunks();

    /***********send memfault data****************/
    bool try_send_memfault_data(void) {
        // buffer to copy chunk data into
        uint8_t buf[128] = {0};
        size_t buf_len = sizeof(buf);

        bool data_available = memfault_packetizer_get_chunk(buf, &buf_len);
        if(!data_available) {
            return false; // no more data to send
        }

        // send payload collected to chunks endpoint
        MEMFAULT_LOG_INFO("memfault_packetizer_get_chunk: %d, %d", buf_len, strlen((char*)buf));
        //user_transport_send_chunk_data(buf, buf_len);
        return true;
    }

    bool send_memfault_data(void) {
        if(!memfault_packetizer_data_available()) {
            return false; // no new data to send
        }

        // [... user specific logic deciding when & how much data to send
        if(try_send_memfault_data()) {
            MEMFAULT_LOG_INFO("Chunk data available2");
        }
        return true;
    }
    send_memfault_data();
    /****************************************************/
#endif

    while(1) {
        chThdSleepMilliseconds(500);
    }

    return 0;
}
