/**
 * @file    usbh_proj.h
 * @brief   USB Host Module header.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */
#ifndef _USBH_PROJ_H_
#define _USBH_PROJ_H_

/*===========================================================================*/
/* Module constants.                                                         */
/*===========================================================================*/
// Buffer USBH_BUFFER_MULT frames
#define USBH_BUFFER_MULT                                                (40)
// Used to tune the package size in case overrun or underrun
#define USBH_SAMPLE_DELTA                                               (2)

#define USBH_AUDIO_FRAME_SIZE(bits, channel)                            ((((bits)+7)/8) * channel)
#define USBH_AUDIO_SUBFRAME_SIZE(bits)                                  (((bits)+7)/8)
#define USBH_AUDIO_PACKET_SIZE(freq, channels, bits)    (((freq) * (channels) * (USBH_AUDIO_SUBFRAME_SIZE(bits)))/1000)
#define USBH_AUDIO_PACKET_SAMPLES(freq)                                 ((freq)/1000)

// The USB host Game Mixer Sample rate/Bit width/channels
#define USBH_GAME_MIX_SAMPLE_RATE                                       (48000)
#define USBH_GAME_MIX_BITS                                              (16)
#define USBH_GAME_MIX_CHANNELS                                          (2)
// USB Host Game Mixer package size
#define USBH_GAME_MIX_PACKET_SIZE       USBH_AUDIO_PACKET_SIZE(USBH_GAME_MIX_SAMPLE_RATE, USBH_GAME_MIX_CHANNELS, USBH_GAME_MIX_BITS)
#define USBH_GAME_MIX_PACKET_SIZE_MAX   (USBH_AUDIO_PACKET_SIZE(USBH_GAME_MIX_SAMPLE_RATE, USBH_GAME_MIX_CHANNELS, USBH_GAME_MIX_BITS) + (USBH_AUDIO_SUBFRAME_SIZE(USBH_GAME_MIX_BITS) * USBH_GAME_MIX_CHANNELS * USBH_SAMPLE_DELTA))
#define USBH_GAME_MIX_PACKET_SIZE_MIN   (USBH_AUDIO_PACKET_SIZE(USBH_GAME_MIX_SAMPLE_RATE, USBH_GAME_MIX_CHANNELS, USBH_GAME_MIX_BITS) - (USBH_AUDIO_SUBFRAME_SIZE(USBH_GAME_MIX_BITS) * USBH_GAME_MIX_CHANNELS * USBH_SAMPLE_DELTA))
// USB Host Game Mixer buffer size
#define USBH_GAME_MIX_BUFFER_SIZE       (USBH_BUFFER_MULT * USBH_GAME_MIX_PACKET_SIZE)
#define USBH_GAME_MIX_BUFFER_HIGH_TH    (3 * USBH_GAME_MIX_BUFFER_SIZE / 4)
#define USBH_GAME_MIX_BUFFER_MID_TH     (USBH_GAME_MIX_BUFFER_SIZE / 2)
#define USBH_GAME_MIX_BUFFER_LOW_TH     (USBH_GAME_MIX_BUFFER_SIZE / 4)

// The USB host Mic Sample rate/Bit width/channels
#define USBH_MIC_SAMPLE_RATE                                            (48000)
#define USBH_MIC_BITS                                                   (16)
#define USBH_MIC_CHANNELS                                               (2)
// USB Host Mic frame/package size
#define USBH_MIC_FRAME_SIZE             USBH_AUDIO_FRAME_SIZE(USBH_MIC_BITS, USBH_MIC_CHANNELS)
#define USBH_MIC_PACKET_SIZE            USBH_AUDIO_PACKET_SIZE(USBH_MIC_SAMPLE_RATE, USBH_MIC_CHANNELS, USBH_MIC_BITS)
#define USBH_MIC_PACKET_SIZE_MAX        (USBH_AUDIO_PACKET_SIZE(USBH_MIC_SAMPLE_RATE, USBH_MIC_CHANNELS, USBH_MIC_BITS) + (USBH_AUDIO_SUBFRAME_SIZE(USBH_MIC_BITS) * USBH_MIC_CHANNELS * USBH_SAMPLE_DELTA))
#define USBH_MIC_PACKET_SIZE_MIN        (USBH_AUDIO_PACKET_SIZE(USBH_MIC_SAMPLE_RATE, USBH_MIC_CHANNELS, USBH_MIC_BITS) - (USBH_AUDIO_SUBFRAME_SIZE(USBH_MIC_BITS) * USBH_MIC_CHANNELS * USBH_SAMPLE_DELTA))
// USB Host Mic buffer size
#define USBH_MIC_BUFFER_SIZE            (USBH_BUFFER_MULT * USBH_MIC_PACKET_SIZE)
#define USBH_MIC_BUFFER_HIGH_TH         (3 * USBH_MIC_BUFFER_SIZE / 4)
#define USBH_MIC_BUFFER_MID_TH          (USBH_MIC_BUFFER_SIZE / 2)
#define USBH_MIC_BUFFER_LOW_TH          (USBH_MIC_BUFFER_SIZE / 4)

/*===========================================================================*/
/* Module pre-compile time settings.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Module constants and error checks.                                       */
/*===========================================================================*/


/*===========================================================================*/
/* Module data structures and types.                                         */
/*===========================================================================*/
typedef enum {
    APPLICATION_IDLE = 0,
    APPLICATION_START,
    APPLICATION_READY,
    APPLICATION_DISCONNECT,
} AUDIO_ApplicationTypeDef;

typedef enum {
    AUDIO_PLAYBACK_STATE_IDLE = 0,
    AUDIO_PLAYBACK_STATE_INIT,
    AUDIO_PLAYBACK_STATE_CONFIG,
    AUDIO_PLAYBACK_STATE_PLAY,
    AUDIO_PLAYBACK_STATE_PLAYING,
    AUDIO_PLAYBACK_STATE_WAIT,
    AUDIO_PLAYBACK_STATE_PAUSE,
    AUDIO_PLAYBACK_STATE_RESUME,
    AUDIO_PLAYBACK_STATE_ERROR,
} AUDIO_PLAYBACK_StateTypeDef;

typedef enum {
    AUDIO_RECORD_STATE_IDLE = 0,
    AUDIO_RECORD_STATE_INIT,
    AUDIO_RECORD_STATE_CONFIG,
    AUDIO_RECORD_STATE_RECORD,
    AUDIO_RECORD_STATE_RECORDING,
    AUDIO_RECORD_STATE_WAIT,
    AUDIO_RECORD_STATE_PAUSE,
    AUDIO_RECORD_STATE_RESUME,
    AUDIO_RECORD_STATE_ERROR,
} AUDIO_RECORD_StateTypeDef;

/*===========================================================================*/
/* Module macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Module exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

bool usbhReadAudioDataFromIq(uint32_t *buf, input_queue_t *iqp, size_t n);

void usbhInit(void);
void usbhDeInit(void);
void usbhStart(void);
void usbhStop(void);

bool isUsbhMicActive(void);
bool isUsbhGameMixActive(void);

#ifdef __cplusplus
}
#endif

#endif /* _USBH_PROJ_H_ */

/** @} */

