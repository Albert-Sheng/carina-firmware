/**
 * @file    usbcfg.c
 * @brief   USB config.
 *
 * @addtogroup USB
 * @{
 */
#include "usbcfg.h"
#include "usb_desc_audio.h"
#include "usb_descriptors.h"

#include "hal_usb.h"

#include "project.h"
#include "build_version.h"

/*
 * USB Endpoints definitions.
 */
#define USB_ENDPOINT_STREAM             1U
#define USB_ENDPOINT_TONES              2U
#define USB_ENDPOINT_HID                3U
/* Includes both IN & OUT endpoint directions if used. */
#define USB_STREAM_ENDPOINT_TOTAL       1U
#define USB_TONES_ENDPOINT_TOTAL        2U
#define USB_HID_ENDPOINT_TOTAL          2U

/*
 * USB string indices
 */
#define STRING_LANG_INDEX               0U
#define STRING_MANUF_INDEX              1U
#define STRING_PRODUCT_INDEX            2U
#define STRING_STREAM_INDEX             3U
#define STRING_TONES_INDEX              4U

/*
 * USB interface addresses
 */
#define INTERFACE_AUDIO_CONTROL_STREAM  0U
#define INTERFACE_AUDIO_STREAM          1U
#define INTERFACE_AUDIO_CONTROL_TONES   2U
#define INTERFACE_AUDIO_TONES           3U
#define INTERFACE_HID                   4U

/*
 * Total USB interfaces for each group
 */
#define INTERFACE_COUNT_STREAM          2U
#define INTERFACE_COUNT_TONES           2U
#define INTERFACE_COUNT_HID             1U

/*
 * Total USB interfaces in all
 */
#define INTERFACE_TOTAL                 (INTERFACE_COUNT_STREAM + INTERFACE_COUNT_TONES + INTERFACE_COUNT_HID)

/*
 * USB Clock attributes
 */
#define CLOCK_SOURCE_STREAM_ID          1U
#define CLOCK_SELECTOR_STREAM_ID        2U
#define CLOCK_SELECTOR_STREAM_PINS      1U
#define CLOCK_SOURCE_TONES_ID           3U
#define CLOCK_SELECTOR_TONES_ID         4U
#define CLOCK_SELECTOR_TONES_PINS       1U

/*
 * USB Feedback rate values
 */

/*
 * USB audio input/output terminal IDs
 */
#define TERMINAL_IN_DSP_STREAM_ID       5U
#define TERMINAL_OUT_STREAM_USB_ID      6U
#define TERMINAL_IN_USB_TONES_ID        7U
#define TERMINAL_OUT_TONES_HEADPHONES   8U

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * Capability UUID of Microsoft. Supports
 * Microsoft Windows v8.1 and later.
 */
#define MS_UUID_TIME_LOW                0xD8DD60DFU
#define MS_UUID_TIME_MID                0x4589U
#define MS_UUID_TIME_HIGH               0x4CC7U
#define MS_UUID_CLOCK_SEQ               0xD29CU
#define MS_UUID_NODE_ONE                0x649E9D65U
#define MS_UUID_NODE_TWO                0x9F8AU

/*
 * Total number of device capability descriptors
 */
#define USB_DEVICECAPS_TOTAL            1U

/*
 * NTDDI Windows version constants taken from sdkddkver.h
 * of the Windows 10 SDK. Used by the MS_PlatformDesc_t struct
 * below.
 */
#define NTDDI_WINBLUE                   0x06030000  /* Win 8.1 */
#define NTDDI_WINTHRESHOLD              0x0A000000  /* ABRACADABRA_THRESHOLD */
#define NTDDI_WIN10                     0x0A000000  /* ABRACADABRA_THRESHOLD */
#define NTDDI_WIN10_TH2                 0x0A000001  /* ABRACADABRA_WIN10_TH2 */
#define NTDDI_WIN10_RS1                 0x0A000002  /* ABRACADABRA_WIN10_RS1 */
#define NTDDI_WIN10_RS2                 0x0A000003  /* ABRACADABRA_WIN10_RS2 */
#define NTDDI_WIN10_RS3                 0x0A000004  /* ABRACADABRA_WIN10_RS3 */
#define NTDDI_WIN10_RS4                 0x0A000005  /* ABRACADABRA_WIN10_RS4 */
#define NTDDI_WIN10_RS5                 0x0A000006  /* ABRACADABRA_WIN10_RS5 */
#define NTDDI_WIN10_19H1                0x0A000007  /* ABRACADABRA_WIN10_19H1*/

/*
 * Microsoft specific Descriptors
 */
#pragma pack(push,1)
typedef struct {
    uint32_t        dwWindowsVersion;
    uint16_t        wMSOSDescriptorSetTotalLength;
    uint8_t         bMS_VendorCode;
    uint8_t         bAltEnumCode;
} desc_MS_InformationSet_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint32_t        dwWindowsVersion;
    uint16_t        wTotalLength;
} desc_SetHeader_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint8_t         bConfigurationValue;
    uint8_t         bReserved;
    uint16_t        wTotalLength;
} desc_ConfigSubset_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint8_t         bFirstInterface;
    uint8_t         bReserved;
    uint16_t        wSubsetLength;
} desc_FuncSubset_t;

typedef struct {
    uint16_t        wLength;
    uint16_t        wDescriptorType;
    uint8_t         CompatibleID[8];
    uint8_t         SubCompatibleID[8];
} desc_CompatibleID_t;
#pragma pack(pop)

#define MS_OS_20_SET_HEADER_DESCRIPTOR          0x00U
#define MS_OS_20_SUBSET_HEADER_CONFIGURATION    0x01U
#define MS_OS_20_SUBSET_HEADER_FUNCTION         0x02U
#define MS_OS_20_FEATURE_COMPATIBLE_ID           0x03U

/*
 * Microsoft specific vendor codes used to retreive
 * set descriptors on Windows OS's.
 */
#define USB_MS_SET1_VENDOR_CODE         0x80U
#endif

/*
 * USB endpoint periodic interval
 * Refer to the below link for calculating the interval.
 * https://docs.microsoft.com/en-us/windows-hardware/drivers/usbcon/transfer-data-to-isochronous-endpoints
 * The STM32H750 requires the interval rate to be 1, since it expects to transmit at least 1 packet every
 * frame (FS) or micro-fram (HS).
 */
#define PERIODIC_INTERVAL_AUDIO         1U
#define PERIODIC_INTERVAL_HID           4U
#define PERIODIC_INTERVAL_FEEDBACK      4U

/*
 * USB HID Driver structure.
 */
USBHIDDriver UHD1;
USBAUDIODriver UAD2;
USBAUDIODriver UAD3;

/*
 * Variable used to report/set the current sampling frequency.
 * Must be uint32_t when reporting current sampling frequency
 * according to UAC v2.0 spec.
 */
static uint32_t current_fs __attribute__((section(".ram3_init"))) = { USB_STREAM_FREQ_1 };
static uint32_t current_tone_fs __attribute__((section(".ram3_init"))) = { USB_TONES_FREQ_1 };

/*
 * Supported sampling frequencies.
 * Reporting sampling frequency range requires layout 3 block
 * according to UAC v2.0 spec.
 */
#define USB_STREAM_FS_COUNT 1
static usbaudio2_range3_t fs __attribute__((section(".ram3_init"))) = {
    .size = USB_STREAM_FS_COUNT,
    .ranges = {
        {USB_STREAM_FREQ_1, USB_STREAM_FREQ_1, 0},
    },
};

#define USB_TONES_FS_COUNT 1
static usbaudio2_range3_t tone_fs __attribute__((section(".ram3_init"))) = {
    .size = USB_TONES_FS_COUNT,
    .ranges = {
        {USB_TONES_FREQ_1, USB_TONES_FREQ_1, 0},
    },
};

/*
 * USB HID driver configuration.
 */
static const USBHIDConfig usbhidcfg = {
    &USBD2,
    USB_ENDPOINT_HID,
    USB_ENDPOINT_HID
};

/*
 * USB Audio driver configuration for stream audio.
 */
static output_buffers_queue_t           stream_obq;

#define USB_STREAM_BUFFER_NUMBER        8
#define USB_STREAM_BUFFER_SIZE          384

static uint8_t stream_ob[BQ_BUFFER_SIZE(USB_STREAM_BUFFER_NUMBER,
                                        USB_STREAM_BUFFER_SIZE)] __attribute__((section(".ram3"))) = { 0 };

static const USBAUDIOConfig usbaudio_streamcfg = {
    .usbp       = &USBD2,
    .int_in     = USB_ENDPOINT_STREAM,
    .int_out    = 0,
    .bcfg       = {
        .ibqueue = NULL,
        .obqueue = &stream_obq,
        .ibp = NULL,
        .obp = stream_ob,
        .ibs = 0,
        .obs = USB_STREAM_BUFFER_SIZE,
        .ibn = 0,
        .obn = USB_STREAM_BUFFER_NUMBER,
    },
};

/*
 * USB Audio driver configuration for tones audio.
 */
static input_buffers_queue_t            tone_ibq;
static output_buffers_queue_t           tone_obq;

#define USB_TONE_AUDIO_BUFFER_NUMBER    8
#define USB_TONE_AUDIO_BUFFER_SIZE      384

#define USB_TONE_FB_BUFFER_NUMBER       8
#define USB_TONE_FB_BUFFER_SIZE         4

static uint8_t tone_ib[BQ_BUFFER_SIZE(USB_TONE_AUDIO_BUFFER_NUMBER,
                                      USB_TONE_AUDIO_BUFFER_SIZE)] __attribute__((section(".ram3"))) = { 0 };
static uint8_t tone_ob[BQ_BUFFER_SIZE(USB_TONE_FB_BUFFER_NUMBER,
                                      USB_TONE_FB_BUFFER_SIZE)] __attribute__((section(".ram3"))) = { 0 };

static const USBAUDIOConfig usbaudio_tonescfg = {
    .usbp       = &USBD2,
    .int_in     = USB_ENDPOINT_TONES,
    .int_out    = USB_ENDPOINT_TONES,
    .bcfg       = {
        .ibqueue = &tone_ibq,
        .obqueue = &tone_obq,
        .ibp = tone_ib,
        .obp = tone_ob,
        .ibs = USB_TONE_AUDIO_BUFFER_SIZE,
        .obs = USB_TONE_FB_BUFFER_SIZE,
        .ibn = USB_TONE_AUDIO_BUFFER_NUMBER,
        .obn = USB_TONE_FB_BUFFER_NUMBER,
    },
};

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * USB Device Descriptor.
 */
static const desc_Device_t device_descriptor_data = {
    .bLength            = sizeof(desc_Device_t),
    .bDescriptorType    = USB_DESCRIPTOR_DEVICE,
    .bcdUSB             = 0x0201,
    .bDeviceClass       = USB_DEVICE_CLASS_MISC,
    .bDeviceSubClass    = USB_DEVICE_SUBCLASS_COMMON,
    .bDeviceProtocol    = USB_DEVICE_PROTOCOL_IAD,
    .bMaxPacketSize     = 64,
    .idVendor           = USB_VID,
    .idProduct          = USB_PID_PC,
    .bcdDevice          = ((VERSION_VMAJOR << 8) | VERSION_VMINOR),
    .iManufacturer      = STRING_MANUF_INDEX,
    .iProduct           = STRING_PRODUCT_INDEX,
    .iSerialNumber      = 0,
    .bNumConfigurations = 1
};
#else
/*
 * USB Device Descriptor.
 */
static const desc_Device_t device_descriptor_data = {
    .bLength            = sizeof(desc_Device_t),
    .bDescriptorType    = USB_DESCRIPTOR_DEVICE,
    .bcdUSB             = 0x0200,
    .bDeviceClass       = USB_DEVICE_CLASS_MISC,
    .bDeviceSubClass    = USB_DEVICE_SUBCLASS_COMMON,
    .bDeviceProtocol    = USB_DEVICE_PROTOCOL_IAD,
    .bMaxPacketSize     = 64,
    .idVendor           = USB_VID,
    .idProduct          = USB_PID_PC,
    .bcdDevice          = ((VERSION_MAJOR << 8) | VERSION_MINOR),
    .iManufacturer      = STRING_MANUF_INDEX,
    .iProduct           = STRING_PRODUCT_INDEX,
    .iSerialNumber      = 0,
    .bNumConfigurations = 1
};
#endif

#if USB_PC_USE_DEVICE_QUALIFIER
/*
 * USB Device Qualifier Descriptor.
 */
static const desc_Qualifier_t device_qualifier_descriptor_data = {
    .bLength            = sizeof(desc_Qualifier_t),
    .bDescriptorType    = USB_DESCRIPTOR_DEVICE,
    .bcdUSB             = 0x0200,
    .bDeviceClass       = device_descriptor_data.bDeviceClass,
    .bDeviceSubClass    = device_descriptor_data.bDeviceSubClass,
    .bDeviceProtocol    = device_descriptor_data.bDeviceProtocol,
    .bMaxPacketSize     = 64,
    .bNumConfigurations = 1,
    .bReserved          = 0
};

/*
 * Device Qualifier Descriptor wrapper.
 */
static const USBDescriptor device_qualifier_descriptor = {
    .ud_size    = sizeof(device_qualifier_descriptor_data),
    .ud_string  = (uint8_t *)&device_qualifier_descriptor_data
};
#endif

/*
 * Device Descriptor wrapper.
 */
static const USBDescriptor device_descriptor = {
    .ud_size    = sizeof(device_descriptor_data),
    .ud_string  = (uint8_t *)&device_descriptor_data
};

/*
 * HID Report Descriptor
 *
 * This is the description of the format and the content of the
 * different IN or/and OUT reports that your application can
 * receive/send
 *
 * See "Device Class Definition for Human Interface Devices (HID)"
 * (http://www.usb.org/developers/hidpage/HID1_11.pdf) for the
 * detailed description of all the fields
 */
static const uint8_t hid_report_descriptor_data[] = {
    0x06, 0x32, 0xff,       // usage page 0xff32 used to tell software tools to use Report ID's.
    0x09, 0x74,             // usage 0x74

    0xa1, 0x01,             // Collection Application
    0xa1, 0x03,             // Collection Report
    0x85, 0x02,             // Report ID 2

    // Read from device, 64 raw bytes
    0x75, 0x08,             // report size: 8 bits
    0x15, 0x00,             // logical minimum: 0
    0x26, 0xff, 0x00,       // logical maximum: 255
    0x95, 63,               // report count: 63
    0x09, 0x75,             // usage
    0x81, 0x02,             // input (array)

    // Write to device, 64 raw bytes
    0x75, 0x08,             // report size: 8 bits
    0x15, 0x00,             // logical minimum: 0
    0x26, 0xff, 0x00,       // logical maximum: 255
    0x95, 63,               // report count: 63
    0x09, 0x76,             // usage
    0x91, 0x02,             // output (array)
    0xc0,
    0xc0                    // end collection
};

/*
 * Configuration descriptor tree.
 */
static const struct {
    desc_Config_t config;

    desc_InterfaceAssociation_t interface_assoc_stream;

    struct {
        desc_Interface_t                interface;
        usbaudio2_ac_header_desc_t      header;
        usbaudio2_csrc_desc_t           clock_source_i2s;
        usbaudio2_csel_desc_t           clock_selector;
        usbaudio2_iterm_desc_t          iterminal_dsp_mix;
        usbaudio2_oterm_desc_t          oterminal_stream_usb;
    } interface_audio_control_stream;

    struct {
        struct {
            desc_Interface_t            interface;
        } alt0_off;

        struct {
            desc_Interface_t            interface;
            usbaudio2_as_interface_desc_t as_class_interface;
            usbaudio2_fmt_desc_t        format;
            desc_EndpointIsoc_t         endpoint;
            usbaudio2_endpt_desc_t      audio_endpoint;
        } alt1_active;

    } interface_audio_stream;

    desc_InterfaceAssociation_t interface_assoc_tones;

    struct {
        desc_Interface_t                interface;
        usbaudio2_ac_header_desc_t      header;
        usbaudio2_csrc_desc_t           clock_source;
        usbaudio2_csel_desc_t           clock_selector;
        usbaudio2_iterm_desc_t          iterminal_stream_usb;
        usbaudio2_oterm_desc_t          oterminal_headphones;
    } interface_audio_control_tones;

    struct {
        struct {
            desc_Interface_t            interface;
        } alt0_off;

        struct {
            desc_Interface_t            interface;
            usbaudio2_as_interface_desc_t as_class_interface;
            usbaudio2_fmt_desc_t        format;
            desc_EndpointIsoc_t         endpoint;
            usbaudio2_endpt_desc_t      audio_endpoint;
            usbaudio2_fb_endpt_desc_t   fb_endpoint;
        } alt1_active;

    } interface_audio_tones;

    struct {
        desc_Interface_t    interface;
        desc_HID_t          header;
        desc_Endpoint_t     endpoint_in;
        desc_Endpoint_t     endpoint_out;
    } interface_hid;
} configuration_descriptor_data = {
    .config = {
        .bLength                = sizeof(desc_Config_t),
        .bDescriptorType        = USB_DESCRIPTOR_CONFIGURATION,
        .wTotalLength           = sizeof(configuration_descriptor_data),
        .bNumInterface          = INTERFACE_TOTAL,
        .bConfigurationValue    = 1,
        .iConfiguration         = 0,
        .bmAttributes           = (0x80 | USB_CONFIG_ATTR_BUSPOWERED),
        .bMaxPower              = 250,
    },

    .interface_assoc_stream = {
        .bLength                = sizeof(desc_InterfaceAssociation_t),
        .bDescriptorType        = USB_DESCRIPTOR_INTERFACE_ASSOCIATION,
        .bFirstInterface        = INTERFACE_AUDIO_CONTROL_STREAM,
        .bInterfaceCount        = INTERFACE_COUNT_STREAM,
        .bFunctionClass         = USB_AUDIO_FUNCTION,
        .bFunctionSubClass      = USB_AUDIO_FUNCTION_SUBCLASS_UNDEFINED,
        .bFunctionProtocol      = AF_VERSION_02_00,
        .iFunction              = 0,
    },

    .interface_audio_control_stream = {
        .interface = {
            .bLength            = sizeof(desc_Interface_t),
            .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
            .bInterfaceNumber   = INTERFACE_AUDIO_CONTROL_STREAM,
            .bAlternateSetting  = 0,
            .bNumEndpoints      = 0,
            .bInterfaceClass    = USB_AUDIO_CLASS,
            .bInterfaceSubclass = USB_AUDIO_INTERFACE_SUBCLASS_CONTROL,
            .bInterfaceProtocol = USB_AUDIO_INTERFACE_PROTOCOL_IP_VERSION_02_00,
            .iInterface         = STRING_STREAM_INDEX,
        },
        .header = {
            .bLength            = sizeof(usbaudio2_ac_header_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_HEADER,
            .bcdADC             = USB_AUDIO_CLASS_SPEC_2,
            .bCategory          = USB_AUDIO_FUNCTION_CTG_PROAUDIO,
            .wTotalLength       = sizeof(configuration_descriptor_data.interface_audio_control_stream) - sizeof(desc_Interface_t),
            .bmControls         = 0,
        },
        .clock_source_i2s = {
            .bLength            = sizeof(usbaudio2_csrc_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_CLOCK_SOURCE,
            .bClockID           = CLOCK_SOURCE_STREAM_ID,
            .bmAttributes       = USB_AUDIO_CLOCK_SOURCE_ATTR_INTFIXED,
            .bmControls         = USB_AUDIO_CLOCK_FREQ_CTL_R,
            .bAssocTerminal     = 0,
            .iClockSource       = 0,
        },
        .clock_selector = {
            .bLength            = sizeof(configuration_descriptor_data.interface_audio_control_stream.clock_selector),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_CLOCK_SELECTOR,
            .bClockID           = CLOCK_SELECTOR_STREAM_ID,
            .bNrInPins          = CLOCK_SELECTOR_STREAM_PINS,
            .baCSourceID        = CLOCK_SOURCE_STREAM_ID,
            .bmControls         = USB_AUDIO_CLKSEL_CTL_RW,
            .iClockSelector     = 0,
        },
        .iterminal_dsp_mix = {
            .bLength            = sizeof(usbaudio2_iterm_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_INPUT_TERMINAL,
            .bTerminalID        = TERMINAL_IN_DSP_STREAM_ID,
            .wTerminalType      = USB_AUDIO_EXTERN_TERMINAL_DIGITAL,
            .bAssocTerminal     = 0,
            .bCSourceID         = CLOCK_SELECTOR_STREAM_ID,
            .bNrChannels        = USB_STREAM_CHANNELS,
            .bmChannelConfig    = USB_AUDIO_CHANNEL_FL | USB_AUDIO_CHANNEL_FR,
            .iChannelNames      = 0,
            .bmControls         = 0,
            .iTerminal          = 0,
        },
        .oterminal_stream_usb = {
            .bLength            = sizeof(usbaudio2_oterm_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_OUTPUT_TERMINAL,
            .bTerminalID        = TERMINAL_OUT_STREAM_USB_ID,
            .wTerminalType      = USB_AUDIO_USB_TERMINAL_STREAMING,
            .bAssocTerminal     = 0,
            .bSourceID          = TERMINAL_IN_DSP_STREAM_ID,
            .bCSourceID         = CLOCK_SELECTOR_STREAM_ID,
            .bmControls         = 0,
            .iTerminal          = 0,
        },

    },

    .interface_audio_stream = {
        .alt0_off = {
            .interface = {
                .bLength            = sizeof(desc_Interface_t),
                .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
                .bInterfaceNumber   = INTERFACE_AUDIO_STREAM,
                .bAlternateSetting  = 0,
                .bNumEndpoints      = 0,
                .bInterfaceClass    = USB_AUDIO_CLASS,
                .bInterfaceSubclass = USB_AUDIO_INTERFACE_SUBCLASS_AUDIOSTREAMING,
                .bInterfaceProtocol = USB_AUDIO_INTERFACE_PROTOCOL_IP_VERSION_02_00,
                .iInterface         = 0,
            },
        },
        .alt1_active = {
            .interface = {
                .bLength            = sizeof(desc_Interface_t),
                .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
                .bInterfaceNumber   = INTERFACE_AUDIO_STREAM,
                .bAlternateSetting  = 1,
                .bNumEndpoints      = USB_STREAM_ENDPOINT_TOTAL,
                .bInterfaceClass    = USB_AUDIO_CLASS,
                .bInterfaceSubclass = USB_AUDIO_INTERFACE_SUBCLASS_AUDIOSTREAMING,
                .bInterfaceProtocol = USB_AUDIO_INTERFACE_PROTOCOL_IP_VERSION_02_00,
                .iInterface         = 0,
            },
            .as_class_interface = {
                .bLength            = sizeof(usbaudio2_as_interface_desc_t),
                .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
                .bDescriptorSubtype = USB_AUDIO_AS_SUBTYPE_AS_GENERAL,
                .bTerminalLink      = TERMINAL_OUT_STREAM_USB_ID,
                .bmControls         = 0,
                .bFormatType        = USB_AUDIO_FORMAT_TYPE_I,
                .bmFormats          = USB_AUDIO_PCM,
                .bNrChannels        = USB_STREAM_CHANNELS,
                .bmChannelConfig    = USB_AUDIO_CHANNEL_FL | USB_AUDIO_CHANNEL_FR,
                .iChannelNames      = 0,
            },
            .format = {
                .bLength            = sizeof(usbaudio2_fmt_desc_t),
                .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
                .bDescriptorSubtype = USB_AUDIO_AS_SUBTYPE_FORMAT_TYPE,
                .bFormatType        = USB_AUDIO_PCM,
                .bSubSlotSize       = AUDIO_SUBFRAME_SIZE(USB_STREAM_BITS),
                .bBitResolution     = USB_STREAM_BITS,
            },
            .endpoint = {
                .bLength            = sizeof(desc_EndpointIsoc_t),
                .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
                .bEndpointAddress   = USB_ENDPOINT_ADDR_DIR_IN | USB_ENDPOINT_STREAM,
                .bmAttributes       = USB_ENDPOINT_ATT_TYPE_ISOC | USB_ENDPOINT_ATT_SYNC_ASYNC,
                .wMaxPacketSize     = STREAM_PACKET_SIZE_MAX,
                .bInterval          = PERIODIC_INTERVAL_AUDIO,
            },
            .audio_endpoint = {
                .bLength            = sizeof(usbaudio2_endpt_desc_t),
                .bDescriptorType    = USB_DESCRIPTOR_CLASSENDPOINT,
                .bDescriptorSubtype = USB_AUDIO_CS_ENDP_SUBTYPES_EP_GENERAL,
                .bmControls         = 0,
                .bmAttributes       = 0,
                .bLockDelayUnits    = 0,
                .wLockDelay         = 0,
            },
        },
    },

    .interface_assoc_tones = {
        .bLength                = sizeof(desc_InterfaceAssociation_t),
        .bDescriptorType        = USB_DESCRIPTOR_INTERFACE_ASSOCIATION,
        .bFirstInterface        = INTERFACE_AUDIO_CONTROL_TONES,
        .bInterfaceCount        = INTERFACE_COUNT_TONES,
        .bFunctionClass         = USB_AUDIO_FUNCTION,
        .bFunctionSubClass      = USB_AUDIO_FUNCTION_SUBCLASS_UNDEFINED,
        .bFunctionProtocol      = AF_VERSION_02_00,
        .iFunction              = 0,
    },

    .interface_audio_control_tones = {
        .interface = {
            .bLength            = sizeof(desc_Interface_t),
            .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
            .bInterfaceNumber   = INTERFACE_AUDIO_CONTROL_TONES,
            .bAlternateSetting  = 0,
            .bNumEndpoints      = 0,
            .bInterfaceClass    = USB_AUDIO_CLASS,
            .bInterfaceSubclass = USB_AUDIO_INTERFACE_SUBCLASS_CONTROL,
            .bInterfaceProtocol = USB_AUDIO_INTERFACE_PROTOCOL_IP_VERSION_02_00,
            .iInterface         = STRING_TONES_INDEX,
        },
        .header = {
            .bLength            = sizeof(usbaudio2_ac_header_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_HEADER,
            .bcdADC             = USB_AUDIO_CLASS_SPEC_2,
            .bCategory          = USB_AUDIO_FUNCTION_CTG_PROAUDIO,
            .wTotalLength       = sizeof(configuration_descriptor_data.interface_audio_control_tones) - sizeof(desc_Interface_t),
            .bmControls         = 0,
        },
        .clock_source = {
            .bLength            = sizeof(usbaudio2_csrc_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_CLOCK_SOURCE,
            .bClockID           = CLOCK_SOURCE_TONES_ID,
            .bmAttributes       = USB_AUDIO_CLOCK_SOURCE_ATTR_INTFIXED,
            .bmControls         = USB_AUDIO_CLOCK_FREQ_CTL_R,
            .bAssocTerminal     = 0,
            .iClockSource       = 0,
        },
        .clock_selector = {
            .bLength            = sizeof(configuration_descriptor_data.interface_audio_control_stream.clock_selector),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_CLOCK_SELECTOR,
            .bClockID           = CLOCK_SELECTOR_TONES_ID,
            .bNrInPins          = CLOCK_SELECTOR_TONES_PINS,
            .baCSourceID        = CLOCK_SOURCE_TONES_ID,
            .bmControls         = USB_AUDIO_CLKSEL_CTL_RW,
            .iClockSelector     = 0,
        },
        .iterminal_stream_usb = {
            .bLength            = sizeof(usbaudio2_iterm_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_INPUT_TERMINAL,
            .bTerminalID        = TERMINAL_IN_USB_TONES_ID,
            .wTerminalType      = USB_AUDIO_USB_TERMINAL_STREAMING,
            .bAssocTerminal     = 0,
            .bCSourceID         = CLOCK_SELECTOR_TONES_ID,
            .bNrChannels        = USB_TONES_CHANNELS,
            .bmChannelConfig    = USB_AUDIO_CHANNEL_FL | USB_AUDIO_CHANNEL_FR,
            .iChannelNames      = 0,
            .bmControls         = 0,
            .iTerminal          = 0,
        },
        .oterminal_headphones = {
            .bLength            = sizeof(usbaudio2_oterm_desc_t),
            .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_AC_SUBTYPE_OUTPUT_TERMINAL,
            .bTerminalID        = TERMINAL_OUT_TONES_HEADPHONES,
            .wTerminalType      = USB_AUDIO_OUTPUT_TERMINAL_HEADPHONES,
            .bAssocTerminal     = 0,
            .bSourceID          = TERMINAL_IN_USB_TONES_ID,
            .bCSourceID         = CLOCK_SELECTOR_TONES_ID,
            .bmControls         = 0,
            .iTerminal          = 0,
        },

    },

    .interface_audio_tones = {
        .alt0_off = {
            .interface = {
                .bLength            = sizeof(desc_Interface_t),
                .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
                .bInterfaceNumber   = INTERFACE_AUDIO_TONES,
                .bAlternateSetting  = 0,
                .bNumEndpoints      = 0,
                .bInterfaceClass    = USB_AUDIO_CLASS,
                .bInterfaceSubclass = USB_AUDIO_INTERFACE_SUBCLASS_AUDIOSTREAMING,
                .bInterfaceProtocol = USB_AUDIO_INTERFACE_PROTOCOL_IP_VERSION_02_00,
                .iInterface         = 0,
            },
        },
        .alt1_active = {
            .interface = {
                .bLength            = sizeof(desc_Interface_t),
                .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
                .bInterfaceNumber   = INTERFACE_AUDIO_TONES,
                .bAlternateSetting  = 1,
                .bNumEndpoints      = USB_TONES_ENDPOINT_TOTAL,
                .bInterfaceClass    = USB_AUDIO_CLASS,
                .bInterfaceSubclass = USB_AUDIO_INTERFACE_SUBCLASS_AUDIOSTREAMING,
                .bInterfaceProtocol = USB_AUDIO_INTERFACE_PROTOCOL_IP_VERSION_02_00,
                .iInterface         = 0,
            },
            .as_class_interface = {
                .bLength            = sizeof(usbaudio2_as_interface_desc_t),
                .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
                .bDescriptorSubtype = USB_AUDIO_AS_SUBTYPE_AS_GENERAL,
                .bTerminalLink      = TERMINAL_IN_USB_TONES_ID,
                .bmControls         = 0,
                .bFormatType        = USB_AUDIO_FORMAT_TYPE_I,
                .bmFormats          = USB_AUDIO_PCM,
                .bNrChannels        = USB_TONES_CHANNELS,
                .bmChannelConfig    = USB_AUDIO_CHANNEL_FL | USB_AUDIO_CHANNEL_FR,
                .iChannelNames      = 0,
            },
            .format = {
                .bLength            = sizeof(usbaudio2_fmt_desc_t),
                .bDescriptorType    = USB_AUDIO_CS_ENDPOINT_INTERFACE,
                .bDescriptorSubtype = USB_AUDIO_AS_SUBTYPE_FORMAT_TYPE,
                .bFormatType        = USB_AUDIO_PCM,
                .bSubSlotSize       = AUDIO_SUBFRAME_SIZE(USB_TONES_BITS),
                .bBitResolution     = USB_TONES_BITS,
            },
            .endpoint = {
                .bLength            = sizeof(desc_EndpointIsoc_t),
                .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
                .bEndpointAddress   = (USB_ENDPOINT_ADDR_DIR_OUT | USB_ENDPOINT_TONES),
                .bmAttributes       = (USB_ENDPOINT_ATT_TYPE_ISOC | USB_ENDPOINT_ATT_SYNC_ASYNC),
                .wMaxPacketSize     = TONES_PACKET_SIZE_MAX,
                .bInterval          = PERIODIC_INTERVAL_AUDIO,
            },
            .audio_endpoint = {
                .bLength            = sizeof(usbaudio2_endpt_desc_t),
                .bDescriptorType    = USB_DESCRIPTOR_CLASSENDPOINT,
                .bDescriptorSubtype = USB_AUDIO_CS_ENDP_SUBTYPES_EP_GENERAL,
                .bmControls         = 0,
                .bmAttributes       = 0,
                .bLockDelayUnits    = 0,
                .wLockDelay         = 0,
            },
            .fb_endpoint = {
                .bLength            = sizeof(usbaudio2_fb_endpt_desc_t),
                .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
                .bEndpointAddress   = (USB_ENDPOINT_ADDR_DIR_IN | USB_ENDPOINT_TONES),
                .bmAttributes       = (USB_ENDPOINT_ATT_TYPE_ISOC | USB_ENDPOINT_ATT_SYNC_NONE | USB_ENDPOINT_ATT_USAGE_FEEDBACK),
                .wMaxPacketSize     = TONES_FB_PACKET_SIZE_MAX,
                .bInterval          = PERIODIC_INTERVAL_FEEDBACK,
            },
        },
    },

    .interface_hid = {
        .interface = {
            .bLength            = sizeof(desc_Interface_t),
            .bDescriptorType    = USB_DESCRIPTOR_INTERFACE,
            .bInterfaceNumber   = INTERFACE_HID,
            .bAlternateSetting  = 0,
            .bNumEndpoints      = USB_HID_ENDPOINT_TOTAL,
            .bInterfaceClass    = HID_INTERFACE_CLASS,
            .bInterfaceSubclass = HID_NONE_INTERFACE,
            .bInterfaceProtocol = HID_NONE_PROTOCOL,
            .iInterface         = 0,
        },
        .header = {
            .bLength            = sizeof(desc_HID_t),
            .bDescriptorType    = USB_DESCRIPTOR_HID,
            .bcdHID             = 0x100,
            .bCountryCode       = 0,
            .bNumDescriptors    = 1,
            .bExtDescType       = HID_REPORT,
            .wExtDescLength     = sizeof(hid_report_descriptor_data),
        },
        .endpoint_in = {
            .bLength            = sizeof(desc_Endpoint_t),
            .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
            .bEndpointAddress   = USB_ENDPOINT_ADDR_DIR_IN | USB_ENDPOINT_HID,
            .bmAttributes       = USB_ENDPOINT_ATT_TYPE_INT,
            .wMaxPacketSize     = USB_HID_BUFFERS_SIZE,
            .bInterval          = PERIODIC_INTERVAL_HID,
        },
        .endpoint_out = {
            .bLength            = sizeof(desc_Endpoint_t),
            .bDescriptorType    = USB_DESCRIPTOR_ENDPOINT,
            .bEndpointAddress   = USB_ENDPOINT_ADDR_DIR_OUT | USB_ENDPOINT_HID,
            .bmAttributes       = USB_ENDPOINT_ATT_TYPE_INT,
            .wMaxPacketSize     = USB_HID_BUFFERS_SIZE,
            .bInterval          = PERIODIC_INTERVAL_HID,
        },
    },
};

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * MS OS 2.0 Set Descriptor for Vendor Code 0x80
 */
static const struct {
    desc_SetHeader_t    header;

    struct {
        desc_ConfigSubset_t cfg_subset;
        struct {
            desc_FuncSubset_t   func_subset;
            desc_CompatibleID_t compat_id;
        } func_one;
    } config_one;

} ms_set_desc = {
    .header = {
        .wLength            = sizeof(desc_SetHeader_t),
        .wDescriptorType    = MS_OS_20_SET_HEADER_DESCRIPTOR,
        .dwWindowsVersion   = NTDDI_WINBLUE,
        .wTotalLength       = sizeof(ms_set_desc),
    },

    .config_one = {
        .cfg_subset = {
            .wLength            = sizeof(desc_ConfigSubset_t),
            .wDescriptorType    = MS_OS_20_SUBSET_HEADER_CONFIGURATION,
            .bConfigurationValue    = 1,
            .bReserved          = 0,
            .wTotalLength       = sizeof(ms_set_desc.config_one),
        },
        .func_one = {
            .func_subset = {
                .wLength            = sizeof(desc_FuncSubset_t),
                .wDescriptorType    = MS_OS_20_SUBSET_HEADER_FUNCTION,
                .bFirstInterface    = INTERFACE_AUDIO_CONTROL_STREAM,
                .bReserved          = 0,
                .wSubsetLength      = sizeof(ms_set_desc.config_one.func_one),
            },
            .compat_id = {
                .wLength            = sizeof(desc_CompatibleID_t),
                .wDescriptorType    = MS_OS_20_FEATURE_COMPATIBLE_ID,
                .CompatibleID = {
                    0x4D, 0x54, 0x50, 0x00,
                    0x00, 0x00, 0x00, 0x00
                },
                .SubCompatibleID = {
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00
                },
            },
        },
    },
};

/*
 * Device Level Descriptor
 */
static const struct {
    desc_BOS_t  bos;

    struct {
        desc_PlatformHeader_t header;
        struct {
            desc_MS_InformationSet_t    info_set_one;
        } info_set;
    } microsoft_compat_platform_desc;
} device_level_desc = {
    .bos = {
        .bLength                = sizeof(desc_BOS_t),
        .bDescriptorType        = USB_DESCRIPTOR_BOS,
        .wTotalLength           = sizeof(device_level_desc),
        .bNumDeviceCaps         = USB_DEVICECAPS_TOTAL
    },

    .microsoft_compat_platform_desc = {
        .header = {
            .bLength                = sizeof(device_level_desc.microsoft_compat_platform_desc),
            .bDescriptorType        = USB_DESCRIPTOR_DEVICE_CAPABILITY,
            .bDevCapabilityType     = USB_CAPABILITY_TYPE_PLATFORM,
            .bReserved              = 0,
            .PlatformCapabilityUUID1    = MS_UUID_TIME_LOW,
            .PlatformCapabilityUUID2    = MS_UUID_TIME_MID,
            .PlatformCapabilityUUID3    = MS_UUID_TIME_HIGH,
            .PlatformCapabilityUUID4    = MS_UUID_CLOCK_SEQ,
            .PlatformCapabilityUUID5    = MS_UUID_NODE_ONE,
            .PlatformCapabilityUUID6    = MS_UUID_NODE_TWO
        },
        .info_set = {
            .info_set_one = {
                .dwWindowsVersion               = ms_set_desc.header.dwWindowsVersion,
                .wMSOSDescriptorSetTotalLength  = ms_set_desc.header.wTotalLength,
                .bMS_VendorCode                 = USB_MS_SET1_VENDOR_CODE,
                .bAltEnumCode                   = 0
            },
        },
    },
};
#endif

/*
 * Configuration Descriptor wrapper.
 */
static const USBDescriptor configuration_descriptor = {
    .ud_size    = sizeof(configuration_descriptor_data),
    .ud_string  = (uint8_t *)&configuration_descriptor_data
};

/*
 * HID Descriptor wrapper.
 */
static const USBDescriptor hid_descriptor = {
    .ud_size    = sizeof(configuration_descriptor_data.interface_hid.header),
    .ud_string  = (uint8_t *)&configuration_descriptor_data.interface_hid.header
};

/*
 * HID Report Descriptor wrapper
 */
static const USBDescriptor hid_report_descriptor = {
    .ud_size    = sizeof(hid_report_descriptor_data),
    .ud_string  = (uint8_t *)&hid_report_descriptor_data
};

#if USB_PC_USE_MS_OS_DESCRIPTORS
/*
 * BOS Descriptor wrapper.
 */
static const USBDescriptor bos_descriptor = {
    .ud_size    = sizeof(device_level_desc),
    .ud_string  = (uint8_t *)&device_level_desc
};

/*
 * MS OS 2.0 Set Descriptor wrapper.
 */
static const USBDescriptor ms_set_one_descriptor = {
    .ud_size    = sizeof(ms_set_desc),
    .ud_string  = (uint8_t *)&ms_set_desc
};
#endif

/*
 * U.S. English language identifier.
 */
static const uint8_t lang_string[] = {
    4,                                    /* bLength.                         */
    USB_DESCRIPTOR_STRING,                /* bDescriptorType.                 */
    0x04, 0x09                            /* wLANGID (U.S. English).          */
};

/*
 * Vendor string.
 */
static const uint8_t vend_string[] = {
    26,                                   /* bLength.                         */
    USB_DESCRIPTOR_STRING,                /* bDescriptorType.                 */
    'A', 0, 's', 0, 't', 0, 'r', 0, 'o', 0, ' ', 0, 'G', 0, 'a', 0, 'm', 0, 'i', 0, 'n', 0, 'g', 0
};

/*
 * Device Description string.
 */
static const uint8_t dev_string[] = {
    26,                                   /* bLength.                         */
    USB_DESCRIPTOR_STRING,                /* bDescriptorType.                 */
    'A', 0, 's', 0, 't', 0, 'r', 0, 'o', 0, ' ', 0, 'S', 0, 't', 0, 'u', 0, 'd', 0, 'i', 0, 'o', 0
};

/*
 * Stream Audio Control Interface descriptor string
 */
static const uint8_t stream_ac_string[] = {
    40,
    USB_DESCRIPTOR_STRING,
    'A', 0, 's', 0, 't', 0, 'r', 0, 'o', 0, ' ', 0, 'S', 0, 't', 0, 'u', 0, 'd', 0, 'i', 0, 'o', 0, ' ', 0, 'S', 0, 't', 0, 'r', 0, 'e', 0, 'a', 0, 'm', 0
};

/*
 * Tones Audio Control Interface descriptor string
 */
static const uint8_t tones_ac_string[] = {
    38,
    USB_DESCRIPTOR_STRING,
    'A', 0, 's', 0, 't', 0, 'r', 0, 'o', 0, ' ', 0, 'S', 0, 't', 0, 'u', 0, 'd', 0, 'i', 0, 'o', 0, ' ', 0, 'T', 0, 'o', 0, 'n', 0, 'e', 0, 's', 0
};

/*
 * Strings wrappers array.
 */
static const USBDescriptor desc_strings[] = {
    {sizeof(lang_string), lang_string},
    {sizeof(vend_string), vend_string},
    {sizeof(dev_string), dev_string},
    {sizeof(stream_ac_string), stream_ac_string},
    {sizeof(tones_ac_string), tones_ac_string}
};

/**
 * @brief   Returns a descriptor string.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return a descriptor string during
 *          enumeration.
 *
 * @param[in]   dindex      index value indicating which string to return.
 * @retval hid_report_descriptor    Pointer to the USB PC hid report descriptor.
 */
static const USBDescriptor *get_descriptor_str(uint8_t dindex) {
    return &desc_strings[dindex];
}

/**
 * @brief   Returns the number of string descriptors.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the number of string descriptors
 *          so that if the host requests a string descriptor it doesn't try to access
 *          memory that is out of bounds.
 *
 * @retval size_t total number of string descriptors.
 */
static size_t get_str_desc_total(void) {
    return (sizeof(desc_strings) / sizeof(USBDescriptor));
}

/**
 * @brief   Returns the hid report descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the hid report descriptor during
 *          enumeration.
 *
 * @retval hid_report_descriptor    Pointer to the USB PC hid report descriptor.
 */
static const USBDescriptor *get_hid_report_desc(void) {
    return &hid_report_descriptor;
}

/**
 * @brief   Returns the hid descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the hid descriptor during
 *          enumeration.
 *
 * @retval hid_descriptor    Pointer to the USB PC hid descriptor.
 */
static const USBDescriptor *get_hid_desc(void) {
    return &hid_descriptor;
}

/**
 * @brief   Returns the configuration descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the configuration descriptor during
 *          enumeration.
 *
 * @retval configuration_descriptor    Pointer to the USB PC configuration descriptor.
 */
static const USBDescriptor *get_config_desc(void) {
    return &configuration_descriptor;
}

/**
 * @brief   Returns the device descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the device descriptor during
 *          enumeration.
 *
 * @retval device_descriptor    Pointer to the USB PC device descriptor.
 */
static const USBDescriptor *get_device_desc(void) {
    return &device_descriptor;
}

#if USB_PC_USE_DEVICE_QUALIFIER
/**
 * @brief   Returns the device qualifier descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the device qualifier descriptor during
 *          enumeration.
 *
 * @retval device_qualifier_descriptor    Pointer to the USB PC device qualifier descriptor.
 */
static const USBDescriptor *get_device_qualifier_desc(void) {
    return &device_qualifier_descriptor;
}
#endif

#if USB_PC_USE_MS_OS_DESCRIPTORS
/**
 * @brief   Returns the BOS descriptor.
 * @details This callback function is used by the usb_mode_pc config
 *          struct. It's used to return the BOS descriptor during
 *          enumeration.
 *          This is required to support MS OS 2.0 descriptors by Microsoft.
 *
 * @retval bos_descriptor   Pointer to the USB PC BOS descriptor.
 */
static const USBDescriptor *get_bos_descriptor(void) {
    return &bos_descriptor;
}
#endif

/**
 * @brief   IN endpoint states.
 */
static USBInEndpointState ep_state_hid_in;
static USBInEndpointState ep_state_stream_in;
static USBInEndpointState ep_state_tones_in;

/**
 * @brief   OUT endpoint states.
 */
static USBOutEndpointState ep_state_hid_out;
static USBOutEndpointState ep_state_tones_out;

/**
 * @brief   USB_ENDPOINT_HID initialization structure (both IN and OUT).
 */
static const USBEndpointConfig ep_hid_config = {
    .ep_mode        = USB_EP_MODE_TYPE_INTR,
    .setup_cb       = NULL,
    .in_cb          = hidDataTransmitted,
    .out_cb         = hidDataReceived,
    .in_maxsize     = USB_HID_BUFFERS_SIZE,
    .out_maxsize    = USB_HID_BUFFERS_SIZE,
    .in_state       = &ep_state_hid_in,
    .out_state      = &ep_state_hid_out,
    .in_multiplier  = 1,
    .setup_buf      = NULL
};

/**
 * @brief   USB_ENDPOINT_STREAM initialization structure (only IN).
 */
static const USBEndpointConfig ep_stream_config = {
    .ep_mode        = USB_EP_MODE_TYPE_ISOC,
    .setup_cb       = NULL,
    .in_cb          = usbAudioDataTransmitted,
    .out_cb         = NULL,
    .in_maxsize     = STREAM_PACKET_SIZE_MAX,
    .out_maxsize    = 0,
    .in_state       = &ep_state_stream_in,
    .out_state      = NULL,
    .in_multiplier  = 1,
    .setup_buf      = NULL
};

/**
 * @brief   USB_ENDPOINT_TONES initialization structure (both IN and OUT).
 */
static const USBEndpointConfig ep_tones_config = {
    .ep_mode        = USB_EP_MODE_TYPE_ISOC,
    .setup_cb       = NULL,
    .in_cb          = usbFeedBackTransmitted,
    .out_cb         = usbAudioDataReceived,
    .in_maxsize     = TONES_FB_PACKET_SIZE_MAX,
    .out_maxsize    = TONES_PACKET_SIZE_MAX,
    .in_state       = &ep_state_tones_in,
    .out_state      = &ep_state_tones_out,
    .in_multiplier  = 1,
    .setup_buf      = NULL
};

/**
 * @breif   Application specific USB configurations
 * @details This callback function is application specific.
 *          It's executed when a USB configured event occurs.
 *
 * @param[in]   usbp        usb driver pointer
 */
static void usb_configured(USBDriver *usbp) {
    /* Enables the endpoints specified into the configuration.
       Note, this callback is invoked from an ISR so I-Class functions
       must be used.*/
    usbInitEndpointI(usbp, USB_ENDPOINT_HID, &ep_hid_config);
    usbInitEndpointI(usbp, USB_ENDPOINT_STREAM, &ep_stream_config);
    usbInitEndpointI(usbp, USB_ENDPOINT_TONES, &ep_tones_config);

    /* Resetting the state of the HID subsystem.*/
    hidConfigureHookI(&UHD1);
}

/**
 * @breif   Fuction to handle control stream interface requests.
 * @details This callback function is application specific.
 *          It's executed when a USB control request is sent to
 *          to the control stream interface.
 *
 * @param[in]   usbp        usb driver pointer
 * @param[in]   setup       control setup packet
 */
static bool stream_control_handler(USBDriver *usbp, setupPacket_t *setup) {

    static uint8_t cs = 1;
    static bool devicetohost = false;

    /* Determine if the direction of the packet requires a response to the host.*/
    if((setup->bmRequestType & USB_RTYPE_DIR_MASK) == USB_RTYPE_DIR_DEV2HOST) {
        devicetohost = true;
    }

    switch(GET_ENTITY_ID(setup->wIndex)) {
        case CLOCK_SOURCE_STREAM_ID:
            switch(GET_CONTROL_SELECTOR(setup->wValue)) {
                case USB_AUDIO_CS_SAM_FREQ_CONTROL:
                    switch(setup->bRequest) {
                        case USB_AUDIO_CS_REQ_CODE_CUR:
                            if(devicetohost) {
                                usbSetupTransfer(usbp, (uint8_t *)&current_fs, sizeof(uint32_t), NULL);
                                return true;
                            } else {
                                return false;
                            }

                        case USB_AUDIO_CS_REQ_CODE_RANGE:
                            if(devicetohost) {
                                usbSetupTransfer(usbp, (uint8_t *)&fs, USB_AUDIO_GET_RANGE3_SIZE(fs.size), NULL);
                                return true;
                            } else {
                                return false;
                            }
                        default:
                            return false;
                    }
                default:
                    return false;
            }

        /* The following is taken from Microsoft UAC v2.0 driver documentation:
         * The USB Audio 2.0 driver does not support clock selection. The driver uses the
         * Clock Source Entity which is selected by default and never issues a Clock Selector
         * Control SET CUR request. The Clock Selector Control GET CUR request (ADC-2 5.2.5.2.1)
         * must be implemented in compatible USB Audio 2.0 hardware.*/
        case CLOCK_SELECTOR_STREAM_ID:
            switch(GET_CONTROL_SELECTOR(setup->wValue)) {
                case USB_AUDIO_CX_CLOCK_SELECTOR_CONTROL:
                    switch(setup->bRequest) {
                        case USB_AUDIO_CS_REQ_CODE_CUR:
                            if(devicetohost) {
                                usbSetupTransfer(usbp, &cs, sizeof(cs), NULL);
                                return true;
                            } else {
                                /* Respond true here to indicate that the clock has been
                                 * selected. If we add more clocks then we can add some
                                 * code to handle them, but since we only have one then
                                 * we don't need to do anything special.*/
                                return true;
                            }
                        default:
                            return false;
                    }
            }
            return true;

        default:
            return false;
    }

    return false;
}

/**
 * @breif   Fuction to handle control tones interface requests.
 * @details This callback function is application specific.
 *          It's executed when a USB control request is sent to
 *          to the control stream interface.
 *
 * @param[in]   usbp        usb driver pointer
 * @param[in]   setup       control setup packet
 */
static bool tones_control_handler(USBDriver *usbp, setupPacket_t *setup) {

    static uint8_t cs = 1;
    static bool devicetohost = false;

    /* Determine if the direction of the packet requires a response to the host.*/
    if((setup->bmRequestType & USB_RTYPE_DIR_MASK) == USB_RTYPE_DIR_DEV2HOST) {
        devicetohost = true;
    }

    switch(GET_ENTITY_ID(setup->wIndex)) {
        case CLOCK_SOURCE_TONES_ID:
            switch(GET_CONTROL_SELECTOR(setup->wValue)) {
                case USB_AUDIO_CS_SAM_FREQ_CONTROL:
                    switch(setup->bRequest) {
                        case USB_AUDIO_CS_REQ_CODE_CUR:
                            if(devicetohost) {
                                usbSetupTransfer(usbp, (uint8_t *)&current_tone_fs, sizeof(uint32_t), NULL);
                                return true;
                            } else {
                                return false;
                            }

                        case USB_AUDIO_CS_REQ_CODE_RANGE:
                            if(devicetohost) {
                                usbSetupTransfer(usbp, (uint8_t *)&tone_fs, USB_AUDIO_GET_RANGE3_SIZE(tone_fs.size), NULL);
                                return true;
                            } else {
                                return false;
                            }
                        default:
                            return false;
                    }
                default:
                    return false;
            }

        /* The following is taken from Microsoft UAC v2.0 driver documentation:
         * The USB Audio 2.0 driver does not support clock selection. The driver uses the
         * Clock Source Entity which is selected by default and never issues a Clock Selector
         * Control SET CUR request. The Clock Selector Control GET CUR request (ADC-2 5.2.5.2.1)
         * must be implemented in compatible USB Audio 2.0 hardware.*/
        case CLOCK_SELECTOR_TONES_ID:
            switch(GET_CONTROL_SELECTOR(setup->wValue)) {
                case USB_AUDIO_CX_CLOCK_SELECTOR_CONTROL:
                    switch(setup->bRequest) {
                        case USB_AUDIO_CS_REQ_CODE_CUR:
                            if(devicetohost) {
                                usbSetupTransfer(usbp, &cs, sizeof(cs), NULL);
                                return true;
                            } else {
                                /* Respond true here to indicate that the clock has been
                                 * selected. If we add more clocks then we can add some
                                 * code to handle them, but since we only have one then
                                 * we don't need to do anything special.*/
                                return true;
                            }
                        default:
                            return false;
                    }
            }
            return true;

        default:
            return false;
    }

    return false;
}

/**
 * @breif   USB initializations
 * @details This callback function is application specific.
 *          It's executed before the USB peripheral is started
 *          if started by calling usbModeStart. Place any USB
 *          driver/variable initializations here.
 *
 * @param[in]   usbp        usb driver pointer
 */
void usb_obj_init(USBDriver *usbp) {
    (void)usbp;
    /*
     * Initializes a USB HID driver.
     */
    hidObjectInit(&UHD1);
    hidStart(&UHD1, &usbhidcfg);

    /*
     * Initializes a USB Audio driver.
     */
    usbAudioObjectInit(&UAD2, &usbaudio_streamcfg.bcfg);
    usbAudioStart(&UAD2, &usbaudio_streamcfg);

    usbAudioObjectInit(&UAD3, &usbaudio_tonescfg.bcfg);
    usbAudioStart(&UAD3, &usbaudio_tonescfg);
}

/**
 * @breif   USB requests handler
 * @details This callback function is application specific.
 *          It's executed when a USB Requests event occurs.
 *          Any specific functions needed to occur when a requests
 *          event occurs, should be placed here.
 *
 * @param[in]   usbp        usb driver pointer
 */
static bool usb_req_handler(USBDriver *usbp) {
    setupPacket_t *setup = (setupPacket_t*)usbp->setup;

    if(((setup->bmRequestType & USB_RTYPE_RECIPIENT_MASK) == USB_RTYPE_RECIPIENT_INTERFACE) &&
       ((setup->bmRequestType & USB_RTYPE_TYPE_MASK) == USB_RTYPE_TYPE_CLASS)) {

        switch(GET_INTERFACE_NUM(setup->wIndex)) {
            case INTERFACE_AUDIO_CONTROL_STREAM:
                return stream_control_handler(usbp, setup);

            case INTERFACE_AUDIO_CONTROL_TONES:
                return tones_control_handler(usbp, setup);

            case INTERFACE_AUDIO_STREAM:
            case INTERFACE_AUDIO_TONES:
            default:
                return false;
        }
    }

    if(((setup->bmRequestType & USB_RTYPE_DIR_MASK) == USB_RTYPE_DIR_HOST2DEV) &&
       ((setup->bmRequestType & USB_RTYPE_RECIPIENT_MASK) == USB_RTYPE_RECIPIENT_INTERFACE) &&
       ((setup->bmRequestType & USB_RTYPE_TYPE_MASK) == USB_RTYPE_TYPE_STD)) {
        switch(setup->bRequest) {
            case USB_REQ_SET_INTERFACE:
                switch(setup->wIndex) {
                    case INTERFACE_AUDIO_STREAM:
                        if(setup->wValue == 1) {
                            osalSysLockFromISR();
                            usbAudioConfigureHookI(&UAD2);
                            osalSysUnlockFromISR();
                        } else {
                            osalSysLockFromISR();
                            usbAudioDisconnectI(&UAD2);
                            osalSysUnlockFromISR();
                        }
                        return true;

                    case INTERFACE_AUDIO_TONES:
                        if(setup->wValue == 1) {
                            osalSysLockFromISR();
                            usbAudioConfigureHookI(&UAD3);
                            osalSysUnlockFromISR();
                        } else {
                            osalSysLockFromISR();
                            usbAudioDisconnectI(&UAD3);
                            osalSysUnlockFromISR();
                        }
                        return true;

                    case INTERFACE_AUDIO_CONTROL_STREAM:
                    case INTERFACE_AUDIO_CONTROL_TONES:
                        return true;

                    default:
                        return false;
                }
                return false;

            default:
                return false;
        }

    }


    if(((setup->bmRequestType & USB_RTYPE_DIR_MASK) == USB_RTYPE_DIR_DEV2HOST) &&
       ((setup->bmRequestType & USB_RTYPE_RECIPIENT_MASK) == USB_RTYPE_RECIPIENT_DEVICE) &&
       ((setup->bmRequestType & USB_RTYPE_TYPE_MASK) == USB_RTYPE_TYPE_VENDOR)) {
        switch(setup->bRequest) {
#if USB_PC_USE_MS_OS_DESCRIPTORS
            case USB_MS_SET1_VENDOR_CODE:
                switch(setup->wIndex) {
                    case MS_OS_20_DESCRIPTOR_INDEX:
                        usbSetupTransfer(usbp, (uint8_t *)ms_set_one_descriptor.ud_string, ms_set_one_descriptor.ud_size, NULL);
                        return true;
                }
#endif

            default:
                return false;
        }
    }

    return false;
}

#if 0
static bool tones_fb_update = false;
/*
 * Handles the USB driver global events.
 */
static void pc_sof_handler(USBDriver *usbp) {
    (void)usbp;

    static uint8_t sof_counter = 0;

    osalSysLockFromISR();

    sof_counter++;
    if(sof_counter == 10) {
        sof_counter = 0;
        tones_fb_update = true;
    }

    osalSysUnlockFromISR();
}
#endif

/*
 * USB Mode driver configuration.
 */
static const USBModeConfig_t usb_mode_pc = {
    .usb_configured_cb  = usb_configured,
    .usb_obj_init_cb    = usb_obj_init,
    .get_desc_str_cb    = get_descriptor_str,
    .get_hid_report_desc_cb = get_hid_report_desc,
    .get_hid_desc_cb    = get_hid_desc,
    .get_config_desc_cb = get_config_desc,
    .get_device_desc_cb = get_device_desc,

#if USB_PC_USE_DEVICE_QUALIFIER
    .get_device_qualifier_desc_cb = get_device_qualifier_desc,
#else
    .get_device_qualifier_desc_cb = NULL,
#endif

    .usb_requests_cb    = usb_req_handler,
    .get_str_desc_total_cb = get_str_desc_total,

#if USB_PC_USE_MS_OS_DESCRIPTORS
    .get_bos_desc_cb    = get_bos_descriptor,
#else
    .get_bos_desc_cb    = NULL,
#endif
    .usb_sof_cb         = usbAudioSOFHook,
};

/**
 * @brief   Returns the PC configuration struct.
 * @details This function returns the configuration struct
 *          that contains the USB PC specific callback functions.
 *
 * @retval usb_mode_pc       The USB PC config struct.
 */
const USBModeConfig_t *usbGetModePC(void) {
    return &usb_mode_pc;
}
