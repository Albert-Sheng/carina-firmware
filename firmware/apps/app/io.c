/**
 * @file    io.c
 * @brief   I/O control code.
 *
 * @addtogroup CARINA
 * @ingroup CARINA
 * @{
 */

#include "proximity_snsr.h"
#include "cs47l90_regdefs.h"
#include "debug.h"
#include "dsp.h"
#include "io.h"

/*===========================================================================*/
/* App local definitions.                                                    */
/*===========================================================================*/

#define MAILBOX_SIZE            4
#define EVENT_FETCH_TIMEOUT     TIME_MS2I(1000)

#define ENCODER_SELECT_LINE     PAL_LINE(GPIOI, 3)
#define MIC_MUTE_LINE           PAL_LINE(GPIOG, 2)
#define PHANTOM_POWER_LINE      PAL_LINE(GPIOD, 3)

#define LEFT_ENCODER_INT0_LINE  PAL_LINE(GPIOD, 5)
#define LEFT_ENCODER_INT1_LINE  PAL_LINE(GPIOI, 4)
#define RIGHT_ENCODER_INT0_LINE PAL_LINE(GPIOG, 13)
#define RIGHT_ENCODER_INT1_LINE PAL_LINE(GPIOG, 14)

/*===========================================================================*/
/* App local variables and types.                                            */
/*===========================================================================*/

enum {
    BUTTON_EVENT,
    ENCODER_EVENT,
    JACK_EVENT,
};


enum {
    JACK_UNPLUGGED,
    JACK_PLUGGED_IN,
};

#pragma pack(push,1)
typedef union {
    msg_t frame;

    struct {
        uint8_t  type;
        union {   // CR:NV Not Nce but what can you to do...
            uint8_t data[3];

            struct {
                uint8_t dev;
                uint16_t event;
            } io;
        };
    };
} io_msg_t;

typedef struct {
    EncoderCb encoderRightCW;
    EncoderCb encoderRightCCW;
    EncoderCb encoderLeftCW;
    EncoderCb encoderLeftCCW;
    JackCb    auxJack;
    JackCb    hpJack;
    ButtonCb  encoderButton;
    ButtonCb  micMute;
    ButtonCb  phantomPower;
} io_callbacks_t;
#pragma pack(pop)

static msg_t s_mailbox_buffer[MAILBOX_SIZE];
static MAILBOX_DECL(s_mailbox, s_mailbox_buffer, MAILBOX_SIZE);

static io_msg_t io_msg_global = {
    .type = 0xFF,
    .io = {
        .event = 0xFFFF,
        .dev = 0xFF,
    },
};
static io_callbacks_t s_callbacks = {
    NULL
};


/*===========================================================================*/
/* App exported variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* App external variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* App local functions.                                                      */
/*===========================================================================*/

/*
 * This is executed in interrupt context.
 */
static void button_cb(ioline_t pal_line, button_event_t button_event) {
    io_msg_t msg = {
        .type = BUTTON_EVENT,
        .io = {
            .event = button_event,
        }
    };

    switch(pal_line) {
        case ENCODER_SELECT_LINE:
            msg.io.dev = ENCODER_SELECT_BUTTON;
            break;
        case MIC_MUTE_LINE:
            msg.io.dev = MIC_MUTE_BUTTON;
            break;
        case PHANTOM_POWER_LINE:
            msg.io.dev = PHANTOM_POWER_BUTTON;
            break;
        default:
            break;
    }

    chSysLockFromISR();
    chMBPostI(&s_mailbox, msg.frame);
    chSysUnlockFromISR();
};


/*
 * This is executed in interrupt context.
 */
static void encoder_cb(ioline_t pal_line_int0, encoder_event_t encoder_event) {
    io_msg_t msg = {
        .type = ENCODER_EVENT,
        .io = {
            .event = encoder_event,
        }
    };

    switch(pal_line_int0) {
        case LEFT_ENCODER_INT0_LINE:
            msg.io.dev = LEFT_ENCODER;
            break;
        case RIGHT_ENCODER_INT0_LINE:
            msg.io.dev = RIGHT_ENCODER;
            break;
        default:
            break;
    }

    chSysLockFromISR();
    chMBPostI(&s_mailbox, msg.frame);
    chSysUnlockFromISR();
};

static void io_button_handler(io_msg_t msg) {
    switch(msg.io.dev) {
        case ENCODER_SELECT_BUTTON:
            if(s_callbacks.encoderButton) {
                (*s_callbacks.encoderButton)((button_event_t) msg.io.event);
                break;
            case MIC_MUTE_BUTTON:
                if(s_callbacks.micMute) {
                    (*s_callbacks.micMute)((button_event_t) msg.io.event);
                }
                break;
            case PHANTOM_POWER_BUTTON:
                if(s_callbacks.phantomPower) {
                    (*s_callbacks.phantomPower)((button_event_t) msg.io.event);
                }
                break;
            default:
                Dbg_printf(DBG_ERROR, "Unknown button: %d", msg.io.dev);
                break;
            }
    }
}
static void io_encoder_left(io_msg_t msg) {
    switch(msg.io.event) {
        case ENCODER_CLOCKWISE_EVENT:
            if(s_callbacks.encoderLeftCW) {
                (*s_callbacks.encoderLeftCW)();
            }
            break;
        case ENCODER_COUNTER_CLOCKWISE_EVENT:
            if(s_callbacks.encoderLeftCCW) {
                (*s_callbacks.encoderLeftCCW)();
            }
            break;

        default:
            Dbg_printf(DBG_IO, "Unknown LE event: %d", msg.io.event);
            break;
    }
}

static void io_encoder_right(io_msg_t msg) {
    switch(msg.io.event) {
        case ENCODER_CLOCKWISE_EVENT:
            if(s_callbacks.encoderRightCW) {
                (*s_callbacks.encoderRightCW)();
            }
            break;
        case ENCODER_COUNTER_CLOCKWISE_EVENT:
            if(s_callbacks.encoderRightCCW) {
                (*s_callbacks.encoderRightCCW)();
            }
            break;

        default:
            Dbg_printf(DBG_IO, "Unknown RE event: %d", msg.io.event);
            break;
    }
}

static void io_encoder_handler(io_msg_t msg) {
    switch(msg.io.dev) {
        case LEFT_ENCODER:
            io_encoder_left(msg);
            break;

        case RIGHT_ENCODER:
            io_encoder_right(msg);
            break;

        default:
            Dbg_printf(DBG_ERROR, "Unknown encoder device: %d", msg.io.dev);
            break;
    }
}

static void io_jack_handler(io_msg_t msg) {
    switch(msg.io.dev) {
        case HP_JACK:
            if(s_callbacks.hpJack) {
                (*s_callbacks.hpJack)(JACK_PLUGGED_IN == msg.io.event);
            }
            break;

        case AUX_JACK:
            if(s_callbacks.auxJack) {
                (*s_callbacks.auxJack)(JACK_PLUGGED_IN == msg.io.event);
            }
            break;

        default:
            Dbg_printf(DBG_ERROR, "Unknown jack device: %d", msg.io.dev);
            break;
    }
}

static void ioMessageStore(io_msg_t msg) {
    io_msg_global = msg;
}

/*===========================================================================*/
/* App exported functions.                                                   */
/*===========================================================================*/

/**
 * @brief   IO Thread.
 *
 * @details This thread handles both event based and polling based I/O events.
 *
 * @param[in] arg optional thread argument.
 */
THD_FUNCTION(IO_Thread, arg)
{
    uint32_t prev_jack_sts = 0;
    uint32_t jack_sts = 0;
    // uint32_t freq;
    // uint32_t prev_freq = 0;
    io_msg_t io_msg;

    (void)arg;

    chRegSetThreadName("I/O");

    chMBObjectInit(&s_mailbox, s_mailbox_buffer, MAILBOX_SIZE);

    ButtonRegisterCB(ENCODER_SELECT_LINE, button_cb);
    ButtonRegisterCB(MIC_MUTE_LINE, button_cb);
    buttonPollingStart(PHANTOM_POWER_LINE, button_cb);

    EncoderRegisterCB(LEFT_ENCODER_INT0_LINE,  LEFT_ENCODER_INT1_LINE, encoder_cb);
    EncoderRegisterCB(RIGHT_ENCODER_INT0_LINE, RIGHT_ENCODER_INT1_LINE, encoder_cb);

    Dbg_printf(DBG_INFO, "I/O thread started.");

    while(!chThdShouldTerminateX()) {

        // Handle events
        // Note: This code section is blocking for the duration of EVENT_FETCH_TIMEOUT unless a message can be fetched within that duration.
        {
            if (MSG_OK == chMBFetchTimeout(&s_mailbox, &(io_msg.frame), EVENT_FETCH_TIMEOUT)) {
                ioMessageStore(io_msg);
                switch (io_msg.type) {
                    case BUTTON_EVENT:
                        io_button_handler(io_msg);
                        Dbg_printf(DBG_IO, "button event");
                        break;
                    case ENCODER_EVENT:
                        io_encoder_handler(io_msg);
                        Dbg_printf(DBG_IO, "encoder event");
                        break;
                    case JACK_EVENT:
                        io_jack_handler(io_msg);
                        Dbg_printf(DBG_IO, "jack event");
                        break;
                    default:
                        Dbg_printf(DBG_IO, "Unknown msg type: %d", io_msg.type);
                        break;
                }
            }
        }

        // Handle polls
        {
            if(isProxiSensorDetected()) {
                TODO("perform proximity sensor function here.")
            }

            dspReadReg((uint8_t *)&jack_sts, CS47L90_IRQ1_RAW_STATUS_7, sizeof(jack_sts));

            if((prev_jack_sts & CS47L90_JD1_STS1) != (jack_sts & CS47L90_JD1_STS1)) {
                if(jack_sts & CS47L90_JD1_STS1) {
                    io_msg.io.event = JACK_PLUGGED_IN;
                } else {
                    io_msg.io.event = JACK_UNPLUGGED;
                }

                io_msg.type = JACK_EVENT;
                io_msg.io.dev = HP_JACK;

                chSysLock();
                chMBPostI(&s_mailbox, io_msg.frame);
                chSysUnlock();
            }

            if((prev_jack_sts & CS47L90_JD2_STS1) != (jack_sts & CS47L90_JD2_STS1)) {
                if(jack_sts & CS47L90_JD2_STS1) {
                    io_msg.io.event = JACK_PLUGGED_IN;
                } else {
                    io_msg.io.event = JACK_UNPLUGGED;
                }

                io_msg.type = JACK_EVENT;
                io_msg.io.dev = AUX_JACK;

                chSysLock();
                chMBPostI(&s_mailbox, io_msg.frame);
                chSysUnlock();
            }

            prev_jack_sts = jack_sts;
        }
    }

    EncoderUnregisterCB(LEFT_ENCODER_INT0_LINE);
    EncoderUnregisterCB(RIGHT_ENCODER_INT0_LINE);
    ButtonUnregisterCB(ENCODER_SELECT_LINE);
    ButtonUnregisterCB(MIC_MUTE_LINE);
    buttonPollingStop(PHANTOM_POWER_LINE);

    Dbg_printf(DBG_INFO, "I/O thread finished.");
    chThdExit(MSG_OK);
}

msg_t ioGetMessage(void) {
    msg_t msg = io_msg_global.frame;

    // clear the io message
    io_msg_global.frame = 0xFFFFFFFF;
    return msg;
}

void ioSetMessage(msg_t message) {
    io_msg_global.frame = message;

    chSysLock();
    chMBPostI(&s_mailbox, io_msg_global.frame);
    chSysUnlock();
}
/**
 * Register an encoder callback function
 * @param encoder The encoder being registered
 * @param pCB     The callback function
 */
void ioRegisterEncoderCb(io_encoder_t encoder, encoder_event_t encEvent, EncoderCb pCB) {
    if(pCB) {
        if(encoder == RIGHT_ENCODER) {
            if(encEvent == ENCODER_CLOCKWISE_EVENT) {
                s_callbacks.encoderRightCW = pCB;
            } else {
                s_callbacks.encoderRightCCW = pCB;
            }
        } else if(encoder == LEFT_ENCODER) {
            if(encEvent == ENCODER_CLOCKWISE_EVENT) {
                s_callbacks.encoderLeftCW = pCB;
            } else {
                s_callbacks.encoderLeftCCW = pCB;
            }
        }
    }
}
/**
 * Register a button callback
 * @param btn   The button to issue the callback
 * @param btnCB The callback function
 */
void ioRegisterButtonCb(io_button_t btn, ButtonCb pCB) {
    if(pCB) {
        switch(btn) {
            case ENCODER_SELECT_BUTTON:
                s_callbacks.encoderButton = pCB;
                break;
            case MIC_MUTE_BUTTON:
                s_callbacks.micMute = pCB;
                break;
            case PHANTOM_POWER_BUTTON:
                s_callbacks.phantomPower = pCB;
                break;
            default:
                break;
        }
    }
}
/**
 * Register for a jack callback
 * @param jack   The jack to issue the callback
 * @param jackCB The callback function
 */
void ioRegisterJackCb(io_jack_t jack, JackCb pCB) {
    if(pCB) {
        if(jack == AUX_JACK) {
            s_callbacks.auxJack = pCB;
        } else if(jack == HP_JACK) {
            s_callbacks.hpJack = pCB;
        }
    }
}
