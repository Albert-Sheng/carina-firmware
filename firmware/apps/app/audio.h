#pragma once

#include "dsp.h"

extern const uint32_t HPOUT1_gain_regs[2];
extern const dspTable_t HPOUT1_gain[];
