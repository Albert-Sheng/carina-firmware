
#ifndef _TH_AUDIO_H_
#define _TH_AUDIO_H_

#define MIC_CHANNELS_USB        1
#define MIC_CHANNELS_GIP        1
#define MIC_CHANNELS_SAI        2
#define MIC_BITS                32
#define MIC_SAMPLE_RATE_USB     48000
#define MIC_SAMPLE_RATE_GIP     16000
#define MIC_SAMPLE_RATE_SAI     96000

#define GAME_MIX_CHANNELS_USB       2
#define GAME_MIX_CHANNELS_SAI       2
#define GAME_MIX_BITS               32
#define GAME_MIX_SAMPLE_RATE_USB    192000
#define GAME_MIX_SAMPLE_RATE_GIP    48000
#define GAME_MIX_SAMPLE_RATE_SAI    48000

#define SAMPLE_BYTES(bits, channels) ((((bits)+7)/8)*channels)

#endif

