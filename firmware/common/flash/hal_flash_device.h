/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    hal_flash_device.h
 * @brief   MACRONIX MX25L serial flash driver header.
 *
 * @addtogroup MACRONIX MX25L
 * @{
 */

#ifndef HAL_FLASH_DEVICE_H
#define HAL_FLASH_DEVICE_H

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/**
 * @name    Device capabilities
 * @{
 */
#define SNOR_DEVICE_SUPPORTS_XIP            FALSE
/** @} */

/**
 * @name    Device identification
 * @{
 */
#define MX25L_SUPPORTED_MANUFACTURE_IDS             {0xC2}
#define MX25L_SUPPORTED_MEMORY_TYPE_IDS             {0x20}
/** @} */

/**
 * @name    Command sets
 * @{
 */
#define MX25L_CMD_READ                              0x03 /*normal read*/
#define MX25L_CMD_FAST_READ                         0x0B /*fast read data*/
#define MX25L_CMD_2READ                             0xBB /*2 x I/O read command*/
#define MX25L_CMD_DREAD                             0x3B /*1I / 2O read command*/
#define MX25L_CMD_4READ                             0xEB /*4 x I/O read command*/
#define MX25L_CMD_QREAD                             0x6B /*1I / 4O read command*/

#define MX25L_CMD_WREN                              0x06 /*write enable*/
#define MX25L_CMD_WRDI                              0x04 /*write disable*/
#define MX25L_CMD_RDSR                              0x05 /*read status register*/
#define MX25L_CMD_RDCR                              0x15 /*read configuration register*/

#define MX25L_CMD_WRSR                              0x01 /*write status/configuration register*/
#define MX25L_CMD_4PP                               0x38 /*quad page program*/
#define MX25L_CMD_SE                                0x20 /*sector erase*/
#define MX25L_CMD_BE_32K                            0x52 /*block erase 32KB*/
#define MX25L_CMD_BE_64K                            0xD8 /*block erase 64KB*/
#define MX25L_CMD_CE                                0x60 /*chip erase 60H or C7H (hex)*/
#define MX25L_CMD_PP                                0x02 /*page program*/
#define MX25L_CMD_DP                                0xB9 /*Deep power down*/
#define MX25L_CMD_RDP                               0xAB /*Release from deep power down*/
#define MX25L_CMD_PGM_SUSPEND                       0x75 /*Suspends Program*/
#define MX25L_CMD_ERS_SUSPEND                       0xB0 /*Suspends Erase*/
#define MX25L_CMD_PGM_RESUME                        0x7A /*Resumes Program*/
#define MX25L_CMD_ERS_RESUME                        0x30 /*Resumes Erase*/
#define MX25L_CMD_RDID                              0x9F /*read identification*/
#define MX25L_CMD_RES                               0xAB /*read electronic ID*/
#define MX25L_CMD_REMS                              0x90 /*read electronic manufacturer & device ID*/
#define MX25L_CMD_ENSO                              0xB1 /*enter secured OTP*/
#define MX25L_CMD_EXSO                              0xC1 /*exit secured OTP*/
#define MX25L_CMD_WRSCUR                            0x2F /*write security register*/
#define MX25L_CMD_RDSCUR                            0x2B /*read security register*/
#define MX25L_CMD_RSTEN                             0x66 /*Reset Enable*/
#define MX25L_CMD_RST                               0x99 /*Reset Memory*/
#define MX25L_CMD_RDSFDP                            0x5A /*Read SFDP (Serial Flash Discoverable Parameter) mode*/
#define MX25L_CMD_SBL                               0xC0 /*Set Burst Length C0/77 (hex)*/
#define MX25L_CMD_NOP                               0x00 /*No Operation*/
/** @} */

/**
 * @name    Status register bits
 * @{
 */
#define MX25L_STS_REG_SRWD_MASK                     0x80U /*Status Register Write Disable*/
#define MX25L_STS_REG_QE_MASK                       0x40U /*Quad Enable: 1= Quad Enable, 0=not Quad Enable*/
#define MX25L_STS_REG_QE_ENABLE                     0x40U /*Quad Enable*/
#define MX25L_STS_REG_QE_DISABLE                    0x00U /*Quad Disable*/
#define MX25L_STS_REG_BP3_MASK                      0x20U
#define MX25L_STS_REG_BP2_MASK                      0x10U
#define MX25L_STS_REG_BP1_MASK                      0x08U
#define MX25L_STS_REG_BP0_MASK                      0x04U
#define MX25L_STS_REG_WEL_MASK                      0x02U /*write enable latch*/
#define MX25L_STS_REG_WIP_MASK                      0x01U /*write in progress bit*/
/** @} */

/**
 * @name    Configuration register bits
 * @{
 */
#define MX25L_CFG_REG_DC_MASK                       0x40U /*Dummy Cycle*/
#define MX25L_CFG_REG_TB_MASK                       0x08U /*top/bottom selected*/
#define MX25L_CFG_REG_ODS_MASK                      0x01U /*Output driver strength*/

#define MX25L_CFG_REG_DC_2READ_4CYCLES              0x00U
#define MX25L_CFG_REG_DC_2READ_8CYCLES              0x40U
#define MX25L_CFG_REG_DC_4READ_6CYCLES              0x00U
#define MX25L_CFG_REG_DC_4READ_10CYCLES             0x40U
/** @} */

/**
 * @name    Security register bits
 * @{
 */
#define MX25L_SECURITY_REG_E_FAIL_MASK              0x40U /*erase fail or not*/
#define MX25L_SECURITY_REG_P_FAIL_MASK              0x20U /*program fail or not*/
#define MX25L_SECURITY_REG_ESB_MASK                 0x08U /*Erase Suspend status*/
#define MX25L_SECURITY_REG_PSB_MASK                 0x04U /*Program Suspend status*/
#define MX25L_SECURITY_REG_LDSO_MASK                0x02U /*lock-down 1st 4K-bit Secured OTP*/
#define MX25L_SECURITY_REG_SOTPIB_MASK              0x01U /*Secured OTP Indicator bit (2nd 4K-bit Secured OTP)*/
/** @} */

/**
 * @name    Bus interface modes.
 * @{
 */
#define MX25L_BUS_MODE_WSPI1L                       1U
#define MX25L_BUS_MODE_WSPI2L                       2U
#define MX25L_BUS_MODE_WSPI4L                       4U
/** @} */

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @brief   Switch WSPI bus width on initialization.
 * @details A bus width initialization is performed by enabling Quad mode.
 *          If the flash device is configured using the Non Volatile Configuration
 *          Register then this option is not required.
 * @note    This option is only valid in WSPI bus mode.
 */
#if !defined(MX25L_SWITCH_WIDTH) || defined(__DOXYGEN__)
#define MX25L_SWITCH_WIDTH                   FALSE
#endif

/**
 * @brief   Device bus mode to be used.
 * #note    if @p MX25L_SWITCH_WIDTH is @p FALSE then this is the bus mode
 *          that the device is expected to be using.
 * #note    if @p MX25L_SWITCH_WIDTH is @p TRUE then this is the bus mode
 *          that the device will be switched in.
 * @note    This option is only valid in WSPI bus mode.
 */
#if !defined(MX25L_BUS_MODE) || defined(__DOXYGEN__)
#define MX25L_BUS_MODE                       MX25L_BUS_MODE_WSPI4L
#if MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI2L
#error "MX25L6433F DOESN'T support dual mode program, just ignore dual read and 2IO read!"
#endif
#endif

/**
 * @brief   Delays insertions.
 * @details If enabled this options inserts delays into the flash waiting
 *          routines releasing some extra CPU time for threads with lower
 *          priority, this may slow down the driver a bit however.
 */
#if !defined(MX25L_NICE_WAITING) || defined(__DOXYGEN__)
#define MX25L_NICE_WAITING                   TRUE
#endif

/**
 * @brief   Uses 32kB sub-Blocks rather than 64kB Blocks.
 */
#if !defined(MX25L_USE_SUB_BLOCKS) || defined(__DOXYGEN__)
#define MX25L_USE_SUB_BLOCKS                FALSE
#endif

/**
 * @brief   Size of the compare buffer.
 * @details This buffer is allocated in the stack frame of the function
 *          @p flashVerifyErase() and its size must be a power of two.
 *          Larger buffers lead to better verify performance but increase
 *          stack usage for that function.
 */
#if !defined(MX25L_COMPARE_BUFFER_SIZE) || defined(__DOXYGEN__)
#define MX25L_COMPARE_BUFFER_SIZE            32
#endif

/**
 * @brief   Number of dummy cycles for FAST READ, DUAL READ and QUAD READ (1..15).
 * @details This is the number of dummy cycles to be used for FAST READ, DUAL READ
 *          and QUAD READ operations.
 */
#if !defined(MX25L_READ_DUMMY_CYCLES) || defined(__DOXYGEN__)
#define MX25L_READ_DUMMY_CYCLES              8
#endif

/**
 * @brief   Number of dummy cycles for Memory Mapped Mode (Performance Enhanced).
 * @details This is the number of dummy cycles used for Performance Enhanced Memory Mapped
 *          Mode. For Performance Enhanced mode, the dummy cycles are shared with 
 *          the performance enhanced bit settings, which is why these dummy cycles are less
 *          than the Read dummy cycles shown above.
 */
#if !defined(MX25L_MMAP_DUMMY_CYCLES) || defined(__DOXYGEN__)
#define MX25L_MMAP_DUMMY_CYCLES             4
#endif

/**
 * @brief   Number of dummy cycles for 2READ and 4READ (1..15).
 * @details This is the number of dummy cycles to be used for 2READ, 4READ
 *          operations.
 */
#if !defined(MX25L_2READ_DUMMY_4CYCLES) || defined(__DOXYGEN__)
#define MX25L_2READ_DUMMY_4CYCLES              4
#endif
#if !defined(MX25L_2READ_DUMMY_8CYCLES) || defined(__DOXYGEN__)
#define MX25L_2READ_DUMMY_8CYCLES              8
#endif
#if !defined(MX25L_4READ_DUMMY_6CYCLES) || defined(__DOXYGEN__)
#define MX25L_4READ_DUMMY_6CYCLES              6
#endif
#if !defined(MX25L_4READ_DUMMY_10CYCLES) || defined(__DOXYGEN__)
#define MX25L_4READ_DUMMY_10CYCLES             10
#endif

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

#if (MX25L_COMPARE_BUFFER_SIZE & (MX25L_COMPARE_BUFFER_SIZE - 1)) != 0
#error "invalid MX25L_COMPARE_BUFFER_SIZE value"
#endif

#if (MX25L_READ_DUMMY_CYCLES < 1) || (MX25L_READ_DUMMY_CYCLES > 15)
#error "invalid MX25L_READ_DUMMY_CYCLES value (1..15)"
#endif

/**For the following definition, we only consider the normal/fast read, quad read
 * program, and 4IO program
 */
#if (MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI4L) || defined(__DOXYGEN__)
/**
 * @brief   WSPI settings for command only.
 */
#define SNOR_WSPI_CFG_CMD                  (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_NONE          | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_NONE          | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

/**
 * @brief   WSPI settings for command and address.
 */
#define SNOR_WSPI_CFG_CMD_ADDR             (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_ONE_LINE      | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_NONE          | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

/**
 * @brief   WSPI settings for command and data.
 */
#define SNOR_WSPI_CFG_CMD_DATA             (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_NONE          | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_ONE_LINE      | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

/**
 * @brief   WSPI settings for command, address and data.
 */
#define SNOR_WSPI_CFG_CMD_ADDR_DATA_RCV    (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_ONE_LINE      | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_FOUR_LINES    | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

#define SNOR_WSPI_CFG_CMD_ADDR_DATA_SEND   (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_FOUR_LINES    | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_FOUR_LINES    | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

#define SNOR_WSPI_CFG_CMD_ADDR_DATA        ((cmd == MX25L_CMD_4PP)?SNOR_WSPI_CFG_CMD_ADDR_DATA_SEND \
                                            : SNOR_WSPI_CFG_CMD_ADDR_DATA_RCV)

#elif MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI1L
#define SNOR_WSPI_CFG_CMD                  (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_NONE          | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_NONE          | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

#define SNOR_WSPI_CFG_CMD_ADDR             (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_ONE_LINE      | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_NONE          | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

#define SNOR_WSPI_CFG_CMD_DATA             (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_NONE          | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_ONE_LINE      | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)

#define SNOR_WSPI_CFG_CMD_ADDR_DATA        (WSPI_CFG_CMD_MODE_ONE_LINE       | \
                                            WSPI_CFG_ADDR_MODE_ONE_LINE      | \
                                            WSPI_CFG_ALT_MODE_NONE           | \
                                            WSPI_CFG_DATA_MODE_ONE_LINE      | \
                                            WSPI_CFG_CMD_SIZE_8              | \
                                            WSPI_CFG_ADDR_SIZE_24)
#else
#error "invalid MX25L_BUS_MODE setting"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#if !defined(__DOXYGEN__)
extern flash_descriptor_t snor_descriptor;
#endif

#if (SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI) && (WSPI_SUPPORTS_MEMMAP == TRUE)
extern const wspi_command_t snor_memmap_read;
#endif

#ifdef __cplusplus
extern "C" {
#endif
  void snor_device_cb(WSPIDriver *wspip);
  void snor_device_init(SNORDriver *devp);
  flash_error_t snor_device_read(SNORDriver *devp, flash_offset_t offset,
                                 size_t n, uint8_t *rp);
  flash_error_t snor_device_program(SNORDriver *devp, flash_offset_t offset,
                                    size_t n, const uint8_t *pp);
  flash_error_t snor_device_start_erase_all(SNORDriver *devp);
  flash_error_t snor_device_start_erase_sector(SNORDriver *devp,
                                               flash_sector_t sector);
  flash_error_t snor_device_verify_erase(SNORDriver *devp,
                                         flash_sector_t sector);
  flash_error_t snor_device_query_erase(SNORDriver *devp, uint32_t *msec);
  flash_error_t snor_device_read_sfdp(SNORDriver *devp, flash_offset_t offset,
                                      size_t n, uint8_t *rp);
#if (SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI) &&                            \
    (SNOR_DEVICE_SUPPORTS_XIP == TRUE)
  void snor_activate_xip(SNORDriver *devp);
  void snor_reset_xip(SNORDriver *devp);
#endif
#ifdef __cplusplus
}
#endif

#endif /* HAL_FLASH_DEVICE_H */

/** @} */

