/**
 * @file    hal_flash_device.c
 * @brief   Macronix MX25L serial flash driver code.
 *
 * @addtogroup MACROMIX_MX25L
 * @{
 */

#include <string.h>

#include "hal.h"
#include "hal_serial_nor.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

#define PAGE_SIZE                           256U
#define PAGE_MASK                           (PAGE_SIZE - 1U)
#define SECTOR_SIZE                         0x00001000U /*4K sector*/

#if MX25L_USE_SUB_BLOCKS == TRUE
#define BLOCKS_SIZE                         0x00008000U /*32K Block*/
#define CMD_BLOCKS_ERASE                    MX25L_CMD_BE_32K
#else
#define BLOCKS_SIZE                         0x00010000U /*64K Block*/
#define CMD_BLOCKS_ERASE                    MX25L_CMD_BE_64K
#endif

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/**
 * @brief   MX25L descriptor.
 */
flash_descriptor_t snor_descriptor = {
  .attributes       = FLASH_ATTR_ERASED_IS_ONE | FLASH_ATTR_REWRITABLE |
                      FLASH_ATTR_SUSPEND_ERASE_CAPABLE,
  .page_size        = 256U,
  .sectors_count    = 0U,           /* It is overwritten.*/
  .sectors          = NULL,
  .sectors_size     = SECTOR_SIZE,
  .address          = 0U,
  .size             = 0U            /* It is overwritten.*/

};

#if (SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI) || defined(__DOXYGEN__)
#if (WSPI_SUPPORTS_MEMMAP == TRUE) || defined(__DOXYGEN__)
/**
 * @brief   Fast read command for memory mapped mode.
 */
const wspi_command_t snor_memmap_read = {
  .cmd              = MX25L_CMD_4READ,
  .addr             = 0,
  .alt              = 0x0F,
  .dummy            = MX25L_MMAP_DUMMY_CYCLES,
  .cfg              = WSPI_CFG_ADDR_SIZE_24 |           /* The address size is always 3bytes */
                      WSPI_CFG_CMD_MODE_ONE_LINE |      /* The command is always 1 line */
#if MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI1L
                      WSPI_CFG_ADDR_MODE_ONE_LINE |
                      WSPI_CFG_DATA_MODE_ONE_LINE |
#elif MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI2L
                      WSPI_CFG_ADDR_MODE_TWO_LINES |
                      WSPI_CFG_DATA_MODE_TWO_LINES |
#else
                      WSPI_CFG_ADDR_MODE_FOUR_LINES |
                      WSPI_CFG_DATA_MODE_FOUR_LINES |
#endif
                      WSPI_CFG_ALT_MODE_FOUR_LINES |          /* No alt, note.*/
                      WSPI_CFG_ALT_SIZE_8 |
                      WSPI_CFG_SIOO
};
#endif
#endif

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/

#if SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI
/* Initial MX25L_CMD_RDID command.*/
//static const wspi_command_t mx25l_cmd_read_id = {
//  .cmd              = MX25L_CMD_RDID,
//  .cfg              = 0U | WSPI_CFG_CMD_MODE_ONE_LINE |
//                      WSPI_CFG_DATA_MODE_ONE_LINE,

//  .addr             = 0,
//  .alt              = 0,
//  .dummy            = 0
//};
#endif /* SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI */

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

static bool mx25l_find_id(const uint8_t *set, size_t size, uint8_t element) {
  size_t i;

  for (i = 0; i < size; i++) {
    if (set[i] == element) {
      return true;
    }
  }
  return false;
}

static flash_error_t mx25l_poll_status(SNORDriver *devp) {
  static uint8_t sts __attribute__((section(".axi_nocache")));

  do {
#if MX25L_NICE_WAITING == TRUE
    osalThreadSleepMilliseconds(1);
#endif
    /* Read Status Register command.*/
    bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                    1, &sts);
  } while ((sts & MX25L_STS_REG_WIP_MASK) != 0U);

  /* Read Security Register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSCUR,
                  1, &sts);

  /* Checking for errors.*/
  if ((sts & MX25L_SECURITY_REG_P_FAIL_MASK) != 0U) {
    /* Program operation failed.*/
    return FLASH_ERROR_PROGRAM;
  }

  return FLASH_NO_ERROR;
}

#if (SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI) || defined(__DOXYGEN__)
static void mx25l_reset_memory(SNORDriver *devp) {

  /* 1x N25Q_CMD_RESET_ENABLE command.*/
  bus_cmd(devp->config->busp, MX25L_CMD_RSTEN);
  /* 1x N25Q_CMD_RESET_MEMORY command.*/
  bus_cmd(devp->config->busp, MX25L_CMD_RST);

  /*Add some delay for reset settling time otherwise device don't respond
     to the commands given after */
   chThdSleepMilliseconds(15);
}
#endif /* SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI */

flash_error_t mx25l_write_status_reg(SNORDriver *devp,
                             uint8_t bit_mask, uint8_t bit_value) {
  static uint8_t sts_cfg[2] __attribute((aligned (32))) __attribute__((section(".axi_nocache"))); 

  /* Read Status Register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                  1, &sts_cfg[0]);

  /* Read Configuration Register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDCR,
                  1, &sts_cfg[1]);

  do {
    /* Enabling write operation.*/
    bus_cmd(devp->config->busp, MX25L_CMD_WREN);
    /* Read Status register command.*/
    bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                    1, &sts_cfg[0]);
  } while((sts_cfg[0] & MX25L_STS_REG_WEL_MASK) == 0U);

  sts_cfg[0] = (sts_cfg[0] & (~bit_mask)) | bit_value;
  bus_cmd_send(devp->config->busp, MX25L_CMD_WRSR,
                2, sts_cfg);

  return FLASH_NO_ERROR;
}

flash_error_t mx25l_write_config_reg(SNORDriver *devp,
                             uint8_t bit_mask, uint8_t bit_value) {
  static uint8_t sts_cfg[2] __attribute((aligned (32))) __attribute__((section(".axi_nocache"))); 

  /* Read Status Register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                  1, &sts_cfg[0]);

  /* Read Configuration Register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDCR,
                  1, &sts_cfg[1]);

  do {
    /* Enabling write operation.*/
    bus_cmd(devp->config->busp, MX25L_CMD_WREN);
    /* Read Status register command.*/
    bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                    1, &sts_cfg[0]);
  } while((sts_cfg[0] & MX25L_STS_REG_WEL_MASK) == 0U);

  sts_cfg[1] = (sts_cfg[1] & (~bit_mask)) | bit_value;
  bus_cmd_send(devp->config->busp, MX25L_CMD_WRSR,
                2, sts_cfg);

  return FLASH_NO_ERROR;
}

static const uint8_t mx25l_manufacturer_ids[] = MX25L_SUPPORTED_MANUFACTURE_IDS;
static const uint8_t mx25l_memory_type_ids[] = MX25L_SUPPORTED_MEMORY_TYPE_IDS;

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
void snor_device_cb(WSPIDriver *wspip)
{
    (void)wspip;
}

void snor_device_init(SNORDriver *devp) {
  static uint8_t sts __attribute__((section(".axi_nocache"))) __attribute__((aligned (32))); 
  static uint8_t device_id[sizeof devp->device_id] __attribute__((section(".axi_nocache"))); 

#if SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_SPI
  /* Reading device ID.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDID,
                  sizeof devp->device_id, devp->device_id);

#else /* SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI */
#if SNOR_DEVICE_SUPPORTS_XIP == TRUE
  /* Attempting a reset of the XIP mode, it could be in an unexpected state
     because a CPU reset does not reset the memory too.*/
  snor_reset_xip(devp);
#endif

  /* Attempting a reset of the device, it could be in an unexpected state
     because a CPU reset does not reset the memory too.*/
  mx25l_reset_memory(devp);

  /* Reading device ID and unique ID.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDID,
                  sizeof devp->device_id, device_id);

  /* Copy Device ID to devp->device_id */
  for(uint8_t i = 0; i < sizeof devp->device_id; i++)
  {
    devp->device_id[i] = device_id[i];
  }

  /* We need to enable QUAD mode for 4 Line operations*/
#if MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI4L
  /* Read Status register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                  1, &sts);

  if ((sts & MX25L_STS_REG_QE_MASK) == 0U) {
    do {
      /* Enabling write operation.*/
      bus_cmd(devp->config->busp, MX25L_CMD_WREN);
      /* Read Status register command.*/
      bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                      1, &sts);
    } while((sts & MX25L_STS_REG_WEL_MASK) == 0U);

    /*enable QUAD*/
    sts |= MX25L_STS_REG_QE_MASK;
    bus_cmd_send(devp->config->busp, MX25L_CMD_WRSR,
                  1, &sts);
  }
#endif
#endif /* SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI */

  /* Checking if the device is white listed.*/
  osalDbgAssert(mx25l_find_id(mx25l_manufacturer_ids,
                             sizeof mx25l_manufacturer_ids,
                             devp->device_id[0]),
                "invalid manufacturer id");
  osalDbgAssert(mx25l_find_id(mx25l_memory_type_ids,
                             sizeof mx25l_memory_type_ids,
                             devp->device_id[1]),
                "invalid memory type id");

  /* Setting up the device size.*/
  snor_descriptor.sectors_count = (1U << (size_t)devp->device_id[2]) /
                                  SECTOR_SIZE;
  snor_descriptor.size = (size_t)snor_descriptor.sectors_count * SECTOR_SIZE;
}

flash_error_t snor_device_read(SNORDriver *devp, flash_offset_t offset,
                               size_t n, uint8_t *rp) {

#if SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI
#if MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI4L
  /* Quad read command in MX25L_BUS_MODE_WSPI4L mode.*/
  bus_cmd_addr_dummy_receive(devp->config->busp, MX25L_CMD_QREAD,
                             offset, MX25L_READ_DUMMY_CYCLES, n, rp);
#else
  /* Fast read command in WSPI mode.*/
  bus_cmd_addr_dummy_receive(devp->config->busp, MX25L_CMD_FAST_READ,
                             offset, MX25L_READ_DUMMY_CYCLES, n, rp);
#endif
#else
  /* Normal read command in SPI mode.*/
  bus_cmd_addr_receive(devp->config->busp, MX25L_CMD_READ,
                       offset, n, rp);
#endif

  return FLASH_NO_ERROR;
}

flash_error_t snor_device_program(SNORDriver *devp, flash_offset_t offset,
                                  size_t n, const uint8_t *pp) {

  /* Data is programmed page by page.*/
  while (n > 0U) {
    flash_error_t err;

    /* Data size that can be written in a single program page operation.*/
    size_t chunk = (size_t)(((offset | PAGE_MASK) + 1U) - offset);
    if (chunk > n) {
      chunk = n;
    }

    /* Enabling write operation.*/
    bus_cmd(devp->config->busp, MX25L_CMD_WREN);

#if MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI4L
    /* Page program command.*/
    bus_cmd_addr_send(devp->config->busp, MX25L_CMD_4PP, offset,
                      chunk, pp);
#else
    /* Page program command.*/
    bus_cmd_addr_send(devp->config->busp, MX25L_CMD_PP, offset,
                      chunk, pp);
#endif

    /* Wait for status and check errors.*/
    err = mx25l_poll_status(devp);
    if (err != FLASH_NO_ERROR) {
      return err;
    }

    /* Next page.*/
    offset += chunk;
    pp     += chunk;
    n      -= chunk;
  }

  return FLASH_NO_ERROR;
}

flash_error_t snor_device_start_erase_all(SNORDriver *devp) {

  /* Enabling write operation.*/
  bus_cmd(devp->config->busp, MX25L_CMD_WREN);

  /* Chip erase command.*/
  bus_cmd(devp->config->busp, MX25L_CMD_CE);

  return FLASH_NO_ERROR;
}

flash_error_t snor_device_start_erase_sector(SNORDriver *devp,
                                             flash_sector_t sector) {
  flash_offset_t offset = (flash_offset_t)(sector * SECTOR_SIZE);

  /* Enabling write operation.*/
  bus_cmd(devp->config->busp, MX25L_CMD_WREN);

  /* Sector erase command.*/
  bus_cmd_addr(devp->config->busp, MX25L_CMD_SE, offset);

  return FLASH_NO_ERROR;
}

flash_error_t snor_device_verify_erase(SNORDriver *devp,
                                       flash_sector_t sector) {
  static uint8_t cmpbuf[MX25L_COMPARE_BUFFER_SIZE] __attribute__((section(".axi_nocache"))); 
  flash_offset_t offset;
  size_t n;

  /* Read command.*/
  offset = (flash_offset_t)(sector * SECTOR_SIZE);
  n = SECTOR_SIZE;
  while (n > 0U) {
    uint8_t *p;

#if SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI
#if MX25L_BUS_MODE == MX25L_BUS_MODE_WSPI4L
    /* Quad read command in MX25L_BUS_MODE_WSPI4L mode.*/
    bus_cmd_addr_dummy_receive(devp->config->busp, MX25L_CMD_QREAD,
                               offset, MX25L_READ_DUMMY_CYCLES,
                                sizeof cmpbuf, cmpbuf);
#else
    bus_cmd_addr_dummy_receive(devp->config->busp, MX25L_CMD_FAST_READ,
                               offset, MX25L_READ_DUMMY_CYCLES,
                                sizeof cmpbuf, cmpbuf);
#endif
#else
   /* Normal read command in SPI mode.*/
    bus_cmd_addr_receive(devp->config->busp, MX25L_CMD_READ,
                         offset, sizeof cmpbuf, cmpbuf);
#endif

    // wait for the transaction done.
    while(isWspiTransfDone(devp->config->busp) == FALSE);

    /* Checking for erased state of current buffer.*/
    for (p = cmpbuf; p < &cmpbuf[MX25L_COMPARE_BUFFER_SIZE]; p++) {
      if (*p != 0xFFU) {
        /* Ready state again.*/
        devp->state = FLASH_READY;

        return FLASH_ERROR_VERIFY;
      }
    }

    offset += sizeof cmpbuf;
    n -= sizeof cmpbuf;
  }

  return FLASH_NO_ERROR;
}

flash_error_t snor_device_query_erase(SNORDriver *devp, uint32_t *msec) {
  static uint8_t __attribute__((section(".axi_nocache")))  sts, security;

  /* Read Status register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSR,
                  1, &sts);

  /* Read Security register command.*/
  bus_cmd_receive(devp->config->busp, MX25L_CMD_RDSCUR,
                  1, &security);

  /* If the WIP bit is none-zero (busy) or the flash in a suspended state then
     report that the operation is still in progress.*/
  if (((sts & MX25L_STS_REG_WIP_MASK) != 0U) ||
      ((security & MX25L_SECURITY_REG_ESB_MASK) != 0U)) {

    /* Recommended time before polling again, this is a simplified
       implementation.*/
    if (msec != NULL) {
      *msec = 1U;
    }

    return FLASH_BUSY_ERASING;
  }

  /* Checking for errors.*/
  if ((security & MX25L_SECURITY_REG_E_FAIL_MASK) != 0U) {
    /* Erase operation failed.*/
    return FLASH_ERROR_ERASE;
  }

  return FLASH_NO_ERROR;
}

flash_error_t snor_device_read_sfdp(SNORDriver *devp, flash_offset_t offset,
                                    size_t n, uint8_t *rp) {

  (void)devp;
  (void)rp;
  (void)offset;
  (void)n;

  return FLASH_NO_ERROR;
}

#if (SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI) || defined(__DOXYGEN__)
#if SNOR_DEVICE_SUPPORTS_XIP == TRUE
void snor_activate_xip(SNORDriver *devp) {
  // MX25L DOESN'T Support XIP
  (void)devp;
}

void snor_reset_xip(SNORDriver *devp) {
  // MX25L DOESN'T Support XIP
  (void)devp;
}
#endif
#endif /* SNOR_BUS_DRIVER == SNOR_BUS_DRIVER_WSPI */

/** @} */
