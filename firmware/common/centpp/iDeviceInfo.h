#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef void (*factoryResetCb)(void);
typedef uint32_t (*getHardwareRevisionCb)(void);
typedef uint8_t (*getModelIdCb)(void);
typedef void (*getFirmwareVersionCb)(uint8_t *major_version, uint8_t *minor_version, uint16_t *build);
typedef const uint8_t *(*getDeviceSerialNumberCb)(void);

typedef struct {
    getModelIdCb getModelId;
    getFirmwareVersionCb getFirmwareVersion;
    getHardwareRevisionCb getHardwareRevision;
    getDeviceSerialNumberCb getDeviceSerialNumber;
    factoryResetCb factoryReset;
} ideviceinfo_config_t;

bool CentPPRegister_iDeviceInfo(const ideviceinfo_config_t* ideviceinfo_cfg);
