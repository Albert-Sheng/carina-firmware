#include "iGenericDFU.h"

#include <centpp-src/device/mbg_centpp_api.h>

#include <stdlib.h>

/******************************************************
 *[0x010A]              iGenericDFU
 ******************************************************/
#define CENTPP_FEATURE_ID_iGENERIC_DFU  0x010A

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *               Variables Definitions
 ******************************************************/
static uint8_t s_dfu_updated = false;
static const igenericdfu_cfg_t *s_igenericdfu_cfg;
static tCentPPHeader iGenericDFU_EventHeader = {.featureIdx = CENTPP_FEATUREIDX_ERROR, .SWID = 0, .functionIndex = 0};

/******************************************************
 *               Function Definitions
 ******************************************************/
static tCentPPResponse *iGenericDFU_Prepare(uint16_t cbInput, tCentPPHeader *pIn);

#if APP_IMAGE
static tCentPPResponse *iGenericDFU_Confirm(uint16_t cbInput, tCentPPHeader *pIn);

static const tCentPPFunction iGenericDFU_Functions[] = {
    { 0, iGenericDFU_Prepare    },
    { 4, NULL                   },
    { 0, NULL                   },
    { 0, NULL                   },
    { 0, iGenericDFU_Confirm    },
};
#else
static tCentPPResponse *iGenericDFU_Write(uint16_t cbInput, tCentPPHeader *pIn);
static tCentPPResponse *iGenericDFU_Verify(uint16_t cbInput, tCentPPHeader *pIn);
static tCentPPResponse *iGenericDFU_Install(uint16_t cbInput, tCentPPHeader *pIn);

static const tCentPPFunction iGenericDFU_Functions[] = {
    { 0, iGenericDFU_Prepare    },
    { 4, iGenericDFU_Write      },
    { 0, iGenericDFU_Verify     },
    { 0, iGenericDFU_Install    },
    { 0, NULL                   },
};
#endif

// Type set to 0, as this is a "normal" feature (not hidden, engineering or obsolete)
static tCentPPFeature iGenericDFU = {
    .featureID      = CENTPP_FEATURE_ID_iGENERIC_DFU,
    .featureType    = FEATURE_TYPE_NORMAL,
    .featureVersion = 0x04,
    .functionCount  = ITEMCNT(iGenericDFU_Functions),
    .remoteHostIdx  = 0xFF,
    .Functions      = iGenericDFU_Functions,
    .next           = NULL
};

bool
CentPPRegister_iGenericDFU(const igenericdfu_cfg_t* igenericdfu_cfg)
{
    if (CentPPRegisterFeature(&iGenericDFU) != CENTPP_RES_SUCCESS)
        return FALSE;

    s_igenericdfu_cfg = igenericdfu_cfg;
    if (iGenericDFU_EventHeader.featureIdx == CENTPP_FEATUREIDX_ERROR) {
        if ((iGenericDFU_EventHeader.featureIdx = GetCentPPFeatureIndex(CENTPP_FEATURE_ID_iGENERIC_DFU)) == CENTPP_FEATUREIDX_ERROR) {
            centpp_log(CENTPP_LOG_ERROR, " ERROR! Invalid feature Index for iGenericDFU");
            return FALSE;
        }
    }

    return TRUE;
}

static
tCentPPResponse*
iGenericDFU_Prepare(uint16_t cbInput, tCentPPHeader* pIn)
{
    (void)cbInput;

    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_Prepare()");

    uint8_t output[4];

    if (s_igenericdfu_cfg == NULL || s_igenericdfu_cfg->prepareEvent == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - iGenericDFU_Prepare callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    // Assemble response
    output[0] = s_igenericdfu_cfg->maxEraseDurationSecs;
    output[1] = s_igenericdfu_cfg->dfuWriteSize >> 8;   // MSB
    output[2] = s_igenericdfu_cfg->dfuWriteSize & 0xFF; // LSB
    output[3] = 0xFF; // unable to determine active partition at this time. Prepare event will send the active partition number to the host

    // Prepare
    s_igenericdfu_cfg->prepareEvent();

    // Respond
    return CentPPBuildResponseSimple(pIn, output, sizeof(output));
}

#if !APP_IMAGE
static
tCentPPResponse*
iGenericDFU_Write(uint16_t cbInput, tCentPPHeader* pIn)
{
    (void)cbInput;

    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_Write()");

    if(s_igenericdfu_cfg == NULL || s_igenericdfu_cfg->writeBlock == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - iGenericDFU_Write callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    if(s_igenericdfu_cfg->writeBlock(pIn->payload, s_igenericdfu_cfg->dfuWriteSize) != true) {
        centpp_log(CENTPP_LOG_ERROR, "DFU write failed");
        return CentPPBuildError(pIn, CENTPP_INVALID_SIZE);
    }

    return CentPPBuildResponseSimple(pIn, NULL, FALSE);
}

static
tCentPPResponse*
iGenericDFU_Verify(uint16_t cbInput, tCentPPHeader* pIn)
{
    (void)cbInput;

    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_Verify()");

    if(s_igenericdfu_cfg == NULL || s_igenericdfu_cfg->initiateFirmwareVerification == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - iGenericDFU_Verify callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    uint8_t output = s_igenericdfu_cfg->maxVerifyDurationSecs;
    s_igenericdfu_cfg->initiateFirmwareVerification();

    return CentPPBuildResponseSimple(pIn, &output, sizeof(uint8_t));
}

static
tCentPPResponse*
iGenericDFU_Install(uint16_t cbInput, tCentPPHeader* pIn)
{
    (void)cbInput;

    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_Install()");

    if(s_igenericdfu_cfg == NULL || s_igenericdfu_cfg->initiateFirmwareInstallation == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - iGenericDFU_Install callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    uint8_t output = s_igenericdfu_cfg->maxInstallDurationSecs;
    s_igenericdfu_cfg->initiateFirmwareInstallation();

    return CentPPBuildResponseSimple(pIn, &output, sizeof(uint8_t));
}
#else
static
tCentPPResponse*
iGenericDFU_Confirm(uint16_t cbInput, tCentPPHeader* pIn)
{
    (void)cbInput;

    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_Confirm()");

    /*
     * This function is required to be received by the newly installed FW to ensure that the new
     * firmware can successfully connect and communicate with the host. Hence, send success.
     */
    return CentPPBuildResponseSimple(pIn, NULL, FALSE);
}
#endif

static
void
iGenericDFU_PrepareCompleteEvent(void* arg)
{
    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_PrepareCompleteEvent()");

    tCentPPResponse *response = NULL;
    iGenericDFU_EventHeader.functionIndex = 0;
    uint8_t output[2];
    output[0] = 0;  // Erase success
    output[1] = 1;  // Always partition 1

    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU prepare complete");

    response = CentPPBuildResponseSimple(&iGenericDFU_EventHeader, output, sizeof(output));

    if(response) {
        cpl_transmit_to_host((uint8_t *)&response->data, response->cbLen, arg);
        free(response);
    } else {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - %s failed!!", __func__);
    }
}

static
void
iGenericDFU_InstallCompleteEvent(void* arg)
{
    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_InstallCompleteEvent()");

    tCentPPResponse *response = NULL;

    iGenericDFU_EventHeader.functionIndex = 3;
    centpp_log(CENTPP_LOG_DEBUG, " iGenericDFU install complete");
    response = CentPPBuildResponseSimple(&iGenericDFU_EventHeader, (uint8_t *)&s_dfu_updated, sizeof(uint8_t));

    if(response) {
        cpl_transmit_to_host((uint8_t *)&response->data, response->cbLen, arg);
        free(response);
    } else {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - %s failed!!", __func__);
    }
}

static
void
iGenericDFU_VerifyCompleteEvent(void* arg)
{
    centpp_log(CENTPP_LOG_DEBUG, "iGenericDFU_VerifyCompleteEvent()");

    tCentPPResponse *response = NULL;

    if(s_igenericdfu_cfg == NULL || s_igenericdfu_cfg->getSignatureResult == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - iGenericDFU_VerifyCompleteEvent callback invalid");
        return;
    }

    iGenericDFU_EventHeader.functionIndex = 1;
    uint8_t output = s_igenericdfu_cfg->getSignatureResult();
    centpp_log(CENTPP_LOG_DEBUG, " iGenericDFU verify complete");
    response = CentPPBuildResponseSimple(&iGenericDFU_EventHeader, &output, 1);

    if(response) {
        cpl_transmit_to_host((uint8_t *)&response->data, response->cbLen, arg);
        free(response);
    } else {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! - %s failed!!", __func__);
    }
}

bool
CentPPPrepareCompleteEvent(void)
{
    centpp_log(CENTPP_LOG_DEBUG, __func__);

    centpp_transport_t *transport_info = NULL;

    if((transport_info = centpp_prepare_event_transport_header()) == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! %s - Failed to allocate memory transport_info", __func__);
        return FALSE;
    }

    return mbg_centpp_tx_fifo_push(iGenericDFU_PrepareCompleteEvent, transport_info);
}

bool
CentPPVerifyCompleteEvent(void)
{
    centpp_log(CENTPP_LOG_DEBUG, __func__);

    centpp_transport_t *transport_info = NULL;

    if((transport_info = centpp_prepare_event_transport_header()) == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! %s - Failed to allocate memory transport_info", __func__);
        return FALSE;
    }

    return mbg_centpp_tx_fifo_push(iGenericDFU_VerifyCompleteEvent, transport_info);
}

bool
CentPPInstallCompleteEvent(bool dfu_status)
{
    centpp_log(CENTPP_LOG_DEBUG, __func__);

    centpp_transport_t *transport_info = NULL;

    s_dfu_updated = dfu_status;

    if((transport_info = centpp_prepare_event_transport_header()) == NULL) {
        centpp_log(CENTPP_LOG_ERROR, " ERROR! %s - Failed to allocate memory transport_info", __func__);
        return FALSE;
    }

    return mbg_centpp_tx_fifo_push(iGenericDFU_InstallCompleteEvent, transport_info);
}
