#include "centpp.h"
#include "debug.h"

#include "mbg_centpp_api.h"

#include <chprintf.h>
#include <stdio.h>

/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define __SWAP32__(val) ( (uint32_t) ((((val) & 0xFF000000UL) >> 24 ) | \
                                      (((val) & 0x00FF0000UL) >> 8)   | \
                                      (((val) & 0x0000FF00UL) << 8)   | \
                                      (((val) & 0x000000FFUL) << 24)) )

#define __SWAP16__(val) ( (uint16_t) ((((val) & 0xFF00) >> 8) | \
                                      (((val) & 0x00FF) << 8)))


#define LOG_BUF_SIZE    128


/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

void centpp_resp_mutex_lock(void) {
    CentPP_MutexLock();
}

void centpp_resp_mutex_unlock(void) {
    CentPP_MutexUnlock();
}

void centpp_alive(void) {

}

uint16_t centpp_ntohs(uint16_t val) {
    return __SWAP16__(val);
}

uint16_t centpp_htons(uint16_t val) {
    return centpp_ntohs(val);
}

uint32_t centpp_htonl(uint32_t val) {
    return centpp_ntohl(val);
}


uint32_t centpp_ntohl(uint32_t val) {
    return __SWAP32__(val);
}

void centpp_log(CENTPP_LOG_LEVEL_T level, const char *fmt, ...) {
    char log_buf[LOG_BUF_SIZE] = { 0 };
    va_list args;

    va_start(args, fmt);

    chvsnprintf(log_buf, LOG_BUF_SIZE, fmt, args);

    switch(level) {
        case CENTPP_LOG_ERROR:
            Dbg_printf(DBG_ERROR, log_buf);
            break;
        case CENTPP_LOG_INFO:
            Dbg_printf(DBG_INFO, log_buf);
            break;
        case CENTPP_LOG_DEBUG:
            Dbg_printf(DBG_CENTPP, log_buf);
            break;
        default:
            break;
    }

    va_end(args);
}

void centpp_tx_fifo_data_avail_event(void) {
    CentPP_TxDataAvailable();
}

uint32_t centpp_get_time_ms(void) {
    return TIME_I2MS(chVTGetSystemTimeX());
}

