#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef void (*prepareEventCb)(void);
typedef uint8_t (*getSignatureResultCb)(void);
typedef bool (*writeBlockCb)(uint8_t *data, size_t bytes);
typedef void (*initiateFirmwareVerificationCb)(void);
typedef void (*initiateFirmwareInstallationCb)(void);

typedef struct {
    uint16_t dfuWriteSize;
    uint16_t maxVerifyDurationSecs;
    uint16_t maxInstallDurationSecs;
    uint16_t maxEraseDurationSecs;
    writeBlockCb writeBlock;
    prepareEventCb prepareEvent;
    getSignatureResultCb getSignatureResult;
    initiateFirmwareVerificationCb initiateFirmwareVerification;
    initiateFirmwareInstallationCb initiateFirmwareInstallation;
} igenericdfu_cfg_t;

bool CentPPRegister_iGenericDFU(const igenericdfu_cfg_t* igenericdfu_cfg);
