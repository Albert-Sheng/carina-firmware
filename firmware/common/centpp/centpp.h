#pragma once

#include "centpp_cfg.h"

#include <stdint.h>
#include <ch.h>

#ifdef ENABLE_CENTPP
    bool CentPP_Start(const centpp_config_t *centpp_cfg);
    bool CentPP_Stop(void);
    void CentPP_TxDataAvailable(void);
    void CentPP_MutexLock(void);
    void CentPP_MutexUnlock(void);
    bool CentPPPrepareCompleteEvent(void);
    bool CentPPVerifyCompleteEvent(void);
    bool CentPPInstallCompleteEvent(bool dfu_status);
#else
    #define CentPP_Start(centpp_cfg) false
    #define CentPP_Stop() false
    #define CentPP_TxDataAvailable()
    #define CentPP_MutexLock()
    #define CentPP_MutexUnlock()
    #define CentPPPrepareCompleteEvent(void) false
    #define CentPPVerifyCompleteEvent(void) false
#endif
