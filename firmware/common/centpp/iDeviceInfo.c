#include "iDeviceInfo.h"

#include <centpp-src/device/mbg_centpp_api.h>

#include <ch.h>

#include <stdlib.h>
#include <string.h>

#define CENTPP_FEATURE_ID_iDEVICE_INFO 0x0100

typedef struct {
    uint8_t modelID;
    uint8_t hw_rev;
    uint16_t colorcode;
    uint32_t SKUAttribute;
} GetHWInfoRsp_t;

typedef struct {
    uint8_t major_version;
    uint8_t minor_version;
    uint16_t build_version;
} FwVersionRsp_t;

static tCentPPResponse *CentPPiDeviceInfoGetHardwareInfo(uint16_t cbInput, tCentPPHeader *pIn);
static tCentPPResponse *CentPPiDeviceInfoGetFirmwareVersion(uint16_t cbInput, tCentPPHeader *pIn);
static tCentPPResponse *CentPPiDeviceInfoGetSerialNumber(uint16_t cbInput, tCentPPHeader *pIn);
static tCentPPResponse *CentPPiDeviceInfoGetUpTime(uint16_t cbInput, tCentPPHeader *pIn);
static tCentPPResponse *CentPPiDeviceFactoryResetInput(uint16_t cbInput, tCentPPHeader *pIn);

static const tCentPPFunction iDeviceInfoFunctions[] = {
    { 0, CentPPiDeviceInfoGetHardwareInfo       },
    { 0, CentPPiDeviceInfoGetFirmwareVersion    },
    { 0, CentPPiDeviceInfoGetSerialNumber       },
    { 0, CentPPiDeviceInfoGetUpTime             },
    { 0, CentPPiDeviceFactoryResetInput         },
};

static const ideviceinfo_config_t *s_ideviceinfo_cfg;

static tCentPPFeature iDeviceInfo = {
    .featureID      = CENTPP_FEATURE_ID_iDEVICE_INFO,
    .featureType    = FEATURE_TYPE_NORMAL,
    .featureVersion = 0x02,
    .functionCount  = ITEMCNT(iDeviceInfoFunctions),
    .remoteHostIdx  = 0xFF,
    .Functions      = iDeviceInfoFunctions,
    .next           = NULL
};

/******************************************************
 *               Function Definitions
 ******************************************************/
bool CentPPRegister_iDeviceInfo(const ideviceinfo_config_t *ideviceinfo_cfg) {
    s_ideviceinfo_cfg = ideviceinfo_cfg;
    return (CentPPRegisterFeature(&iDeviceInfo) == CENTPP_RES_SUCCESS);
}

static tCentPPResponse *CentPPiDeviceInfoGetHardwareInfo(uint16_t cbInput, tCentPPHeader *pIn) {
    ((void)(cbInput));

    GetHWInfoRsp_t rsp = { 0 };

    if(s_ideviceinfo_cfg == NULL || s_ideviceinfo_cfg->getModelId == NULL || s_ideviceinfo_cfg->getHardwareRevision == NULL) {
        centpp_log(CENTPP_LOG_ERROR, "ERROR! - CentPPiDeviceInfoGetHardwareInfo callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    rsp.modelID   = s_ideviceinfo_cfg->getModelId();
    rsp.hw_rev    = s_ideviceinfo_cfg->getHardwareRevision();

    return CentPPBuildResponseSimple(pIn, (uint8_t *)&rsp, sizeof(rsp));
}

static tCentPPResponse *CentPPiDeviceInfoGetFirmwareVersion(uint16_t cbInput, tCentPPHeader *pIn) {
    ((void)(cbInput));

    FwVersionRsp_t rsp = {0};

    if(s_ideviceinfo_cfg == NULL || s_ideviceinfo_cfg->getFirmwareVersion == NULL) {
        centpp_log(CENTPP_LOG_ERROR, "ERROR! - CentPPiDeviceInfoGetFirmwareVersion callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    s_ideviceinfo_cfg->getFirmwareVersion(&rsp.major_version, &rsp.minor_version, &rsp.build_version);
    rsp.build_version = centpp_htons(rsp.build_version);
    return CentPPBuildResponseSimple(pIn, (uint8_t *)&rsp, sizeof(rsp));
}

static tCentPPResponse *CentPPiDeviceInfoGetSerialNumber(uint16_t cbInput, tCentPPHeader *pIn) {
    ((void)(cbInput));

    if(s_ideviceinfo_cfg == NULL || s_ideviceinfo_cfg->getDeviceSerialNumber == NULL) {
        centpp_log(CENTPP_LOG_ERROR, "ERROR! - CentPPiDeviceInfoGetSerialNumber callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    uint8_t output[255 + 1] = {0};
    const uint8_t len = strlen((char *)s_ideviceinfo_cfg->getDeviceSerialNumber());
    output[0] = len;
    memcpy(&output[1], s_ideviceinfo_cfg->getDeviceSerialNumber(), len);
    return CentPPBuildResponseSimple(pIn, output, strlen((char *)output));
}

static tCentPPResponse *CentPPiDeviceInfoGetUpTime(uint16_t cbInput, tCentPPHeader *pIn) {
    ((void)(cbInput));
    systime_t current_time_ms = centpp_get_time_ms();
    centpp_log(CENTPP_LOG_DEBUG, "Up Time: %lu ms", current_time_ms);
    current_time_ms = centpp_htonl(current_time_ms);
    return CentPPBuildResponseSimple(pIn, (uint8_t *)&current_time_ms, sizeof(uint32_t));
}

static tCentPPResponse *CentPPiDeviceFactoryResetInput(uint16_t cbInput, tCentPPHeader *pIn) {
    ((void)(cbInput));

    if(s_ideviceinfo_cfg == NULL || s_ideviceinfo_cfg->getDeviceSerialNumber == NULL || s_ideviceinfo_cfg->factoryReset == NULL) {
        centpp_log(CENTPP_LOG_ERROR, "ERROR! - CentPPiDeviceFactoryResetInput callback invalid");
        return CentPPBuildError(pIn, CENTPP_FAILED);
    }

    const uint8_t len = strlen((char *)s_ideviceinfo_cfg->getDeviceSerialNumber());

    if(len != pIn->payload[0]) {
        centpp_log(CENTPP_LOG_ERROR, "ERROR! - Invalid length");
        return CentPPBuildError(pIn, CENTPP_INVALID_SIZE);
    }

    if(memcmp(s_ideviceinfo_cfg->getDeviceSerialNumber(), (char *)&pIn->payload[1], len) != 0) {
        centpp_log(CENTPP_LOG_ERROR, "ERROR! - CentPPiDeviceFactoryResetInput failed");
        return CentPPBuildError(pIn, CENTPP_INVALID_PARAM);
    }

    s_ideviceinfo_cfg->factoryReset();

    return CentPPBuildResponseSimple(pIn, NULL, FALSE);
}
