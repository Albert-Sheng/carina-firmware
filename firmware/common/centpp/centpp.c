#include "centpp.h"
#include "mbg_centpp_api.h"
#include "debug.h"
#include "project.h"

#include <ch.h>

#include <stdlib.h>

/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */
#define CENTPP_TX_DATA_AVAILABLE            (1 << 0)

/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

static THD_WORKING_AREA(waCentPP, CENTPP_THREAD_STACK_SIZE);
static thread_t *s_thread_ptr = NULL;
static mutex_t s_centpp_mutex;

/*
 ******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************
 */

static THD_FUNCTION(CentPP_Thread, arg) {
    (void)arg;

    chRegSetThreadName("CentPP");

    while(!chThdShouldTerminateX()) {
        const eventmask_t event_mask = chEvtWaitAnyTimeout(CENTPP_TX_DATA_AVAILABLE, TIME_MS2I(200));

        if(event_mask & CENTPP_TX_DATA_AVAILABLE) {
            while(!mbg_centpp_tx_fifo_empty()) {
                centpp_send_callback_t callback;
                void *carg;

                if(mbg_centpp_tx_fifo_pop(&callback, &carg) == true) {
                    callback(carg);
                    if(carg) {
                        free(carg);
                    }
                }
            }
        }
    }

    chThdExit(MSG_OK);
}


/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

bool CentPP_Start(const centpp_config_t *centpp_cfg) {
    static centpp_pl_config_t s_centpp_pl_config = {
        .max_transport_count            = CENTPP_MAX_TRANSPORT_COUNT,
        .transport_disconnect_callback  = NULL,
        .max_rx_message_size            = CENTPP_MAX_TRANSPORT_BUFFER_LENGTH,
    };

    if(s_thread_ptr != NULL) {
        Dbg_printf(DBG_INFO, "CentPP thread already running");
        return false;
    }

    mbg_centpp_init(&s_centpp_pl_config);

    if(centpp_cfg->register_features_cb != NULL) {
        centpp_cfg->register_features_cb();
    }

    chMtxObjectInit(&s_centpp_mutex);

    s_thread_ptr = chThdCreateStatic(waCentPP, sizeof(waCentPP), NORMALPRIO, CentPP_Thread, NULL);

    if(!s_thread_ptr) {
        Dbg_printf(DBG_ERROR, "Failed to create CentPP thread");
        return false;
    }

    return true;
}

bool CentPP_Stop(void) {
    if(s_thread_ptr == NULL) {
        Dbg_printf(DBG_INFO, "CentPP thread already stopped");
        return false;
    }

    Dbg_printf(DBG_INFO, "%s", __func__);

    chThdTerminate(s_thread_ptr);
    chThdWait(s_thread_ptr);
    s_thread_ptr = NULL;

    return true;
}

void CentPP_TxDataAvailable(void) {
    chEvtSignal(s_thread_ptr, CENTPP_TX_DATA_AVAILABLE);
}

void CentPP_MutexLock(void) {
    chMtxLock(&s_centpp_mutex);
}


void CentPP_MutexUnlock(void) {
    chMtxUnlock(&s_centpp_mutex);
}
