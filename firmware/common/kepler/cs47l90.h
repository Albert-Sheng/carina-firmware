/**
 * @file    cs47l90.h
 * @brief   CS47L90 DSP/Codec header.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */
#ifndef _CS47L90_H_
#define _CS47L90_H_

#include "hal.h"
#include "cs47l90_regdefs.h"

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @name    Configuration options
 * @{
 */
/**
 * @brief   CS47L90 I2C interface switch.
 * @details If set to @p TRUE the support for I2C is included.
 * @note    The default is @p FALSE.
 */
#if !defined(CS47L90_USE_I2C) || defined(__DOXYGEN__)
#define CS47L90_USE_I2C                     FALSE
#endif

/**
 * @brief   CS47L90 shared I2C switch.
 * @details If set to @p TRUE the device acquires I2C bus ownership
 *          on each transaction.
 * @note    The default is @p FALSE. Requires I2C_USE_MUTUAL_EXCLUSION.
 */
#if !defined(CS47L90_SHARED_I2C) || defined(__DOXYGEN__)
#define CS47L90_SHARED_I2C                  FALSE
#endif

/**
 * @brief   CS47L90 SPI interface switch.
 * @details If set to @p TRUE the support for SPI is included.
 * @note    The default is @p TRUE.
 */
#if !defined(CS47L90_USE_SPI) || defined(__DOXYGEN__)
#define CS47L90_USE_SPI                     TRUE
#endif

/**
 * @brief   CS47L90 shared SPI switch.
 * @details If set to @p TRUE the device acquires SPI bus ownership
 *          on each transaction.
 * @note    The default is @p TRUE.
 */
#if !defined(CS47L90_SHARED_SPI) || defined(__DOXYGEN__)
#define CS47L90_SHARED_SPI                  TRUE
#endif

/**
 * @brief   CS47L90 FLL1 enable switch.
 * @details If set to @p TRUE the device will enable the FLL1 after the
            initial register dump.
 * @note    The default is @p TRUE.
 */
#if !defined(CS47L90_USE_FLL1) || defined(__DOXYGEN__)
#define CS47L90_USE_FLL1                    TRUE
#endif

/**
 * @brief   CS47L90 FLL2 enable switch.
 * @details If set to @p TRUE the device will enable the FLL2 after the
            initial register dump.
 * @note    The default is @p FALSE.
 */
#if !defined(CS47L90_USE_FLL2) || defined(__DOXYGEN__)
#define CS47L90_USE_FLL2                    TRUE
#endif

/**
 * @brief   CS47L90 SYSCLK enable switch.
 * @details If set to @p TRUE the device will enable the SYSCLK after the
            initial register dump.
 * @note    The default is @p TRUE.
 */
#if !defined(CS47L90_USE_SYSCLK) || defined(__DOXYGEN__)
#define CS47L90_USE_SYSCLK                  TRUE
#endif

/**
 * @brief   CS47L90 ASYNC enable switch.
 * @details If set to @p TRUE the device will enable the ASYNC after the
            initial register dump.
 * @note    The default is @p TRUE.
 */
#if !defined(CS47L90_USE_ASYNC) || defined(__DOXYGEN__)
#define CS47L90_USE_ASYNC                   TRUE
#endif

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/
#if CS47L90_USE_I2C && !HAL_USE_I2C
#error "CS47L90_USE_I2C requires HAL_USE_I2C"
#endif

#if CS47L90_SHARED_I2C && !I2C_USE_MUTUAL_EXCLUSION
#error "CS47L90_SHARED_I2C requires I2C_USE_MUTUAL_EXCLUSION"
#endif

#if CS47L90_USE_SPI && !HAL_USE_SPI
#error "CS47L90_USE_SPI requires HAL_USE_SPI"
#endif

#if CS47L90_SHARED_SPI && !SPI_USE_MUTUAL_EXCLUSION
#error "CS47L90_SHARED_SPI requires SPI_USE_MUTUAL_EXCLUSION"
#endif

#if CS47L90_USE_I2C && CS47L90_USE_SPI
#error "Currently I2C and SPI interfaces can not be supported at the same time"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @name    CS47L90 data structures and types.
 * @{
 */
/**
 * @brief   CS47L90 Response Messages.
 */
typedef enum {
    CS47L90Msg_Success = 0,              // Indicates a successful message execution.
    CS47L90Msg_Failed = 1,               // Indicates a general operation failure.
} cs47l90_msg_t;
/** @} */

/**
 * @brief   Structure representing a CS47L90 driver.
 */
typedef struct CS47L90Driver CS47L90Driver;

/**
 * @brief  CS47L90 slave address
 */
typedef enum {
    /*TODO: since I2C interface is not used, we configure a
            invalid value here, it is determined by CS47L90*/
    CS47L90_SAD = 0x00,               /**< Slave Address   */
} cs47l90_sad_t;

/**
 * @brief CS47L90 sequence struct
 */
typedef struct {
    uint32_t addr;
    uint32_t value;
    uint32_t mask;
    uint32_t delay; // in ms
    uint32_t check;
} cs47l90_sequence_t;
/**
 * @brief Must be used at the end of a sequence
 */
#define cs47l90_sequence_end {0xffffffff, 0x0000, 0x0000, 0, 0x0000}

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
    CS47L90_UNINIT = 0,               /**< Not initialized.                */
    CS47L90_STOP = 1,                 /**< Stopped.                        */
    CS47L90_READY = 2,                /**< Ready.                          */
} cs47l90_state_t;

/**
 * @brief   CS47L90 configuration structure.
 */
typedef struct {
#if CS47L90_USE_I2C || defined(__DOXYGEN__)
    /**
     * @brief I2C driver associated to CS47L90.
     */
    I2CDriver                   *i2cp;
    /**
     * @brief I2C configuration associated to CS47L90.
     */
    const I2CConfig             *i2ccfg;
    /**
     * @brief CS47L90 slave address
     */
    cs47l90_sad_t               slaveaddress;
#endif /* CS47L90_USE_I2C */

#if CS47L90_USE_SPI || defined(__DOXYGEN__)
    /**
     * @brief SPI driver associated to CS47L90.
     */
    SPIDriver                   *spip;
    /**
     * @brief SPI configuration associated to CS47L90.
     */
    const SPIConfig             *spicfg;
#endif /* CS47L90_USE_SPI */
} CS47L90Config;

/**
 * @brief   @p CS47L90 specific methods.
 * @note    No methods so far, just a common ancestor interface.
 */
#define _cs47l90_methods_alone

/**
 * @brief @p CS47L90 specific methods with inherited ones.
 */
#define _cs47l90_methods                                                \
    _base_object_methods                                                \
    _cs47l90_methods_alone

/**
 * @extends BaseObjectVMT
 *
 * @brief @p CS47L90 virtual methods table.
 */
struct CS47L90VMT {
    _cs47l90_methods
};

/**
 * @brief   @p CS47L90Driver specific data.
 */
#define _cs47l90_data                                                   \
    /* Driver state.*/                                                  \
    cs47l90_state_t             state;                                  \
    /* Current configuration data.*/                                    \
    const CS47L90Config         *config;

/**
 * @brief   CS47L90 class.
 */
struct CS47L90Driver {
    /** @brief Virtual Methods Table.*/
    const struct CS47L90VMT   *vmt;
    _cs47l90_data
};
/** @} */

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief   Read from a register in the CS47L90.
 *
 * @param[out] devp             pointer to the @p CS47L90Driver object
 * @param[in]  buf              pointer to the receive buffer
 * @param[in]  addr             the cs47l90 address to read from
 * @param[in]  len              length of the receive buffer
 *
 * @retval CS47L90Msg_Success   if the function succeeded.
 * @retval CS47L90Msg_Failed    if the function failed.
 */
cs47l90_msg_t cs47l90RegRead(CS47L90Driver *devp, uint8_t *buf, uint32_t addr, size_t len);

/**
 * @brief   Modify a register in the CS47L90.
 *
 * @param[out] devp             pointer to the @p CS47L90Driver object
 * @param[in]  addr             the cs47l90 address to write
 * @param[in]  value            the value to write to the address
 * @param[in]  mask             the bits to mask
 * @param[in]  check            the bits to check after the write
 *
 * @retval CS47L90Msg_Success   if the function succeeded.
 * @retval CS47L90Msg_Failed    if the function failed.
 */
cs47l90_msg_t cs47l90RegModify(CS47L90Driver *devp, uint32_t addr, uint32_t value, uint32_t mask, uint32_t check);

/**
 * @brief   Send a sequence of register writes to the CS47L90.
 *
 * @param[out] devp             pointer to the @p CS47L90Driver object
 * @param[in]  seq              pointer to the sequence of register data to be sent
 *
 * @return                      the ais command flag bytes.
 * @retval CS47L90Msg_Success   if the function succeeded.
 * @retval CS47L90Msg_Failed    if the function failed.
 */
cs47l90_msg_t cs47l90SendSequence(CS47L90Driver *devp, const cs47l90_sequence_t *seq);

/**
 * @brief   Initializes an instance.
 *
 * @param[out] devp     pointer to the @p CS47L90Driver object
 *
 * @init
 */
void cs47l90ObjectInit(CS47L90Driver *devp, const CS47L90Config *config);

/**
 * @brief   Configures and activates CS47L90 Complex Driver peripheral.
 *
 * @param[in] devp      pointer to the @p CS47L90Driver object
 *
 * @api
 */
void cs47l90Start(CS47L90Driver *devp);

/**
 * @brief   Deactivates the CS47L90 Complex Driver peripheral.
 *
 * @param[in] devp       pointer to the @p CS47L90Driver object
 *
 * @api
 */
void cs47l90Stop(CS47L90Driver *devp);


/**
 * @brief   Performs a soft reset of the CS47L90.
 *
 * @param[in] devp       pointer to the @p CS47L90Driver object
 *
 * @api
 */
void cs47l90SoftReset(CS47L90Driver *devp);

#ifdef __cplusplus
}
#endif

#endif /* _CS47L90_H_ */
