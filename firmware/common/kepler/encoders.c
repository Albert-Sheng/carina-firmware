#include "encoders.h"
#include "debug.h"
#include "project.h"

#include <ch.h>
#include <hal.h>

#include <string.h>

/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

typedef struct encoder_arg {
    uint8_t prev_int0;          // Cache previous int0 state
    uint32_t pal_line_int0;     // Port | Pad
    uint32_t pal_line_int1;     // Port | Pad
    encoder_cb_t cb;            // Encoder event callback
} encoder_arg_t;

/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

static encoder_arg_t s_encoder_arg_pool[NUM_ENCODERS];

/*
 ******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************
 */

static void pal_line_cb(void *arg) {
    encoder_arg_t *encoder_args = (encoder_arg_t *)arg;
    uint8_t cur_int0;

    osalDbgAssert(arg, "invalid arg");

    cur_int0 = palReadLine(encoder_args->pal_line_int0);

    if(encoder_args->prev_int0 != cur_int0) {
        if(cur_int0 != palReadLine(encoder_args->pal_line_int1)) {
            encoder_args->cb(encoder_args->pal_line_int0, ENCODER_CLOCKWISE_EVENT);
        } else {
            encoder_args->cb(encoder_args->pal_line_int0, ENCODER_COUNTER_CLOCKWISE_EVENT);
        }

        encoder_args->prev_int0 = cur_int0;
    }
}

/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

bool EncoderRegisterCB(ioline_t pal_line_int0, ioline_t pal_line_int1, encoder_cb_t encoder_cb) {
    encoder_arg_t *node = NULL;

    if(palIsLineEventEnabledX(pal_line_int0)) {
        Dbg_printf(DBG_INFO, "Encoder int0 0x%x already registered", pal_line_int0);
        return false;
    }

    /* Find a free node in pool */
    for(int i = 0; i < NUM_ENCODERS; i++) {
        if(s_encoder_arg_pool[i].pal_line_int0 == 0) {
            node = &s_encoder_arg_pool[i];
            break;
        }
    }

    osalDbgAssert(node, "no available encoder nodes!");

    node->prev_int0 = palReadLine(pal_line_int0);
    node->pal_line_int0 = pal_line_int0;
    node->pal_line_int1 = pal_line_int1;
    node->cb = encoder_cb;

    palSetLineCallback(pal_line_int0, pal_line_cb, node);
    palEnableLineEvent(pal_line_int0, PAL_EVENT_MODE_BOTH_EDGES);

    return true;
}


void EncoderUnregisterCB(ioline_t pal_line_int0) {
    /* Find and reset the node */
    for(int i = 0; i < NUM_ENCODERS; i++) {
        if(s_encoder_arg_pool[i].pal_line_int0 == pal_line_int0) {
            memset(&s_encoder_arg_pool[i], 0, sizeof(s_encoder_arg_pool[i]));
            break;
        }
    }

    if(palIsLineEventEnabledX(pal_line_int0)) {
        palDisableLineEvent(pal_line_int0);
    } else {
        Dbg_printf(DBG_ERROR, "Encoder int0 0x%x not registered!", pal_line_int0);
    }
}
