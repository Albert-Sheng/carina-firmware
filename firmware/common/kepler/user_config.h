#ifndef _USER_CONFIG_H_
#define _USER_CONFIG_H_

/******************* User Config Parameter Structure **********************/
#define MAX_SIZE_DEVICE_NAME  48
#pragma pack(push,1)

typedef struct {
    struct {
        char device_name[MAX_SIZE_DEVICE_NAME];
    } params;
    //list may grow
} User_Config_t;
#pragma pack(pop)

extern User_Config_t userConfig;
#endif
