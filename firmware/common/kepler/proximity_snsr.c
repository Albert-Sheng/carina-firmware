/**
 * @file    proximity_snsr.c
 * @brief   Proximity sensor driver code.
 *
 * @addtogroup COMMON
 * @{
 */

#include "proximity_snsr.h"

#include <hal.h>

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/
#define PROXIMITY_SNSR_LINE                                 PAL_LINE(GPIOC, 2)
#define PROXIMITY_SNSR_LINE_CHECK_NUM                       (5)
#define PROXIMITY_SNSR_LINE_CHECK_DLY                       (4)

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/


/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
bool isProxiSensorDetected(void) {
    uint8_t i;

    // Detect the proximity sensor line PROXIMITY_SNSR_LINE_CHECK_NUM in
    // PROXIMITY_SNSR_LINE_CHECK_DLY*PROXIMITY_SNSR_LINE_CHECK_NUM ms to
    // avoid bounce
    for(i = 0; i < PROXIMITY_SNSR_LINE_CHECK_NUM; i++) {
        if(palReadLine(PROXIMITY_SNSR_LINE) == PAL_HIGH) {
            return FALSE;
        }
        chThdSleepMilliseconds(PROXIMITY_SNSR_LINE_CHECK_DLY);
    }
    return (palReadLine(PROXIMITY_SNSR_LINE) == PAL_LOW);
}

