/**
 * @file    stm32f446_imperium.c
 * @brief   STM32F446 CO-Processor imperium module code.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */

#include "hal.h"
#include "debug.h"
#include "stm32f446.h"
#include "xb1_imperium.h"
#include "coprocessor.h"
#include "stm32f446_imperium.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/
/**
 * @name    imperium misc
 * @{
 */
#define IMPERIUM_MAX_TRY_NUM                                            (100)
#define IMPERIUM_PUID_LEN                                               (20)
#define IMPERIUM_MAX_TRANSACTION_SIZE                                   (64)
#define IMPERIUM_MAX_PACKET_SIZE                                        (2048)

/** @} */



/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
static THD_WORKING_AREA(waimperium, 1024);
static thread_t *imperium_thread_ptr = NULL;
static volatile imperium_op_mode_t imperium_op;
static uint8_t imperium_buf[IMPERIUM_MAX_PACKET_SIZE];
static bool imperium_exists = FALSE;

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/



/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/
static THD_FUNCTION(imperium_Thread, arg) {
    (void)arg;

    chRegSetThreadName("imperium thread");
    Dbg_printf(DBG_INFO, "imperium thread is running!");

    // tell STM32F446 that the imperium exist immediately
    imperiumSetExist(TRUE);

    while(!chThdShouldTerminateX()) {
        // polling the operation mode
        imperium_op = imperiumGetOPMode();

        switch(imperium_op) {
            case IMPERIUM_OP_MODE_INIT:
                imperiumSetOPStatus(IMPERIUM_OP_STS_BUSY);
                if(TRUE == imperiumStart()) {
                    imperiumSetExist(TRUE);
                }
                imperiumSetOPStatus(IMPERIUM_OP_STS_DONE);
                break;
            case IMPERIUM_OP_MODE_SHUTDOWN:
                imperiumSetOPStatus(IMPERIUM_OP_STS_BUSY);
                Imperium_ShutDown();
                imperiumSetOPStatus(IMPERIUM_OP_STS_DONE);
                break;
            case IMPERIUM_OP_MODE_READ_PUID:
                imperiumSetOPStatus(IMPERIUM_OP_STS_BUSY);
                if(MSG_OK != imperiumSetPUID()) {
                    imperiumSetOPStatus(IMPERIUM_OP_STS_FAIL);
                } else {
                    imperiumSetOPStatus(IMPERIUM_OP_STS_DONE);
                }
                break;

            case IMPERIUM_OP_MODE_SEND_DATA:
                imperiumGetTxData(imperium_buf, imperiumGetTxLength());
                break;

            case IMPERIUM_OP_MODE_RECEIVE_DATA:
                imperiumSetRxData(imperium_buf, imperiumGetRxLength());
                break;

            case IMPERIUM_OP_MODE_NONE:
            case IMPERIUM_OP_MODE_UNKNOWN:
            default:
                break;
        }

        chThdSleepMilliseconds(100);
    }
    chThdExit(MSG_OK);
}


/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
imperium_op_mode_t imperiumGetOPMode(void) {
    uint8_t op_mode;
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return IMPERIUM_OP_MODE_UNKNOWN;
    }

    op_mode = stm32f446GetImperiumCtrlReg(&stm32f446);
    if(op_mode == 0xFF) {
        Dbg_printf(DBG_ERROR, "i2c communication fail");
        return IMPERIUM_OP_MODE_UNKNOWN;
    }

    return op_mode;
}

imperium_op_sts_t imperiumGetOPStatus(void) {
    uint8_t op_status;
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return IMPERIUM_OP_STS_UNKNOW;
    }

    op_status = stm32f446GetImperiumStsReg(&stm32f446);
    if(op_status == 0xFF) {
        Dbg_printf(DBG_ERROR, "i2c communication fail");
        return IMPERIUM_OP_STS_UNKNOW;
    }

    return (op_status & IMPERIUM_STS_OP_MSK);
}

msg_t imperiumSetOPStatus(imperium_op_sts_t sts) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;
    }

    return stm32f446SetImperiumStsRegBit(&stm32f446, IMPERIUM_STS_OP_MSK, sts);
}

msg_t imperiumSetExist(bool exist) {
    uint8_t bit_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;
    }

    bit_val = exist ? (IMPERIUM_STS_PRESENT) : (IMPERIUM_STS_ABSENT);
    return stm32f446SetImperiumStsRegBit(&stm32f446, IMPERIUM_STS_EXIST_MSK, bit_val);
}

msg_t imperiumSetPUID(void) {
    uint8_t puid_buf[IMPERIUM_PUID_LEN];

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;
    }

    // read the PUID from imperium
    if(IMPERIUM_OK != Imperium_PUID_get(puid_buf, IMPERIUM_PUID_LEN)) {
        Dbg_printf(DBG_ERROR, "read PUID from imperium fail");
        return MSG_TIMEOUT;
    }

    // write the PUID to stm32f446
    return stm32f446SetImperiumPUIDReg(&stm32f446, puid_buf, IMPERIUM_PUID_LEN);
}

uint16_t imperiumGetTxLength(void) {

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return 0;
    }

    return stm32f446GetImperiumTxLenReg(&stm32f446);
}

uint16_t imperiumGetRxLength(void) {

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return 0;
    }

    return stm32f446GetImperiumRxLenReg(&stm32f446);
}

msg_t imperiumGetTxDataTransaction(uint8_t *rxbuf, uint8_t bytes) {
    osalDbgCheck((rxbuf != NULL) && (bytes <= IMPERIUM_MAX_TRANSACTION_SIZE));

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;
    }

    return stm32f446GetImperiumTxDataReg(&stm32f446, rxbuf, bytes);
}

msg_t imperiumSetRxDataTransaction(uint8_t *txbuf, uint8_t bytes) {
    osalDbgCheck((txbuf != NULL) && (bytes <= IMPERIUM_MAX_TRANSACTION_SIZE));

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;
    }

    return stm32f446SetImperiumRxDataReg(&stm32f446, txbuf, bytes);
}

msg_t imperiumGetTxData(uint8_t *rxbuf, uint16_t bytes) {
    uint8_t trans_len;
    uint8_t *tmpbuf = rxbuf;
    uint16_t length = bytes;
    uint8_t try_cnt = 0;
    msg_t msg = MSG_OK;

    osalDbgCheck((rxbuf != NULL) && (bytes <= IMPERIUM_MAX_PACKET_SIZE));

    while(length) {
        // before the transaction, set the status to busy
        imperiumSetOPStatus(IMPERIUM_OP_STS_BUSY);

        if(length > IMPERIUM_MAX_TRANSACTION_SIZE) {
            trans_len = IMPERIUM_MAX_TRANSACTION_SIZE;
        } else {
            trans_len = length;
        }

        msg = imperiumGetTxDataTransaction(tmpbuf, trans_len);
        if(MSG_OK != msg) {
            Dbg_printf(DBG_ERROR, "receive %d bytes TX data fail", trans_len);
            imperiumSetOPStatus(IMPERIUM_OP_STS_FAIL);
            return msg;
        }
        imperiumSetOPStatus(IMPERIUM_OP_STS_DONE);

        tmpbuf += trans_len;
        length -= trans_len;

        // need to wait for the next operation before the next transaction
        try_cnt = 0;
        do {
            if(try_cnt++ > IMPERIUM_MAX_TRY_NUM) {
                Dbg_printf(DBG_ERROR, "check the operation mode %d times, fail!", try_cnt);
                return MSG_TIMEOUT;
            }

            chThdSleepMilliseconds(5);
        } while((0 != length) && (IMPERIUM_OP_MODE_SEND_DATA != imperiumGetOPMode()));
    }

    // send the whole data to imperium
    //if(IMPERIUM_OK != Imperium_APDUpassthrough_send(rxbuf, bytes)) {
    //    Dbg_printf(DBG_ERROR, "send %d bytes to imperium fail", bytes);
    //    return MSG_TIMEOUT;
    //}

    return MSG_OK;
}

msg_t imperiumSetRxData(uint8_t *txbuf, uint16_t bytes) {
    uint8_t trans_len;
    uint8_t try_cnt = 0;
    msg_t msg = MSG_OK;

    osalDbgCheck((txbuf != NULL) && (bytes <= IMPERIUM_MAX_PACKET_SIZE));

    // get the data from imperium first
    //if(IMPERIUM_OK != Imperium_APDUpassthrough_recv(txbuf, bytes)) {
    //    Dbg_printf(DBG_ERROR, "receive %d bytes from imperium fail", bytes);
    //    return MSG_TIMEOUT;
    //}

    while(bytes) {
        // before the transaction, set the status to busy
        imperiumSetOPStatus(IMPERIUM_OP_STS_BUSY);

        if(bytes > IMPERIUM_MAX_TRANSACTION_SIZE) {
            trans_len = IMPERIUM_MAX_TRANSACTION_SIZE;
        } else {
            trans_len = bytes;
        }

        msg = imperiumSetRxDataTransaction(txbuf, trans_len);
        if(MSG_OK != msg) {
            Dbg_printf(DBG_ERROR, "send %d bytes RX data fail", trans_len);
            imperiumSetOPStatus(IMPERIUM_OP_STS_FAIL);
            return msg;
        }
        imperiumSetOPStatus(IMPERIUM_OP_STS_DONE);

        txbuf += trans_len;
        bytes -= trans_len;

        // need to wait for the next operation before the next transaction
        try_cnt = 0;
        do {
            if(try_cnt++ > IMPERIUM_MAX_TRY_NUM) {
                Dbg_printf(DBG_ERROR, "check the operation mode %d times, fail!", try_cnt);
                return MSG_TIMEOUT;
            }

            chThdSleepMilliseconds(5);
        } while((0 != bytes) && (IMPERIUM_OP_MODE_RECEIVE_DATA != imperiumGetOPMode()));
    }

    return MSG_OK;
}

void imperiumReset(void) {
    // Reset XB1 Imperium security IC if present
    Dbg_printf(DBG_INFO, "Resetting imperium...");

    coProcessorEnableImperiumReset(TRUE);
    chThdSleepMilliseconds(5);
    coProcessorEnableImperiumReset(FALSE);
    chThdSleepMilliseconds(10);
}

bool imperiumStart(void) {

    // Boot XB1 Imperium security IC if present
    Dbg_printf(DBG_INFO, "Starting imperium...");
    int retries = 5;
    while(retries-- && Imperium_Init(&I2CD2, &i2c2cfg, NULL, 0) != IMPERIUM_OK) {
        Dbg_printf(DBG_ERROR, "Imperium Init failed, retrying...");
        chThdSleepMilliseconds(25);
    }
    if(retries > 0) {
        Dbg_printf(DBG_INFO, "Success.");
        imperium_exists = TRUE;
        return TRUE;
    } else {
        Dbg_printf(DBG_ERROR, "Imperium not present.");
        imperium_exists = FALSE;
        return FALSE;
    }
}

bool isImperiumExisting(void) {
    return imperium_exists;
}

bool isImperiumThreadNeeded(void) {
    // only STM32F446 in game mode, and the xbox imperium exists
    if((TRUE == isImperiumExisting()) &&
       (COPROCESSOR_GAME_MODE_GAME == coProcessorGetGameMode())) {
        return TRUE;
    }

    return FALSE;
}

bool imperiumThreadStart(void) {
    if(NULL != imperium_thread_ptr) {
        Dbg_printf(DBG_ERROR, "imperium thread already running");
        return FALSE;
    }
    imperium_thread_ptr = chThdCreateStatic(waimperium, sizeof(waimperium), NORMALPRIO, imperium_Thread, NULL);
    if(NULL == imperium_thread_ptr) {
        Dbg_printf(DBG_ERROR, "Failed to create imperium thread");
        return FALSE;
    }

    return TRUE;
}

bool imperiumThreadStop(void) {
    if(NULL == imperium_thread_ptr) {
        Dbg_printf(DBG_ERROR, "imperium thread already stopped");
        return FALSE;
    }


    chThdTerminate(imperium_thread_ptr);
    chThdWait(imperium_thread_ptr);
    imperium_thread_ptr = NULL;

    return TRUE;
}

/** @} */
