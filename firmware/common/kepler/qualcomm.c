/**
 * @file    qualcomm.c
 * @brief   Qualcomm Bluetooth Wireless Audio Processor module code.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */

#include "hal.h"
#include "debug.h"
#include "qualcomm.h"
#include "project.h"
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
#include "qcc3024.h"
#endif

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
/**
 * @name    Qualcomm Driver variables and types
 * @{
 */

#if QUALCOMM_USE_MUTEX
/**
 * @brief   Qualcomm Static Mutex Static.
 */
MUTEX_DECL(qualcomm_mtx);

#endif

/** @} */

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
/*
 * QCC3024 Uart Communication Pipe related
 */
static const UARTConfig qcc3024_uart4_cfg = {
    .txend1_cb = NULL,
    .txend2_cb = NULL,
    .rxend_cb = NULL,
    .rxchar_cb = NULL,
    .rxerr_cb = NULL,
    .timeout_cb = NULL,
    .timeout = 500,
    .speed = 115200,
    .cr1 = 0x0000,
    .cr2 = 0x0000,
    .cr3 = 0x0000,
};

static const QCC3024Config qcc3024cfg = {
    &UARTD4,
    &qcc3024_uart4_cfg,
};
static QCC3024Driver QCC3024;
#endif

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/
/**
 * @brief   Lock Qualcomm Driver.
 *
 * @param[in] void      None
 *
 * @return              None
 *
 * @api
 */
static void qualcommLock(void) {
#if QUALCOMM_USE_MUTEX
    chMtxLock(&qualcomm_mtx);
#endif
}

/**
 * @brief   Unlock Qualcomm Driver.
 *
 * @param[in] void      None
 *
 * @return              None
 *
 * @api
 */
static void qualcommUnlock(void) {
#if QUALCOMM_USE_MUTEX
    chMtxUnlock(&qualcomm_mtx);
#endif
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
msg_t qualcommEnterPairing(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret =  qcc3024EnterPairing(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommCancelPairing(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024CancelPairing(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

bdaddr_t qualcommGetBdAddr(void) {
    bdaddr_t ret = {.lap = 0xFFFFFFFF, .uap = 0xFF, .nap = 0xFFFF};

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return (bdaddr_t) {
            0xFFFFFFFF, 0xFF, 0xFFFF
        }; // return an invalid BD address
    }

    ret = qcc3024GetBdAddr(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSetBdAddr(bdaddr_t bdaddr) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SetBdAddr(&QCC3024, bdaddr);
#endif
    qualcommUnlock();
    return ret;
}

conn_profile_t qualcommGetProfilesConnected(void) {
    conn_profile_t ret;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return 0x00;
    }

    ret = qcc3024GetProfilesConnected(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

hfp_call_state_t qualcommGetHFPCallState(void) {
    hfp_call_state_t ret;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return hfp_call_state_idle;
    }

    ret = qcc3024GetHFPCallState(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

a2dp_stream_state_t qualcommGetA2DPStreamState(void) {
    a2dp_stream_state_t ret;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return a2dp_stream_idle;
    }

    ret = qcc3024GetA2DPStreamState(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSetTxPwr(uint16_t txpwr) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SetTxPwr(&QCC3024, txpwr);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommStartContTx(uint16_t lo_freq, uint16_t level, uint16_t mod_freq) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024StartContTx(&QCC3024, lo_freq, level, mod_freq);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommPauseRadioTest(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024PauseRadioTest(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSendUpgradeCmd(uint8_t *data, uint8_t len) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SendUpgradeCmd(&QCC3024, data, len);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSendUpgradeData(uint8_t *data, uint8_t len) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SendUpgradeData(&QCC3024, data, len);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommReceiveUpgradeResp(uint8_t *data, uint8_t *len) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024ReceiveUpgradeResp(&QCC3024, data, len);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommClearUpgradeResp(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024ClearUpgradeResp(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommClearPDL(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024ClearPDL(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSetDUT(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SetDUT(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

uint16_t qualcommGetMaxTrustedDevice(void) {
    uint16_t ret = 0;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return 0;
    }

    ret = qcc3024GetMaxTrustedDevice(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

bdaddr_t qualcommGetIndexedTdlDevice(uint16_t index) {
    bdaddr_t ret = {.lap = 0xFFFFFFFF, .uap = 0xFF, .nap = 0xFFFF};

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return (bdaddr_t) {
            0xFFFFFFFF, 0xFF, 0xFFFF
        }; // return an invalid BD address
    }

    ret = qcc3024GetIndexedTdlDevice(&QCC3024, index);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommStartBleBonding(void) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024StartBleBonding(&QCC3024);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommReadCentppServerData(uint8_t *buf, uint16_t *length) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024ReadCentppServerData(&QCC3024, buf, length);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommWriteCentppServerData(uint8_t *buf, uint16_t length) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024WriteCentppServerData(&QCC3024, buf, length);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSendCentppServerReadNotification(uint8_t *buf, uint16_t length) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SendCentppServerReadNotification(&QCC3024, buf, length);
#endif
    qualcommUnlock();
    return ret;
}

msg_t qualcommSendCentppServerWriteNotification(uint8_t *buf, uint16_t length) {
    msg_t ret = MSG_OK;

    qualcommLock();
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    if(FALSE == isQcc3024Ready(&QCC3024)) {
        Dbg_printf(DBG_ERROR, "QCC3024 is not ready");
        qualcommUnlock();
        return MSG_TIMEOUT;
    }

    ret = qcc3024SendCentppServerWriteNotification(&QCC3024, buf, length);
#endif
    qualcommUnlock();
    return ret;
}

void qualcommInit(void) {
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    qcc3024ObjectInit(&QCC3024);
#endif
}

void qualcommStart(void) {
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    qcc3024Start(&QCC3024, &qcc3024cfg);
#endif
}

void qualcommStop(void) {
#if QUALCOMM_CHIP_ID & QCC3024_CHIP_ID
    qcc3024Stop(&QCC3024);
#endif
}

void qualcommReset(void) {

}

/** @} */
