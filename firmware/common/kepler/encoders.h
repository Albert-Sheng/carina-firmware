#pragma once

#include <hal.h>

typedef enum {
    ENCODER_CLOCKWISE_EVENT,
    ENCODER_COUNTER_CLOCKWISE_EVENT,
} encoder_event_t;


typedef void (*encoder_cb_t)(ioline_t pal_line_int0, encoder_event_t encoder_event);


bool EncoderRegisterCB(ioline_t pal_line_int0, ioline_t pal_line_int1, encoder_cb_t encoder_cb);
void EncoderUnregisterCB(ioline_t pal_line_int0);
