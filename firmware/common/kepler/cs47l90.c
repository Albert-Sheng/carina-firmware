/**
 * @file    cs47l90.c
 * @brief   CS47L90 Audio DSP/Codec module code.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */

#include "hal.h"
#include "cs47l90.h"
#include "debug.h"

#include <string.h>

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/* H750 is little endian, so must do byte swapping when sending and receiving with SPI */
#define BSWAP_32(x) (uint32_t)((((x) & 0xFF000000UL) >> 24UL) | \
                               (((x) & 0x00FF0000UL) >>  8UL) | \
                               (((x) & 0x0000FF00UL) <<  8UL) | \
                               (((x) & 0x000000FFUL) << 24UL))

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/

static const struct CS47L90VMT vmt_device = {
    (size_t)0
};

/* DMA is enabled for SPI. Buffer must be accessible by DMA */
static uint32_t s_reg __attribute__((section(".nocache")));
static uint32_t s_val __attribute__((section(".nocache")));
static uint16_t s_pad __attribute__((section(".nocache")));
static uint8_t cs47l90_rxdummy_buffer[4] __attribute__((section(".nocache")));
static uint8_t cs47l90_txdummy_buffer[4] __attribute__((section(".nocache"))) = {0};

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/*===========================================================================*/
/* Driver external variables.                                                */
/*===========================================================================*/

extern const cs47l90_sequence_t CS47L90_Sequence_Init;

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

#if (CS47L90_USE_SPI) || defined(__DOXYGEN__)
static void cs47l90SpiWrite(CS47L90Driver *devp, uint32_t reg, uint32_t val) {

    osalDbgCheck(devp != NULL);

    static const uint16_t pad = 0;

    s_reg = BSWAP_32(reg);

#if CS47L90_SHARED_SPI
    spiAcquireBus(devp->config->spip);
#endif
    spiStart(devp->config->spip, devp->config->spicfg);

    spiSelect(devp->config->spip);
    spiExchange(devp->config->spip, sizeof(s_reg), &s_reg, cs47l90_rxdummy_buffer);

    if(reg >= 0x3000) {
        spiExchange(devp->config->spip, 2, &pad, cs47l90_rxdummy_buffer);
    } else {
        s_val &= 0xFFFF;        // 16-bit reg
    }

    s_val = BSWAP_32(val);
    spiExchange(devp->config->spip, sizeof(val), &s_val, cs47l90_rxdummy_buffer);
    spiUnselect(devp->config->spip);

#if CS47L90_SHARED_SPI
    spiReleaseBus(devp->config->spip);
#endif
}

static uint32_t cs47l90SpiRead(CS47L90Driver *devp, uint32_t reg) {
    osalDbgCheck(devp != NULL);

    s_reg = BSWAP_32(reg | (1 << 31));

#if CS47L90_SHARED_SPI
    spiAcquireBus(devp->config->spip);
#endif
    spiStart(devp->config->spip, devp->config->spicfg);

    spiSelect(devp->config->spip);
    spiExchange(devp->config->spip, sizeof(s_reg), &s_reg, cs47l90_rxdummy_buffer);

    if(reg >= 0x3000) {
        spiExchange(devp->config->spip, 2, cs47l90_txdummy_buffer, &s_pad); // 32-bit reg requires 2 bytes of pad
    }

    spiExchange(devp->config->spip, sizeof(s_val), cs47l90_txdummy_buffer, &s_val);
    spiUnselect(devp->config->spip);

#if CS47L90_SHARED_SPI
    spiReleaseBus(devp->config->spip);
#endif

    return BSWAP_32(s_val);
}
#endif


/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Read a register in the CS47L90.
 *
 * @param[out] devp             pointer to the @p CS47L90Driver object
 * @param[out] buf              pointer to the receive buffer
 * @param[in]  addr             the cs47l90 address to read from
 * @param[in]  len              length of the receive buffer
 *
 * @retval CS47L90Msg_Success   if the function succeeded.
 * @retval CS47L90Msg_Failed    if the function failed.
 */
cs47l90_msg_t cs47l90RegRead(CS47L90Driver *devp, uint8_t *buf, uint32_t addr, size_t len) {
    uint32_t val = 0;

    if(sizeof(len) != sizeof(val)) {
        Dbg_printf(DBG_ERROR, "CS47L90 receive buffer invalid.");
        return CS47L90Msg_Failed;
    }

    val = cs47l90SpiRead(devp, addr);

    Dbg_printf(DBG_CS47L90, "reg read: reg: 0x%x, val: 0x%x", addr, val);

    memcpy(buf, &val, sizeof(val));

    return CS47L90Msg_Success;
}

/**
 * @brief   Modify a register in the CS47L90.
 *
 * @param[out] devp             pointer to the @p CS47L90Driver object
 * @param[in]  addr             the cs47l90 address to write
 * @param[in]  value            the value to write to the address
 * @param[in]  mask             the bits to mask
 * @param[in]  check            the bits to check after the write
 *
 * @retval CS47L90Msg_Success   if the function succeeded.
 * @retval CS47L90Msg_Failed    if the function failed.
 */
cs47l90_msg_t cs47l90RegModify(CS47L90Driver *devp, uint32_t addr, uint32_t value, uint32_t mask, uint32_t check) {
    uint32_t oldval = 0xcafe;
    uint32_t newval = value;

    if(mask != 0xffffffff) {
#ifdef CS47L90_USE_SPI
        oldval = cs47l90SpiRead(devp, addr);
#endif
        newval = (value & mask) | (oldval & ~mask);
    }


#ifdef CS47L90_USE_SPI
    cs47l90SpiWrite(devp, addr, newval);
#endif

    uint32_t readback;
#ifdef CS47L90_USE_SPI
    readback = cs47l90SpiRead(devp, addr);
#endif

    if((readback & check) != (newval & check)) {
        Dbg_printf(DBG_ERROR, "CS47L90 reg(%08x) write failed! w(%08x) r(%08x)", addr, newval, readback);
        return CS47L90Msg_Failed;
    } else {
        Dbg_printf(DBG_CS47L90, "reg: %08x(%08x->%08x)", addr, oldval, newval);
    }

    return CS47L90Msg_Success;
}

/**
 * @brief   Send a sequence of register writes to the CS47L90.
 *
 * @param[out] devp             pointer to the @p CS47L90Driver object
 * @param[in]  seq              pointer to the sequence of register data to be sent
 *
 * @retval CS47L90Msg_Success   if the function succeeded.
 * @retval CS47L90Msg_Failed    if the function failed.
 */
cs47l90_msg_t cs47l90SendSequence(CS47L90Driver *devp, const cs47l90_sequence_t *seq) {

    if(cs47l90RegModify(devp, CS47L90_SOFTWARE_RESET, 0, 0xffffffff, 0xffff0000) != CS47L90Msg_Success) {
        return CS47L90Msg_Failed;
    }

#ifdef CS47L90_USE_SPI
    while((cs47l90SpiRead(devp, CS47L90_IRQ1_RAW_STATUS_1) & (1 << 7)) == 0) {
        chThdSleepMilliseconds(1);
    }
#endif

    while(seq->addr != (cs47l90_sequence_t)cs47l90_sequence_end.addr) {
        if(cs47l90RegModify(devp, seq->addr, seq->value, seq->mask, seq->check) != CS47L90Msg_Success) {
            return CS47L90Msg_Failed;
        }

        if(seq->delay > 0) {
            chThdSleepMilliseconds(seq->delay);
        }

        seq++;
    }

    return CS47L90Msg_Success;
}

/**
 * @brief   Initializes an instance.
 *
 * @param[out] devp     pointer to the @p CS47L90Driver object
 *
 * @init
 */
void cs47l90ObjectInit(CS47L90Driver *devp, const CS47L90Config *config) {

    osalDbgCheck((devp != NULL) && (config != NULL));

    devp->vmt = &vmt_device;

    devp->config = config;

    devp->state = CS47L90_STOP;
}

/**
 * @brief   Configures and activates CS47L90 Complex Driver peripheral.
 *
 * @param[in] devp      pointer to the @p CS47L90Driver object
 *
 * @api
 */
void cs47l90Start(CS47L90Driver *devp) {

    cs47l90_msg_t status;

    osalDbgAssert((devp->state == CS47L90_STOP) || (devp->state == CS47L90_READY),
                  "cs47l90Start(), invalid state");

    // Download the register data for the CS47L90.
    Dbg_printf(DBG_INFO, "CS47L90 booting up.");
    status = cs47l90SendSequence(devp, &CS47L90_Sequence_Init);
    if(status != CS47L90Msg_Success) {
        Dbg_printf(DBG_ERROR, "Failed to boot CS47L90.");
    }

#if CS47L90_USE_FLL1
    status = cs47l90RegModify(devp, CS47L90_FLL1_CONTROL_1, CS47L90_FLL1CTRL_ENA, CS47L90_FLL1CTRL_ENA_Msk, CS47L90_FLL1CTRL_ENA);
    if(status != CS47L90Msg_Success) {
        Dbg_printf(DBG_ERROR, "Failed to boot CS47L90.");
    }
#endif

#if CS47L90_USE_FLL2
    status = cs47l90RegModify(devp, CS47L90_FLL2_CONTROL_1, CS47L90_FLL2CTRL_ENA, CS47L90_FLL2CTRL_ENA_Msk, CS47L90_FLL2CTRL_ENA);
    if(status != CS47L90Msg_Success) {
        Dbg_printf(DBG_ERROR, "Failed to boot CS47L90.");
    }
#endif

#if CS47L90_USE_SYSCLK
    status = cs47l90RegModify(devp, CS47L90_SYSTEM_CLOCK_1, CS47L90_SYSCLK_ENA, CS47L90_SYSCLK_ENA_Msk, CS47L90_SYSCLK_ENA);
    if(status != CS47L90Msg_Success) {
        Dbg_printf(DBG_ERROR, "Failed to boot CS47L90.");
    }
#endif

#if CS47L90_USE_ASYNC
    status = cs47l90RegModify(devp, CS47L90_ASYNC_CLOCK_1, CS47L90_ASYNC_ENA, CS47L90_ASYNC_ENA_Msk, CS47L90_ASYNC_ENA);
    if(status != CS47L90Msg_Success) {
        Dbg_printf(DBG_ERROR, "Failed to boot CS47L90.");
    }
#endif

    Dbg_printf(DBG_INFO, "CS47L90 boot up success.");
    devp->state = CS47L90_READY;
}

/**
 * @brief   Deactivates the CS47L90 Complex Driver peripheral.
 *          and performs a chip reset on the CS47L90.
 *
 * @param[in] devp       pointer to the @p CS47L90Driver object
 *
 * @api
 */
void cs47l90Stop(CS47L90Driver *devp) {
    osalDbgCheck(devp != NULL);
    osalDbgAssert((devp->state == CS47L90_STOP) || (devp->state == CS47L90_READY),
                  "cs47l90Stop(), invalid state");

    cs47l90RegModify(devp, CS47L90_SOFTWARE_RESET, 0, 0xffffffff, 0x00000000);

    if(devp->state == CS47L90_READY) {
        spiStop(devp->config->spip);
    }

    devp->state = CS47L90_STOP;
}

/**
 * @brief   Performs a soft reset of the CS47L90.
 *
 * @param[in] devp       pointer to the @p CS47L90Driver object
 *
 * @api
 */
void cs47l90SoftReset(CS47L90Driver *devp) {
    cs47l90RegModify(devp, 0x00000000, 0x00000000, 0x00000000, 0x00000000);
}
/** @} */
