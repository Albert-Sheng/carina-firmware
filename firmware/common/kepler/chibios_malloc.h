#ifndef __CHIBIOS_MALLOC_H__
#define __CHIBIOS_MALLOC_H__

// OS stuff
#include "ch.h"

#define malloc(x)       chHeapAlloc(NULL,x)
#define calloc(x,y)     memset(chHeapAlloc(NULL,(x)*(y)),0,(x)*(y))
#define free(x)         chHeapFree(x)

#endif
