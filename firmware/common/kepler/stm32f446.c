/**
 * @file    stm32f446.c
 * @brief   STM32F446 CO-Processor module code.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */

#include <string.h>
#include "hal.h"
#include "stm32f446.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
static const struct STM32F446VMT vmt_device = {
    (size_t)0
};

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

#if (STM32F446_USE_I2C) || defined(__DOXYGEN__)
/**
 * @brief   Reads registers value using I2C.
 * @pre     The I2C interface must be initialized and the driver started.
 *
 * @param[in]  i2cp      pointer to the I2C interface
 * @param[in]  sad       slave address without R bit
 * @param[in]  reg       first sub-register address
 * @param[out] rxbuf     pointer to an output buffer
 * @param[in]  n         number of consecutive register to read
 * @return               the operation status.
 *
 * @notapi
 */
static msg_t stm32f446I2CReadRegister(I2CDriver *i2cp, stm32f446_sad_t sad,
                                      uint8_t reg, uint8_t* rxbuf, size_t n) {
    static uint8_t reg_addr __attribute__((section(".nocache")));
    reg_addr = reg;
    return i2cMasterTransmitTimeout(i2cp, sad, &reg_addr, 1, rxbuf, n,
                                    STM32F446_I2C_TIMEOUT);
}

/**
 * @brief   Writes a value into a register using I2C.
 * @pre     The I2C interface must be initialized and the driver started.
 *
 * @param[in] i2cp       pointer to the I2C interface
 * @param[in] sad        slave address without R bit
 * @param[in] txbuf      buffer containing sub-address value in first position
 *                       and values to write
 * @param[in] n          size of txbuf less one (not considering the first
 *                       element)
 * @return               the operation status.
 *
 * @notapi
 */
#define stm32f446I2CWriteRegister(i2cp, sad, txbuf, n)                      \
        i2cMasterTransmitTimeout(i2cp, sad, txbuf, n + 1, NULL, 0,          \
                                  STM32F446_I2C_TIMEOUT)
#endif /* STM32F446_USE_I2C */

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
uint8_t stm32f446GetTestReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_TEST_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0xFF; // return an invalid value when the i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetEnableReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_ENABLE_REG;
    cr[1] = val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetEnableRegBit(STM32F446Driver *devp,
                               uint8_t bit_mask, uint8_t bit_val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    // read back the value of STM32F446_ENABLE_REG first
    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_ENABLE_REG, &cr[1], 1);
    if(MSG_OK != msg) {
#if STM32F446_SHARED_I2C
        i2cReleaseBus((devp)->config->i2cp);
#endif
        return msg;
    }

    cr[0] = STM32F446_ENABLE_REG;
    cr[1] = (cr[1] & ~bit_mask) | bit_val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetEnableReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_ENABLE_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0x00; // return the reset value when the i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetGameModeReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_GAME_MODE_REG;
    cr[1] = val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetGameModeRegBit(STM32F446Driver *devp,
                                 uint8_t bit_mask, uint8_t bit_val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    // read back the value of STM32F446_GAME_MODE_REG first
    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_GAME_MODE_REG, &cr[1], 1);
    if(MSG_OK != msg) {
#if STM32F446_SHARED_I2C
        i2cReleaseBus((devp)->config->i2cp);
#endif
        return msg;
    }

    cr[0] = STM32F446_GAME_MODE_REG;
    cr[1] = (cr[1] & ~bit_mask) | bit_val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetGameModeReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_GAME_MODE_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0xFF; // return an invalid value if i2c communication fails.
    }

#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

uint8_t stm32f446GetBootModeReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_BOOT_MODE_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0xFF; // return an invalid value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetDfuCtrlReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_DFU_CTRL_REG;
    cr[1] = val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetDfuCtrlRegBit(STM32F446Driver *devp,
                                uint8_t bit_mask, uint8_t bit_val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    // read back the value of STM32F446_DFU_CTRL_REG first
    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_DFU_CTRL_REG, &cr[1], 1);
    if(MSG_OK != msg) {
#if STM32F446_SHARED_I2C
        i2cReleaseBus((devp)->config->i2cp);
#endif
        return msg;
    }

    cr[0] = STM32F446_DFU_CTRL_REG;
    cr[1] = (cr[1] & ~bit_mask) | bit_val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetDfuCtrlReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_DFU_CTRL_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0x00; // return the reset value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetAppReg(STM32F446Driver *devp,
                         const uint8_t *txbuf, uint8_t size) {
    static uint8_t cr[65] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

    osalDbgCheck((devp != NULL) && (txbuf != NULL));
    osalDbgAssert(((size <= 64) && (0 == size % 4)), "invalid size");

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_APP_REG;
    // TODO: use DMA to speed up
    memcpy((uint8_t *)&cr[1], txbuf, size);
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, size);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint32_t stm32f446GetChecksumReg(STM32F446Driver *devp) {
    static uint8_t cr[4] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_CHECKSUM_REG, cr, 4);
    if(MSG_OK != msg) {
        cr[0] = cr[1] = cr[2] = cr[3] = 0; // return an invalid value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return (uint32_t)cr[0] << 24 | (uint32_t)cr[1] << 16 | (uint32_t)cr[2] << 8 | (uint32_t)cr[3];
}

uint8_t stm32f446GetDfuReasonReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_DFU_REASON_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0; // return the reset value if i2c communication fails.
    }

#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetResetReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_RESET_REG;
    cr[1] = val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetResetRegBit(STM32F446Driver *devp,
                              uint8_t bit_mask, uint8_t bit_val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    // read back the value of STM32F446_RESET_REG first
    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_RESET_REG, &cr[1], 1);
    if(MSG_OK != msg) {
#if STM32F446_SHARED_I2C
        i2cReleaseBus((devp)->config->i2cp);
#endif
        return msg;
    }

    cr[0] = STM32F446_RESET_REG;
    cr[1] = (cr[1] & ~bit_mask) | bit_val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetResetReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_RESET_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0x00; // return the reset value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

uint8_t stm32f446GetImperiumCtrlReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_IMPERIUM_CTRL_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0xFF; // return an invalid value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetImperiumStsReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_IMPERIUM_STATUS_REG;
    cr[1] = val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetImperiumStsRegBit(STM32F446Driver *devp,
                                    uint8_t bit_mask, uint8_t bit_val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    // read back the value of STM32F446_DFU_CTRL_REG first
    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_IMPERIUM_STATUS_REG, &cr[1], 1);
    if(MSG_OK != msg) {
#if STM32F446_SHARED_I2C
        i2cReleaseBus((devp)->config->i2cp);
#endif
        return msg;
    }

    cr[0] = STM32F446_IMPERIUM_STATUS_REG;
    cr[1] = (cr[1] & ~bit_mask) | bit_val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetImperiumStsReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_IMPERIUM_STATUS_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0xFF; // return an invalid value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetImperiumPUIDReg(STM32F446Driver *devp,
                                  const uint8_t *txbuf, uint8_t size) {
    static uint8_t cr[65] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

    osalDbgCheck((devp != NULL) && (txbuf != NULL));
    osalDbgAssert(((size <= 64) && (0 != size)), "invalid size");

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_IMPERIUM_PUID_REG;
    // TODO: use DMA to speed up
    memcpy((uint8_t *)&cr[1], txbuf, size);
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, size);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint16_t stm32f446GetImperiumTxLenReg(STM32F446Driver *devp) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_IMPERIUM_TXLEN_REG, cr, 2);
    if(MSG_OK != msg) {
        cr[0] = cr[1] = 0x00; // return the reset value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return (uint16_t)cr[0] << 8 | (uint16_t)cr[1];
}

msg_t stm32f446GetImperiumTxDataReg(STM32F446Driver *devp,
                                    uint8_t *rxbuf, uint8_t size) {
    static uint8_t cr[64] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

    osalDbgCheck((devp != NULL) && (rxbuf != NULL));
    osalDbgAssert(((size <= 64) && (0 != size)), "invalid size");

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_IMPERIUM_TXDATA_REG, cr, size);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    if(MSG_OK == msg) {
        // copy the data from the cr buffer.
        memcpy(rxbuf, cr, size);
    }

    return msg;
}

uint16_t stm32f446GetImperiumRxLenReg(STM32F446Driver *devp) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_IMPERIUM_RXLEN_REG, cr, 2);
    if(MSG_OK != msg) {
        cr[0] = cr[1] = 0x00; // return the reset value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return (uint16_t)cr[0] << 8 | (uint16_t)cr[1];
}

msg_t stm32f446SetImperiumRxDataReg(STM32F446Driver *devp,
                                    const uint8_t *txbuf, uint8_t size) {
    static uint8_t cr[65] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

    osalDbgCheck((devp != NULL) && (txbuf != NULL));
    osalDbgAssert(((size <= 64) && (0 != size)), "invalid size");

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_IMPERIUM_RXDATA_REG;
    // TODO: use DMA to speed up
    memcpy((uint8_t *)&cr[1], txbuf, size);
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, size);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetPerpheryStsReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_PERIPHERY_STS_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0x00; // return the reset value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetFtyPtnAccessOpReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_FTY_PTN_ACCESS_OP_REG;
    cr[1] = val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetFtyPtnAccessOpRegBit(STM32F446Driver *devp,
                                       uint8_t bit_mask, uint8_t bit_val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    // read back the value of STM32F446_FTY_PTN_ACCESS_OP_REG first
    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_FTY_PTN_ACCESS_OP_REG, &cr[1], 1);
    if(MSG_OK != msg) {
#if STM32F446_SHARED_I2C
        i2cReleaseBus((devp)->config->i2cp);
#endif
        return msg;
    }

    cr[0] = STM32F446_FTY_PTN_ACCESS_OP_REG;
    cr[1] = (cr[1] & ~bit_mask) | bit_val;
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

uint8_t stm32f446GetFtyPtnAccessOpReg(STM32F446Driver *devp) {
    static uint8_t cr __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_FTY_PTN_ACCESS_OP_REG, &cr, 1);
    if(MSG_OK != msg) {
        cr = 0xFF; // return the invalid value if i2c communication fails.
    }
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return cr;
}

msg_t stm32f446SetFtyPtnAccessAddrReg(STM32F446Driver *devp, uint32_t val) {
    static uint8_t cr[5] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_FTY_PTN_ACCESS_ADDR_REG;
    memcpy(&cr[1], (uint8_t *)&val, sizeof(val));

    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 4);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446SetFtyPtnAccessLenReg(STM32F446Driver *devp, uint8_t val) {
    static uint8_t cr[2] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_FTY_PTN_ACCESS_LEN_REG;
    cr[1] = val;

    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, 1);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

msg_t stm32f446GetFtyPtnAccessBufReg(STM32F446Driver *devp,
                                     uint8_t *rxbuf, uint8_t size) {
    static uint8_t cr[64] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

    osalDbgCheck((devp != NULL) && (rxbuf != NULL));
    osalDbgAssert(((size <= 64) && (0 != size)), "invalid size");

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    msg = stm32f446I2CReadRegister(devp->config->i2cp, devp->config->slaveaddress,
                                   STM32F446_FTY_PTN_ACCESS_BUF_REG, cr, size);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    if(MSG_OK == msg) {
        // copy the data from the cr buffer.
        memcpy(rxbuf, cr, size);
    }

    return msg;
}

msg_t stm32f446SetFtyPtnAccessBufReg(STM32F446Driver *devp,
                                     const uint8_t *txbuf, uint8_t size) {
    static uint8_t cr[65] __attribute__((section(".nocache")));
    msg_t msg = MSG_OK;

    osalDbgCheck((devp != NULL) && (txbuf != NULL));
    osalDbgAssert(((size <= 64) && (0 == size % 4)), "invalid size");

#if STM32F446_SHARED_I2C
    i2cAcquireBus(devp->config->i2cp);
#endif
    i2cStart(devp->config->i2cp, devp->config->i2ccfg);

    cr[0] = STM32F446_FTY_PTN_ACCESS_BUF_REG;
    // TODO: use DMA to speed up
    memcpy((uint8_t *)&cr[1], txbuf, size);
    msg = stm32f446I2CWriteRegister(devp->config->i2cp, devp->config->slaveaddress,
                                    cr, size);
#if STM32F446_SHARED_I2C
    i2cReleaseBus((devp)->config->i2cp);
#endif

    return msg;
}

bool isStm32f446Ready(STM32F446Driver *devp) {
    return (devp->state == STM32F446_READY);
}

void stm32f446ObjectInit(STM32F446Driver *devp) {

    devp->vmt = &vmt_device;

    devp->config = NULL;

    devp->state = STM32F446_STOP;
}

void stm32f446Start(STM32F446Driver *devp, const STM32F446Config *config) {
    uint8_t test_reg_val;

    osalDbgCheck((devp != NULL) && (config != NULL));
    osalDbgAssert((devp->state == STM32F446_STOP) || (devp->state == STM32F446_READY),
                  "stm32f446Start(), invalid state");

    devp->config = config;

    /* Checking if the device is ready.*/
    test_reg_val = stm32f446GetTestReg(devp);
    if(STM32F446_TEST_REG_DATA != test_reg_val) {
        return;
    }

    devp->state = STM32F446_READY;
}

void stm32f446Stop(STM32F446Driver *devp) {
    osalDbgCheck(devp != NULL);
    osalDbgAssert((devp->state == STM32F446_STOP) || (devp->state == STM32F446_READY),
                  "stm32f446Stop(), invalid state");

    // deactivates the I2C bus.
    if(devp->state == STM32F446_READY) {
        i2cStop((devp)->config->i2cp);
    }

    devp->state = STM32F446_STOP;
}
/** @} */
