
#include "kepler.h"

#include "ep91z5a.h"


/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define EP9472_I2C_ADDR         (0xC2>>1)
#define I2C_TIMEOUT_MS          100

/* It appears that there are documented bits in the registers that have
   Been seen Emperically...
 */
#define EP9472_REG_SYS_STATUS_1 0x20
#define EP9472_REG_SYS_STATUS_2 0x21
#define EP9472_REG_AUDIO_INFO   0x22

#define SYS_STATUS_1_OK 0x50
#define SYS_STATUS_2_OK 0xC4

static uint32_t AUDIO_INFO_SAMP_FREQS[] = {
    32000,
    44100,
    48000,
    88200,
    96000,
    176400,
    192000,
    768000
};

/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */
static msg_t EP9472_RegisterRead(I2CDriver *i2cp,  uint8_t regaddr, uint8_t *value) {
    static uint8_t reg __attribute__((section(".ram3")));
    static uint8_t data __attribute__((section(".ram3")));
    reg = regaddr;
    msg_t msg;

    msg = i2cMasterTransmitTimeout(i2cp, EP9472_I2C_ADDR, (uint8_t *)&reg, sizeof(reg), &data, sizeof(data), TIME_MS2I(I2C_TIMEOUT_MS));
    *value = data;
    //Dbg_printf(DBG_INFO, "[%3d reg=%02x status=%3d data=%02x]", cnt++, reg,  msg, *value);
    //Dbg_printf(DBG_INFO, "[reg=%02x data=%02x]", reg, *value);
    //Dbg_printf(DBG_INFO, "[data=%02x]", *value);
    return msg;
}

uint32_t EP9472_GetSampleFrequency(I2CDriver *i2cp) {
    if(i2cp == NULL) {
        Dbg_printf(DBG_ERROR, "%s() NULL argument\n", __func__);
        return 0;
    }
    msg_t msg;
    uint8_t  data;
    uint32_t freq = 0;

    i2cAcquireBus(i2cp);

    do {
//static int cnt =0;
//      Dbg_printf(DBG_INFO, "%3d", cnt++);
        msg = EP9472_RegisterRead(i2cp, EP9472_REG_SYS_STATUS_1, &data);
        if((msg != MSG_OK) || ((data & SYS_STATUS_1_OK) != SYS_STATUS_1_OK)) {
            //    if((msg != MSG_OK) || (SYS_STATUS_1_OK != data)) {
            break;
        }

        msg = EP9472_RegisterRead(i2cp, EP9472_REG_SYS_STATUS_2, &data);
        if((msg != MSG_OK) || ((SYS_STATUS_2_OK & data) != SYS_STATUS_2_OK)) {
            break;
        }

        msg = EP9472_RegisterRead(i2cp, EP9472_REG_AUDIO_INFO, &data);
        if(msg != MSG_OK) {
            break;
        }
        freq = AUDIO_INFO_SAMP_FREQS [data & 0x7];
        break;
    } while(0);

    i2cReleaseBus(i2cp);

    return freq;
}
