/**
 * @file    stm32f446_imperium.h
 * @brief   STM32F446 CO-Processor imperium header.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */
#ifndef _STM32F446_IMPERIUM_H_
#define _STM32F446_IMPERIUM_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/



/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/



/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @brief  CO-Processor Imperium operation
 */
typedef enum {
    IMPERIUM_OP_MODE_NONE = 0,
    IMPERIUM_OP_MODE_INIT = 1,
    IMPERIUM_OP_MODE_SHUTDOWN = 2,
    IMPERIUM_OP_MODE_READ_PUID = 3,
    IMPERIUM_OP_MODE_SEND_DATA = 4,
    IMPERIUM_OP_MODE_RECEIVE_DATA = 5,
    IMPERIUM_OP_MODE_UNKNOWN,
} imperium_op_mode_t;

/**
 * @brief  CO-Processor Imperium operation status
 */
typedef enum {
    IMPERIUM_OP_STS_IDLE = 0,
    IMPERIUM_OP_STS_BUSY = 1,
    IMPERIUM_OP_STS_DONE = 2,
    IMPERIUM_OP_STS_FAIL = 3,
    IMPERIUM_OP_STS_UNKNOW,
} imperium_op_sts_t;

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/



/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief   Get imperium operations mode.
 *
 * @param[in] void      None
 *
 * @return              the imperium operation mode.
 */
imperium_op_mode_t imperiumGetOPMode(void);

/**
 * @brief   Get imperium operations status.
 *
 * @param[in] void      None
 *
 * @return              the imperium operation status.
 */
imperium_op_sts_t imperiumGetOPStatus(void);

/**
 * @brief   Set imperium operations status.
 *
 * @param[in] sts       the operation status to set
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumSetOPStatus(imperium_op_sts_t sts);

/**
 * @brief   Set imperium exist status.
 *
 * @param[in] exist     bool parameter to indicate the imperium exist or not
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumSetExist(bool exist);

/**
 * @brief   Set imperium PUID.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumSetPUID(void);

/**
 * @brief   Get imperium tx length.
 *
 * @param[in] void      None
 *
 * @return              the imperium tx operation data length.
 */
uint16_t imperiumGetTxLength(void);

/**
 * @brief   Get imperium rx length.
 *
 * @param[in] void      None
 *
 * @return              the imperium rx operation data length.
 */
uint16_t imperiumGetRxLength(void);

/**
 * @brief   get imperium tx data transaction.
 *
 * @param[in] rxbuf     Buffer to store the tx data
 * @param[in] bytes     size of the tx data to store, must be less that 64
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumGetTxDataTransaction(uint8_t *rxbuf, uint8_t bytes);

/**
 * @brief   set imperium rx data transaction.
 *
 * @param[in] txbuf     Buffer of data to send to stm32f446
 * @param[in] bytes     size of the tx data to send, must be less that 64
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumSetRxDataTransaction(uint8_t *txbuf, uint8_t bytes);

/**
 * @brief   get imperium tx data and send it to imperium.
 *
 * @param[in] rxbuf     Buffer to store the tx data
 * @param[in] bytes     size of the tx data to store
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumGetTxData(uint8_t *rxbuf, uint16_t bytes);

/**
 * @brief   receive data from imperium and set it to stm32f446.
 *
 * @param[in] txbuf     Buffer of data to send to stm32f446
 * @param[in] bytes     size of the tx data to send
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t imperiumSetRxData(uint8_t *txbuf, uint16_t bytes);

/**
 * @brief   reset imperium.
 *
 * @param[in] void      None
 *
 * @return              if reset imperium successfully.
 * @retval              TRUE: reset successfully, FALSE: reset fails.
 */
void imperiumReset(void);

/**
 * @brief   start imperium.
 *
 * @param[in] void      None
 *
 * @return              if the imperium boots successfully.
 * @retval              TRUE: imperium exists and boot successfully, FALSE: imperium absent.
 */
bool imperiumStart(void);

/**
 * @brief   check if imperium exists.
 *
 * @param[in] void      None
 *
 * @return              if the imperium exists.
 * @retval              TRUE: imperium exists and boot successfully, FALSE: imperium absent.
 */
bool isImperiumExisting(void);

/**
 * @brief   check if it is needed to start imperium thread.
 *
 * @param[in] void      None
 *
 * @return              if the imperium boots successfully.
 * @retval              TRUE: need to start the thread, FALSE: no need to start the thread.
 */
bool isImperiumThreadNeeded(void);

/**
 * @brief   start imperium thread.
 *
 * @param[in] void      None
 *
 * @return              if the imperium thread starts successfully.
 */
bool imperiumThreadStart(void);

/**
 * @brief   stop imperium thread.
 *
 * @param[in] void      None
 *
 * @return              if the imperium thread stops successfully.
 */
bool imperiumThreadStop(void);

#ifdef __cplusplus
}
#endif

#endif /* _STM32F446_IMPERIUM_H_ */

/** @} */

