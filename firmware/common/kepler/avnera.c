/**
 * @file    avnera.c
 * @brief   Avnera Wireless Audio Processor module code.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */

#include "hal.h"
#include "debug.h"
#include "avnera.h"
#include "project.h"
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
#include "sky76351_21.h"
#endif

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
/**
 * @brief   avnera mac statistics structure.
 */
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
typedef struct sky7635121_macstats avnera_macstats_t;
#endif

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
/*
 * SKY7635121 SPI Communication Pipe related
 */
static const SPIConfig sky7635121_spi3_cfg = {
    .circular = false,
    .end_cb = NULL,
    .ssport = GPIOH,
    .sspad = 8,
    .cfg1 = SPI_CFG1_MBR_DIV16 | SPI_CFG1_DSIZE_VALUE(8 - 1), // 8-bit
    .cfg2 = SPI_CFG2_CPHA, // CPHA = 1, CPOL = 0
};

static const SKY7635121Config sky7635121cfg = {
    &SPID3,
    &sky7635121_spi3_cfg,
};
static SKY7635121Driver SKY7635121;
#endif

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/


/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
/**
 * @brief   Read avnera chipset memory.
 *
 * @param[in] buf       buffer to store the val read from avnera chipset memory
 * @param[in] addr      address of avnera chipset memory to be read
 * @param[in] len       size of the val to read from avnera chipset memory
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraReadMem(uint8_t *buf, uint32_t addr, uint16_t len) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint8_t transf_size;
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure;
    }

    while(len) {
        if(len > AVNERA_COMM_MAX_TRANSFER_SIZE) {
            transf_size = AVNERA_COMM_MAX_TRANSFER_SIZE;
        } else {
            transf_size = len;
        }

        ret_msg = sky7635121ReadMem(&SKY7635121, buf, addr, transf_size);
        if(ret_msg != AISErrMsg_Success) {
            Dbg_printf(DBG_ERROR, "Read memory %d bytes from %x fail!", transf_size, addr);
            return ret_msg;
        }
        buf += transf_size;
        addr += transf_size;
        len -= transf_size;
    }

    return AISErrMsg_Success;
#endif
}

/**
 * @brief   Write avnera chipset memory.
 *
 * @param[in] buf       buffer of the val to write to avnera chipset memory
 * @param[in] addr      address of avnera chipset memory to be written
 * @param[in] len       size of the val to write to avnera chipset memory
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraWriteMem(uint8_t *buf, uint32_t addr, uint16_t len) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint8_t transf_size;
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure;
    }

    while(len) {
        if(len > AVNERA_COMM_MAX_TRANSFER_SIZE) {
            transf_size = AVNERA_COMM_MAX_TRANSFER_SIZE;
        } else {
            transf_size = len;
        }

        ret_msg = sky7635121WriteMem(&SKY7635121, buf, addr, transf_size);
        if(ret_msg != AISErrMsg_Success) {
            Dbg_printf(DBG_ERROR, "Write memory %d bytes to %x fail!", transf_size, addr);
            return ret_msg;
        }
        buf += transf_size;
        addr += transf_size;
        len -= transf_size;
    }

    return AISErrMsg_Success;
#endif
}

/**
 * @brief   get avnera chipset flash operation status.
 *
 * @param[void] void    No parameter
 * @return              the flash operation status
 * @retval 0xFFFFFFFF   if the function failed.
 */
uint32_t avneraGetFlashOpSts(void) {
    uint32_t flash_op_sts;

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return 0xFFFFFFFF; // return an invalid status
    }

    if(AISErrMsg_Success != sky7635121GetParameter(&SKY7635121,
                                                   SKY7635121_FLASH_MODULE, CmdParameter_FLASH_OP_STATUS, &flash_op_sts)) {
        Dbg_printf(DBG_ERROR, "Read Flash Operation Status failed!");
        return 0xFFFFFFFF; // return an invalid status
    }
    return flash_op_sts;
#endif
}

/**
 * @brief   Read avnera chipset flash.
 *
 * @param[in] buf       buffer to store the val read from avnera chipset flash
 * @param[in] addr      address of avnera chipset flash to be read
 * @param[in] len       size of the val to read from avnera chipset flash
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraReadFlash(uint8_t *buf, uint32_t addr, uint16_t len) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint8_t transf_size;
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure;
    }

    while(len) {
        if(len > AVNERA_COMM_MAX_TRANSFER_SIZE) {
            transf_size = AVNERA_COMM_MAX_TRANSFER_SIZE;
        } else {
            transf_size = len;
        }

        ret_msg = sky7635121ReadFlash(&SKY7635121, buf, addr, transf_size);
        if(ret_msg != AISErrMsg_Success) {
            Dbg_printf(DBG_ERROR, "Read flash %d bytes from %x fail!", transf_size, addr);
            return ret_msg;
        }
        buf += transf_size;
        addr += transf_size;
        len -= transf_size;
    }

    return AISErrMsg_Success;
#endif
}

/**
 * @brief   Write avnera chipset flash.
 *
 * @param[in] buf       buffer of the val to write to avnera chipset flash
 * @param[in] addr      address of avnera chipset flash to be written
 * @param[in] len       size of the val to write to avnera chipset flash
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraWriteFlash(uint8_t *buf, uint32_t addr, uint16_t len) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint8_t transf_size;
    uint32_t flash_op_sts;
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure;
    }

    while(len) {
        if(len > AVNERA_COMM_MAX_TRANSFER_SIZE) {
            transf_size = AVNERA_COMM_MAX_TRANSFER_SIZE;
        } else {
            transf_size = len;
        }

        ret_msg = sky7635121WriteFlash(&SKY7635121, buf, addr, transf_size);
        if(ret_msg != AISErrMsg_Success) {
            Dbg_printf(DBG_ERROR, "Write flash %d bytes to %x fail!", transf_size, addr);
            return ret_msg;
        }

        do {
            flash_op_sts = avneraGetFlashOpSts();
            if(0xFFFFFFFF == flash_op_sts) {
                return AISErrMsg_CommFailure;
            } else if(0 == flash_op_sts) {
                break;
            } else {
                continue;
            }
        } while(TRUE);

        buf += transf_size;
        addr += transf_size;
        len -= transf_size;
    }

    return AISErrMsg_Success;
#endif
}

/**
 * @brief   Erase avnera chipset flash.
 *
 * @param[in] addr      address of avnera chipset flash to be erased, it should be 4K-aligned
 * @param[in] size      size of avnera chipset flash to be erased This is
 *                      4k and bound to multiples of those sizes.
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraEraseFlash(uint32_t addr, uint32_t size) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    ais_msg_t ret_msg;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure;
    }

    // Setting size 0 wipes the flash, only support sector erase or block erase
    if(0 == size || ((size != 4096) && (size != 65536))) {
        Dbg_printf(DBG_ERROR, "Erase size shouldn't be 0 or greater than 64K");
        return AISErrMsg_NotSupported;
    }

    // set Erase 4K control
    ret_msg = sky7635121SetParameter(&SKY7635121, SKY7635121_FLASH_MODULE,
                                     CmdParameter_FLASH_SET_ERASE_4KCTRL, 1);
    if(ret_msg != AISErrMsg_Success) {
        Dbg_printf(DBG_ERROR, "Set Erase 4K control");
        return ret_msg;
    }

    // set Erase Address
    ret_msg = sky7635121SetParameter(&SKY7635121, SKY7635121_FLASH_MODULE,
                                     CmdParameter_FLASH_SET_ERASE_ADDR, addr);
    if(ret_msg != AISErrMsg_Success) {
        Dbg_printf(DBG_ERROR, "Set Erase Address");
        return ret_msg;
    }

    // set Size and Erase
    // Sets the size of the flash you want to erase. This is either 4k or 64k minimum and bound
    // to multiples of those sizes. This command also executes the erase.
    ret_msg = sky7635121SetParameter(&SKY7635121, SKY7635121_FLASH_MODULE,
                                     CmdParameter_FLASH_SET_ERASE_SIZE, size);
    if(ret_msg != AISErrMsg_Success) {
        Dbg_printf(DBG_ERROR, "Set Size and Erase");
        return ret_msg;
    }

    return AISErrMsg_Success;
#endif
}

/**
 * @brief   get avnera chipset arbiter id.
 *
 * @param[void] void    No parameter
 * @return              the chipset arbiter id
 * @retval 0            if the function failed.
 */
uint32_t avneraGetArbiterID(void) {
    uint32_t arbiter_id;

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return 0; // return an invalid address
    }

    if(AISErrMsg_Success != sky7635121GetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                                   CmdParameter_MAC_ARBITER_ID, &arbiter_id)) {
        Dbg_printf(DBG_ERROR, "Read arbiter id failed!");
        return 0; // return an invalid address
    }
    return arbiter_id;
#endif
}

/**
 * @brief   set avnera chipset arbiter id.
 *
 * @param[in] arbiter_id    arbiter id to set
 * @return                  the system flag bytes.
 * @retval MSG_OK           if the function succeeded.
 */
msg_t avneraSetArbiterID(uint32_t arbiter_id) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_ARBITER_ID, arbiter_id);
#endif
}

/**
 * @brief   save avnera chipset arbiter id.
 *
 * @param[in] void          No parameter
 * @return                  the system flag bytes.
 * @retval MSG_OK           if the function succeeded.
 */
msg_t avneraSaveArbiterID(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_SAVE_VM, 0);
#endif
}

/**
 * @brief   get avnera chipset link State.
 *
 * @param[void] void                No parameter
 * @return                          the chipset mac address
 * @retval MacCtxtState_Invalid     if the function failed.
 */
uint32_t avneraGetLinkState(void) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint32_t link_state;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return MacCtxtState_Invalid; // return an invalid state
    }

    if(AISErrMsg_Success != sky7635121GetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                                   CmdParameter_MAC_GET_LINK_STS, &link_state)) {
        Dbg_printf(DBG_ERROR, "Read link state failed!");
        return MacCtxtState_Invalid; // return an invalid state
    }
    return link_state;
#endif
}

/**
 * @brief   set avnera chipset in pairing mode.
 *
 * @param[in] void              None parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetOpenPairing(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_OPEN_PAIRING, 1);
#endif
}

/**
 * @brief   set avnera chipset in fixed channel pairing mode.
 *
 * @param[in] channel           select the channel
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetFixedChannelPairing(uint8_t channel) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(channel > SKY7635121_RF_CHANNEL_MAX) {
        Dbg_printf(DBG_ERROR, "Parameter channel is out of range, %d", channel);
        return AISErrMsg_Failed; // return a failure
    }

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    ret_msg = sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                     CmdParameter_MAC_CHANNEL_SELECT, channel);
    if(ret_msg != AISErrMsg_Success) {
        Dbg_printf(DBG_ERROR, "Select %d channel fail!", channel);
        return ret_msg;
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_OPEN_PAIRING, 1);
#endif
}

/**
 * @brief   cancel pairing.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraCancelPairing(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_CANCEL_PAIRING, 0);
#endif
}

/**
 * @brief   set modulation mode.
 *
 * @param[in] modul_mode        Modulation mode to set
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetModulationMode(uint32_t modul_mode) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(modul_mode > Modulation_Mode_Pattern) {
        Dbg_printf(DBG_ERROR, "Parameter modul_mode is out of range, %d", modul_mode);
        return AISErrMsg_Failed; // return a failure
    }

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_MODULATION_MODE, modul_mode);
#endif
}

/**
 * @brief   select carrier.
 *
 * @param[in] carrier           Carrier to select
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectCarrier(uint32_t carrier) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(carrier > Carrier_DualSide) {
        Dbg_printf(DBG_ERROR, "Parameter carrier is out of range, %d", carrier);
        return AISErrMsg_Failed; // return a failure
    }

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_CARRIER_SELECT, carrier);
#endif
}

/**
 * @brief   select channel.
 *
 * @param[in] channel           Channel to select, Channel Range: [0..39]
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectChannel(uint32_t channel) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(channel > SKY7635121_RF_CHANNEL_MAX) {
        Dbg_printf(DBG_ERROR, "Parameter channel is out of range, %d", channel);
        return AISErrMsg_Failed; // return a failure
    }

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_CHANNEL_SELECT, channel);
#endif
}

/**
 * @brief   select antenna.
 *
 * @param[in] antenna           Antenna to select
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectAntenna(uint32_t antenna) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(antenna > Antenna_Antenna1) {
        Dbg_printf(DBG_ERROR, "Parameter antenna is out of range, %d", antenna);
        return AISErrMsg_Failed; // return a failure
    }

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_ANTENNA_SELECT, antenna);
#endif
}

/**
 * @brief   set TX power.
 *
 * @param[in] tx_pwr            TX Power to set, Power Range: [0x3F..0x00] (0.5 dB/step)
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetTxPower(uint32_t tx_pwr) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(tx_pwr > SKY7635121_RF_TXPWR_MAX) {
        Dbg_printf(DBG_ERROR, "Parameter tx_pwr is out of range, %d", tx_pwr);
        return AISErrMsg_Failed; // return a failure
    }

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_TX_POWER, tx_pwr);
#endif
}

/**
 * @brief   Enable continuous TX mode.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraEnableContTx(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_CONT_TX_EN, 0);
#endif
}

/**
 * @brief   Enable continuous RX mode.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraEnableContRx(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_CONT_RX_EN, 0);
#endif
}

/**
 * @brief   select fixed frequency.
 *
 * @param[in] freq              Frequency to select.
                                            F = freq
                                            UNIT = F*(4/3)
                                            INT = floor(F/48)
                                            FRAC = (F-48*INT)*2^13*(2/3)+250
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectFixedFreq(uint32_t freq) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_FIXED_FREQ_SELECT, freq);
#endif
}

/**
 * @brief   Read PEC and Frames from avnera chipset MAC statistics.
 *
 * @param[in] buf       buffer to store the PEC and Frames from avnera chipset mac statistics
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraReadMacPer(uint8_t *buf) {

#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    avnera_macstats_t macstats;
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure;
    }

    ret_msg = sky7635121ReadMac(&SKY7635121, (uint8_t *)&macstats, 0, sizeof(macstats));
    if(ret_msg != AISErrMsg_Success) {
        Dbg_printf(DBG_ERROR, "avnera read MAC statistics fail!");
        return ret_msg;
    }

    // prepare the data
    *buf = (uint8_t)(macstats.rxPeer[0].pec & 0x00FF);                  // PEC LSB
    *(buf + 1) = (uint8_t)((macstats.rxPeer[0].pec & 0xFF00) >> 8);     // PEC MSB
    *(buf + 2) = (uint8_t)(macstats.frmcnt & 0x00FF);                   // Frames LSB
    *(buf + 3) = (uint8_t)((macstats.frmcnt & 0xFF00) >> 8);            // Frames MSB

    return AISErrMsg_Success;
#endif
}

/**
 * @brief   start Mac Statistics.
 *
 * @param[in] start             Start or stop the mac statistics
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraStartMacStats(bool start) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint32_t start_stats_val = 0;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    start_stats_val = start ? 0 : 1;
    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_START_STATS, start_stats_val);
#endif
}

/**
 * @brief   clear Mac Statistics.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraClearMacStats(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(MacCtxtState_Invalid == avneraGetLinkState()) {
        Dbg_printf(DBG_ERROR, "Avnera chipset is in invalid link state!");
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_MAC_MODULE,
                                  CmdParameter_MAC_CLEAR_STATS, 0);
#endif
}

/**
 * @brief   set next active boot app FW, app0 or app1.
 *
 * @param[in] boot_app          the next boot app to select
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetNextBoot(avnera_boot_app_t boot_app) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    if(boot_app > BootApp_App1) {
        Dbg_printf(DBG_ERROR, "The parameter boot_app is invalid: %d", boot_app);
        return AISErrMsg_Failed; // return failed
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_AVBOOT_MODULE,
                                  CmdParameter_AVBOOT_APP_INFO, boot_app);
#endif
}

/**
 * @brief   get boot info.
 *
 * @param[in] void              No parameter
 * @return                      the boot information.
 * @retval {0xFF, 0xFF}         if the function failed.
 */
avnera_boot_info_t avneraGetBootInfo(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint32_t app_info;
    avnera_boot_info_t boot_info = {.now = 0xFF, .next = 0xFF};

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return boot_info; // return an invalid status
    }

    if(AISErrMsg_Success != sky7635121GetParameter(&SKY7635121,
                                                   SKY7635121_AVBOOT_MODULE, CmdParameter_AVBOOT_APP_INFO, &app_info)) {
        Dbg_printf(DBG_ERROR, "Read avboot app information failed!");
        return boot_info; // return an invalid status
    }

    boot_info.now = (app_info & 0x02) >> 1;
    boot_info.next = app_info & 0x01;

    return boot_info;
#endif
}

/**
 * @brief   get FW flash Address.
 *
 * @param[in] boot_app          the boot app to select
 * @return                      the FW flash Address.
 * @retval 0xFFFFFFFF           if the function failed.
 */
uint32_t avneraGetAppFWAddr(avnera_boot_app_t boot_app) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    uint32_t fw_addr;
    uint32_t module_image_info_addr;
    ais_msg_t ret_msg = AISErrMsg_Success;

    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return 0xFFFFFFFF; // return an invalid status
    }

    if(boot_app > BootApp_App1) {
        Dbg_printf(DBG_ERROR, "The parameter boot_app is invalid: %d", boot_app);
        return 0xFFFFFFFF; // return an invalid status
    }

    if(boot_app == BootApp_App0) {
        module_image_info_addr = AVBOOT_MODULE_IMAGE_INFO_FW_SET0_ADDR;
    } else {
        module_image_info_addr = AVBOOT_MODULE_IMAGE_INFO_FW_SET1_ADDR;
    }

    ret_msg = sky7635121ReadAvBoot(&SKY7635121, (uint8_t *)&fw_addr, module_image_info_addr, sizeof(fw_addr));
    if(ret_msg != AISErrMsg_Success) {
        Dbg_printf(DBG_ERROR, "avnera read avboot image info fail!");
        return 0xFFFFFFFF; // return an invalid status
    }

    return fw_addr;
#endif
}

/**
 * @brief   avboot reset.
 *
 * @param[in] void              None
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraAvbootReset(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return AISErrMsg_CommFailure; // return an communication failure
    }

    return sky7635121SetParameter(&SKY7635121, SKY7635121_AVBOOT_MODULE,
                                  CmdParameter_AVBOOT_RESET, 0x01);
#endif
}

/**
 * @brief   Avnera Authentication Procedure.
 *
 * @param[in] void              None
 * @return                      TRUE: Authentication Procedure Succeed, FALSE: Authentication Procedure Fail.
 */
bool avneraAuthProcedure(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    if(FALSE == isSky7635121Ready(&SKY7635121)) {
        Dbg_printf(DBG_ERROR, "SPI slave sky7635121 is not ready");
        return FALSE;
    }

    return sky7635121AuthProcedure(&SKY7635121);
#endif
}

/**
 * @brief   Initializes an instance.
 *
 *
 * @init
 */
void avneraInit(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    sky7635121ObjectInit(&SKY7635121);
#endif
}

/**
 * @brief   Configures and activates Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void avneraStart(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    sky7635121Start(&SKY7635121, &sky7635121cfg);
#endif
}

/**
 * @brief   Deactivates the Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void avneraStop(void) {
#if AVNERA_CHIP_ID & SKY7635121_CHIP_ID
    sky7635121Stop(&SKY7635121);
#endif
}

/**
 * @brief   Reset the Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void avneraReset(void) {
    palClearPad(GPIOD, 4);
    chThdSleepMilliseconds(1);
    palSetPad(GPIOD, 4);
}

/** @} */
