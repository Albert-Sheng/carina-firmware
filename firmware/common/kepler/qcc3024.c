/**
 * @file    qcc3024.c
 * @brief   QCC3024 Bluetooth Wireless Audio Processor module code.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */

#include <string.h>
#include "hal.h"
#include "qcc3024.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/**
 * @name    QCC3024 Command Constants
 * @{
 */
/**
 * @brief   The command reserved value
 * @details
 * @note
 */
#define QCC3024_CMD_MSG_RESERVED                                        (0x02)

/**
 * @brief   The command message type
 * @details
 * @note
 */
#define QCC3024_CMD_MSG_TYPE_TEST                                       (0x00)
#define QCC3024_CMD_MSG_TYPE_ENTER_PAIRING                              (0x90)
#define QCC3024_CMD_MSG_TYPE_CANCEL_PAIRING                             (0x91)
#define QCC3024_CMD_MSG_TYPE_GET_BD_ADDR                                (0x92)
#define QCC3024_CMD_MSG_TYPE_SET_BD_ADDR                                (0x93)
#define QCC3024_CMD_MSG_TYPE_GET_PROF_CONN                              (0x94)
#define QCC3024_CMD_MSG_TYPE_GET_HFP_CALL_STATE                         (0x95)
#define QCC3024_CMD_MSG_TYPE_GET_A2DP_STREAM_STATE                      (0x96)
#define QCC3024_CMD_MSG_TYPE_SET_TX_PWR                                 (0x97)
#define QCC3024_CMD_MSG_TYPE_START_CONT_TX                              (0x98)
#define QCC3024_CMD_MSG_TYPE_PAUSE_RADIO_TEST                           (0x99)
#define QCC3024_CMD_MSG_TYPE_UPGRADE_COMMAND                            (0x9A)
#define QCC3024_CMD_MSG_TYPE_UPGRADE_DATA_TRANSFER                      (0x9B)
#define QCC3024_CMD_MSG_TYPE_UPGRADE_RESPONSE_GET                       (0x9C)
#define QCC3024_CMD_MSG_TYPE_UPGRADE_RESPONSE_CLR                       (0x9D)
/* 0x9E ~ 0x9F Reserved */
#define QCC3024_CMD_MSG_TYPE_CLEAR_PDL                                  (0xA0)
#define QCC3024_CMD_MSG_TYPE_SET_DUT                                    (0xA1)
#define QCC3024_CMD_MSG_TYPE_GET_MAX_TRUSTED_DEVICES                    (0xA2)
#define QCC3024_CMD_MSG_TYPE_GET_INDEXED_TDL_DEVICE                     (0xA3)
/* 0xA4 ~ 0xAF Reserved */
#define QCC3024_CMD_MSG_TYPE_BLE_START_BONDING                          (0xB0)
/* 0xB1 ~ 0xBF Reserved */
#define QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_GET_DATA_LENGTH              (0xC0)
#define QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_SET_DATA_LENGTH              (0xC1)
#define QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_READ                         (0xC2)
#define QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_WRITE                        (0xC3)
#define QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_READ_NOTIFY                  (0xC4)
#define QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_WRITE_NOTIFY                 (0xC5)

/**
 * @brief   The command response type
 * @details
 * @note
 */
#define QCC3024_CMD_RESP_ACK                                            (0x00)
#define QCC3024_CMD_RESP_ERROR                                          (0x01)
#define QCC3024_CMD_RESP_DATA                                           (0x02)

/**
 * @brief   The buffer size for commands
 * @details So far the TX and RX share the same size, 128 bytes.
 * @note
 */
#define QCC3024_CMD_BUFFER_SIZE                                         (128)
#define QCC3024_CMD_MAX_TRY_NUM                                         (10)
#define QCC3024_UART_TIMEOUT                                    OSAL_MS2I(100)

/** @} */

/**
 * @name    QCC3024 API Constants
 * @{
 */
/**
 * @brief   The api return message
 * @details Use MSG_OK/MSG_TIMEOUT/MSG_RESET defined in chschd.h, extand more for special
 *          purpose.
 * @note
 */

#define MSG_RESP_TYPE_ERROR                                             (-3)
#define MSG_LENGTH_ERROR                                                (-4)

/** @} */

/**
 * @name    QCC3024 GATT Server Constants
 * @{
 */
/**
 * @brief   The GATT Server max payload size
 * @details These two macro limit the max payload size and notification size of GATT Server.
 *
 * @note
 */
#define GATT_SERVER_MAX_PAYLOAD_SIZE                                    (512)
#define GATT_SERVER_MAX_NOTIFICATION_SIZE                               (20)
#define GATT_SERVER_DATA_TRANSFER_SIZE                                  (32)
/** @} */


/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
/**
 * @name    QCC3024 Command variables and types
 * @{
 */

/**
 * @brief   QCC3024 Command/Response Message structure.
 */
#pragma pack(push,1)
typedef struct {
    uint8_t Reserved; // Fixed to QCC3024_CMD_MSG_RESERVED
    uint8_t type;
    union {
        uint8_t attr;
        struct {
            unsigned int bytes : 6;
            unsigned int sequence : 2;
        };
    };
    uint8_t payload[61];
} qcc3024_msg_t;
#pragma pack(pop)

static uint8_t qcc3024_cmd_tx_buffer[QCC3024_CMD_BUFFER_SIZE] __attribute__((section(".nocache")));
static uint8_t qcc3024_cmd_rx_buffer[QCC3024_CMD_BUFFER_SIZE] __attribute__((section(".nocache")));

/** @} */

static const struct QCC3024VMT vmt_device = {
    (size_t)0
};

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/
#if (QCC3024_USE_UART) || defined(__DOXYGEN__)
/**
 * @brief   Write data to QCC3024.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] txbuf     the pointer to the transmit buffer
 * @param[in,out] np    number of data to transmit, on exit the number
 *                      of data actually transmitted
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 * @retval MSG_TIMEOUT  if the operation timed out.
 *
 * @api
 */
static msg_t qcc3024UartWrite(QCC3024Driver *devp, uint8_t *txbuf, size_t *np) {
    msg_t ret = MSG_OK;

    osalDbgCheck((devp != NULL) && (txbuf != NULL) && (*np < QCC3024_CMD_BUFFER_SIZE));

#if QCC3024_SHARED_UART
    uartAcquireBus(devp->config->uartp);
#endif
    uartStart(devp->config->uartp, devp->config->uartcfg);

    ret = uartSendFullTimeout(devp->config->uartp, np, txbuf, QCC3024_UART_TIMEOUT);

#if QCC3024_SHARED_UART
    uartReleaseBus(devp->config->uartp);
#endif

    return ret;
}

/**
 * @brief   Read data from QCC3024.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] rxbuf     the pointer to the receive buffer
 * @param[in,out] np    number of data to receive, on exit the number
 *                      of data actually received
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 * @retval MSG_TIMEOUT  if the operation timed out.
 *
 * @api
 */
static msg_t qcc3024UartRead(QCC3024Driver *devp, uint8_t *rxbuf, size_t *np) {
    msg_t ret = MSG_OK;

    osalDbgCheck((devp != NULL) && (rxbuf != NULL) && (*np < QCC3024_CMD_BUFFER_SIZE));

#if QCC3024_SHARED_UART
    uartAcquireBus(devp->config->uartp);
#endif
    uartStart(devp->config->uartp, devp->config->uartcfg);

    ret = uartReceiveTimeout(devp->config->uartp, np, rxbuf, QCC3024_UART_TIMEOUT);

#if QCC3024_SHARED_UART
    uartReleaseBus(devp->config->uartp);
#endif

    return ret;
}
#endif /* QCC3024_USE_UART */

/**
 * @brief   Send Command to QCC3024.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] cmd       the pointer to the transmit command buffer
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 * @retval MSG_TIMEOUT  if the operation timed out.
 *
 * @api
 */
static msg_t qcc3024CommandSend(QCC3024Driver *devp, qcc3024_msg_t *cmd) {
    msg_t ret = MSG_OK;
    uint8_t try_cnt = 0;
    size_t length = cmd->bytes + 3;
    size_t tx_size = length;
    uint8_t *tx_buf = qcc3024_cmd_tx_buffer;

    osalDbgCheck((devp != NULL) && (cmd != NULL) && (length <= QCC3024_CMD_BUFFER_SIZE));

    // copy the command data into tx buffer
    memcpy(tx_buf, (uint8_t *)cmd, length);

    // send the data to QCC3024
    do {
#if (QCC3024_USE_UART) || defined(__DOXYGEN__)
        ret = qcc3024UartWrite(devp, tx_buf, &tx_size);
#endif /* QCC3024_USE_UART */

        // we have already sent enough data we want even the return value is wrong
        if((ret != MSG_OK) && (tx_size == length)) {
            ret = MSG_OK;
            break;
        }

        if((ret != MSG_OK) && (tx_size < length)) {
            // timeout, we need to send the remaining data
            tx_buf += tx_size;  // skip the data which has already been transmitted
            length -= tx_size;  // update the variable which records the length of remaining data
            tx_size = length;   // update the next transfer size
        }
    } while((ret != MSG_OK) && (try_cnt++ < QCC3024_CMD_MAX_TRY_NUM));

    return ret;
}

/**
 * @brief   Receive command response from QCC3024.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in,out] resp  the pointer to the receive response buffer
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 * @retval MSG_TIMEOUT  if the operation timed out.
 *
 * @api
 */
static msg_t qcc3024ResponseReceive(QCC3024Driver *devp, qcc3024_msg_t *resp) {
    msg_t ret = MSG_OK;
    uint8_t try_cnt = 0;
    size_t length;
    size_t rx_size;
    uint8_t *rx_buf = qcc3024_cmd_rx_buffer;
    qcc3024_msg_t *tmp_resp_buf = (qcc3024_msg_t *)rx_buf;

    osalDbgCheck((devp != NULL) && (resp != NULL));

    // for response receiving, QCC3024 will send back the whole 64 bytes response.
    length = rx_size = sizeof(qcc3024_msg_t);

    do {
#if (QCC3024_USE_UART) || defined(__DOXYGEN__)
        ret = qcc3024UartRead(devp, rx_buf, &rx_size);
#endif /* QCC3024_USE_UART */

        // we have already read back enough data we want even the return value is wrong
        if((ret != MSG_OK) && (rx_size == length)) {
            ret = MSG_OK;
            break;
        }

        if((ret != MSG_OK) && (rx_size < length)) {
            // timeout, we need to receive the remaining data
            rx_buf += rx_size;  // skip the data which has already been received
            length -= rx_size;  // update the variable which records the length of remaining data
            rx_size = length;   // update the next transfer size
        }
    } while((ret != MSG_OK) && (try_cnt++ < QCC3024_CMD_MAX_TRY_NUM));

    // receive header fail
    if((ret != MSG_OK) && (try_cnt > QCC3024_CMD_MAX_TRY_NUM)) {
        return ret;
    }

    // copy the data to response
    memcpy((uint8_t *)resp, (uint8_t *)tmp_resp_buf, tmp_resp_buf->bytes + 3);

    return MSG_OK;
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
/**
 * @brief   Use test command to test the Uart Communication pipe.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024TestCommPipe(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the test command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_TEST;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return ret;
}

msg_t qcc3024EnterPairing(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_ENTER_PAIRING;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024CancelPairing(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CANCEL_PAIRING;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

bdaddr_t qcc3024GetBdAddr(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    bdaddr_t bd_addr = {.lap = 0xFFFFFFFF, .uap = 0xFF, .nap = 0xFFFF};

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_GET_BD_ADDR;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return bd_addr; // return an invalid bd address when failed.
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return bd_addr; // return an invalid bd address when failed.
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return bd_addr; // return an invalid bd address when failed.
    }

    // copy the bdaddr to bd_addr, the structure alignment should be the same as qcc3024
    memcpy((uint8_t *)&bd_addr, resp.payload, sizeof(bdaddr_t));

    return bd_addr;
}

msg_t qcc3024SetBdAddr(QCC3024Driver *devp, bdaddr_t bdaddr) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_SET_BD_ADDR;

    if((cmd.bytes = sizeof(bdaddr_t)) > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }
    memcpy(cmd.payload, (uint8_t *)&bdaddr, sizeof(bdaddr_t));

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

conn_profile_t qcc3024GetProfilesConnected(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    conn_profile_t profile;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_GET_PROF_CONN;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return 0x00; // return an invalid value when failed.
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return 0x00; // return an invalid value when failed.
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return 0x00; // return an invalid value when failed.
    }

    // copy the bdaddr to bd_addr, the structure alignment should be the same as qcc3024
    memcpy((uint8_t *)&profile, resp.payload, sizeof(conn_profile_t));

    return profile;
}

hfp_call_state_t qcc3024GetHFPCallState(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    conn_profile_t profile = qcc3024GetProfilesConnected(devp);
    hfp_call_state_t call_state;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_GET_HFP_CALL_STATE;
    cmd.bytes = 1;
    cmd.payload[0] = (profile & conn_hfp_pri) ? hfp_primary_link
                     : ((profile & conn_hfp_sec) ? hfp_secondary_link : hfp_invalid_link);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return hfp_call_state_idle;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return hfp_call_state_idle;
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return hfp_call_state_idle;
    }

    // copy the bdaddr to bd_addr, the structure alignment should be the same as qcc3024
    memcpy((uint8_t *)&call_state, resp.payload, sizeof(hfp_call_state_t));

    return call_state;
}

a2dp_stream_state_t qcc3024GetA2DPStreamState(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    conn_profile_t profile = qcc3024GetProfilesConnected(devp);
    a2dp_stream_state_t stream_state;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_GET_A2DP_STREAM_STATE;
    cmd.bytes = 1;
    cmd.payload[0] = (profile & conn_a2dp_pri) ? a2dp_primary
                     : ((profile & conn_a2dp_sec) ? a2dp_secondary : a2dp_invalid);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return a2dp_stream_idle;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return a2dp_stream_idle;
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return a2dp_stream_idle;
    }

    // copy the bdaddr to bd_addr, the structure alignment should be the same as qcc3024
    memcpy((uint8_t *)&stream_state, resp.payload, sizeof(a2dp_stream_state_t));

    return stream_state;
}

msg_t qcc3024SetTxPwr(QCC3024Driver *devp, uint16_t txpwr) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_SET_TX_PWR;

    cmd.bytes = 2;
    cmd.payload[0] = (uint8_t)((txpwr & 0xFF00) >> 8);
    cmd.payload[1] = (uint8_t)(txpwr & 0x00FF);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024StartContTx(QCC3024Driver *devp, uint16_t lo_freq,
                         uint16_t level, uint16_t mod_freq) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_START_CONT_TX;

    cmd.bytes = 6;
    cmd.payload[0] = (uint8_t)((lo_freq & 0xFF00) >> 8);
    cmd.payload[1] = (uint8_t)(lo_freq & 0x00FF);
    cmd.payload[2] = (uint8_t)((level & 0xFF00) >> 8);
    cmd.payload[3] = (uint8_t)(level & 0x00FF);
    cmd.payload[4] = (uint8_t)((mod_freq & 0xFF00) >> 8);
    cmd.payload[5] = (uint8_t)(mod_freq & 0x00FF);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024PauseRadioTest(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_PAUSE_RADIO_TEST;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024SendUpgradeCmd(QCC3024Driver *devp,  uint8_t *data, uint8_t len) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_UPGRADE_COMMAND;

    if(len > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }

    cmd.bytes = len; // the value may be shortened since cmd.bytes only have 6 bits.
    memcpy(cmd.payload, data, len);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024SendUpgradeData(QCC3024Driver *devp,  uint8_t *data, uint8_t len) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    static uint8_t seq_id = 0;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_UPGRADE_DATA_TRANSFER;
    cmd.sequence = seq_id;

    if(len > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }

    cmd.bytes = len; // the value may be shortened since cmd.bytes only have 6 bits.
    memcpy(cmd.payload, data, len);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    // update the sequence id
    seq_id = (seq_id + 1) & 0x03;

    return MSG_OK;
}

msg_t qcc3024ReceiveUpgradeResp(QCC3024Driver *devp,  uint8_t *data, uint8_t *len) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_UPGRADE_RESPONSE_GET;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return MSG_RESP_TYPE_ERROR;
    }

    // Output the received response
    *len = resp.bytes;
    memcpy(data, resp.payload, resp.bytes);

    return MSG_OK;
}

msg_t qcc3024ClearUpgradeResp(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_UPGRADE_RESPONSE_CLR;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024ClearPDL(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CLEAR_PDL;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024SetDUT(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_SET_DUT;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

uint16_t qcc3024GetMaxTrustedDevice(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_GET_MAX_TRUSTED_DEVICES;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return 0; // return 0 when failed.
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return 0; // return 0 when failed.
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return 0; // return 0 when failed.
    }

    return ((uint16_t)resp.payload[1]) << 8 | (uint16_t)resp.payload[0];
}

bdaddr_t qcc3024GetIndexedTdlDevice(QCC3024Driver *devp, uint16_t index) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    bdaddr_t bd_taddr = {.lap = 0xFFFFFFFF, .uap = 0xFF, .nap = 0xFFFF};

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_GET_INDEXED_TDL_DEVICE;
    cmd.bytes = 2;
    cmd.payload[0] = (uint8_t)((index & 0xFF00) >> 8);
    cmd.payload[1] = (uint8_t)(index & 0x00FF);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return bd_taddr; // return an invalid bd address when failed.
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return bd_taddr; // return an invalid bd address when failed.
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return bd_taddr; // return an invalid bd address when failed.
    }

    // copy the bdaddr to bd_addr, the structure alignment should be the same as qcc3024
    memcpy((uint8_t *)&bd_taddr, resp.payload, sizeof(bdaddr_t));

    return bd_taddr;
}

msg_t qcc3024StartBleBonding(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_BLE_START_BONDING;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

/**
 * @brief   Get Centpp Server Data Length.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The length of the Centpp Server Data to be read.
 *
 * @api
 */
uint16_t qcc3024GetCentppServerDataLength(QCC3024Driver *devp) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_GET_DATA_LENGTH;

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return 0xFFFF; // return an invalid valid when failed.
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return 0xFFFF; // return an invalid valid when failed.
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return 0xFFFF; // return an invalid valid when failed.
    }

    return ((uint16_t)resp.payload[1]) << 8 | (uint16_t)resp.payload[0];
}

/**
 * @brief   Set the length of the Centpp Server data.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] length    the length of the Centpp Server data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SetCentppServerDataLength(QCC3024Driver *devp, uint16_t length) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_SET_DATA_LENGTH;

    if((cmd.bytes = sizeof(length)) > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }

    // store the length to payload
    cmd.payload[0] = (uint8_t)((length & 0xFF00) >> 8);     // MSB
    cmd.payload[1] = (uint8_t)(length & 0x00FF);            // LSB

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

/**
 * @brief   Read the Centpp Server data from special position.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[out] buf      the buffer to store the Centpp Server data read
 * @param[in] offset    the position offset of the Centpp Server data
 * @param[in] length    the length of the Centpp Server data to read
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024ReadCentppServerPositionData(QCC3024Driver *devp,
                                          uint8_t *buf,
                                          uint16_t offset,
                                          uint16_t length) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_READ;

    if((cmd.bytes = sizeof(offset)  + sizeof(length)) > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }

    // store the length to payload
    cmd.payload[0] = (uint8_t)((offset & 0xFF00) >> 8);     // MSB
    cmd.payload[1] = (uint8_t)(offset & 0x00FF);            // LSB
    cmd.payload[2] = (uint8_t)((length & 0xFF00) >> 8);     // MSB
    cmd.payload[3] = (uint8_t)(length & 0x00FF);            // LSB

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_DATA) {
        return MSG_RESP_TYPE_ERROR;
    }

    // copy the Centpp Server data read
    memcpy(buf, resp.payload, length);

    return MSG_OK;
}

/**
 * @brief   Write the Centpp Server data to special position.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] buf       the buffer which stores the Centpp Server data to write
 * @param[in] offset    the position offset of the Centpp Server data
 * @param[in] length    the length of the Centpp Server data to write
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024WriteCentppServerPositionData(QCC3024Driver *devp,
                                           uint8_t *buf,
                                           uint16_t offset,
                                           uint16_t length) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_WRITE;

    if((4 + length) > (uint16_t)sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }
    cmd.bytes = (4 + length); // the value may be shortened since cmd.bytes only have 6 bits.

    // store the length to payload
    cmd.payload[0] = (uint8_t)((offset & 0xFF00) >> 8);     // MSB
    cmd.payload[1] = (uint8_t)(offset & 0x00FF);            // LSB
    cmd.payload[2] = (uint8_t)((length & 0xFF00) >> 8);     // MSB
    cmd.payload[3] = (uint8_t)(length & 0x00FF);            // LSB
    memcpy(&cmd.payload[4], buf, length);


    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    return MSG_OK;
}

msg_t qcc3024ReadCentppServerData(QCC3024Driver *devp,
                                  uint8_t *buf,
                                  uint16_t *length) {
    uint16_t offset = 0;
    uint16_t transfer_size = 0;
    uint16_t remaining_data_length;
    msg_t ret = MSG_OK;

    // Get the Centpp Server Data Length first
    *length = qcc3024GetCentppServerDataLength(devp);
    if(*length > GATT_SERVER_MAX_PAYLOAD_SIZE) { // there is an error when talking to QCC3024
        return MSG_LENGTH_ERROR;
    }

    if(0 == *length) {                           // no data to read currently
        return MSG_OK;
    }

    // read the Centpp Server Data
    remaining_data_length = *length;
    while(remaining_data_length) {
        if(remaining_data_length > GATT_SERVER_DATA_TRANSFER_SIZE) {
            transfer_size = GATT_SERVER_DATA_TRANSFER_SIZE;
        } else {
            transfer_size = remaining_data_length;
        }
        ret = qcc3024ReadCentppServerPositionData(devp, buf + offset, offset, transfer_size);
        if(MSG_OK != ret) {
            return ret;
        }

        offset += transfer_size;
        remaining_data_length -= transfer_size;
    }

    return MSG_OK;
}

msg_t qcc3024WriteCentppServerData(QCC3024Driver *devp,
                                   uint8_t *buf,
                                   uint16_t length) {
    uint16_t offset = 0;
    uint16_t transfer_size = 0;
    uint16_t remaining_data_length;
    msg_t ret = MSG_OK;

    // check the length
    if(length > GATT_SERVER_MAX_PAYLOAD_SIZE) { // there is an error when talking to QCC3024
        return MSG_LENGTH_ERROR;
    }

    // write the Centpp Server Data
    remaining_data_length = length;
    while(remaining_data_length) {
        if(remaining_data_length > GATT_SERVER_DATA_TRANSFER_SIZE) {
            transfer_size = GATT_SERVER_DATA_TRANSFER_SIZE;
        } else {
            transfer_size = remaining_data_length;
        }
        ret = qcc3024WriteCentppServerPositionData(devp, buf + offset, offset, transfer_size);
        if(MSG_OK != ret) {
            return ret;
        }

        offset += transfer_size;
        remaining_data_length -= transfer_size;
    }

    // set the Centpp Server Data length
    ret = qcc3024SetCentppServerDataLength(devp, length);
    return ret;
}

msg_t qcc3024SendCentppServerReadNotification(QCC3024Driver *devp,
                                              uint8_t *buf,
                                              uint16_t length) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    static uint8_t seq_id = 0;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_READ_NOTIFY;
    cmd.sequence = seq_id;

    if(length > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }
    cmd.bytes = length; // the value may be shortened since cmd.bytes only have 6 bits.

    // store the data to payload
    memcpy(cmd.payload, buf, length);


    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    // update the sequence id
    seq_id = (seq_id + 1) & 0x03;

    return MSG_OK;
}

msg_t qcc3024SendCentppServerWriteNotification(QCC3024Driver *devp,
                                               uint8_t *buf,
                                               uint16_t length) {
    qcc3024_msg_t cmd, resp;
    msg_t ret = MSG_OK;
    static uint8_t seq_id = 0;

    // initialize the command
    memset((uint8_t *)&cmd, 0, sizeof(qcc3024_msg_t));
    cmd.Reserved = QCC3024_CMD_MSG_RESERVED;
    cmd.type = QCC3024_CMD_MSG_TYPE_CENTPP_SERVER_WRITE_NOTIFY;
    cmd.sequence = seq_id;

    if(length > sizeof(cmd.payload)) {
        return MSG_LENGTH_ERROR;
    }
    cmd.bytes = length; // the value may be shortened since cmd.bytes only have 6 bits.
    // store the data to payload
    memcpy(cmd.payload, buf, length);

    // send the command to qcc3024
    ret = qcc3024CommandSend(devp, &cmd);
    if(MSG_OK != ret) {
        return ret;
    }

    // receive the reponse from qcc3024
    ret = qcc3024ResponseReceive(devp, &resp);
    if(MSG_OK != ret) {
        return ret;
    }

    if(resp.type != QCC3024_CMD_RESP_ACK) {
        return MSG_RESP_TYPE_ERROR;
    }

    // update the sequence id
    seq_id = (seq_id + 1) & 0x03;

    return MSG_OK;
}

bool isQcc3024Ready(QCC3024Driver *devp) {
    if(devp->state == QCC3024_READY) {
        return TRUE;
    } else {
        // devp->config is set during the initialization
        qcc3024Start(devp, devp->config);
    }
    return (devp->state == QCC3024_READY);
}

void qcc3024ObjectInit(QCC3024Driver *devp) {

    devp->vmt = &vmt_device;

    devp->config = NULL;

    devp->state = QCC3024_STOP;
}

void qcc3024Start(QCC3024Driver *devp, const QCC3024Config *config) {

    osalDbgCheck((devp != NULL) && (config != NULL));
    osalDbgAssert((devp->state == QCC3024_STOP) || (devp->state == QCC3024_READY),
                  "qcc3024Start(), invalid state");
    devp->config = config;

    // test the communication pipe
    if(MSG_OK != qcc3024TestCommPipe(devp)) {
        return;
    }

    devp->state = QCC3024_READY;
}

void qcc3024Stop(QCC3024Driver *devp) {
    osalDbgCheck(devp != NULL);
    osalDbgAssert((devp->state == QCC3024_STOP) || (devp->state == QCC3024_READY),
                  "qcc3024Stop(), invalid state");

    if(devp->state == QCC3024_READY) {
        uartStop(devp->config->uartp);
    }

    devp->state = QCC3024_STOP;
}
/** @} */
