#pragma once

#include <stdint.h>

typedef struct  {
    uint32_t left;
    uint32_t right;
} FRAME2x32;

typedef struct  {
    uint16_t left;
    uint16_t right;
} FRAME2x16;

static inline void rc_frame2x32_downmix(FRAME2x32 *dest,  const FRAME2x32 *src, uint32_t num_frames, uint8_t mix_ratio)
{
    for(uint32_t outf = 0; outf < (num_frames / mix_ratio); outf++) {
        *dest++ = *src;
        src += mix_ratio;
    }
}

static inline void rc_frame2x32_to_2x16_downmix(FRAME2x16 *dest, const FRAME2x32 *src,  uint32_t num_frames, uint8_t mix_ratio)
{
    for(uint32_t inf = 0; inf < num_frames; inf++, src++) {
        for(uint32_t outf = 0; outf < mix_ratio; outf++, dest++) {
            dest->left = (uint16_t)(src->left >> 16);
            dest->right = (uint16_t)(src->right >> 16);
        }
    }
}

static inline void rc_frame2x32_to_4x32_upmix(FRAME2x32 *dest, const FRAME2x32 *src,  uint32_t num_frames, uint8_t mix_ratio)
{
    for(uint32_t inf = 0; inf < num_frames; inf++, src++) {
        for(uint32_t outf = 0; outf < mix_ratio; outf++, dest += 2) {
            *dest = *src;
        }
    }
}

static inline void rc_frame2x16_to_4x32_upmix(FRAME2x32 *dest, const FRAME2x16 *src,  uint32_t num_frames, uint8_t mix_ratio)
{
    for(uint32_t inf = 0; inf < num_frames; inf++, src++) {
        for(uint32_t outf = 0; outf < mix_ratio; outf++, dest += 2) {
            dest->left = (uint32_t)src->left << 16;
            dest->right = (uint32_t)src->right << 16;
        }
    }
}

