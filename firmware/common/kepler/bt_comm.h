/**
 * @file    bt_comm.h
 * @brief   Qualcomm Bluetooth Common header.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */
#ifndef _BT_COMM_H_
#define _BT_COMM_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @brief  Bluetooth address structure.
 */
typedef struct {
    uint32_t lap;
    uint8_t uap;
    uint16_t nap;
} bdaddr_t;

/**
 * @brief  Connection mask
 */
typedef enum {
    conn_hfp_pri  = 1 << 0,
    conn_hfp_sec  = 1 << 1,
    conn_a2dp_pri = 1 << 2,
    conn_a2dp_sec = 1 << 3
} conn_profile_t;

#define conn_hfp  (conn_hfp_pri | conn_hfp_sec)
#define conn_a2dp (conn_a2dp_pri | conn_a2dp_sec)

/**
 * @brief  The call state of a connection. Maps directly onto the call SM.
 */
typedef enum {
    hfp_call_state_idle,
    hfp_call_state_incoming,
    hfp_call_state_incoming_held,
    hfp_call_state_outgoing,
    hfp_call_state_active,
    hfp_call_state_twc_incoming,    /* TWC: Three way call */
    hfp_call_state_twc_outgoing,
    hfp_call_state_held_active,
    hfp_call_state_held_remaining,
    hfp_call_state_multiparty
} hfp_call_state_t;

/**
 * @brief  Link priority is used to identify different links to
 *         AG devices using the order in which the devices were connected.
 */
typedef enum {
    /*! Invalid Link. */
    hfp_invalid_link,
    /*! The link that was connected first. */
    hfp_primary_link,
    /*! The link that was connected second. */
    hfp_secondary_link
} hfp_link_priority_t;

/**
 * @brief  The stream state of a a2dp connection.
 */
typedef enum {
    a2dp_stream_idle,                   /*!< Dormant state.  No actions being performed.  No Media channel present. */
    a2dp_stream_discovering,            /*!< A list of codecs is being requested */
    a2dp_stream_configuring,            /*!< Negotiating a suitable codec and parameters */
    a2dp_stream_configured,             /*!< A codec has been selected and configured */
    a2dp_stream_opening,                /*!< A Media channel is being opened */
    a2dp_stream_open,                   /*!< A Media channel has been established */
    a2dp_stream_starting,               /*!< Preparing to begin audio streaming */
    a2dp_stream_streaming,              /*!< Audio is cuurently being streamed over a Media channel */
    a2dp_stream_suspending,             /*!< Preparing to cease audio streaming */
    a2dp_stream_closing,                /*!< A Media channel is being closed */
    a2dp_stream_reconfiguring,          /*!< Reconfiguring a codec's parameters */
    a2dp_stream_aborting,               /*!< An error condition has occurred */
    a2dp_stream_state_dummy = 0xFFFF,   /*!< A dummy state to make sure the emums size will be at least 16 bits */
} a2dp_stream_state_t;

/**
 * @brief  Index priority is used to identify different links to
 *         AG devices using the order in which the devices were connected.
 */
typedef enum {
    a2dp_primary = 0x00,
    a2dp_secondary = 0x01,
    a2dp_invalid = 0xff
} a2dp_index_t;

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


#endif /* _BT_COMM_H_ */

/** @} */

