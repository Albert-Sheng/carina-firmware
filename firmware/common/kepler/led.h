/**
 * @file    led.h
 * @brief   LED driver header.
 *
 * @addtogroup COMMON
 * @{
 */

#ifndef _LED_H_
#define _LED_H_

#include "hal.h"

#if (HAL_USE_LED == TRUE)
#include "led_cfgs.h"

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @brief   Type of driver state machine states.
 */
typedef enum {
    LED_UNINIT = 0,
    LED_STOP = 1,
    LED_READY = 2,
    LED_ON = 3,
    LED_OFF = 4,
    LED_FADE_IN = 5,
    LED_FADE_OUT = 6
} led_state_t;

/**
 * @brief   Type of a LED configuration structure.
 */
typedef struct {
    PWMDriver                 *pwmdrv;
    PWMConfig                 *pwmcfg;
    pwmchannel_t              channel;
    uint8_t                   brightness;
    led_state_t               state;
} led_cfg_t;

/**
 * @brief   Type of a LED configuration structure.
 */
typedef struct {
    led_cfg_t                 config[LED_NUMBER];
    uint8_t                   brightness_level;
    uint8_t                   fade_in_step;
    uint8_t                   fade_out_step;
} LEDDriver;

/**
 * @brief   Type of a LED Index structure.
 */
typedef enum {
#if LED_NUMBER >= 1
    LED_0 = 0,
#endif
#if LED_NUMBER >= 2
    LED_1,
#endif
#if LED_NUMBER >= 3
    LED_2,
#endif
#if LED_NUMBER >= 4
    LED_3,
#endif
#if LED_NUMBER >= 5
    LED_4,
#endif
#if LED_NUMBER >= 6
    LED_5,
#endif
#if LED_NUMBER >= 7
    LED_6,
#endif
#if LED_NUMBER >= 8
    LED_7,
#endif
#if LED_NUMBER >= 9
    LED_8,
#endif
#if LED_NUMBER >= 10
    LED_9,
#endif
#if LED_NUMBER >= 11
    LED_10,
#endif
#if LED_NUMBER >= 12
    LED_11,
#endif
#if LED_NUMBER >= 13
    LED_12,
#endif
#if LED_NUMBER >= 14
    LED_13,
#endif
#if LED_NUMBER >= 15
    LED_14,
#endif
#if LED_NUMBER >= 16
    LED_15,
#endif
} led_index_t;

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
void ledInit(void);
void ledDeinit(void);
led_state_t ledGetState(led_index_t led);
void ledSetBrightness(led_index_t led, uint8_t brightness);
void ledSetBrightnessI(led_index_t led, uint8_t brightness);
void ledLightOn(led_index_t led);
void ledLightOnI(led_index_t led);
void ledLightOff(led_index_t led);
void ledLightOffI(led_index_t led);
void ledFadeIn(led_index_t led);
void ledFadeOut(led_index_t led);
void ledFadeToBrightness(led_index_t led, uint8_t brightness);
void ledSetBrightnesslevel(uint8_t brightness);
uint8_t ledGetBrightnesslevel(void);
void ledSetFadeInStep(uint8_t step);
uint8_t ledGetFadeInStep(void);
void ledSetFadeOutStep(uint8_t step);
uint8_t ledGetFadeOutStep(void);
void ledRampingProc(uint8_t nleds);
void ledLightAllOn(void);
void ledLightAllOff(void);
void ledFadeInAll(void);
void ledFadeOutAll(void);
#if PHANTOM_PWR_LED_EXIST
void ledPhantomPwrLedOn(void);
void ledPhantomPwrLedOff(void);
#endif
#ifdef __cplusplus
}
#endif

#endif /* HAL_USE_LED == TRUE */

#endif /* _LED_H_ */

/** @} */

