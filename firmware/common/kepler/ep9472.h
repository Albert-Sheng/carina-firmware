
#pragma once


/**
 * @brief   Get the sampling frequency of the HDMI audio input. MCLK frequency
 *          will be 256x.
 *
 * @param[in] i2cp  pointer to the I2C driver instance
 *
 * @retval  Returns sampling frequency (e.g. 48000, 192000, etc.)
 *          or 0 for no detected HDMI audio input.
 */
uint32_t EP9472_GetSampleFrequency(I2CDriver *i2cp);
