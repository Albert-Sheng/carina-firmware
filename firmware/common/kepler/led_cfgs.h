/**
 * @file    led_cfgs.h
 * @brief   LED driver configuration header.
 *
 * @addtogroup board/carina
 * @{
 */

#ifndef _LED_CFGS_H_
#define _LED_CFGS_H_
#include "hal.h"

#if (HAL_USE_LED == TRUE)
#include "hal_pwm.h"

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/
#define LED_NUMBER                                          (15)

#define LED_PWM_CLOCK_FREQUENCY                             (10000)
#define LED_PWM_PERIOD                                      (40)

#define LED_MIN_BRIGHTNESS                                  (0)
#define LED_MID_BRIGHTNESS                                  (50)
#define LED_MAX_BRIGHTNESS                                  (100)
#define LED_FADE_IN_STEP                                    (10)
#define LED_FADE_OUT_STEP                                   (5)

#define LED_FADE_INTERVAL_MS                                (50)
#define LED_RAMP_INTERVAL_MS                                (50)

#define LED_REVERSE_ENABLE                                  (1)

// For PHANTOM_PWR_LED
#define PHANTOM_PWR_LED_EXIST                               (TRUE)
#define PHANTOM_PWR_LED_PORT                                (GPIOC)
#define PHANTOM_PWR_LED_PAD                                 (5)

// For led type
#define LED_TYPE_NORMAL                                     (0)
#define LED_TYPE_PHANTOM                                    (1)

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/
#if LED_NUMBER < 1
#error "There should be at least one LED!"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* HAL_USE_LED == TRUE */

#endif /* _LED_CFGS_H_ */

/** @} */

