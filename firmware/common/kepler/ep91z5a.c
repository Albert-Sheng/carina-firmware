
#include "kepler.h"

#include "ep91z5a.h"


/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define EP91Z5A_I2C_ADDR            0x3C
#define I2C_TIMEOUT_MS              100

#define EP91Z5A_REG_SYS_STATUS_1    0x200
#define EP91Z5A_REG_SYS_STATUS_2    0x201
#define EP91Z5A_REG_AUDIO_STATUS    0x400

/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

/*
 * I2C config for 100KHz clock
 * I2C Clock = 32Mhz / Prescaler of 8 = 4Mhz
 */
static const I2CConfig i2cconfig = {
    STM32_TIMINGR_PRESC(8U)  |
    STM32_TIMINGR_SCLDEL(4U) | STM32_TIMINGR_SDADEL(2U) |
    STM32_TIMINGR_SCLH(15U)  | STM32_TIMINGR_SCLL(19U),
    0,
    0
};


/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

uint32_t EP91Z5A_GetSampleFrequency(I2CDriver *i2cp) {
    if(i2cp == NULL) {
        Dbg_printf(DBG_ERROR, "%s() NULL argument\n", __func__);
        return 0;
    }

    static uint16_t reg __attribute__((section(".ram3")));
    static uint8_t data __attribute__((section(".ram3")));
    msg_t msg;
    uint32_t freq = 0;

    i2cStart(i2cp, &i2cconfig);
    i2cAcquireBus(i2cp);

    do {
        /* Check HDMI signal indicator */
        reg = BSWAP_16(EP91Z5A_REG_SYS_STATUS_1);

        msg = i2cMasterTransmitTimeout(i2cp, EP91Z5A_I2C_ADDR, (uint8_t *)&reg, sizeof(reg), &data, sizeof(data), TIME_MS2I(I2C_TIMEOUT_MS));

        if((msg != MSG_OK) || ((data & (1 << 4)) == 0)) {
            break;
        }

        /* Check LINK_ON and DE_VALID indicators */
        reg = BSWAP_16(EP91Z5A_REG_SYS_STATUS_2);

        msg = i2cMasterTransmitTimeout(i2cp, EP91Z5A_I2C_ADDR, (uint8_t *)&reg, sizeof(reg), &data, sizeof(data), TIME_MS2I(I2C_TIMEOUT_MS));

        if((msg != MSG_OK) || ((data >> 6) != 0x3)) {
            break;
        }

        /* Get Sample Frequency */
        reg = BSWAP_16(EP91Z5A_REG_AUDIO_STATUS);

        msg = i2cMasterTransmitTimeout(i2cp, EP91Z5A_I2C_ADDR, (uint8_t *)&reg, sizeof(reg), &data, sizeof(data), TIME_MS2I(I2C_TIMEOUT_MS));

        if(msg != MSG_OK) {
            break;
        }

        switch(data & 0x7) {
            case 0:
                freq = 32000;
                break;
            case 1:
                freq = 44100;
                break;
            case 2:
                freq = 48000;
                break;
            case 3:
                freq = 88200;
                break;
            case 4:
                freq = 96000;
                break;
            case 5:
                freq = 176400;
                break;
            case 6:
                freq = 192000;
                break;
            case 7:
                freq = 768000;
                break;
        }
    } while(0);


    i2cReleaseBus(i2cp);
    i2cStop(i2cp);

    return freq;
}

