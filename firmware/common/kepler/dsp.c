/**
 * @file    dsp.c
 * @brief   Audio codec code.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */

#include "hal.h"

#if (HAL_USE_DSP == TRUE)

#include "debug.h"
#include "dsp.h"
#include "project.h"
#if DSP_CHIP_ID & CS47L90_CHIP_ID
#include "cs47l90.h"
#endif
#ifdef PROJECT_CARINA
#include "coprocessor.h"
#endif

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

#if DSP_CHIP_ID & CS47L90_CHIP_ID
#if defined(PROJECT_CARINA)
/*
 * CS47L90 SPI Communication Pipe related
 */
static const SPIConfig cs47l90_spi1_cfg = {
    .circular = false,
    .end_cb = NULL,
    .ssport = GPIOC,
    .sspad = 14,
    .cfg1 = SPI_CFG1_MBR_DIV16 | SPI_CFG1_DSIZE_VALUE(8 - 1),  // 8-bit
    .cfg2 = 0,
};

static const CS47L90Config cs47l90cfg = {
    &SPID1,
    &cs47l90_spi1_cfg,
};
#elif defined(PROJECT_BE)
/*
 * CS47L90 SPI Communication Pipe related
 */
static const SPIConfig cs47l90_spi1_cfg = {
    .circular = false,
    .end_cb = NULL,
    .ssport = GPIOC,
    .sspad = 14,
    .cfg1 = SPI_CFG1_MBR_DIV16 | SPI_CFG1_DSIZE_VALUE(8 - 1),  // 8-bit
    .cfg2 = 0,
};

static const CS47L90Config cs47l90cfg = {
    &SPID1,
    &cs47l90_spi1_cfg,
};
#else
#error "No project defined for DSP driver."
#endif
static CS47L90Driver CS47L90;
#endif

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/
/**
 * @brief   Performs a table lookup (useful for creating custom curves
 *          such as for a volume table).
 *
 * @param[in] x         the value to be looked up.
 * @param[in] table     the table to use for conducting the lookup.
 *
 * @return              the corresponding value associated with the lookup value.
 */
static uint16_t dspTableLookup(int16_t x, const dspTable_t *table) {
    const dspTable_t *a = table;
    const dspTable_t *b;
    while(a->x != dspTableEnd.x) {
        if(a->x == x) {
            return a->y;
        }

        b = a + 1;

        if((a->x < x) && (x < b->x)) {
            int16_t dx;
            uint16_t dy;
            int16_t xf;
            uint16_t y;
            if(a->y < b->y) {
                xf = x - a->x;
                dx = b->x - a->x;
                dy = b->y - a->y;
                y = ((dy * (uint16_t)xf) / dx) + a->y;
            } else if(a->y > b->y) {
                xf = x - a->x;
                dx = b->x - a->x;
                dy = a->y - b->y;
                y = a->y - ((dy * (uint16_t)xf) / dx);
            } else {
                return a->y;
            }
            return y;
        }

        a++;
    }

    return a->y;
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
/**
 * @brief   Reads the contents of a register.
 *
 * @param[in] gain      gain to be set
 * @param[in] table     pointer to a dspTable_t variable
 * @param[in] addr      list of addresses of DSP memory where gain will be applied
 *
 * @return              the system flag bytes.
 * @retval DSP_OK       if the function succeeded.
 * @retval DSP_FAIL     if the function failed.
 */
dsp_msg_t dspSetGain(uint16_t gain, const dspTable_t *table, const uint32_t *addr, size_t gain_regs) {
    uint16_t gain_val = dspTableLookup(gain, table);
#if DSP_CHIP_ID & CS47L90_CHIP_ID
    while(gain_regs--) {
        if(cs47l90RegModify(&CS47L90, *addr, gain_val, 0x0000ffff, 0x0000ffff) != CS47L90Msg_Success) {
            return DSP_FAIL;
        }

        // Setting the VU bit is necessary for updating the new gain value. It's always bit 9 in each
        // gain setting register.
        if(cs47l90RegModify(&CS47L90, *addr++, 0x00000200, 0x00000200, 0x00000000) != CS47L90Msg_Success) {
            return DSP_FAIL;
        }
    }
#endif

    return DSP_OK;
}

/**
 * @brief   Reads the contents of a register.
 *
 * @param[in] buf       buffer to store the val read from the DSP register
 * @param[in] addr      address of DSP memory to be read
 * @param[in] len       length of the receive buffer
 *
 * @return              the system flag bytes.
 * @retval DSP_OK       if the function succeeded.
 * @retval DSP_FAIL     if the function failed.
 */
dsp_msg_t dspReadReg(uint8_t *buf, uint32_t addr, size_t len) {
#if DSP_CHIP_ID & CS47L90_CHIP_ID
    if(cs47l90RegRead(&CS47L90, buf, addr, len) == CS47L90Msg_Success) {
        return DSP_OK;
    }

    return DSP_FAIL;
#endif
}

/**
 * @brief   Modify a register in the DSP.
 *
 * @param[in]  addr             the address to write to
 * @param[in]  value            the value to write to the address
 * @param[in]  mask             the bits to mask
 * @param[in]  check            the bits to check after the write
 *
 * @retval DSP_OK               if the function succeeded.
 * @retval DSP_FAIL             if the function failed.
 */
dsp_msg_t dspWriteReg(uint32_t addr, uint32_t value, uint32_t mask, uint32_t check) {
#if DSP_CHIP_ID & CS47L90_CHIP_ID
    if(cs47l90RegModify(&CS47L90, addr, value, mask, check) == CS47L90Msg_Success) {
        return DSP_OK;
    }

    return DSP_FAIL;
#endif
}

/**
 * @brief   Initializes an instance.
 *
 *
 * @init
 */
void dspInit(void) {
#if DSP_CHIP_ID & CS47L90_CHIP_ID
    cs47l90ObjectInit(&CS47L90, &cs47l90cfg);
#endif
}

/**
 * @brief   Configures and activates Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void dspStart(void) {
#if DSP_CHIP_ID & CS47L90_CHIP_ID
    cs47l90Start(&CS47L90);
#endif
}

/**
 * @brief   Deactivates the Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void dspStop(void) {
#if DSP_CHIP_ID & CS47L90_CHIP_ID
    cs47l90Stop(&CS47L90);
#endif
}

/**
 * @brief   Reset the Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void dspReset(void) {
#if defined(PROJECT_CARINA)
    coProcessorEnableCodecReset(TRUE);
    chThdSleepMilliseconds(1);
    coProcessorEnableCodecReset(FALSE);
#elif defined(PROJECT_BE)
    cs47l90SoftReset(&CS47L90);
#else
#error "No DSP Reset occurring."
#endif
}

#endif /* HAL_USE_DSP == TRUE */

/** @} */
