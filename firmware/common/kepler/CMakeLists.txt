target_include_directories(
    ${TARGET}
    INTERFACE
        ${CMAKE_CURRENT_LIST_DIR}
)

target_compile_definitions(
    ${TARGET}
    INTERFACE
        PROJECT_CARINA
)

target_sources(
    ${TARGET}
    INTERFACE
        # These are the minimum required files due to lots of inter-dependencies
        ${CMAKE_CURRENT_LIST_DIR}/chibios_malloc.h
        ${CMAKE_CURRENT_LIST_DIR}/led.c
        ${CMAKE_CURRENT_LIST_DIR}/led.h
        ${CMAKE_CURRENT_LIST_DIR}/pots.c
        ${CMAKE_CURRENT_LIST_DIR}/pots.h
        ${CMAKE_CURRENT_LIST_DIR}/user_config.c
        ${CMAKE_CURRENT_LIST_DIR}/user_config.h
)
