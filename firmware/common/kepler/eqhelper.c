/**
 * @file    eqhelper.c
 * @brief   Methods for reading or writing an EQProfile (to/from the CS4790 CODEC)
 *
 * @addtogroup be
 * @ingroup BE
 * @{
 */
#include "kepler.h"
#include "eqhelper.h"
#include "cs47l90_regdefs.h"
#include "dsp.h"

#define  DB_TO_REG(x, bi)   ((((uint32_t)(x+12)) & 0x1f) << bi)
#define  REG_TO_DB(x, bi)   ((int8_t)((regValue >> bi) & 0x1f)-12)

static EQProfile const  s_defaultEQProfiles[MAX_EQ_PROFILES] = {
    {{0, 0, 0, 0, 0 }}, // Flat
    {{9, 3, 0, 0, 0 }}, // Bass Boost
    {{0, 0, 0, 3, 9 }}, // Treble Boost
    {{-3, 0, 9, 0, -3 }}  // Mid Boost
};
static uint16_t const EQ_REG_LOOKUP[] = {
    CS47L90_EQ1_1,
    CS47L90_EQ2_1,
    CS47L90_EQ3_1,
    CS47L90_EQ4_1
};

EQProfile const *EQGetDefaultProfile(uint8_t index) {
    EQProfile const *profile = NULL;
    if(index < MAX_EQ_PROFILES) {
        profile = &s_defaultEQProfiles[index];
    }
    return profile;
}
/**
 * Helper method to set an EQ Profile by index
 * @param profile      The profile values in a structure
 */
void EQSetProfile(EQProfile const *profile) {

    // Validate the EQ Profile
    if(!profile) {
        return;
    }
    for(int eq = 0; eq < 2; eq++) {
        // Convert from dB to the binary equivalent for the CS47. Values go from 0x00 to 0x18
        // send each value to the EQ on the device
        uint32_t address = EQ_REG_LOOKUP[eq];
        uint32_t regValue = 0x0001; // Enable the EQ profile
        regValue |= DB_TO_REG(profile->bandGains[0], 11u);
        regValue |= DB_TO_REG(profile->bandGains[1], 6u);
        regValue |= DB_TO_REG(profile->bandGains[2], 1u);

        // Write and check the entire register. EQ Profile sets 2 registers for the gains
        bool success = false;
        if(DSP_OK == dspWriteReg(address, regValue, 0xffff, 0xffff)) {
            ++address;
            regValue = DB_TO_REG(profile->bandGains[3], 11u);
            regValue |= DB_TO_REG(profile->bandGains[4], 6u);

            if(DSP_OK == dspWriteReg(address, regValue, 0xffff, 0xffff)) {
                success = true;
            }
        }
        // TODO - add setting the EQ frequency values...
        if(success) {
            Dbg_printf(DBG_INFO, "Set Eq Block %d succeeded", eq);
        } else {
            Dbg_printf(DBG_INFO, "Set Eq Block %d failed", eq);
        }
    }
}
/**
 * Helper to read back an EQ profile by index
 * @param profile      The profile structure to populate
 */
void EQGetProfile(EQProfile *profile) {
    // Validate the object we're populating
    if(!profile) {
        return;
    }
    uint16_t address = EQ_REG_LOOKUP[0];
    uint32_t regValue;
    if(DSP_OK == dspReadReg((uint8_t *)&regValue, address, sizeof(regValue))) {
        profile->bandGains[0] = REG_TO_DB(regValue, 11u);
        profile->bandGains[1] = REG_TO_DB(regValue, 6u);
        profile->bandGains[2] = REG_TO_DB(regValue, 1u);
    }
    ++address;
    if(DSP_OK == dspReadReg((uint8_t *)&regValue, address, sizeof(regValue))) {
        profile->bandGains[3] = REG_TO_DB(regValue, 11u);
        profile->bandGains[4] = REG_TO_DB(regValue, 6u);
    }
}
/**
 * Method to get EQ frequencies
 * @param frequencies structure that holds frequencies
 */
void EQGetFrequencies(EQFrequencyProfile *freqProfile) {
    if(freqProfile) {
        // TBD - these are the defaults for now. They need to be changed
        freqProfile->bandFrequencies[0] = 18;
        freqProfile->bandFrequencies[1] = 50;
        freqProfile->bandFrequencies[2] = 142;
        freqProfile->bandFrequencies[3] = 400;
        freqProfile->bandFrequencies[4] = 1152;
    }
}