#pragma once

#include <stdint.h>

#define POT_FILTER_TAPS 64

uint16_t UI_POT_Filter(uint16_t samples[]);
