/**
 * @file    coprocessor.c
 * @brief   CO-Processor module code.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */

#include "hal.h"
#include "debug.h"
#include "stm32f446.h"
#include "coprocessor.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/
/*
 * STM32F446 I2C Slave related
 */
const I2CConfig i2c2cfg = {
    STM32_TIMINGR_PRESC(15U) |
    STM32_TIMINGR_SCLDEL(4U) | STM32_TIMINGR_SDADEL(2U) |
    STM32_TIMINGR_SCLH(15U)  | STM32_TIMINGR_SCLL(21U),
    0,
    0
};

static const STM32F446Config stm32f446cfg = {
    &I2CD2,
    &i2c2cfg,
    STM32F446_SAD,
};

STM32F446Driver stm32f446;

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/


/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
msg_t coProcessorEnableHDMIPassthrough(bool enable) {
    uint8_t bit_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    bit_val = enable ? (STM32F446_ENABLE_REG_HDMI_EN) : (STM32F446_ENABLE_REG_HDMI_DIS);
    return stm32f446SetEnableRegBit(&stm32f446, STM32F446_ENABLE_REG_HDMI_MASK, bit_val);
}

msg_t coProcessorEnablePhantomPwr(bool enable) {
    uint8_t bit_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    bit_val = enable ? (STM32F446_ENABLE_REG_PHTM_EN) : (STM32F446_ENABLE_REG_PHTM_DIS);
    return stm32f446SetEnableRegBit(&stm32f446, STM32F446_ENABLE_REG_PHTM_MASK, bit_val);
}

msg_t coProcessorEnableCodecReset(bool enable) {
    uint8_t bit_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    bit_val = enable ? (STM32F446_ENABLE_REG_CODC_EN) : (STM32F446_ENABLE_REG_CODC_DIS);
    return stm32f446SetEnableRegBit(&stm32f446, STM32F446_ENABLE_REG_CODC_MASK, bit_val);
}

msg_t coProcessorEnableCodecPower(bool enable) {
    uint8_t bit_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    bit_val = enable ? (STM32F446_ENABLE_REG_CPWR_EN) : (STM32F446_ENABLE_REG_CPWR_DIS);
    return stm32f446SetEnableRegBit(&stm32f446, STM32F446_ENABLE_REG_CPWR_MASK, bit_val);
}

msg_t coProcessorEnablePGA2505Power(bool enable) {
    uint8_t bit_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    bit_val = enable ? (STM32F446_ENABLE_REG_PGAP_EN) : (STM32F446_ENABLE_REG_PGAP_DIS);
    return stm32f446SetEnableRegBit(&stm32f446, STM32F446_ENABLE_REG_PGAP_MASK, bit_val);
}

msg_t coProcessorEnableImperiumReset(bool enable) {
    msg_t ret = MSG_OK;
    uint8_t bit_val, sts_reg_val, sts_bit_val;
    int retries = 20;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    bit_val = enable ? (STM32F446_ENABLE_REG_IMPRST_EN) : (STM32F446_ENABLE_REG_IMPRST_DIS);
    sts_bit_val = enable ? (STM32F446_IMPERIUM_RESET_EN) : (STM32F446_IMPERIUM_RESET_DIS);
    ret = stm32f446SetEnableRegBit(&stm32f446, STM32F446_ENABLE_REG_IMPRST_MASK, bit_val);
    if(MSG_OK != ret) {
        Dbg_printf(DBG_ERROR, "Set Enable Reg Imperium Reset bit fail");
        return ret;
    }

    do {
        chThdSleepMilliseconds(10);
        sts_reg_val = stm32f446GetPerpheryStsReg(&stm32f446);
    } while((retries--) && ((sts_reg_val & STM32F446_IMPERIUM_RESET_MSK) != sts_bit_val));

    if(retries <= 0) {
        Dbg_printf(DBG_ERROR, "Set Enable Reg Imperium Reset bit fail");
        return MSG_TIMEOUT;
    }

    return MSG_OK;
}

msg_t coProcessorSetGameMode(coprocessor_game_mode_t mode) {
    uint8_t game_mode;
    msg_t msg = MSG_OK;
    systime_t start, end;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    msg = stm32f446SetGameModeRegBit(&stm32f446, STM32F446_GAME_MODE_REG_MODE_MASK, mode);
    if(MSG_OK != msg) {
        return msg;
    }

    /* Calculating the time window for the timeout on the busy bus condition.*/
    start = osalOsGetSystemTimeX();
    end = osalTimeAddX(start, OSAL_MS2I(COPROCESSOR_GAME_MODE_BUSY_TIMEOUT));

    /* Waits until MSTS bit is set or, alternatively, for a timeout
       condition.*/
    while(true) {
        game_mode = stm32f446GetGameModeReg(&stm32f446);
        if(0xFF == game_mode) {
            Dbg_printf(DBG_ERROR, "i2c communication fail");
            return MSG_TIMEOUT;
        }
        if((game_mode & STM32F446_GAME_MODE_REG_MSTS) != 0) {
            break;
        }

        /* If the system time went outside the allowed window then a timeout
            condition is returned.*/
        if(!osalTimeIsInRangeX(osalOsGetSystemTimeX(), start, end)) {
            Dbg_printf(DBG_ERROR, "MSTS is always zero, and timeout");
            return MSG_TIMEOUT;
        }
    }

    return MSG_OK;
}

coprocessor_game_mode_t coProcessorGetGameMode(void) {
    uint8_t game_mode;
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return COPROCESSOR_GAME_MODE_UNKNOWN;
    }

    game_mode = stm32f446GetGameModeReg(&stm32f446);
    if(game_mode == 0xFF) {
        Dbg_printf(DBG_ERROR, "i2c communication fail");
        return COPROCESSOR_GAME_MODE_UNKNOWN;
    }

    return (game_mode & STM32F446_GAME_MODE_REG_MODE_MASK);
}

coprocessor_boot_mode_t coProcessorGetBootMode(void) {
    uint8_t boot_mode;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return COPROCESSOR_BOOT_MODE_UNKNOWN;
    }

    boot_mode = stm32f446GetBootModeReg(&stm32f446);
    if(boot_mode > STM32F446_BOOT_MODE_REG_BLD_MODE) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 communication fails");
        return COPROCESSOR_BOOT_MODE_UNKNOWN;
    }

    return (STM32F446_BOOT_MODE_REG_MODE_MASK & boot_mode);
}

msg_t coProcessorEnterBld(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    return stm32f446SetDfuCtrlReg(&stm32f446, STM32F446_DFU_CTRL_REG_BTLD);
}

msg_t coProcessorEraseApp(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    return stm32f446SetDfuCtrlReg(&stm32f446, STM32F446_DFU_CTRL_REG_ERASE);
}

bool isCoProcessorReadyForDfu(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return FALSE;
    }

    return (STM32F446_DFU_CTRL_REG_RDY & stm32f446GetDfuCtrlReg(&stm32f446));
}

msg_t coProcessorStartDFU(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    return stm32f446SetDfuCtrlReg(&stm32f446, STM32F446_DFU_CTRL_REG_START_DFU);
}

msg_t coProcessorEndDFU(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    return stm32f446SetDfuCtrlReg(&stm32f446, STM32F446_DFU_CTRL_REG_END_DFU);
}

coprocessor_dfu_sts_t coProcessorGetDfuStatus(void) {
    uint8_t dfu_ctrl_reg_val;
    coprocessor_dfu_sts_t dfu_sts;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return COPROCESSOR_DFU_STS_UNKNOWN;
    }

    dfu_ctrl_reg_val = stm32f446GetDfuCtrlReg(&stm32f446);
    dfu_sts = (dfu_ctrl_reg_val & STM32F446_DFU_CTRL_REG_DFU_STS_MASK) >> 5;
    return dfu_sts;
}

msg_t coProcessorWriteAppData(const uint8_t *txbuf, uint8_t size) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    return stm32f446SetAppReg(&stm32f446, txbuf, size);
}

uint32_t coProcessorGetChecksum(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return 0;   // return an invalid value if stm32f446 is not ready
    }

    return stm32f446GetChecksumReg(&stm32f446);
}

coprocessor_dfu_reason_t coProcessorGetDfuReason(void) {
    uint8_t dfu_reason_reg_val;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return COPROCESSOR_DFU_UNKNOWN;
    }

    dfu_reason_reg_val = stm32f446GetDfuReasonReg(&stm32f446);
    switch(STM32F446_DFU_REASON_REG_MASK & dfu_reason_reg_val) {
        case STM32F446_DFU_REASON_REG_NAPP:
            return COPROCESSOR_DFU_NAPP;
        case STM32F446_DFU_REASON_REG_FORCE:
            return COPROCESSOR_DFU_FORCE;
        case STM32F446_DFU_REASON_REG_CHKM:
            return COPROCESSOR_DFU_CHKM;
        default:
            return COPROCESSOR_DFU_UNKNOWN;
    }
}

msg_t coProcessorReset(void) {
    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    return stm32f446SetResetRegBit(&stm32f446, STM32F446_RESET_REG_MASK, STM32F446_RESET_REG_RST);
}

bool coProcessorIsPGA2505Pwrd(void) {
    uint8_t perphery_sts;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return FALSE;
    }

    perphery_sts = stm32f446GetPerpheryStsReg(&stm32f446);
    return ((perphery_sts & STM32F446_PGA2505_PWR_MSK) == STM32F446_PGA2505_IS_PWRD);
}

msg_t coProcessorFtyPtnRead(uint32_t addr, uint8_t *buf, uint8_t len) {
    msg_t msg = MSG_OK;
    uint8_t fty_ptn_access_op = 0;
    int retries = 20;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    // set the address and length information before read
    msg = stm32f446SetFtyPtnAccessAddrReg(&stm32f446, addr);
    if(MSG_OK != msg) {
        return msg;
    }
    msg = stm32f446SetFtyPtnAccessLenReg(&stm32f446, len);
    if(MSG_OK != msg) {
        return msg;
    }

    // set read operation and check until the operation is done
    msg = stm32f446SetFtyPtnAccessOpRegBit(&stm32f446, STM32F446_FTY_PTN_ACCESS_OP_MSK, STM32F446_FTY_PTN_ACCESS_OP_RD);
    if(MSG_OK != msg) {
        return msg;
    }

    do {
        chThdSleepMilliseconds(10);
        fty_ptn_access_op = stm32f446GetFtyPtnAccessOpReg(&stm32f446);
        if((fty_ptn_access_op & STM32F446_FTY_PTN_ACCESS_OPSTS_MSK) == STM32F446_FTY_PTN_ACCESS_OPSTS_FAIL) {
            Dbg_printf(DBG_ERROR, "factory partition read operation fail, %x!", fty_ptn_access_op);
            return MSG_TIMEOUT; // We should return failure here.
        }
    } while((retries--) && (fty_ptn_access_op & STM32F446_FTY_PTN_ACCESS_OPSTS_MSK) != STM32F446_FTY_PTN_ACCESS_OPSTS_DONE);

    if(retries <= 0) {
        Dbg_printf(DBG_ERROR, "factory partition access operation is always busy!");
        return MSG_TIMEOUT;
    }

    // read the buffer to get the data we need
    return stm32f446GetFtyPtnAccessBufReg(&stm32f446, buf, len);
}

msg_t coProcessorFtyPtnWrite(uint32_t addr, uint8_t *buf, uint8_t len) {
    msg_t msg = MSG_OK;
    uint8_t fty_ptn_access_op = 0;
    int retries = 20;

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    // set the address and length information before read
    msg = stm32f446SetFtyPtnAccessAddrReg(&stm32f446, addr);
    if(MSG_OK != msg) {
        return msg;
    }
    msg = stm32f446SetFtyPtnAccessLenReg(&stm32f446, len);
    if(MSG_OK != msg) {
        return msg;
    }

    // set the write data to coprocessor buffer
    msg = stm32f446SetFtyPtnAccessBufReg(&stm32f446, buf, len);
    if(MSG_OK != msg) {
        return msg;
    }

    // set write operation and check until the operation is done
    msg = stm32f446SetFtyPtnAccessOpRegBit(&stm32f446, STM32F446_FTY_PTN_ACCESS_OP_MSK, STM32F446_FTY_PTN_ACCESS_OP_WR);
    if(MSG_OK != msg) {
        return msg;
    }

    do {
        chThdSleepMilliseconds(10);
        fty_ptn_access_op = stm32f446GetFtyPtnAccessOpReg(&stm32f446);
        if((fty_ptn_access_op & STM32F446_FTY_PTN_ACCESS_OPSTS_MSK) == STM32F446_FTY_PTN_ACCESS_OPSTS_FAIL) {
            Dbg_printf(DBG_ERROR, "factory partition write operation fail, %x!", fty_ptn_access_op);
            return MSG_TIMEOUT; // We should return failure here.
        }
    } while((retries--) && (fty_ptn_access_op & STM32F446_FTY_PTN_ACCESS_OPSTS_MSK) != STM32F446_FTY_PTN_ACCESS_OPSTS_DONE);

    if(retries <= 0) {
        Dbg_printf(DBG_ERROR, "factory partition access operation is always busy!");
        return MSG_TIMEOUT;
    }

    return MSG_OK;
}

msg_t coProcessorFtyPtnErase(void) {
    msg_t msg = MSG_OK;
    uint8_t fty_ptn_access_op = 0;
    int retries = 500; // give more time to erase the factory partition

    if(FALSE == isStm32f446Ready(&stm32f446)) {
        Dbg_printf(DBG_ERROR, "i2c slave stm32f446 is not ready");
        return MSG_TIMEOUT;   // maybe we need to define its only Error code
    }

    // set erase operation and check until the operation is done
    msg = stm32f446SetFtyPtnAccessOpRegBit(&stm32f446, STM32F446_FTY_PTN_ACCESS_OP_MSK, STM32F446_FTY_PTN_ACCESS_OP_ER);
    if(MSG_OK != msg) {
        return msg;
    }

    do {
        chThdSleepMilliseconds(10);
        fty_ptn_access_op = stm32f446GetFtyPtnAccessOpReg(&stm32f446);
        if((fty_ptn_access_op & STM32F446_FTY_PTN_ACCESS_OPSTS_MSK) == STM32F446_FTY_PTN_ACCESS_OPSTS_FAIL) {
            Dbg_printf(DBG_ERROR, "factory partition erase operation fail, %x!", fty_ptn_access_op);
            return MSG_TIMEOUT; // We should return failure here.
        }
    } while((retries--) && (fty_ptn_access_op & STM32F446_FTY_PTN_ACCESS_OPSTS_MSK) != STM32F446_FTY_PTN_ACCESS_OPSTS_DONE);

    if(retries <= 0) {
        Dbg_printf(DBG_ERROR, "factory partition access operation is always busy!");
        return MSG_TIMEOUT;
    }

    return MSG_OK;
}

void coProcessorInit(void) {
    stm32f446ObjectInit(&stm32f446);
}

void coProcessorStart(void) {
    stm32f446Start(&stm32f446, &stm32f446cfg);
}

void coProcessorStop(void) {
    stm32f446Stop(&stm32f446);
}

/** @} */
