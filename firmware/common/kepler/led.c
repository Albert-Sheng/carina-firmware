/**
 * @file    led.c
 * @brief   LED driver code.
 *
 * @addtogroup COMMON
 * @{
 */
#include "hal.h"

#if (HAL_USE_LED == TRUE)

#include "led.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/



/*===========================================================================*/
/* Driver external variables.                                                */
/*===========================================================================*/
extern LEDDriver leds;


/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/



/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/
static void ledSetState(led_index_t led, led_state_t state) {
    leds.config[led].state = state;
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
led_state_t ledGetState(led_index_t led) {
    return leds.config[led].state;
}

void ledInit(void) {
    uint8_t i;

    for(i = 0; i < LED_NUMBER; i++) {
        pwmStart(leds.config[i].pwmdrv, leds.config[i].pwmcfg);
        pwmEnablePeriodicNotification(leds.config[i].pwmdrv);
        ledSetState(LED_0 + i, LED_READY);
    }
}

void ledDeinit(void) {
    uint8_t i;

    /* Disable all channels first */
    for(i = 0; i < LED_NUMBER; i++) {
        pwmDisableChannel(leds.config[i].pwmdrv, leds.config[i].channel);
        ledSetState(LED_0 + i, LED_STOP);
    }

    /* Stop the PWM */
    for(i = 0; i < LED_NUMBER; i++) {
        pwmStop(leds.config[i].pwmdrv);
        ledSetState(LED_0 + i, LED_UNINIT);
    }
}

void ledSetBrightness(led_index_t led, uint8_t brightness) {
    uint32_t pwm_duty;

    osalDbgCheck((led < LED_NUMBER) && (brightness <= LED_MAX_BRIGHTNESS));
    osalDbgAssert((leds.config[led].pwmdrv->state == PWM_READY),
                  "PWM driver invalid state");
    osalDbgAssert((ledGetState(led) >= LED_READY),
                  "LED driver invalid state");

    pwm_duty = 100 * brightness;
    pwmEnableChannel(leds.config[led].pwmdrv, leds.config[led].channel,
                     PWM_PERCENTAGE_TO_WIDTH(leds.config[led].pwmdrv, pwm_duty));
    leds.config[led].brightness = brightness;
}

void ledSetBrightnessI(led_index_t led, uint8_t brightness) {
    uint32_t pwm_duty;

    osalDbgCheck((led < LED_NUMBER) && (brightness <= LED_MAX_BRIGHTNESS));
    osalDbgAssert((leds.config[led].pwmdrv->state == PWM_READY),
                  "PWM driver invalid state");
    osalDbgAssert((ledGetState(led) >= LED_READY),
                  "LED driver invalid state");

    pwm_duty = 100 * brightness;
    pwmEnableChannelI(leds.config[led].pwmdrv, leds.config[led].channel,
                      PWM_PERCENTAGE_TO_WIDTH(leds.config[led].pwmdrv, pwm_duty));
    leds.config[led].brightness = brightness;
}

uint8_t ledGetBrightness(led_index_t led) {
    osalDbgCheck((led < LED_NUMBER));
    return leds.config[led].brightness;
}

void ledLightOn(led_index_t led) {
    osalDbgCheck((led < LED_NUMBER));
    ledSetBrightness(led, ledGetBrightnesslevel());
    ledSetState(led, LED_ON);
}

void ledLightOff(led_index_t led) {
    osalDbgCheck((led < LED_NUMBER));
    ledSetBrightness(led, LED_MIN_BRIGHTNESS);
    ledSetState(led, LED_OFF);
}

void ledLightOnI(led_index_t led) {
    osalDbgCheck((led < LED_NUMBER));
    ledSetBrightnessI(led, ledGetBrightnesslevel());
    ledSetState(led, LED_ON);
}

void ledLightOffI(led_index_t led) {
    osalDbgCheck((led < LED_NUMBER));
    ledSetBrightnessI(led, LED_MIN_BRIGHTNESS);
    ledSetState(led, LED_OFF);
}

void ledFadeIn(led_index_t led) {
    uint8_t i;
    uint8_t cur_brightness = ledGetBrightness(led);
    uint8_t cur_brightness_level = ledGetBrightnesslevel();
    uint8_t cur_fade_in_step = ledGetFadeInStep();

    osalDbgCheck((led < LED_NUMBER));

    /* change the led state */
    ledSetState(led, LED_FADE_IN);

    /* Tune the brightness step by step */
    if(cur_brightness_level == cur_brightness) {
        return;
    }

    for(i = cur_brightness; i < cur_brightness_level;) {
        if(i < cur_brightness_level - cur_fade_in_step) {
            i += cur_fade_in_step;
        } else {
            i = cur_brightness_level;
        }

        ledSetBrightness(led, i);
        chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
    }
}

void ledFadeOut(led_index_t led) {
    uint8_t i;
    uint8_t cur_brightness = ledGetBrightness(led);
    uint8_t cur_fade_out_step = ledGetFadeOutStep();

    osalDbgCheck((led < LED_NUMBER));

    /* change the led state */
    ledSetState(led, LED_FADE_OUT);

    /* Tune the brightness step by step */
    if(LED_MIN_BRIGHTNESS == cur_brightness) {
        return;
    }

    for(i = cur_brightness; i > LED_MIN_BRIGHTNESS;) {
        if(i > cur_fade_out_step + LED_MIN_BRIGHTNESS) {
            i -= cur_fade_out_step;
        } else {
            i = LED_MIN_BRIGHTNESS;
        }

        ledSetBrightness(led, i);
        chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
    }
}

void ledFadeToBrightness(led_index_t led, uint8_t brightness) {
    uint8_t i;
    uint8_t cur_brightness = ledGetBrightness(led);
    uint8_t cur_brightness_level = ledGetBrightnesslevel();
    uint8_t cur_fade_out_step = ledGetFadeOutStep();
    uint8_t cur_fade_in_step = ledGetFadeInStep();

    osalDbgCheck((led < LED_NUMBER) && (brightness <= cur_brightness_level));

    /* Do not need to tune the brightness */
    if(cur_brightness == brightness) {
        return;
    }

    /* We need to decrease the brightness step by step */
    if(cur_brightness > brightness) {
        /* change the led state */
        ledSetState(led, LED_FADE_OUT);
        for(i = cur_brightness; i > brightness;) {
            if(i > cur_fade_out_step + brightness) {
                i -= cur_fade_out_step;
            } else {
                i = brightness;
            }

            ledSetBrightness(led, i);
            chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
        }
    } else { /* We need to increase the brightness step by step */
        /* change the led state */
        ledSetState(led, LED_FADE_IN);
        for(i = cur_brightness; i < brightness;) {
            if(i < brightness - cur_fade_in_step) {
                i += cur_fade_in_step;
            } else {
                i = brightness;
            }

            ledSetBrightness(led, i);
            chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
        }
    }
}

void ledSetBrightnesslevel(uint8_t brightness) {
    uint8_t i, led;
    uint8_t cur_brightness_level = ledGetBrightnesslevel();
    uint8_t cur_fade_out_step = ledGetFadeOutStep();
    uint8_t cur_fade_in_step = ledGetFadeInStep();

    osalDbgCheck((brightness <= LED_MAX_BRIGHTNESS));
    if(cur_brightness_level == brightness) {
        return;
    }

    leds.brightness_level = brightness;

    /* Update the brightness level of each led */
    if(cur_brightness_level > brightness) { /* We need to decrease the brightness*/
        for(i = cur_brightness_level; i > brightness;) {
            if(i > cur_fade_out_step + brightness) {
                i -= cur_fade_out_step;
            } else {
                i = brightness;
            }

            for(led = 0; led < LED_NUMBER; led++) {
                led_state_t state = ledGetState(LED_0 + led);
                if(state == LED_ON || state == LED_FADE_IN) {
                    ledSetBrightness(LED_0 + led, i);
                    ledSetState(LED_0 + led, state);
                }
            }
            chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
        }
    } else { /* We need to increase the brightness*/
        for(i = cur_brightness_level; i < brightness;) {
            if(i < brightness - cur_fade_in_step) {
                i += cur_fade_in_step;
            } else {
                i = brightness;
            }

            for(led = 0; led < LED_NUMBER; led++) {
                led_state_t state = ledGetState(LED_0 + led);
                if(state == LED_ON || state == LED_FADE_IN) {
                    ledSetBrightness(LED_0 + led, i);
                    ledSetState(LED_0 + led, state);
                }
            }
            chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
        }
    }
}

uint8_t ledGetBrightnesslevel(void) {
    return leds.brightness_level;
}

void ledSetFadeInStep(uint8_t step) {
    leds.fade_in_step = step;
}

uint8_t ledGetFadeInStep(void) {
    return leds.fade_in_step;
}

void ledSetFadeOutStep(uint8_t step) {
    leds.fade_out_step = step;
}

uint8_t ledGetFadeOutStep(void) {
    return leds.fade_out_step;
}

void ledRampingProc(uint8_t nleds) {
    led_index_t i;
    osalDbgCheck((nleds <= LED_NUMBER));

    if(nleds == 0) {
        return;
    }

    /* Ramping Up */
    for(i = LED_0; i < LED_0 + nleds; i++) {
        ledLightOn(i);
        chThdSleepMilliseconds(LED_RAMP_INTERVAL_MS);
    }

    /* Ramping Down */
    for(i = LED_0; i < LED_0 + nleds; i++) {
        ledLightOff(LED_0 + nleds - 1 - i);
        chThdSleepMilliseconds(LED_RAMP_INTERVAL_MS);
    }
}

void ledLightAllOn(void) {
    led_index_t i;

    for(i = LED_0; i < LED_NUMBER; i++) {
        ledLightOn(i);
    }
}

void ledLightAllOff(void) {
    led_index_t i;

    for(i = LED_0; i < LED_NUMBER; i++) {
        ledLightOff(i);
    }
}

void ledFadeInAll(void) {
    uint8_t i;
    led_index_t led_index;
    uint8_t cur_brightness = ledGetBrightness(LED_0);
    uint8_t cur_brightness_level = ledGetBrightnesslevel();
    uint8_t cur_fade_in_step = ledGetFadeInStep();

    /* Tune the brightness step by step */
    if(cur_brightness_level == cur_brightness) {
        return;
    }

    for(i = cur_brightness; i < cur_brightness_level;) {
        if(i < cur_brightness_level - cur_fade_in_step) {
            i += cur_fade_in_step;
        } else {
            i = cur_brightness_level;
        }

        /* Set all of the brightness of all of the LEDs */
        for(led_index = LED_0; led_index < LED_NUMBER; led_index++) {
            ledSetBrightness(led_index, i);
            ledSetState(led_index, LED_FADE_IN);
        }
        chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
    }
}

void ledFadeOutAll(void) {
    uint8_t i;
    led_index_t led_index;
    uint8_t cur_brightness = ledGetBrightness(LED_0);
    uint8_t cur_fade_out_step = ledGetFadeOutStep();

    /* Tune the brightness step by step */
    if(LED_MIN_BRIGHTNESS == cur_brightness) {
        return;
    }

    for(i = cur_brightness; i > LED_MIN_BRIGHTNESS;) {
        if(i > cur_fade_out_step + LED_MIN_BRIGHTNESS) {
            i -= cur_fade_out_step;
        } else {
            i = LED_MIN_BRIGHTNESS;
        }

        /* Set all of the brightness of all of the LEDs */
        for(led_index = LED_0; led_index < LED_NUMBER; led_index++) {
            ledSetBrightness(led_index, i);
            ledSetState(led_index, LED_FADE_OUT);
        }
        chThdSleepMilliseconds(LED_FADE_INTERVAL_MS);
    }
}

#if PHANTOM_PWR_LED_EXIST
void ledPhantomPwrLedOn(void) {
    palSetPad(PHANTOM_PWR_LED_PORT, PHANTOM_PWR_LED_PAD);
}

void ledPhantomPwrLedOff(void) {
    palClearPad(PHANTOM_PWR_LED_PORT, PHANTOM_PWR_LED_PAD);
}
#endif

#endif /* HAL_USE_LED == TRUE */
/** @} */
