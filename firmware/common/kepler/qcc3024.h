/**
 * @file    qcc3024.h
 * @brief   QCC3024 Bluetooth Wireless Audio Processor header.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */
#ifndef _QCC3024_H_
#define _QCC3024_H_

#include "bt_comm.h"

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/



/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @name    Configuration options
 * @{
 */
/**
 * @brief   QCC3024 I2C interface switch.
 * @details If set to @p TRUE the support for I2C is included.
 * @note    The default is @p TRUE.
 */
#if !defined(QCC3024_USE_I2C) || defined(__DOXYGEN__)
#define QCC3024_USE_I2C                     FALSE
#endif

/**
 * @brief   QCC3024 shared I2C switch.
 * @details If set to @p TRUE the device acquires I2C bus ownership
 *          on each transaction.
 * @note    The default is @p FALSE. Requires I2C_USE_MUTUAL_EXCLUSION.
 */
#if !defined(QCC3024_SHARED_I2C) || defined(__DOXYGEN__)
#define QCC3024_SHARED_I2C                  FALSE
#endif

/**
 * @brief   QCC3024 Uart interface switch.
 * @details If set to @p TRUE the support for Uart is included.
 * @note    The default is @p TRUE.
 */
#if !defined(QCC3024_USE_UART) || defined(__DOXYGEN__)
#define QCC3024_USE_UART                     TRUE
#endif

/**
 * @brief   QCC3024 shared UART switch.
 * @details If set to @p TRUE the device acquires UART bus ownership
 *          on each transaction.
 * @note    The default is @p FALSE. Requires UART_USE_MUTUAL_EXCLUSION.
 */
#if !defined(QCC3024_SHARED_UART) || defined(__DOXYGEN__)
#define QCC3024_SHARED_UART                  TRUE
#endif

/**
 * @brief   QCC3024 advanced configurations switch.
 * @details If set to @p TRUE more configurations are available.
 * @note    The default is @p FALSE.
 */
#if !defined(QCC3024_USE_ADVANCED) || defined(__DOXYGEN__)
#define QCC3024_USE_ADVANCED                FALSE
#endif
/** @} */

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/
#if QCC3024_USE_I2C && !HAL_USE_I2C
#error "QCC3024_USE_I2C requires HAL_USE_I2C"
#endif

#if QCC3024_SHARED_I2C && !I2C_USE_MUTUAL_EXCLUSION
#error "QCC3024_SHARED_I2C requires I2C_USE_MUTUAL_EXCLUSION"
#endif

#if QCC3024_USE_UART && !HAL_USE_UART
#error "QCC3024_USE_UART requires HAL_USE_UART"
#endif

#if QCC3024_SHARED_UART && !UART_USE_MUTUAL_EXCLUSION
#error "QCC3024_SHARED_UART requires UART_USE_MUTUAL_EXCLUSION"
#endif

#if QCC3024_USE_I2C && QCC3024_USE_UART
#error "Currently I2C and UART interfaces can not be supported at the same time"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/**
 * @name    QCC3024 data structures and types.
 * @{
 */
/**
 * @brief   Structure representing a QCC3024 driver.
 */
typedef struct QCC3024Driver QCC3024Driver;

/**
 * @brief  QCC3024 slave address
 */
typedef enum {
    /*TODO: since I2C interface is not used, we configure a
            invalid value here, it is determined by QCC3024*/
    QCC3024_SAD = 0x00,               /**< Slave Address   */
} qcc3024_sad_t;

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
    QCC3024_UNINIT = 0,               /**< Not initialized.                */
    QCC3024_STOP = 1,                 /**< Stopped.                        */
    QCC3024_READY = 2,                /**< Ready.                          */
} qcc3024_state_t;

/**
 * @brief   QCC3024 configuration structure.
 */
typedef struct {
#if QCC3024_USE_I2C || defined(__DOXYGEN__)
    /**
     * @brief I2C driver associated to QCC3024.
     */
    I2CDriver                   *i2cp;
    /**
     * @brief I2C configuration associated to QCC3024.
     */
    const I2CConfig             *i2ccfg;
    /**
     * @brief QCC3024 slave address
     */
    qcc3024_sad_t             slaveaddress;
#endif /* QCC3024_USE_I2C */

#if QCC3024_USE_UART || defined(__DOXYGEN__)
    /**
     * @brief UART driver associated to QCC3024.
     */
    UARTDriver                   *uartp;
    /**
     * @brief UART configuration associated to QCC3024.
     */
    const UARTConfig             *uartcfg;
#endif /* QCC3024_USE_UART */
} QCC3024Config;

/**
 * @brief   @p QCC3024 specific methods.
 * @note    No methods so far, just a common ancestor interface.
 */
#define _qcc3024_methods_alone

/**
 * @brief @p QCC3024 specific methods with inherited ones.
 */
#define _qcc3024_methods                                                      \
    _base_object_methods                                                      \
    _qcc3024_methods_alone

/**
 * @extends BaseObjectVMT
 *
 * @brief @p QCC3024 virtual methods table.
 */
struct QCC3024VMT {
    _qcc3024_methods
};

/**
 * @brief   @p QCC3024Driver specific data.
 */
#define _qcc3024_data                                                         \
    /* Driver state.*/                                                        \
    qcc3024_state_t           state;                                          \
    /* Current configuration data.*/                                          \
    const QCC3024Config       *config;

/**
 * @brief   QCC3024 class.
 */
struct QCC3024Driver {
    /** @brief Virtual Methods Table.*/
    const struct QCC3024VMT   *vmt;
    _qcc3024_data
};
/** @} */

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief   Set Bluetooth enter pairing mode.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024EnterPairing(QCC3024Driver *devp);

/**
 * @brief   Cancel Bluetooth pairing mode.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024CancelPairing(QCC3024Driver *devp);

/**
 * @brief   Get Bluetooth bd Addr.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The bd address of QCC3024, {0xFFFFFFFF, 0xFF, 0xFFFF} when failed.
 *
 * @api
 */
bdaddr_t qcc3024GetBdAddr(QCC3024Driver *devp);

/**
 * @brief   Set Bluetooth bd Addr.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] devp      the bd address to set
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SetBdAddr(QCC3024Driver *devp, bdaddr_t bdaddr);

/**
 * @brief   Get Bluetooth all connected profiles.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              All connected profiles of QCC3024, 0x00 when failed.
 *
 * @api
 */
conn_profile_t qcc3024GetProfilesConnected(QCC3024Driver *devp);

/**
 * @brief   Get Bluetooth current HFP call state.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The current call state.
 *
 * @api
 */
hfp_call_state_t qcc3024GetHFPCallState(QCC3024Driver *devp);

/**
 * @brief   Get Bluetooth current a2dp stream state.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The current stream state.
 *
 * @api
 */
a2dp_stream_state_t qcc3024GetA2DPStreamState(QCC3024Driver *devp);

/**
 * @brief   Set Bluetooth TX power.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] txpwr     the tx power to set. Power in dBm corresponding to an
 *                      entry in the BT radio power table which should be used
 *                      for subsequent tests.
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SetTxPwr(QCC3024Driver *devp, uint16_t txpwr);

/**
 * @brief   Start Bluetooth continuous TX mode.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] lo_freq   Transmission frequency in MHz.
 * @param[in] level     The transmit level to use.
 *                      Bluecore:
 *                       The lower 8 bits are the internal gain.
 *                       The upper 8 bits are the external gain.
 *                      CDA devices:
 *                       "level" parameter should regarded as a 16-bit word of four nybbles 0xabcd
 *                       "a" is used as attenuation, "b" is used as mag, the bottom two bits of "c"
 *                       as exp and the top two bits must be 0, "d" must be 0.
 *                       Attenuation is an analogue component and corresponds to the number
 *                       of "segments" switched off; 0 switches off none and produces most power;
 *                       15 switches off all and produces no power.
 *                       Magnitude (mag; signed, range 7 to -8) and exponent (exp; unsigned, range
 *                       0 to 3) act together digitally to produce power proportional to
 *                       (1 + Mag / 16) / power(2, exp).
 *                       For example a 10dBm power table entry of {{0,4,0,0,0}, {0,3,0,0,0}, 10}
 *                       corresponds to level parameter values of 0x0400 for basic rate packets
 *                       and 0x0300 for EDR packets.
 *                      See document 80-CF994-1 for more details.
 * @param[in] mod_freq  Modulation offset. 4096 = 1MHz.
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024StartContTx(QCC3024Driver *devp, uint16_t lo_freq,
                         uint16_t level, uint16_t mod_freq);

/**
 * @brief   Pause Bluetooth radio test.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024PauseRadioTest(QCC3024Driver *devp);

/**
 * @brief   Send upgrade command.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] data      the pointer of the command data to send
 * @param[in] len       the len of command data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SendUpgradeCmd(QCC3024Driver *devp,  uint8_t *data, uint8_t len);

/**
 * @brief   Send upgrade data.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] data      the pointer of the data to send
 * @param[in] len       the len of data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SendUpgradeData(QCC3024Driver *devp,  uint8_t *data, uint8_t len);

/**
 * @brief   Receive upgrade response.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[out] data     the pointer of the buffer for the received response data
 * @param[out] len      the pointer of the len of received response data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024ReceiveUpgradeResp(QCC3024Driver *devp,  uint8_t *data, uint8_t *len);

/**
 * @brief   Clear the upgrade response.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024ClearUpgradeResp(QCC3024Driver *devp);

/**
 * @brief   Clear the Paired Device List.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024ClearPDL(QCC3024Driver *devp);

/**
 * @brief   Set the device under test mode.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SetDUT(QCC3024Driver *devp);

/**
 * @brief   Get Max Trusted devices.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The number of the trusted devices.
 *
 * @api
 */
uint16_t qcc3024GetMaxTrustedDevice(QCC3024Driver *devp);

/**
 * @brief   Get indexed TDL device.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] index     the index of the trusted device in TDL
 *
 * @return              The bd address of the trusted device, {0xFFFFFFFF, 0xFF, 0xFFFF} when failed.
 *
 * @api
 */
bdaddr_t qcc3024GetIndexedTdlDevice(QCC3024Driver *devp, uint16_t index);

/**
 * @brief   Start BLE bonding.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024StartBleBonding(QCC3024Driver *devp);

/**
 * @brief   Read the Centpp Server data.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[out] buf      the buffer to store the Centpp Server data read
 * @param[out] length   the length of the Centpp Server data read
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024ReadCentppServerData(QCC3024Driver *devp,
                                  uint8_t *buf,
                                  uint16_t *length);

/**
 * @brief   Write the Centpp Server data.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] buf       the buffer which stores the Centpp Server data to write
 * @param[in] length    the length of the Centpp Server data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024WriteCentppServerData(QCC3024Driver *devp,
                                   uint8_t *buf,
                                   uint16_t length);

/**
 * @brief   Send the Centpp Server Read Notification to QCC3024.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] buf       the buffer which stores the Centpp Server Read Notification
 * @param[in] length    the length of the Centpp Server Read Notification
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SendCentppServerReadNotification(QCC3024Driver *devp,
                                              uint8_t *buf,
                                              uint16_t length);

/**
 * @brief   Send the Centpp Server Write Notification to QCC3024.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 * @param[in] buf       the buffer which stores the Centpp Server Write Notification
 * @param[in] length    the length of the Centpp Server Write Notification
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qcc3024SendCentppServerWriteNotification(QCC3024Driver *devp,
                                               uint8_t *buf,
                                               uint16_t length);

/**
 * @brief   Check if the instance is ready.
 *
 * @param[in] devp      pointer to the @p QCC3024Driver object
 *
 * @return              TRUE: instance is ready, FALSE: instance is not ready.
 */
bool isQcc3024Ready(QCC3024Driver *devp);

/**
 * @brief   Initializes an instance.
 *
 * @param[out] devp     pointer to the @p QCC3024Driver object
 *
 * @init
 */
void qcc3024ObjectInit(QCC3024Driver *devp);

/**
 * @brief   Configures and activates QCC3024 Complex Driver peripheral.
 *
 * @param[in,out] devp  pointer to the @p QCC3024Driver object
 * @param[in] config    pointer to the @p QCC3024Config object
 *
 * @api
 */
void qcc3024Start(QCC3024Driver *devp, const QCC3024Config *config);

/**
 * @brief   Deactivates the QCC3024 Complex Driver peripheral.
 *
 * @param[in,out] devp       pointer to the @p QCC3024Driver object
 *
 * @api
 */
void qcc3024Stop(QCC3024Driver *devp);
#ifdef __cplusplus
}
#endif

#endif /* _QCC3024_H_ */

/** @} */

