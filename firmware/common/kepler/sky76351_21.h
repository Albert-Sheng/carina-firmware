/**
 * @file    sky76351_21.h
 * @brief   SKY76351-21 Wireless Audio Processor header.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */
#ifndef _SKY76351_21_H_
#define _SKY76351_21_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/
/**
 * @brief   The module identifier of SKY7635121
 * @details List all of the modules used in AIS commands
 * @note
 */
#define SKY7635121_MEMORY_MODULE                                        (0x01)
#define SKY7635121_REGISTER_MODULE                                      (0x02)
#define SKY7635121_SPIM_MODULE                                          (0x04)
#define SKY7635121_FILE_MODULE                                          (0x05)
#define SKY7635121_FLASH_MODULE                                         (0x06)
#define SKY7635121_TWIM_MODULE                                          (0x07)
#define SKY7635121_USB_MODULE                                           (0x08)
#define SKY7635121_JTAG_MODULE                                          (0x09)
#define SKY7635121_LOG_MODULE                                           (0x0A)
#define SKY7635121_UART_MODULE                                          (0x0B)
#define SKY7635121_SPIS_MODULE                                          (0x0C)
#define SKY7635121_TWIS_MODULE                                          (0x0D)
#define SKY7635121_AUDIO_MODULE                                         (0x0E)
#define SKY7635121_DVM_MODULE                                           (0x11)
#define SKY7635121_NVM_MODULE                                           (0x12)
#define SKY7635121_UPGRADE_MODULE                                       (0x14)
#define SKY7635121_SYSPOWER_MODULE                                      (0x16)
#define SKY7635121_MAC_MODULE                                           (0x1A)
#define SKY7635121_MSGROUTE_MODULE                                      (0x1B)
#define SKY7635121_SYSCFG_MODULE                                        (0x1C)
#define SKY7635121_APPLICATION_MODULE                                   (0x40)
#define SKY7635121_AVBOOT_MODULE                                        (0x41)
#define SKY7635121_HOST_MODULE                                          (0x42)

/**
 * @brief   The parameter identifier of SKY7635121 AIS commands
 * @details List all of the parameters used in AIS commands
 * @note
 */
// flash parameter definition
#define CmdParameter_FLASH_OP_STATUS                                    (0x01)
#define CmdParameter_FLASH_SET_ERASE_SIZE                               (0x03)
#define CmdParameter_FLASH_SET_ERASE_ADDR                               (0x04)
#define CmdParameter_FLASH_SET_ERASE_4KCTRL                             (0x05)

// mac parameter definition
#define CmdParameter_MAC_CLEAR_STATS                                    (0x19)
#define CmdParameter_MAC_START_STATS                                    (0x1A)
#define CmdParameter_MAC_FIXED_FREQ_SELECT                              (0x1F)
#define CmdParameter_MAC_CONT_TX_EN                                     (0x20)
#define CmdParameter_MAC_ANTENNA_SELECT                                 (0x21)
#define CmdParameter_MAC_TX_POWER                                       (0x22)
#define CmdParameter_MAC_CHANNEL_SELECT                                 (0x23)
#define CmdParameter_MAC_MODULATION_MODE                                (0x24)
#define CmdParameter_MAC_CARRIER_SELECT                                 (0x25)
#define CmdParameter_MAC_CONT_RX_EN                                     (0x26)
#define CmdParameter_MAC_TEMP_PAIRING                                   (0x28)
#define CmdParameter_MAC_OPEN_PAIRING                                   (0x2A)
#define CmdParameter_MAC_CANCEL_PAIRING                                 (0x2B)
#define CmdParameter_MAC_GET_LINK_STS                                   (0x2D)
#define CmdParameter_MAC_ARBITER_ID                                     (0x2E)
#define CmdParameter_MAC_CLIENT_ID                                      (0x2F)
#define CmdParameter_MAC_SAVE_VM                                        (0x30)

// avboot parameter definition
#define CmdParameter_AVBOOT_AUTH_STS                                    (0x72)
#define CmdParameter_AVBOOT_APP_INFO                                    (0x7B)
#define CmdParameter_AVBOOT_RESET                                       (0x7F)

/**
 * @brief   The module address identifier of SKY7635121 AIS commands
 * @details List all of the address identifier used in AIS module
 * @note
 */
// MAC Module definition
#define MAC_MODULE_STATS_ADDR                                           (0)

// AVBoot Module definition
#define AVBOOT_MODULE_SERIAL_NUM_ADDR                                   (0x1B114200)
#define AVBOOT_MODULE_CHALLENGE_PHRASE_ADDR                             (0x73000000)
#define AVBOOT_MODULE_AUTH_RESPONSE_ADDR                                (0x74000000)

#define AVBOOT_MODULE_IMAGE_INFO_PERSISTENCE_ADDR                       (0x79100000)
#define AVBOOT_MODULE_IMAGE_INFO_PARAM_SET0_ADDR                        (0x79010000)
#define AVBOOT_MODULE_IMAGE_INFO_FW_SET0_ADDR                           (0x79020000)
#define AVBOOT_MODULE_IMAGE_INFO_DATA_SET0_ADDR                         (0x79030000)
#define AVBOOT_MODULE_IMAGE_INFO_PARAM_SET1_ADDR                        (0x79110000)
#define AVBOOT_MODULE_IMAGE_INFO_FW_SET1_ADDR                           (0x79120000)
#define AVBOOT_MODULE_IMAGE_INFO_DATA_SET1_ADDR                         (0x79130000)

/**
 * @brief   Misc definition of SKY7635121 AVBoot Module part
 * @details
 * @note
 */
#define AVBOOT_MODULE_SERIAL_NUM_LEN                                    (0x40)
#define AVBOOT_MODULE_CHALLENGE_PHRASE_LEN                              (0x10)
#define AVBOOT_MODULE_AUTH_RESPONSE_LEN                                 (0x10)

/**
 * @brief   The definition of SKY7635121 RF part
 * @details
 * @note
 */
#define SKY7635121_RF_CHANNEL_MIN                                       (0x00)
#define SKY7635121_RF_CHANNEL_MAX                                       (0x27)

#define SKY7635121_RF_TXPWR_MIN                                         (0x00)
#define SKY7635121_RF_TXPWR_MAX                                         (0x3F)

#define MAC_LINK_CONTEXTS                                               (8)

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @name    Configuration options
 * @{
 */
/**
 * @brief   SKY7635121 I2C interface switch.
 * @details If set to @p TRUE the support for I2C is included.
 * @note    The default is @p TRUE.
 */
#if !defined(SKY7635121_USE_I2C) || defined(__DOXYGEN__)
#define SKY7635121_USE_I2C                     FALSE
#endif

/**
 * @brief   SKY7635121 shared I2C switch.
 * @details If set to @p TRUE the device acquires I2C bus ownership
 *          on each transaction.
 * @note    The default is @p FALSE. Requires I2C_USE_MUTUAL_EXCLUSION.
 */
#if !defined(SKY7635121_SHARED_I2C) || defined(__DOXYGEN__)
#define SKY7635121_SHARED_I2C                  FALSE
#endif

/**
 * @brief   SKY7635121 SPI interface switch.
 * @details If set to @p TRUE the support for SPI is included.
 * @note    The default is @p TRUE.
 */
#if !defined(SKY7635121_USE_SPI) || defined(__DOXYGEN__)
#define SKY7635121_USE_SPI                     TRUE
#endif

/**
 * @brief   SKY7635121 shared SPI switch.
 * @details If set to @p TRUE the device acquires SPI bus ownership
 *          on each transaction.
 * @note    The default is @p FALSE. Requires SPI_USE_MUTUAL_EXCLUSION.
 */
#if !defined(SKY7635121_SHARED_SPI) || defined(__DOXYGEN__)
#define SKY7635121_SHARED_SPI                  TRUE
#endif

/**
 * @brief   SKY7635121 advanced configurations switch.
 * @details If set to @p TRUE more configurations are available.
 * @note    The default is @p FALSE.
 */
#if !defined(SKY7635121_USE_ADVANCED) || defined(__DOXYGEN__)
#define SKY7635121_USE_ADVANCED                FALSE
#endif
/** @} */

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/
#if SKY7635121_USE_I2C && !HAL_USE_I2C
#error "SKY7635121_USE_I2C requires HAL_USE_I2C"
#endif

#if SKY7635121_SHARED_I2C && !I2C_USE_MUTUAL_EXCLUSION
#error "SKY7635121_SHARED_I2C requires I2C_USE_MUTUAL_EXCLUSION"
#endif

#if SKY7635121_USE_SPI && !HAL_USE_SPI
#error "SKY7635121_USE_SPI requires HAL_USE_SPI"
#endif

#if SKY7635121_SHARED_SPI && !SPI_USE_MUTUAL_EXCLUSION
#error "SKY7635121_SHARED_SPI requires SPI_USE_MUTUAL_EXCLUSION"
#endif

#if SKY7635121_USE_I2C && SKY7635121_USE_SPI
#error "Currently I2C and SPI interfaces can not be supported at the same time"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @name    AIS Command data structures and types
 * @{
 */
/**
 * @brief   AIS Message Error Message.
 */
typedef enum {
    AISErrMsg_Success = 0,              // Indicates a successful message execution.
    AISErrMsg_Failed = 1,               // Indicates a general operation failure.
    AISErrMsg_Pending = 2,              // Indicates that the operation is still pending.
    AISErrMsg_NotFound = 3,             // The requested destination module was not found.
    AISErrMsg_NotSupported = 4,         // The requested operation is not supported by the destination module.
    AISErrMsg_ExceededLimits = 5,       // The requested operation exceeded the limits.
    AISErrMsg_NotReady = 6,             // The destination module is not ready.
    AISErrMsg_CommFailure = 7,          // A comms driver has detected a failure.
    AISErrMsg_InvalidFile = 8,          // The file format doesn't match the appropriate settings.
    AISErrMsg_NoCal = 9,                // The chip calibration has not been applied.
    AISErrMsg_OS_Error = 10,            // An OS (MQX) API call failed.
    AISErrMsg_Unauthorized = 11,        // The requested operation has not been authorized.
    AISErrMsg_Busy = 12,                // The requested operation failed because the destination module is busy.
} ais_msg_t;
/** @} */

/**
 * @name    SKY7635121 chiplevel data structures and types
 * @{
 */
/**
 * @brief   SKY7635121 MAC statistics related structure.
 */
typedef union u32_u8_t { /* Reduces mem & cycles of ID parsing code */
    uint32_t as_u32;
    int32_t as_s32;
    uint16_t by_u16[2];
    uint8_t  by_u8[4];
} u_u32_u8;

typedef struct {
    uint16_t resyncSlot;
    uint16_t herr;
    uint16_t pec;
    uint16_t sec;
} MacStatPerSlot;///< MAC statistics block structure.

#pragma pack(push,1)
typedef struct sky7635121_macstats {
    /// Offset      ; Comment
    uint32_t collect;       ///< 0          ; TRUE = collect stats, FALSE = halt so stats can be retrieved

    uint16_t hops;          ///< 4          ; number of hops
    uint16_t resyncs;       ///< 6          ; number of resyncs

    //Size 64
    MacStatPerSlot rxPeer[MAC_LINK_CONTEXTS]; ///< Per rxPeer
    ///< <BR> 8     ; uint16_t resyncSlot;
    ///< <BR> 10    ; uint16_t herr;
    ///< <BR> 12    ; uint16_t pec;
    ///< <BR> 14    ; uint16_t sec;

    uint16_t frmcnt;        ///< 72         ; transaction count

    uint16_t blerAll;       ///< 74         ; all blocks in transit are errored
    uint16_t bler75;        ///< 76         ; >75, <100 blocks are errored
    uint16_t blerLast2All;  ///< 78         ; all blocks in last two transactions are errored
    uint16_t blers;         ///< 80         ; cumulative BLER
    u_u32_u8 blocks;        ///< 82         ; blocks
} sky7635121_macstats_t; ///< MAC statistics block structure.
#pragma pack(pop)

/**
 * @brief   SKY7635121 link state.
 */
typedef enum {
    MacCtxtState_Invalid = 0,                   // 0: Link is invalid.
    MacCtxtState_Active = 1,                    // 1: Link is active.
    MacCtxtState_Pair = 2,                      // 2: Pairing.
    MacCtxtState_Search = 3,                    // 3: Searching.
    MacCtxtState_Standby = 4,                   // 4: Standby.
    MacCtxtState_ReSync = 5,                    // 5: Re-sync in progress.
    MacCtxtState_IScan = 6,                     // 6: Initial (WLAN) scan.
    MacCtxtState_TestMode = 7,                  // 7: Link in test data only mode.
    MacCtxtState_PairCancel = 8,                // 8: Pair is being canceled.
    MacCtxtState_ActiveSearch = 9,              // 9: Arbiter is active and searching in spare slot.
    MacCtxtState_Reserved = 10,                 // 10: Was IQ Imbalance calibration state.
    MacCtxtState_ActivePair = 11,               // 11: Arbiter is active and pairing in spare slot.
} sky7635121_linkstate_t;

/**
 * @brief   SKY7635121 modulation mode.
 */
typedef enum {
    Modulation_Mode_Invalid = 0,
    Modulation_Mode_Random = 1,
    Modulation_Mode_Unmodulated = 2,
    Modulation_Mode_Pattern = 3,
} sky7635121_modulationmode_t;

/**
 * @brief   SKY7635121 Carrier.
 */
typedef enum {
    Carrier_Invalid = 0,
    Carrier_SingleSide = 1,
    Carrier_DualSide = 2,
} sky7635121_carrier_t;

/**
 * @brief   SKY7635121 Antenna.
 */
typedef enum {
    Antenna_Diversity = 0,
    Antenna_Antenna0 = 1,
    Antenna_Antenna1 = 2,
} sky7635121_antenna_t;


/** @} */

/**
 * @name    SKY7635121 data structures and types.
 * @{
 */
/**
 * @brief   Structure representing a SKY7635121 driver.
 */
typedef struct SKY7635121Driver SKY7635121Driver;

/**
 * @brief  SKY7635121 slave address
 */
typedef enum {
    /*TODO: since I2C interface is not used, we configure a
            invalid value here, it is determined by SKY7635121*/
    SKY7635121_SAD = 0x00,               /**< Slave Address   */
} sky7635121_sad_t;

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
    SKY7635121_UNINIT = 0,               /**< Not initialized.                */
    SKY7635121_STOP = 1,                 /**< Stopped.                        */
    SKY7635121_READY = 2,                /**< Ready.                          */
} sky7635121_state_t;

/**
 * @brief   SKY7635121 configuration structure.
 */
typedef struct {
#if SKY7635121_USE_I2C || defined(__DOXYGEN__)
    /**
     * @brief I2C driver associated to SKY7635121.
     */
    I2CDriver                   *i2cp;
    /**
     * @brief I2C configuration associated to SKY7635121.
     */
    const I2CConfig             *i2ccfg;
    /**
     * @brief SKY7635121 slave address
     */
    sky7635121_sad_t             slaveaddress;
#endif /* SKY7635121_USE_I2C */

#if SKY7635121_USE_SPI || defined(__DOXYGEN__)
    /**
     * @brief SPI driver associated to SKY7635121.
     */
    SPIDriver                   *spip;
    /**
     * @brief SPI configuration associated to SKY7635121.
     */
    const SPIConfig             *spicfg;
#endif /* SKY7635121_USE_SPI */
} SKY7635121Config;

/**
 * @brief   @p SKY7635121 specific methods.
 * @note    No methods so far, just a common ancestor interface.
 */
#define _sky7635121_methods_alone

/**
 * @brief @p SKY7635121 specific methods with inherited ones.
 */
#define _sky7635121_methods                                                   \
    _base_object_methods                                                      \
    _sky7635121_methods_alone

/**
 * @extends BaseObjectVMT
 *
 * @brief @p SKY7635121 virtual methods table.
 */
struct SKY7635121VMT {
    _sky7635121_methods
};

/**
 * @brief   @p SKY7635121Driver specific data.
 */
#define _sky7635121_data                                                      \
    /* Driver state.*/                                                        \
    sky7635121_state_t           state;                                       \
    /* Current configuration data.*/                                          \
    const SKY7635121Config       *config;

/**
 * @brief   SKY7635121 class.
 */
struct SKY7635121Driver {
    /** @brief Virtual Methods Table.*/
    const struct SKY7635121VMT   *vmt;
    _sky7635121_data
};
/** @} */

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief   Read sky7635121 memory data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the memory address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadMem(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   write data to sky7635121 memory.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space storing the data to write
 * @param[in]  addr             the memory address to write
 * @param[in]  len              the size of data to write
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121WriteMem(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   Read sky7635121 flash data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the flash address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadFlash(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   write data to sky7635121 flash.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space storing the data to write
 * @param[in]  addr             the flash address to write
 * @param[in]  len              the size of data to write
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121WriteFlash(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   set sky7635121 parameter.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[in]  moduleID         the module ID to set the parameter
 * @param[in]  paramID          the Parameter ID to set
 * @param[in]  paramVal         the value of the parameter to set
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121SetParameter(SKY7635121Driver *devp, uint8_t moduleID,
                                 uint32_t paramID, uint32_t paramVal);

/**
 * @brief   get sky7635121 parameter.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[in]  moduleID         the module ID to get the parameter
 * @param[in]  paramID          the Parameter ID to get
 * @param[out] paramVal         pointer the buffer to store the value of the parameter
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121GetParameter(SKY7635121Driver *devp, uint8_t moduleID,
                                 uint32_t paramID, uint32_t *paramVal);

/**
 * @brief   Read sky7635121 mac data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the mac address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadMac(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   Read sky7635121 AVBoot data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the avboot address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadAvBoot(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   write data to sky7635121 avboot.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space storing the data to write
 * @param[in]  addr             the avboot address to write
 * @param[in]  len              the size of data to write
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121WriteAvBoot(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len);

/**
 * @brief   Authentication Procedure of sky76351-21 chipset.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 *
 * @return                      TRUE: Authentication Procedure Succeed, FALSE: Authentication Procedure Fail.
 */
bool sky7635121AuthProcedure(SKY7635121Driver *devp);

/**
 * @brief   Check if the instance is ready.
 *
 * @param[in] devp     pointer to the @p SKY7635121Driver object
 *
 * @return              TRUE: instance is ready, FALSE: instance is not ready.
 */
bool isSky7635121Ready(SKY7635121Driver *devp);

/**
 * @brief   Initializes an instance.
 *
 * @param[out] devp     pointer to the @p SKY7635121Driver object
 *
 * @init
 */
void sky7635121ObjectInit(SKY7635121Driver *devp);

/**
 * @brief   Configures and activates SKY7635121 Complex Driver peripheral.
 *
 * @param[in] devp      pointer to the @p SKY7635121Driver object
 * @param[in] config    pointer to the @p SKY7635121Config object
 *
 * @api
 */
void sky7635121Start(SKY7635121Driver *devp, const SKY7635121Config *config);

/**
 * @brief   Deactivates the SKY7635121 Complex Driver peripheral.
 *
 * @param[in] devp       pointer to the @p SKY7635121Driver object
 *
 * @api
 */
void sky7635121Stop(SKY7635121Driver *devp);
#ifdef __cplusplus
}
#endif

#endif /* _SKY76351_21_H_ */

/** @} */

