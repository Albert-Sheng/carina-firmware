/**
 * @file    proximity_snsr.h
 * @brief   Proximity sensor driver header.
 *
 * @addtogroup COMMON
 * @{
 */

#ifndef _PROXIMITY_SNSR_H_
#define _PROXIMITY_SNSR_H_

#include <stdbool.h>

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/
#ifdef __cplusplus
extern "C" {
#endif
bool isProxiSensorDetected(void);
#ifdef __cplusplus
}
#endif

#endif // _PROXIMITY_SNSR_H_

/** @} */

