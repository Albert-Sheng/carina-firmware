
#ifndef __XB1_IMPERIUM_H
#define __XB1_IMPERIUM_H

// Imperium certificate reply is just over 1k
#define IMPERIUM_PACKET_SIZE_MAX 1100

#define IMPERIUM_PUID_BYTES 20

typedef enum {
    IMPERIUM_OK = 0,
    IMPERIUM_ERROR = 1,
} imperiumStatus_t;

imperiumStatus_t Imperium_Init(I2CDriver *i2c, const I2CConfig *i2c_config, ioportid_t gpio, iopadid_t pin);
imperiumStatus_t Imperium_PUID_get(uint8_t *puid, size_t bytes);
imperiumStatus_t Imperium_APDUpassthrough_send(uint8_t *buf, size_t bytes);
imperiumStatus_t Imperium_APDUpassthrough_recv(uint8_t *buf, size_t *bytes);
imperiumStatus_t Imperium_ShutDown(void);
bool imperium_Start(I2CDriver *i2c, const I2CConfig *i2c_config);
uint8_t getModelID(void);

#endif
