#include "switch.h"

/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define SWITCH_POLLING_TIME         100

typedef struct switch_arg {
    uint32_t pal_line;      // Port | Pad
    switch_cb_t cb;         // Switch event callback
    switch_event_t switch_event; // Switch event type
    virtual_timer_t vt;
} switch_arg_t;


/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

static switch_arg_t polling_switches[NUM_SWITCHES];

/*
 ******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************
 */

static void switch_polling_timer_cb(void *arg) {
    switch_arg_t *pswitch = (switch_arg_t *)arg;
    osalDbgAssert(arg, "invalid arg");

    if(arg == NULL) {
        return;
    }

    if((palReadLine(pswitch->pal_line) == PAL_LOW)
       && (pswitch->switch_event != SWITCH_OFF)) {
        //Switch off
        pswitch->switch_event = SWITCH_OFF;
        pswitch->cb(pswitch->pal_line, pswitch->switch_event);
    } else if((palReadLine(pswitch->pal_line) == PAL_HIGH)
              && (pswitch->switch_event != SWITCH_ON)) {
        //Switch on
        pswitch->switch_event = SWITCH_ON;
        pswitch->cb(pswitch->pal_line, pswitch->switch_event);
    }

    // restart the timer
    osalSysLockFromISR();
    chVTSetI(&pswitch->vt, TIME_MS2I(SWITCH_POLLING_TIME), switch_polling_timer_cb, pswitch);
    osalSysUnlockFromISR();
}

bool SwitchPollingStart(ioline_t pal_line, switch_cb_t switch_cb) {
    switch_arg_t *node = NULL;

    /* Find a free node in pool */
    for(int i = 0; i < NUM_SWITCHES; i++) {
        if(polling_switches[i].pal_line == 0) {
            node = &polling_switches[i];
            break;
        }
    }

    osalDbgAssert(node, "no available switch nodes!");
    if(node == NULL) {
        Dbg_printf(DBG_ERROR, "%s() no available switch nodes!", __func__);
        return false;
    }

    node->pal_line = pal_line;
    node->cb = switch_cb;

    /* Initialize opposite values for the line read(ON->OFF) so
     * the first poll can call the switch callback with the
     * right value.
     * Default: switch_event: SWITCH_ON(0)
     */
    if(palReadLine(pal_line) == PAL_HIGH) {
        //Switch on
        node->switch_event = SWITCH_OFF;
    }

    // start timer to polling the switch
    chVTSet(&node->vt, TIME_MS2I(SWITCH_POLLING_TIME), switch_polling_timer_cb, node);

    return true;
}

void SwitchPollingStop(ioline_t pal_line) {
    /* Find and reset the node */
    for(int i = 0; i < NUM_SWITCHES; i++) {
        if(polling_switches[i].pal_line == pal_line) {
            // stop the timer first
            chVTReset(&(polling_switches[i].vt));
            memset(&polling_switches[i], 0, sizeof(polling_switches[i]));
            break;
        }
    }
}

