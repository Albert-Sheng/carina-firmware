
#include "hal.h"
#include "endian.h"

void buf_reverse(uint8_t *buf, size_t bytes) {
    uint8_t temp;
    int front, back;
    for(front = 0, back = bytes - 1; front < back; front++, back--) {
        temp = buf[front];
        buf[front] = buf[back];
        buf[back] = temp;
    }
}

void buf_switch_endian(void *buf, size_t word_size, size_t words) {
    uint8_t *buf8 = buf;
    unsigned int word;
    for(word = 0; word < words; word++) {
        buf_reverse(buf8, word_size);
        buf8 += word_size;
    }
}
