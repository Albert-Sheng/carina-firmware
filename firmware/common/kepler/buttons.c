#include "buttons.h"
#include "debug.h"
#include "project.h"

#include <hal.h>

#include <string.h>

/*
 ******************************************************************************
 * DEFINES
 ******************************************************************************
 */

#define LONG_PRESS_THRESH_MS                    2000
#define VERY_LONG_PRESS_THRESH_MS               5000
#define DEBOUNCE_TIME                           100
#define BUTTON_POLLING_TIME                     50
#define TWO_BUTTON_COMBO                        2
#define MAX_COMBO_BUTTON_PRESS_SYNC_MS          2000


typedef struct button_arg {
    unsigned debounce_flag: 1;
    unsigned combo_flag: 1;
    uint8_t combo_btn_idx;
    uint8_t pal_line_idx;
    systime_t press_time;               // To determine if short or long press
    uint32_t pal_line;                  // Port | Pad
    button_cb_t cb;                     // Button event callback
    button_event_t button_event;        // Button event type
    virtual_timer_t vt;
} button_arg_t;

typedef struct {
    ioline_t pal_line;
    systime_t press_time;
} button_info_t;

typedef struct combo_button_arg {
    combo_button_cb_t cb;
    button_info_t info[TWO_BUTTON_COMBO];
    systime_t press_duration;
    uint8_t release_cnt;
    uint8_t identifier;
    virtual_timer_t vt;
} combo_button_arg_t;

/*
 ******************************************************************************
 * PRIVATE VARIABLES
 ******************************************************************************
 */

static button_arg_t s_button_arg_pool[NUM_BUTTONS];
static button_arg_t polling_buttons[NUM_BUTTONS];
#if NUM_COMBO_BUTTONS
static combo_button_arg_t s_combo_button_pool[NUM_COMBO_BUTTONS] = {0};
#endif

/*
 ******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************
 */

static void debounce_delay(void *arg) {
    button_arg_t *button_args = (button_arg_t *)arg;
    button_args->cb(button_args->pal_line, button_args->button_event);
    button_args->debounce_flag = false;
}

#if NUM_COMBO_BUTTONS
static void debounce_delay_combo_reset_timer_cb(void *arg) {
    uint8_t reset_debounce_flag = 0;
    combo_button_arg_t *button_args = (combo_button_arg_t *)arg;

    for(uint8_t idx = 0; idx < NUM_BUTTONS; idx++) {
        if(button_args->info[0].pal_line == s_button_arg_pool[idx].pal_line ||
           button_args->info[1].pal_line == s_button_arg_pool[idx].pal_line) {
            reset_debounce_flag++;
            s_button_arg_pool[idx].debounce_flag = false;
        }

        if(reset_debounce_flag == TWO_BUTTON_COMBO) {
            break;
        }
    }
}

static void debounce_delay_combo_timer_cb(void *arg) {
    combo_button_arg_t *button_args = (combo_button_arg_t *)arg;
    button_args->cb(button_args->identifier);

    debounce_delay_combo_reset_timer_cb(arg);
}
#endif

static void pal_line_cb(void *arg) {
    button_arg_t *button_args = (button_arg_t *)arg;

    osalDbgAssert(arg, "invalid arg");

    if(arg == NULL) {
        return;
    }

    if(!button_args->debounce_flag && (palReadLine(button_args->pal_line) == PAL_LOW)) {
        /* Button press */
        button_args->press_time = chVTGetSystemTimeX();

        button_args->debounce_flag = true;

#if NUM_COMBO_BUTTONS
        if(button_args->combo_flag == true) {
            s_combo_button_pool[button_args->combo_btn_idx].release_cnt = 0;
            s_combo_button_pool[button_args->combo_btn_idx].info[button_args->pal_line_idx].press_time = button_args->press_time;
        }
#endif
    } else if(button_args->debounce_flag && (palReadLine(button_args->pal_line) == PAL_HIGH)) {
        /* Button release */

#if NUM_COMBO_BUTTONS
        if(button_args->combo_flag == true) {

            const systime_t button_pressed_least_time = (s_combo_button_pool[button_args->combo_btn_idx].info[button_args->pal_line_idx].press_time >
                                                         s_combo_button_pool[button_args->combo_btn_idx].info[!button_args->pal_line_idx].press_time) ?
                                                        s_combo_button_pool[button_args->combo_btn_idx].info[button_args->pal_line_idx].press_time :
                                                        s_combo_button_pool[button_args->combo_btn_idx].info[!button_args->pal_line_idx].press_time;

            int diff = s_combo_button_pool[button_args->combo_btn_idx].info[button_args->pal_line_idx].press_time -
                       s_combo_button_pool[button_args->combo_btn_idx].info[!button_args->pal_line_idx].press_time;

            if(diff < 0) {
                diff = -diff;
            }

            s_combo_button_pool[button_args->combo_btn_idx].release_cnt++;

            if(diff > MAX_COMBO_BUTTON_PRESS_SYNC_MS) {
                // before we treat it as a single button press, check if the other combination button is pressed
                ioline_t alt_combo_pal_line = (s_combo_button_pool[button_args->combo_btn_idx].info[0].pal_line == button_args->pal_line) ?
                                              s_combo_button_pool[button_args->combo_btn_idx].info[1].pal_line :
                                              s_combo_button_pool[button_args->combo_btn_idx].info[0].pal_line;

                if(palReadLine(alt_combo_pal_line) == PAL_LOW) {
                    // wait for alternate combo button release interrupt
                    return;
                } else if(s_combo_button_pool[button_args->combo_btn_idx].release_cnt == TWO_BUTTON_COMBO) {
                    // reset debounce flags when the 2nd combo is released
                    chSysLockFromISR();
                    chVTSetI(&s_combo_button_pool[button_args->combo_btn_idx].vt, TIME_MS2I(DEBOUNCE_TIME),
                             debounce_delay_combo_reset_timer_cb,
                             &s_combo_button_pool[button_args->combo_btn_idx]);
                    return chSysUnlockFromISR();
                } else {
                    // fall through to single button press action
                }
            } else {
                chSysLockFromISR();

                if(s_combo_button_pool[button_args->combo_btn_idx].release_cnt == TWO_BUTTON_COMBO) {
                    if(TIME_I2MS(chVTTimeElapsedSinceX(button_pressed_least_time)) > s_combo_button_pool[button_args->combo_btn_idx].press_duration) {
                        chVTSetI(&s_combo_button_pool[button_args->combo_btn_idx].vt, TIME_MS2I(DEBOUNCE_TIME),
                                 debounce_delay_combo_timer_cb,
                                 &s_combo_button_pool[button_args->combo_btn_idx]);
                    } else {
                        chVTSetI(&s_combo_button_pool[button_args->combo_btn_idx].vt, TIME_MS2I(DEBOUNCE_TIME),
                                 debounce_delay_combo_reset_timer_cb,
                                 &s_combo_button_pool[button_args->combo_btn_idx]);
                    }
                }

                return chSysUnlockFromISR();
            }
        }
#endif

        if(chVTIsSystemTimeWithinX(button_args->press_time, button_args->press_time + TIME_MS2I(LONG_PRESS_THRESH_MS))) {
            button_args->button_event = BUTTON_SHORT_PRESS_EVENT;
        } else if(chVTIsSystemTimeWithinX(button_args->press_time, button_args->press_time + TIME_MS2I(VERY_LONG_PRESS_THRESH_MS))) {
            button_args->button_event = BUTTON_LONG_PRESS_EVENT;
        } else {
            button_args->button_event = BUTTON_VERYLONG_PRESS_EVENT;
        }

        chSysLockFromISR();
        chVTSetI(&button_args->vt, TIME_MS2I(DEBOUNCE_TIME), debounce_delay, button_args);
        chSysUnlockFromISR();
    }
}


/*
 ******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************
 */

bool ButtonRegisterCB(ioline_t pal_line, button_cb_t button_cb) {
    button_arg_t *node = NULL;

    if(palIsLineEventEnabledX(pal_line)) {
        Dbg_printf(DBG_INFO, "Button 0x%x already registered", pal_line);
        return false;
    }

    /* Find a free node in pool */
    for(int i = 0; i < NUM_BUTTONS; i++) {
        if(s_button_arg_pool[i].pal_line == 0) {
            node = &s_button_arg_pool[i];
            break;
        }
    }

    osalDbgAssert(node, "no available button nodes!");

    if(node == NULL) {
        Dbg_printf(DBG_ERROR, "%s() no available button nodes!", __func__);
        return false;
    }

    node->press_time = 0;
    node->pal_line = pal_line;
    node->cb = button_cb;
    node->debounce_flag = false;

    palSetLineCallback(pal_line, pal_line_cb, node);
    palEnableLineEvent(pal_line, PAL_EVENT_MODE_BOTH_EDGES);

    return true;
}

void ButtonUnregisterCB(ioline_t pal_line) {
    /* Find and reset the node */
    for(int i = 0; i < NUM_BUTTONS; i++) {
        if(s_button_arg_pool[i].pal_line == pal_line) {
            memset(&s_button_arg_pool[i], 0, sizeof(s_button_arg_pool[i]));
            break;
        }
    }

    if(palIsLineEventEnabledX(pal_line)) {
        palDisableLineEvent(pal_line);
    } else {
        Dbg_printf(DBG_ERROR, "Button 0x%x not registered!", pal_line);
    }
}

static void button_polling_timer_cb(void *arg) {
    button_arg_t *pbutton = (button_arg_t *)arg;
    osalDbgAssert(arg, "invalid arg");
    if(arg == NULL) {
        return;
    }

    if(!pbutton->debounce_flag && (palReadLine(pbutton->pal_line) == PAL_LOW)) {
        /* Button press */
        pbutton->press_time = chVTGetSystemTimeX();
        pbutton->debounce_flag = TRUE;
    } else if(pbutton->debounce_flag && (palReadLine(pbutton->pal_line) == PAL_HIGH)) {
        /* Button release */
        if(chVTIsSystemTimeWithinX(pbutton->press_time, pbutton->press_time + TIME_MS2I(LONG_PRESS_THRESH_MS))) {
            pbutton->button_event = BUTTON_SHORT_PRESS_EVENT;
        } else if(chVTIsSystemTimeWithinX(pbutton->press_time, pbutton->press_time + TIME_MS2I(VERY_LONG_PRESS_THRESH_MS))) {
            pbutton->button_event = BUTTON_LONG_PRESS_EVENT;
        } else {
            pbutton->button_event = BUTTON_VERYLONG_PRESS_EVENT;
        }
        pbutton->debounce_flag = FALSE;
        pbutton->cb(pbutton->pal_line, pbutton->button_event);
    }

    // restart the timer
    osalSysLockFromISR();
    chVTSetI(&pbutton->vt, TIME_MS2I(BUTTON_POLLING_TIME), button_polling_timer_cb, pbutton);
    osalSysUnlockFromISR();
}

bool buttonPollingStart(ioline_t pal_line, button_cb_t button_cb) {
    button_arg_t *node = NULL;

    /* Find a free node in pool */
    for(int i = 0; i < NUM_BUTTONS; i++) {
        if(polling_buttons[i].pal_line == 0) {
            node = &polling_buttons[i];
            break;
        }
    }

    osalDbgAssert(node, "no available button nodes!");
    if(node == NULL) {
        Dbg_printf(DBG_ERROR, "%s() no available button nodes!", __func__);
        return false;
    }

    node->press_time = 0;
    node->pal_line = pal_line;
    node->cb = button_cb;
    node->debounce_flag = FALSE;

    // start timer to polling the button
    chVTSet(&node->vt, TIME_MS2I(BUTTON_POLLING_TIME), button_polling_timer_cb, node);

    return true;
}

void buttonPollingStop(ioline_t pal_line) {
    /* Find and reset the node */
    for(int i = 0; i < NUM_BUTTONS; i++) {
        if(polling_buttons[i].pal_line == pal_line) {
            // stop the timer first
            chVTReset(&(polling_buttons[i].vt));
            memset(&polling_buttons[i], 0, sizeof(polling_buttons[i]));
            break;
        }
    }
}

#if NUM_COMBO_BUTTONS
void ButtonComboRegisterCB(combo_button_t *combo_button) {
    if(!palIsLineEventEnabledX(combo_button->pal_line1) || !palIsLineEventEnabledX(combo_button->pal_line2)) {
        Dbg_printf(DBG_ERROR, "Button combo requires the pal lines to be registered before registering it as combo button");
        return;
    }

    for(uint8_t idx1 = 0; idx1 < NUM_COMBO_BUTTONS; idx1++) {
        if(s_combo_button_pool[idx1].cb == NULL) {
            uint8_t combo_flag_registered = 0;

            s_combo_button_pool[idx1].cb = combo_button->interrupt_cb;
            s_combo_button_pool[idx1].identifier = combo_button->identifier;
            s_combo_button_pool[idx1].info[0].pal_line = combo_button->pal_line1;
            s_combo_button_pool[idx1].info[1].pal_line = combo_button->pal_line2;
            s_combo_button_pool[idx1].press_duration = combo_button->press_duration;

            for(uint8_t idx2 = 0; idx2 < NUM_BUTTONS; idx2++) {
                if(s_button_arg_pool[idx2].pal_line == combo_button->pal_line1) {
                    combo_flag_registered++;
                    s_button_arg_pool[idx2].pal_line_idx = 0;
                } else if(s_button_arg_pool[idx2].pal_line == combo_button->pal_line2) {
                    combo_flag_registered++;
                    s_button_arg_pool[idx2].pal_line_idx = 1;
                }

                s_button_arg_pool[idx2].combo_flag = true;
                s_button_arg_pool[idx2].combo_btn_idx = idx1;

                if(combo_flag_registered == TWO_BUTTON_COMBO) {
                    return;
                }
            }
        }
    }

    Dbg_printf(DBG_ERROR, "Combo button registration failed, no free slots available");
}

void ButtonComboUnregisterCB(uint8_t identifier) {
    for(uint8_t idx = 0; idx < NUM_COMBO_BUTTONS; idx++) {
        if(s_combo_button_pool[idx].identifier == identifier) {
            memset(&s_combo_button_pool[idx], 0, sizeof(combo_button_arg_t));
            break;
        }
    }
}
#endif

