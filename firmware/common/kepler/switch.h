
#pragma once


typedef enum {
    SWITCH_ON,
    SWITCH_OFF,
} switch_event_t;


typedef void (*switch_cb_t)(ioline_t pal_line, switch_event_t switch_event);


bool SwitchPollingStart(ioline_t pal_line, switch_cb_t switch_cb);
void SwitchPollingStop(ioline_t pal_line);

