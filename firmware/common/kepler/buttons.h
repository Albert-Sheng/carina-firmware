#pragma once

#include <hal.h>

typedef enum {
    BUTTON_SHORT_PRESS_EVENT,
    BUTTON_LONG_PRESS_EVENT,
    BUTTON_VERYLONG_PRESS_EVENT,
    BUTTON_COMBO_PRESS_EVENT,
} button_event_t;

typedef void (*combo_button_cb_t)(uint8_t button_identifier);
typedef void (*button_cb_t)(ioline_t pal_line, button_event_t button_event);

typedef struct {
    ioline_t pal_line1;
    ioline_t pal_line2;
    uint8_t identifier;
    uint32_t press_duration;
    combo_button_cb_t interrupt_cb;
} combo_button_t;


bool ButtonRegisterCB(ioline_t pal_line, button_cb_t button_cb);
void ButtonUnregisterCB(ioline_t pal_line);
bool buttonPollingStart(ioline_t pal_line, button_cb_t button_cb);
void buttonPollingStop(ioline_t pal_line);
#if NUM_COMBO_BUTTONS
void ButtonComboRegisterCB(combo_button_t *combo_button);
void ButtonComboUnregisterCB(uint8_t identifier);
#else
#define ButtonComboRegisterCB(combo_button)
#define ButtonComboUnregisterCB(identifier);
#endif
