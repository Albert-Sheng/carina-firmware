/**
 * @file    avnera.h
 * @brief   Avnera Wireless Audio Processor header.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */
#ifndef _AVNERA_H_
#define _AVNERA_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/
#define SKY7635121_CHIP_ID                                              (0x01)
#define SKY7635131_CHIP_ID                                              (0x02)

// define the maximum communication transfer size
#define AVNERA_COMM_MAX_TRANSFER_SIZE                                   (64)
#define AVNERA_EXT_FLASH_CENTPP_ADDR                              (0x000FF000)

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @name    Avnera data structures and types
 * @{
 */
/**
 * @brief   avnera pairing operation.
 */
typedef enum {
    MacPair_Normal_Pairing = 1,         // 1: Enter pairing normally
    MacPair_Fixed_Channel_Pairing = 2,  // 2: Fixed the channel pairing
    MacPair_Cancel_Pairing = 3,         // 3: Cancel the pairing
} avnera_pairingop_t;

/**
 * @brief   avnera boot app.
 */
typedef enum {
    BootApp_App0 = 0,
    BootApp_App1 = 1,
} avnera_boot_app_t;

/**
 * @brief   avnera boot information.
 */
typedef struct {
    avnera_boot_app_t now;             // The current boot app.
    avnera_boot_app_t next;            // The next boot app.
} avnera_boot_info_t;
/** @} */

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief   Read avnera chipset memory.
 *
 * @param[in] buf       buffer to store the val read to avnera chipset memory
 * @param[in] addr      address of avnera chipset memory to be read
 * @param[in] len       size of the val to read from avnera chipset memory
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraReadMem(uint8_t *buf, uint32_t addr, uint16_t len);

/**
 * @brief   Write avnera chipset memory.
 *
 * @param[in] buf       buffer of the val to write to avnera chipset memory
 * @param[in] addr      address of avnera chipset memory to be written
 * @param[in] len       size of the val to write to avnera chipset memory
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraWriteMem(uint8_t *buf, uint32_t addr, uint16_t len);

/**
 * @brief   get avnera chipset flash operation status.
 *
 * @param[void] void    No parameter
 * @return              the flash operation status
 * @retval 0xFFFFFFFF   if the function failed.
 */
uint32_t avneraGetFlashOpSts(void);

/**
 * @brief   Read avnera chipset flash.
 *
 * @param[in] buf       buffer to store the val read from avnera chipset flash
 * @param[in] addr      address of avnera chipset flash to be read
 * @param[in] len       size of the val to read from avnera chipset flash
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraReadFlash(uint8_t *buf, uint32_t addr, uint16_t len);

/**
 * @brief   Write avnera chipset flash.
 *
 * @param[in] buf       buffer of the val to write to avnera chipset flash
 * @param[in] addr      address of avnera chipset flash to be written
 * @param[in] len       size of the val to write to avnera chipset flash
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraWriteFlash(uint8_t *buf, uint32_t addr, uint16_t len);

/**
 * @brief   Erase avnera chipset flash.
 *
 * @param[in] addr      address of avnera chipset flash to be erased, it should be 4K-aligned
 * @param[in] size      size of avnera chipset flash to be erased This is
 *                      4k and bound to multiples of those sizes.
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraEraseFlash(uint32_t addr, uint32_t size);

/**
 * @brief   get avnera chipset arbiter id.
 *
 * @param[void] void    No parameter
 * @return              the chipset arbiter id
 * @retval 0            if the function failed.
 */
uint32_t avneraGetArbiterID(void);

/**
 * @brief   set avnera chipset arbiter id.
 *
 * @param[in] arbiter_id    arbiter id to set
 * @return                  the system flag bytes.
 * @retval MSG_OK           if the function succeeded.
 */
msg_t avneraSetArbiterID(uint32_t arbiter_id);

/**
 * @brief   save avnera chipset arbiter id.
 *
 * @param[in] void          No parameter
 * @return                  the system flag bytes.
 * @retval MSG_OK           if the function succeeded.
 */
msg_t avneraSaveArbiterID(void);

/**
 * @brief   get avnera chipset link State.
 *
 * @param[void] void                No parameter
 * @return                          the chipset link state
 * @retval MacCtxtState_Invalid     if the function failed.
 */
uint32_t avneraGetLinkState(void);

/**
 * @brief   set avnera chipset in pairing mode.
 *
 * @param[in] void      None parameter
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraSetOpenPairing(void);

/**
 * @brief   set avnera chipset in fixed channel pairing mode.
 *
 * @param[in] channel           select the channel
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetFixedChannelPairing(uint8_t channel);

/**
 * @brief   cancel pairing.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraCancelPairing(void);

/**
 * @brief   set modulation mode.
 *
 * @param[in] modul_mode        Modulation mode to set
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetModulationMode(uint32_t modul_mode);

/**
 * @brief   select carrier.
 *
 * @param[in] carrier           Carrier to select
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectCarrier(uint32_t carrier);

/**
 * @brief   select channel.
 *
 * @param[in] channel           Channel to select, Channel Range: [0..39]
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectChannel(uint32_t channel);

/**
 * @brief   select antenna.
 *
 * @param[in] antenna           Antenna to select
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectAntenna(uint32_t antenna);

/**
 * @brief   set TX power.
 *
 * @param[in] tx_pwr            TX Power to set, Power Range: [0x3F..0x00] (0.5 dB/step)
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetTxPower(uint32_t tx_pwr);

/**
 * @brief   Enable continuous TX mode.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraEnableContTx(void);

/**
 * @brief   Enable continuous RX mode.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraEnableContRx(void);

/**
 * @brief   select fixed frequency.
 *
 * @param[in] freq              Frequency to select.
                                            F = freq
                                            UNIT = F*(4/3)
                                            INT = floor(F/48)
                                            FRAC = (F-48*INT)*2^13*(2/3)+250
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSelectFixedFreq(uint32_t freq);

/**
 * @brief   Read PEC and Frames from avnera chipset MAC statistics.
 *
 * @param[in] buf       buffer to store the PEC and Frames from avnera chipset mac statistics
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t avneraReadMacPer(uint8_t *buf);

/**
 * @brief   start Mac Statistics.
 *
 * @param[in] start             Start or stop the mac statistics
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraStartMacStats(bool start);

/**
 * @brief   clear Mac Statistics.
 *
 * @param[in] void              No parameter
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraClearMacStats(void);

/**
 * @brief   set next active boot app FW, app0 or app1.
 *
 * @param[in] boot_app          the next boot app to select
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraSetNextBoot(avnera_boot_app_t boot_app);

/**
 * @brief   get boot info.
 *
 * @param[in] void              No parameter
 * @return                      the boot information.
 * @retval {0xFF, 0xFF}         if the function failed.
 */
avnera_boot_info_t avneraGetBootInfo(void);

/**
 * @brief   get FW flash Address.
 *
 * @param[in] boot_app          the boot app to select
 * @return                      the FW flash Address.
 * @retval 0xFFFFFFFF           if the function failed.
 */
uint32_t avneraGetAppFWAddr(avnera_boot_app_t boot_app);

/**
 * @brief   avboot reset.
 *
 * @param[in] void              None
 * @return                      the system flag bytes.
 * @retval MSG_OK               if the function succeeded.
 */
msg_t avneraAvbootReset(void);

/**
 * @brief   Avnera Authentication Procedure.
 *
 * @param[in] void              None
 * @return                      TRUE: Authentication Procedure Succeed, FALSE: Authentication Procedure Fail.
 */
bool avneraAuthProcedure(void);

/**
 * @brief   Initializes an instance.
 *
 *
 * @init
 */
void avneraInit(void);

/**
 * @brief   Configures and activates Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void avneraStart(void);

/**
 * @brief   Deactivates the Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void avneraStop(void);


/**
 * @brief   Reset the Avnera Wireless Audio Processor.
 *
 *
 * @api
 */
void avneraReset(void);

#ifdef __cplusplus
}
#endif

#endif /* _AVNERA_H_ */

/** @} */

