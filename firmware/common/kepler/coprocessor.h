/**
 * @file    coprocessor.h
 * @brief   CO-Processor header.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */
#ifndef _COPROCESSOR_H_
#define _COPROCESSOR_H_

#include "stm32f446.h"
#include "stm32f446_imperium.h"

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/
#define COPROCESSOR_GAME_MODE_BUSY_TIMEOUT                              (500)



/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/



/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/



/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
/**
 * @brief  CO-Processor Game/PC Mode
 */
typedef enum {
    COPROCESSOR_GAME_MODE_GAME = 0,
    COPROCESSOR_GAME_MODE_PC = 1,
    COPROCESSOR_GAME_MODE_UNKNOWN = 2,
} coprocessor_game_mode_t;

/**
 * @brief  CO-Processor BOOT Mode
 */
typedef enum {
    COPROCESSOR_BOOT_MODE_APP = 0,
    COPROCESSOR_BOOT_MODE_BLD = 1,
    COPROCESSOR_BOOT_MODE_UNKNOWN = 2,
} coprocessor_boot_mode_t;

/**
 * @brief  CO-Processor DFU Status
 */
typedef enum {
    COPROCESSOR_DFU_STS_NO_ERR = 0,
    COPROCESSOR_DFU_STS_BUSY = 1,
    COPROCESSOR_DFU_STS_ERR = 2,
    COPROCESSOR_DFU_STS_UNKNOWN = 3,
} coprocessor_dfu_sts_t;

/**
 * @brief  CO-Processor DFU Reason
 */
typedef enum {
    COPROCESSOR_DFU_NAPP = 0,
    COPROCESSOR_DFU_FORCE = 1,
    COPROCESSOR_DFU_CHKM = 2,
    COPROCESSOR_DFU_UNKNOWN = 3,
} coprocessor_dfu_reason_t;

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/
extern const I2CConfig i2c2cfg;
extern STM32F446Driver stm32f446;

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief   Enable/Disable HDMI Passthrough.
 *
 * @param[in] enable    bool parameter to enable/disable
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnableHDMIPassthrough(bool enable);

/**
 * @brief   Enable/Disable Phantom power.
 *
 * @param[in] enable    bool parameter to enable/disable
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnablePhantomPwr(bool enable);

/**
 * @brief   Enable/Disable Codec Reset.
 *
 * @param[in] enable    bool parameter to enable/disable
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnableCodecReset(bool enable);

/**
 * @brief   Enable/Disable Codec Power.
 *
 * @param[in] enable    bool parameter to enable/disable
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnableCodecPower(bool enable);

/**
 * @brief   Enable/Disable PGA2505 Power.
 *
 * @param[in] enable    bool parameter to enable/disable
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnablePGA2505Power(bool enable);

/**
 * @brief   Enable/Disable Imperium Reset.
 *
 * @param[in] enable    bool parameter to enable/disable
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnableImperiumReset(bool enable);

/**
 * @brief   Set Game/PC mode.
 *
 * @param[in] mode      mode to set to the coprocessor
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorSetGameMode(coprocessor_game_mode_t mode);

/**
 * @brief   Get Game/PC mode.
 *
 * @param[in] void      None
 *
 * @return              the game/pc mode.
 */
coprocessor_game_mode_t coProcessorGetGameMode(void);

/**
 * @brief   Get Boot mode.
 *
 * @param[in] void      None
 *
 * @return              the coprocessor boot mode.
 */
coprocessor_boot_mode_t coProcessorGetBootMode(void);

/**
 * @brief   Enter bootloader mode.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEnterBld(void);

/**
 * @brief   Erase application flash memory.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEraseApp(void);

/**
 * @brief   Erase application flash memory.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval bool         if the coprocessor ready for DFU.
 */
bool isCoProcessorReadyForDfu(void);

/**
 * @brief   Start to DFU.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorStartDFU(void);

/**
 * @brief   End DFU Process.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorEndDFU(void);

/**
 * @brief   Get DFU reason.
 *
 * @param[in] void      None
 *
 * @return              the DFU reason.
 */
coprocessor_dfu_sts_t coProcessorGetDfuStatus(void);

/**
 * @brief   Write APP register.
 *
 * @param[in] txbuf     buffer of  the val to set to STM32F446_APP_REG register
 * @param[in] size      size of the val to set to STM32F446_APP_REG register
 *                      size must not greater than 64, and should be 4-byte aligned
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorWriteAppData(const uint8_t *txbuf, uint8_t size);

/**
 * @brief   Get checksum.
 *
 * @param[in] void      None
 *
 * @return              the checksum.
 */
uint32_t coProcessorGetChecksum(void);

/**
 * @brief   Get DFU reason.
 *
 * @param[in] void      None
 *
 * @return              the DFU reason.
 */
coprocessor_dfu_reason_t coProcessorGetDfuReason(void);

/**
 * @brief   Reset coprocessor.
 *
 * @param[in] void      None
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorReset(void);

/**
 * @brief   check if PGA2505 is powered on.
 *
 * @param[in] void      None
 *
 * @return              if the PGA2505 is powered on.
 * @retval TRUE         if PGA2505 is powered on.
 */
bool coProcessorIsPGA2505Pwrd(void);

/**
 * @brief   Read coprocessor factory partition.
 *
 * @param[in] addr      the address of the factory partition to read
 * @param[inout] buf    buffer to store the the val read from the factory partition
 * @param[in] len       size of the val to read from the factory partition
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorFtyPtnRead(uint32_t addr, uint8_t *buf, uint8_t len);

/**
 * @brief   write coprocessor factory partition.
 *
 * @param[in] addr      the address of the factory partition to write
 * @param[in] buf       buffer to store the the val to write to the factory partition
 * @param[in] len       size of the val to write to the factory partition
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorFtyPtnWrite(uint32_t addr, uint8_t *buf, uint8_t len);

/**
 * @brief   erase coprocessor factory partition.
 *
 * @param[in] void      None parameters
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t coProcessorFtyPtnErase(void);

/**
 * @brief   Initializes an instance.
 *
 *
 * @init
 */
void coProcessorInit(void);

/**
 * @brief   Configures and activates STM32F446 Complex Driver peripheral.
 *
 *
 * @api
 */
void coProcessorStart(void);

/**
 * @brief   Deactivates the STM32F446 Complex Driver peripheral.
 *
 *
 * @api
 */
void coProcessorStop(void);

#ifdef __cplusplus
}
#endif

#endif /* _COPROCESSOR_H_ */

/** @} */

