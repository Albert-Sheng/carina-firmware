#ifndef __IMIXER_H__
#define __IMIXER_H__

#define TOTAL_INPUTS 10

typedef struct {
    uint16_t count;
    union {
        uint16_t inputs_supported;
        struct {
            unsigned int aux             : 1;
            unsigned int headphone       : 1;
            unsigned int trs             : 1;
            unsigned int xlr             : 1;
            unsigned int hdmi            : 1;
            unsigned int usb_game        : 1;
            unsigned int usb_game_chat   : 1;
            unsigned int usb_stream_alert: 1;
            unsigned int bluetooth       : 1;
            unsigned int proprietary_rf  : 1;
        };
    };
} inputs_supported_t;

typedef void (*inputInfo_cb_t)(inputs_supported_t *const);
bool InputInfoRegisterCB(inputInfo_cb_t inputInfo_cb);
void getInputsInfo(inputs_supported_t *const);
#endif
