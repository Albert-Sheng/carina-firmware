/**
 * @file    stm32f446.h
 * @brief   STM32F446 CO-Processor header.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */
#ifndef _STM32F446_H_
#define _STM32F446_H_

#include <hal.h>

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/**
 * @name    STM32F446 register addresses
 * @{
 */
#define STM32F446_TEST_REG                                              (0x00)
#define STM32F446_ENABLE_REG                                            (0x01)
#define STM32F446_GAME_MODE_REG                                         (0x02)
#define STM32F446_BOOT_MODE_REG                                         (0x03)
#define STM32F446_DFU_CTRL_REG                                          (0x04)
#define STM32F446_APP_REG                                               (0x05)
#define STM32F446_CHECKSUM_REG                                          (0x06)
#define STM32F446_DFU_REASON_REG                                        (0x07)
#define STM32F446_RESET_REG                                             (0x08)
#define STM32F446_IMPERIUM_CTRL_REG                                     (0x09)
#define STM32F446_IMPERIUM_STATUS_REG                                   (0x0A)
#define STM32F446_IMPERIUM_PUID_REG                                     (0x0B)
#define STM32F446_IMPERIUM_TXLEN_REG                                    (0x0C)
#define STM32F446_IMPERIUM_TXDATA_REG                                   (0x0D)
#define STM32F446_IMPERIUM_RXLEN_REG                                    (0x0E)
#define STM32F446_IMPERIUM_RXDATA_REG                                   (0x0F)
#define STM32F446_PERIPHERY_STS_REG                                     (0x10)
#define STM32F446_FTY_PTN_ACCESS_OP_REG                                 (0x11)
#define STM32F446_FTY_PTN_ACCESS_ADDR_REG                               (0x12)
#define STM32F446_FTY_PTN_ACCESS_LEN_REG                                (0x13)
#define STM32F446_FTY_PTN_ACCESS_BUF_REG                                (0x14)
/** @} */

/**
 * @name    STM32F446_TEST_REG register bits definitions
 * @{
 */
#define STM32F446_TEST_REG_DATA                                         (0xA5)
/** @} */

/**
 * @name    STM32F446_ENABLE_REG register bits definitions
 * @{
 */
#define STM32F446_ENABLE_REG_HDMI_MASK                                  (0x01)
#define STM32F446_ENABLE_REG_HDMI_EN                                    (0x01)
#define STM32F446_ENABLE_REG_HDMI_DIS                                   (0x00)

#define STM32F446_ENABLE_REG_PHTM_MASK                                  (0x02)
#define STM32F446_ENABLE_REG_PHTM_EN                                    (0x02)
#define STM32F446_ENABLE_REG_PHTM_DIS                                   (0x00)

#define STM32F446_ENABLE_REG_CODC_MASK                                  (0x04)
#define STM32F446_ENABLE_REG_CODC_EN                                    (0x04)
#define STM32F446_ENABLE_REG_CODC_DIS                                   (0x00)

#define STM32F446_ENABLE_REG_CPWR_MASK                                  (0x08)
#define STM32F446_ENABLE_REG_CPWR_EN                                    (0x08)
#define STM32F446_ENABLE_REG_CPWR_DIS                                   (0x00)

#define STM32F446_ENABLE_REG_PGAP_MASK                                  (0x10)
#define STM32F446_ENABLE_REG_PGAP_EN                                    (0x10)
#define STM32F446_ENABLE_REG_PGAP_DIS                                   (0x00)

#define STM32F446_ENABLE_REG_IMPRST_MASK                                (0x20)
#define STM32F446_ENABLE_REG_IMPRST_EN                                  (0x20)
#define STM32F446_ENABLE_REG_IMPRST_DIS                                 (0x00)
/** @} */

/**
 * @name    STM32F446_GAME_MODE_REG register bits definitions
 * @{
 */
#define STM32F446_GAME_MODE_REG_MODE_MASK                               (0x01)
#define STM32F446_GAME_MODE_REG_GAME_MODE                               (0x00)
#define STM32F446_GAME_MODE_REG_PC_MODE                                 (0x01)
#define STM32F446_GAME_MODE_REG_MSTS                                    (0x02)
/** @} */

/**
 * @name    STM32F446_BOOT_MODE_REG register bits definitions
 * @{
 */
#define STM32F446_BOOT_MODE_REG_MODE_MASK                               (0x01)
#define STM32F446_BOOT_MODE_REG_APP_MODE                                (0x00)
#define STM32F446_BOOT_MODE_REG_BLD_MODE                                (0x01)
/** @} */

/**
 * @name    STM32F446_DFU_CTRL_REG register bits definitions
 * @{
 */
#define STM32F446_DFU_CTRL_REG_BTLD                                     (0x01)
#define STM32F446_DFU_CTRL_REG_ERASE                                    (0x02)
#define STM32F446_DFU_CTRL_REG_RDY                                      (0x04)
#define STM32F446_DFU_CTRL_REG_START_DFU                                (0x08)
#define STM32F446_DFU_CTRL_REG_END_DFU                                  (0x10)
#define STM32F446_DFU_CTRL_REG_DFU_STS_MASK                             (0x60)
#define STM32F446_DFU_CTRL_REG_DFU_NO_ERR                               (0x00)
#define STM32F446_DFU_CTRL_REG_DFU_BUSY                                 (0x20)
#define STM32F446_DFU_CTRL_REG_DFU_ERR                                  (0x40)
/** @} */

/**
 * @name    STM32F446_DFU_REASON_REG register bits definitions
 * @{
 */
#define STM32F446_DFU_REASON_REG_MASK                                   (0x07)
#define STM32F446_DFU_REASON_REG_NAPP                                   (0x01)
#define STM32F446_DFU_REASON_REG_FORCE                                  (0x02)
#define STM32F446_DFU_REASON_REG_CHKM                                   (0x04)
/** @} */

/**
 * @name    STM32F446_RESET_REG register bits definitions
 * @{
 */
#define STM32F446_RESET_REG_MASK                                        (0x01)
#define STM32F446_RESET_REG_RST                                         (0x01)
/** @} */

/**
 * @name    STM32F446_IMPERIUM_CTRL_REG register bits definitions
 * @{
 */
#define IMPERIUM_CTRL_NO_OP                                             (0x00)
#define IMPERIUM_CTRL_INITIAL                                           (0x01)
#define IMPERIUM_CTRL_SHUTDOWN                                          (0x02)
#define IMPERIUM_CTRL_READ_PUID                                         (0x03)
#define IMPERIUM_CTRL_SEND                                              (0x04)
#define IMPERIUM_CTRL_RECEIVE                                           (0x05)
/** @} */

/**
 * @name    STM32F446_IMPERIUM_STATUS_REG register bits definitions
 * @{
 */
#define IMPERIUM_STS_EXIST_MSK                                          (0x80)
#define IMPERIUM_STS_PRESENT                                            (0x80)
#define IMPERIUM_STS_ABSENT                                             (0x00)
#define IMPERIUM_STS_OP_MSK                                             (0x7F)
#define IMPERIUM_STS_OP_IDLE                                            (0x00)
#define IMPERIUM_STS_OP_BUSY                                            (0x01)
#define IMPERIUM_STS_OP_DONE                                            (0x02)
#define IMPERIUM_STS_OP_FAIL                                            (0x03)
/** @} */

/**
 * @name    STM32F446_PERIPHERY_STS_REG register bits definitions
 * @{
 */
#define STM32F446_PGA2505_PWR_MSK                                       (0x01)
#define STM32F446_PGA2505_IS_PWRD                                       (0x01)
#define STM32F446_PGA2505_NOT_PWRD                                      (0x00)

#define STM32F446_CODEC_RESET_MSK                                       (0x02)
#define STM32F446_CODEC_RESET_EN                                        (0x02)
#define STM32F446_CODEC_RESET_DIS                                       (0x00)

#define STM32F446_IMPERIUM_RESET_MSK                                    (0x04)
#define STM32F446_IMPERIUM_RESET_EN                                     (0x04)
#define STM32F446_IMPERIUM_RESET_DIS                                    (0x00)
/** @} */

/**
 * @name    STM32F446_FTY_PTN_ACCESS_OP_REG register bits definitions
 * @{
 */
#define STM32F446_FTY_PTN_ACCESS_OP_MSK                                 (0x0F)
#define STM32F446_FTY_PTN_ACCESS_OP_IDLE                                (0x00)
#define STM32F446_FTY_PTN_ACCESS_OP_RD                                  (0x01)
#define STM32F446_FTY_PTN_ACCESS_OP_WR                                  (0x02)
#define STM32F446_FTY_PTN_ACCESS_OP_ER                                  (0x03)
#define STM32F446_FTY_PTN_ACCESS_OPSTS_MSK                              (0xF0)
#define STM32F446_FTY_PTN_ACCESS_OPSTS_IDLE                             (0x00)
#define STM32F446_FTY_PTN_ACCESS_OPSTS_BUSY                             (0x10)
#define STM32F446_FTY_PTN_ACCESS_OPSTS_DONE                             (0x20)
#define STM32F446_FTY_PTN_ACCESS_OPSTS_FAIL                             (0x30)
/** @} */

/**
 * @name    STM32F446 I2C operation timeout
 * @{
 */
#define STM32F446_I2C_TIMEOUT                                   OSAL_MS2I(50)

/** @} */

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @name    Configuration options
 * @{
 */
/**
 * @brief   STM32F446 I2C interface switch.
 * @details If set to @p TRUE the support for I2C is included.
 * @note    The default is @p TRUE.
 */
#if !defined(STM32F446_USE_I2C) || defined(__DOXYGEN__)
#define STM32F446_USE_I2C                     TRUE
#endif

/**
 * @brief   STM32F446 shared I2C switch.
 * @details If set to @p TRUE the device acquires I2C bus ownership
 *          on each transaction.
 * @note    The default is @p FALSE. Requires I2C_USE_MUTUAL_EXCLUSION.
 */
#if !defined(STM32F446_SHARED_I2C) || defined(__DOXYGEN__)
#define STM32F446_SHARED_I2C                  TRUE
#endif

/**
 * @brief   STM32F446 advanced configurations switch.
 * @details If set to @p TRUE more configurations are available.
 * @note    The default is @p FALSE.
 */
#if !defined(STM32F446_USE_ADVANCED) || defined(__DOXYGEN__)
#define STM32F446_USE_ADVANCED                FALSE
#endif
/** @} */

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/
#if STM32F446_USE_I2C && !HAL_USE_I2C
#error "STM32F446_I2C requires HAL_USE_I2C"
#endif

#if STM32F446_SHARED_I2C && !I2C_USE_MUTUAL_EXCLUSION
#error "STM32F446_SHARED_I2C requires I2C_USE_MUTUAL_EXCLUSION"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/**
 * @name    STM32F446 data structures and types.
 * @{
 */
/**
 * @brief   Structure representing a STM32F446 driver.
 */
typedef struct STM32F446Driver STM32F446Driver;

/**
 * @brief  STM32F446 slave address
 */
typedef enum {
    STM32F446_SAD = 0x50,               /**< Slave Address   */
} stm32f446_sad_t;

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
    STM32F446_UNINIT = 0,               /**< Not initialized.                */
    STM32F446_STOP = 1,                 /**< Stopped.                        */
    STM32F446_READY = 2,                /**< Ready.                          */
} stm32f446_state_t;

/**
 * @brief   STM32F446 configuration structure.
 */
typedef struct {
#if STM32F446_USE_I2C || defined(__DOXYGEN__)
    /**
     * @brief I2C driver associated to STM32F446.
     */
    I2CDriver                   *i2cp;
    /**
     * @brief I2C configuration associated to STM32F446.
     */
    const I2CConfig             *i2ccfg;
    /**
     * @brief STM32F446 slave address
     */
    stm32f446_sad_t             slaveaddress;
#endif /* STM32F446_USE_I2C */
} STM32F446Config;

/**
 * @brief   @p STM32F446 specific methods.
 * @note    No methods so far, just a common ancestor interface.
 */
#define _stm32f446_methods_alone

/**
 * @brief @p STM32F446 specific methods with inherited ones.
 */
#define _stm32f446_methods                                                    \
    _base_object_methods                                                      \
    _stm32f446_methods_alone

/**
 * @extends BaseObjectVMT
 *
 * @brief @p STM32F446 virtual methods table.
 */
struct STM32F446VMT {
    _stm32f446_methods
};

/**
 * @brief   @p STM32F446Driver specific data.
 */
#define _stm32f446_data                                                       \
    /* Driver state.*/                                                        \
    stm32f446_state_t           state;                                        \
    /* Current configuration data.*/                                          \
    const STM32F446Config       *config;

/**
 * @brief   STM32F446 class.
 */
struct STM32F446Driver {
    /** @brief Virtual Methods Table.*/
    const struct STM32F446VMT   *vmt;
    _stm32f446_data
};
/** @} */

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief   Grab Test register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Test register value.
 */
uint8_t stm32f446GetTestReg(STM32F446Driver *devp);

/**
 * @brief   Set Enable register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_ENABLE_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetEnableReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Set Enable register bit value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] bit_mask  val to mask the bit of STM32F446_ENABLE_REG register
 * @param[in] bit_val   val to set to the bit of STM32F446_ENABLE_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetEnableRegBit(STM32F446Driver *devp,
                               uint8_t bit_mask, uint8_t bit_val);

/**
 * @brief   Grab Enable register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Enable register value.
 */
uint8_t stm32f446GetEnableReg(STM32F446Driver *devp);

/**
 * @brief   Set Game Mode register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_GAME_MODE_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetGameModeReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Set Game Mode register bit value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] bit_mask  val to mask the bit of STM32F446_GAME_MODE_REG register
 * @param[in] bit_val   val to set to the bit of STM32F446_GAME_MODE_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetGameModeRegBit(STM32F446Driver *devp,
                                 uint8_t bit_mask, uint8_t bit_val);

/**
 * @brief   Grab Game Mode register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Game Mode register value.
 */
uint8_t stm32f446GetGameModeReg(STM32F446Driver *devp);

/**
 * @brief   Grab Boot Mode register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Boot Mode register value.
 */
uint8_t stm32f446GetBootModeReg(STM32F446Driver *devp);

/**
 * @brief   Set DFU CTRL register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_DFU_CTRL_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetDfuCtrlReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Set DFU CTRL register bit value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] bit_mask  val to mask the bit of STM32F446_DFU_CTRL_REG register
 * @param[in] bit_val   val to set to the bit of STM32F446_DFU_CTRL_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetDfuCtrlRegBit(STM32F446Driver *devp,
                                uint8_t bit_mask, uint8_t bit_val);

/**
 * @brief   Grab DFU CTRL register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the DFU CTRL register value.
 */
uint8_t stm32f446GetDfuCtrlReg(STM32F446Driver *devp);

/**
 * @brief   Set App register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] txbuf     buffer of  the val to set to STM32F446_APP_REG register
 * @param[in] size      size of the val to set to STM32F446_APP_REG register
 *                      size must not greater than 64, and should be 4-byte aligned
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetAppReg(STM32F446Driver *devp,
                         const uint8_t *txbuf, uint8_t size);

/**
 * @brief   Grab Checksum register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Checksum register value.
 */
uint32_t stm32f446GetChecksumReg(STM32F446Driver *devp);

/**
 * @brief   Grab DFU Reason register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the DFU Reason register value.
 */
uint8_t stm32f446GetDfuReasonReg(STM32F446Driver *devp);

/**
 * @brief   Set Reset register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_RESET_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetResetReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Set Reset register bit value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] bit_mask  val to mask the bit of STM32F446_RESET_REG register
 * @param[in] bit_val   val to set to the bit of STM32F446_RESET_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetResetRegBit(STM32F446Driver *devp,
                              uint8_t bit_mask, uint8_t bit_val);

/**
 * @brief   Grab Reset register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Reset register value.
 */
uint8_t stm32f446GetResetReg(STM32F446Driver *devp);

/**
 * @brief   Grab imperium_ctrl register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the imperium_ctrl register value.
 */
uint8_t stm32f446GetImperiumCtrlReg(STM32F446Driver *devp);

/**
 * @brief   Set imperium_status register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_IMPERIUM_STATUS_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetImperiumStsReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Set imperium_status register bit value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] bit_mask  val to mask the bit of STM32F446_IMPERIUM_STATUS_REG register
 * @param[in] bit_val   val to set to the bit of STM32F446_IMPERIUM_STATUS_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetImperiumStsRegBit(STM32F446Driver *devp,
                                    uint8_t bit_mask, uint8_t bit_val);

/**
 * @brief   Grab imperium_status register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              theimperium_status register value.
 */
uint8_t stm32f446GetImperiumStsReg(STM32F446Driver *devp);

/**
 * @brief   Set imperium_puid register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] txbuf     buffer of  the val to set to STM32F446_IMPERIUM_PUID_REG register
 * @param[in] size      size of the val to set to STM32F446_IMPERIUM_PUID_REG register
 *                      size must not greater than 64
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetImperiumPUIDReg(STM32F446Driver *devp,
                                  const uint8_t *txbuf, uint8_t size);

/**
 * @brief   Grab imperium_tx_len register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the imperium_tx_len register value.
 */
uint16_t stm32f446GetImperiumTxLenReg(STM32F446Driver *devp);

/**
 * @brief   Grab imperium_tx_data register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] rxbuf     buffer to store the val read from STM32F446_IMPERIUM_TXDATA_REG register
 * @param[in] size      size of the val read from STM32F446_IMPERIUM_TXDATA_REG register
 *                      size must not greater than 64
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446GetImperiumTxDataReg(STM32F446Driver *devp,
                                    uint8_t *rxbuf, uint8_t size);

/**
 * @brief   Grab imperium_rx_len register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the imperium_rx_len register value.
 */
uint16_t stm32f446GetImperiumRxLenReg(STM32F446Driver *devp);
/**
 * @brief   Set imperium_rx_data register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] txbuf     buffer of the val set to STM32F446_IMPERIUM_RXDATA_REG register
 * @param[in] size      size of the val set to STM32F446_IMPERIUM_RXDATA_REG register
 *                      size must not greater than 64.
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetImperiumRxDataReg(STM32F446Driver *devp,
                                    const uint8_t *txbuf, uint8_t size);

/**
 * @brief   Grab Perphery status register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the Perphery status register value.
 */
uint8_t stm32f446GetPerpheryStsReg(STM32F446Driver *devp);

/**
 * @brief   Set factory_partition_access_op register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_FTY_PTN_ACCESS_OP_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetFtyPtnAccessOpReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Set factory_partition_access_op register bit value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] bit_mask  val to mask the bit of STM32F446_FTY_PTN_ACCESS_OP_REG register
 * @param[in] bit_val   val to set to the bit of STM32F446_FTY_PTN_ACCESS_OP_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetFtyPtnAccessOpRegBit(STM32F446Driver *devp,
                                       uint8_t bit_mask, uint8_t bit_val);

/**
 * @brief   Grab factory_partition_access_op register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 *
 * @return              the factory_partition_access_op register value.
 */
uint8_t stm32f446GetFtyPtnAccessOpReg(STM32F446Driver *devp);

/**
 * @brief   Set factory_partition_access_addr register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_FTY_PTN_ACCESS_ADDR_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetFtyPtnAccessAddrReg(STM32F446Driver *devp, uint32_t val);

/**
 * @brief   Set factory_partition_access_len register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] val       val to set to STM32F446_FTY_PTN_ACCESS_LEN_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetFtyPtnAccessLenReg(STM32F446Driver *devp, uint8_t val);

/**
 * @brief   Get factory_partition_access_buf register value from STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] rxbuf     buffer of  the val read from STM32F446_FTY_PTN_ACCESS_BUF_REG register
 * @param[in] size      size of the val read from STM32F446_FTY_PTN_ACCESS_BUF_REG register
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446GetFtyPtnAccessBufReg(STM32F446Driver *devp, uint8_t *rxbuf, uint8_t size);


/**
 * @brief   Set factory_partition_access_buf register value to STM32F446.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] txbuf     buffer of  the val to set to STM32F446_FTY_PTN_ACCESS_BUF_REG register
 * @param[in] size      size of the val to set to STM32F446_FTY_PTN_ACCESS_BUF_REG register
 *                      size must not greater than 64, and should be 4-byte aligned
 *
 * @return              the system flag bytes.
 * @retval MSG_OK       if the function succeeded.
 */
msg_t stm32f446SetFtyPtnAccessBufReg(STM32F446Driver *devp, const uint8_t *txbuf, uint8_t size);

/**
 * @brief   Check if the instance is ready.
 *
 * @param[out] devp     pointer to the @p STM32F446Driver object
 *
 * @return              TRUE: instance is ready, FALSE: instance is not ready.
 */
bool isStm32f446Ready(STM32F446Driver *devp);

/**
 * @brief   Initializes an instance.
 *
 * @param[out] devp     pointer to the @p STM32F446Driver object
 *
 * @init
 */
void stm32f446ObjectInit(STM32F446Driver *devp);

/**
 * @brief   Configures and activates STM32F446 Complex Driver peripheral.
 *
 * @param[in] devp      pointer to the @p STM32F446Driver object
 * @param[in] config    pointer to the @p STM32F446Config object
 *
 * @api
 */
void stm32f446Start(STM32F446Driver *devp, const STM32F446Config *config);

/**
 * @brief   Deactivates the STM32F446 Complex Driver peripheral.
 *
 * @param[in] devp       pointer to the @p STM32F446Driver object
 *
 * @api
 */
void stm32f446Stop(STM32F446Driver *devp);
#ifdef __cplusplus
}
#endif

#endif /* _STM32F446_H_ */

/** @} */

