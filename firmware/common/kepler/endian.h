
#ifndef __ENDIAN_H
#define __ENDIAN_H

void buf_reverse(uint8_t *buf, size_t bytes);
void buf_switch_endian(void *buf, size_t word_size, size_t words);
#define BYTE_SWAP(buf) buf_reverse((uint8_t*)&buf, sizeof(buf))

#endif
