/**
 * @file    eqhelper.h
 * @brief
 *
 * @addtogroup be
 * @ingroup BE
 * @{
 */

#ifndef _EQHELPER_H_
#define _EQHELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#define NUM_EQ_BANDS    (5)
#define MAX_EQ_PROFILES (4)

typedef enum {
    EQ_MODE_CUSTOM = 0x04,
    EQ_MODE_ASTRO_IMMERSIVE = 0x80,
    EQ_MODE_ASTRO_STUDIO = 0x82,
    EQ_MODE_ASTRO_GAMING = 0x83,
    EQ_MODE_ASTRO_PRO = 0x84
} EQModes;

/**
 * This structure helps to save and load EQ Profiles
 * Each band is dB value between -12db and +12dB
 */
typedef struct {
    int8_t bandGains[NUM_EQ_BANDS];
} EQProfile;

typedef struct {
    uint16_t bandFrequencies[NUM_EQ_BANDS];
} EQFrequencyProfile;

EQProfile const *EQGetDefaultProfile(uint8_t index);
void EQSetProfile(EQProfile const *profile);
void EQGetProfile(EQProfile *profile);
void EQGetFrequencies(EQFrequencyProfile *freqProfile);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // _EQHELPER_H_
