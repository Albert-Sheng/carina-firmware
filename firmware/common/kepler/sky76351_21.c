/**
 * @file    sky76351_21.c
 * @brief   SKY76351-21 Wireless Audio Processor module code.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */

#include <string.h>
#include "hal.h"
#include "sky76351_21.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/
/**
 * @name    SKY7635121 Status Encoding Constants
 * @{
 */
#define SPI_SLAVE_STS_RXDATAFULL                                        (0x01)
#define SPI_SLAVE_STS_RXREADY                                           (0x00)
#define SPI_SLAVE_STS_TXDATAREADY                                       (0x02)
#define SPI_SLAVE_STS_TXEMPTY                                           (0x00)
#define SPI_SLAVE_STS_SPIERR_RXOVERFLOW                                 (0x04)
#define SPI_SLAVE_STS_SPIERR_NO                                         (0x00)
#define SPI_SLAVE_STS_SW_BUSY                                           (0x10)
#define SPI_SLAVE_STS_SW_READY                                          (0x00)
#define SPI_SLAVE_STS_SW_ASSERT                                         (0x40)
#define SPI_SLAVE_STS_SW_NORMAL_OP                                      (0x00)
#define SPI_SLAVE_STS_BUSY_MASK                                         (0x11)
#define SPI_SLAVE_STS_ERRORS_MASK                                       (0x44)
/** @} */

/**
 * @name    AIS Command Constants
 * @{
 */

/**
 * @brief   The buffer size for AIS commands
 * @details So far the TX and RX share the same size, 128 bytes.
 * @note
 */
#define AIS_CMD_BUFFER_SIZE                                             (128)
#define AIS_CMD_MAX_TRY_NUM                                             (100)

/**
 * @brief   The CPU Speed of SKY7635121
 * @details List all of the CPU Speed supported
 * @note
 */
#define SKY7635121_CPU_SPEED_1M5HZ                                      (0x00)
#define SKY7635121_CPU_SPEED_3M0HZ                                      (0x01)
#define SKY7635121_CPU_SPEED_6M0HZ                                      (0x02)
#define SKY7635121_CPU_SPEED_12MHZ                                      (0x03)
#define SKY7635121_CPU_SPEED_24MHZ                                      (0x04)
#define SKY7635121_CPU_SPEED_48MHZ                                      (0x05)


/**
 * @brief   Special commands before sending AIS commands
 * @details Before write something to SKY7635121, we need to send 0x00 first,
 *          before read operation, we need to send 0x00 first, and then 0x01
 *          to fetch the SPI Slave Status
 * @note
 */
#define SKY7635121_SPI_SLAVE_STATUS_READ                                (0x00)
#define SKY7635121_SPI_SLAVE_NORMAL_READ                                (0x01)

/** @} */

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
/**
 * @name    AIS Command variables and types
 * @{
 */

/**
 * @brief   AIS Message Header structure.
 */
typedef struct {
    uint8_t Source;                     // The source module identifier.
    uint8_t Dest;                       // The destination module identifier.

    uint8_t Opcode: 6;                  // The opcode for the operation.
    uint8_t Reserved: 1;                // Reserved. Must be 0.
    uint8_t BufferFlag: 1;              // The flag to indicate if a buffer is being used.

    uint8_t Status: 6;                  // The status of the operation (on return).
    uint8_t Type: 2;                    // The type of the message.

    uint32_t Param0;                    // The first parameter for the command.
    uint32_t Param1;                    // The second parameter for the command.
} ais_msg_hdr_t;

/**
 * @brief   AIS Message Header Opcode.
 */
typedef enum {
    CmdOpcode_Read = 1,
    CmdOpcode_Write = 2,
    CmdOpcode_SetParameter = 3,
    CmdOpcode_GetParameter = 4,
    CmdOpcode_Request = 5,
} ais_hdr_opcode_t;

/**
 * @brief   AIS Message Header Buffer Flag.
 */
typedef enum {
    BufferFlag_NoDataBuffer = 0,        // No data buffer is associated with the message.
    BufferFlag_UseDataBuffer = 1,       // A data buffer is associated with the message.
} ais_hdr_bufflag_t;

/**
 * @brief   AIS Message Header Status.
 */
typedef enum {
    ResultStatus_Success = 0,           // Indicates a successful message execution.
    ResultStatus_Failed = 1,            // Indicates a general operation failure.
    ResultStatus_Pending = 2,           // Indicates that the operation is still pending.
    ResultStatus_NotFound = 3,          // The requested destination module was not found.
    ResultStatus_NotSupported = 4,      // The requested operation is not supported by the destination module.
    ResultStatus_ExceededLimits = 5,    // The requested operation exceeded the limits.
    ResultStatus_NotReady = 6,          // The destination module is not ready.
    ResultStatus_CommFailure = 7,       // A comms driver has detected a failure.
    ResultStatus_InvalidFile = 8,       // The file format doesn't match the appropriate settings.
    ResultStatus_NoCal = 9,             // The chip calibration has not been applied.
    ResultStatus_OS_Error = 10,         // An OS (MQX) API call failed.
    ResultStatus_Unauthorized = 11,     // The requested operation has not been authorized.
    ResultStatus_Busy = 12,             // The requested operation failed because the destination module is busy.
} ais_hdr_status_t;

/**
 * @brief   AIS Message Header Type.
 */
typedef enum {
    MessageType_Result = 0,             // Indicates that the message is a result.
    MessageType_Query = 1,              // Indicates that the message is a query (inbound).
} ais_hdr_type_t;

static uint8_t ais_tx_buffer[AIS_CMD_BUFFER_SIZE] __attribute__((section(".nocache")));
static uint8_t ais_rx_buffer[AIS_CMD_BUFFER_SIZE + 1] __attribute__((section(".nocache")));
static uint8_t ais_dummy_buffer[AIS_CMD_BUFFER_SIZE + 1] __attribute__((section(".nocache")));

/** @} */

static const struct SKY7635121VMT vmt_device = {
    (size_t)0
};

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

#if (SKY7635121_USE_SPI) || defined(__DOXYGEN__)
static void sky7635121SpiExchange(SKY7635121Driver *devp, uint8_t *txbuf,
                                  uint8_t *rxbuf, uint8_t len) {

    osalDbgCheck((devp != NULL) && (txbuf != NULL) && (rxbuf != NULL) && (len < AIS_CMD_BUFFER_SIZE));

#if SKY7635121_SHARED_SPI
    spiAcquireBus(devp->config->spip);
#endif
    spiStart(devp->config->spip, devp->config->spicfg);

    spiSelect(devp->config->spip);
    spiExchange(devp->config->spip, len, txbuf, rxbuf);
    spiUnselect(devp->config->spip);

#if SKY7635121_SHARED_SPI
    spiReleaseBus(devp->config->spip);
#endif
}

static void sky7635121SpiWrite(SKY7635121Driver *devp, uint8_t *txbuf, uint8_t len) {

    osalDbgCheck((devp != NULL) && (txbuf != NULL) && (len < AIS_CMD_BUFFER_SIZE));

#if SKY7635121_SHARED_SPI
    spiAcquireBus(devp->config->spip);
#endif
    spiStart(devp->config->spip, devp->config->spicfg);

    spiSelect(devp->config->spip);
    spiExchange(devp->config->spip, len, txbuf, ais_dummy_buffer);
    spiUnselect(devp->config->spip);

#if SKY7635121_SHARED_SPI
    spiReleaseBus(devp->config->spip);
#endif
}

static void sky7635121SpiRead(SKY7635121Driver *devp, uint8_t *rxbuf, uint8_t len) {

    osalDbgCheck((devp != NULL) && (rxbuf != NULL) && (len < AIS_CMD_BUFFER_SIZE));

#if SKY7635121_SHARED_SPI
    spiAcquireBus(devp->config->spip);
#endif
    spiStart(devp->config->spip, devp->config->spicfg);

    memset(ais_dummy_buffer, 0, len + 1);
    ais_dummy_buffer[0] = SKY7635121_SPI_SLAVE_NORMAL_READ;

    spiSelect(devp->config->spip);
    spiExchange(devp->config->spip, len + 1, ais_dummy_buffer, rxbuf);
    spiUnselect(devp->config->spip);

#if SKY7635121_SHARED_SPI
    spiReleaseBus(devp->config->spip);
#endif
}
#endif /* SKY7635121_USE_SPI */

static uint8_t sky7635121GetSlaveStatus(SKY7635121Driver *devp) {
    ais_tx_buffer[0] = SKY7635121_SPI_SLAVE_STATUS_READ;

#if (SKY7635121_USE_SPI) || defined(__DOXYGEN__)
    sky7635121SpiExchange(devp, ais_tx_buffer, ais_rx_buffer, 1);
#endif /* SKY7635121_USE_SPI */

    return ais_rx_buffer[0];
}

static bool sky7635121WaitForReadyToSend(SKY7635121Driver *devp) {
    uint8_t slave_status;
    bool error, busy, txDataReady;
    uint8_t try_cnt = 0;

    do {
        slave_status = sky7635121GetSlaveStatus(devp);
        error = slave_status & SPI_SLAVE_STS_ERRORS_MASK;
        busy = slave_status & SPI_SLAVE_STS_BUSY_MASK;
        txDataReady = slave_status & SPI_SLAVE_STS_TXDATAREADY;

        if(error) {
            return FALSE;
        }

        if(txDataReady) {
            //out of sync here.  We are trying to write data and the previous
            //sequence should have completed with a read already to get the
            //result of the last transaction.

            //This should not happen and I would ASSERT() here during
            //development to catch any SPI sequence issues
            //but take out the ASSERT() for production code to be fault tolerant

            //Here we have no idea how many data are there, so we read back 1 byte each time
            //and drop it
            sky7635121SpiRead(devp, ais_rx_buffer, 1);
            try_cnt = 0; // clear the try_cnt here in case the txData is larger than AIS_CMD_MAX_TRY_NUM bytes
        }

        if(try_cnt++ > AIS_CMD_MAX_TRY_NUM) {
            return FALSE;
        }
        chThdSleepMilliseconds(5);
    } while(busy);

    return TRUE;
}

static bool sky7635121WaitForReadyToReceive(SKY7635121Driver *devp) {
    uint8_t slave_status;
    bool error, txDataReady;
    uint8_t try_cnt = 0;

    do {
        slave_status = sky7635121GetSlaveStatus(devp);
        error = slave_status & SPI_SLAVE_STS_ERRORS_MASK;
        txDataReady = slave_status & SPI_SLAVE_STS_TXDATAREADY;

        if(error) {
            return FALSE;
        }

        if(try_cnt++ > AIS_CMD_MAX_TRY_NUM) {
            return FALSE;
        }
        chThdSleepMilliseconds(5);
    } while(!txDataReady);

    return TRUE;
}

static bool sky7635121AISSend(SKY7635121Driver *devp, ais_msg_hdr_t *hdr,
                              uint8_t *buf, uint8_t len) {
    // parameter checking
    osalDbgCheck((devp != NULL) && (hdr != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // check the SPI Slave Status first
    if(!sky7635121WaitForReadyToSend(devp)) {
        return FALSE;
    }

    // copy message header to ais_tx_buffer
    memcpy(ais_tx_buffer, (uint8_t *)hdr, sizeof(ais_msg_hdr_t));

    // copy data to ais_tx_buffer if needed
    if(buf != NULL && len != 0) {
        memcpy(ais_tx_buffer + sizeof(ais_msg_hdr_t), buf, len);
    }

#if (SKY7635121_USE_SPI) || defined(__DOXYGEN__)
    sky7635121SpiWrite(devp, ais_tx_buffer, sizeof(ais_msg_hdr_t) + len);
#endif /* SKY7635121_USE_SPI */

    return TRUE;
}

static bool sky7635121AISReceive(SKY7635121Driver *devp, ais_msg_hdr_t *hdr,
                                 uint8_t *buf, uint8_t len) {

    // parameter checking
    osalDbgCheck((devp != NULL) && (hdr != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // check the SPI Slave Status first and wait for TX data ready
    if(!sky7635121WaitForReadyToReceive(devp)) {
        return FALSE;
    }

#if (SKY7635121_USE_SPI) || defined(__DOXYGEN__)
    sky7635121SpiRead(devp, ais_rx_buffer, sizeof(ais_msg_hdr_t) + len);
#endif /* SKY7635121_USE_SPI */

    // copy message header from ais_rx_buffer, skip the first byte SlaveStatus
    memcpy((uint8_t *)hdr, ais_rx_buffer + 1, sizeof(ais_msg_hdr_t));

    // copy data from ais_rx_buffer if needed, skip the first byte SlaveStatus
    if(buf != NULL && len != 0) {
        memcpy(buf, ais_rx_buffer + sizeof(ais_msg_hdr_t) + 1, len);
    }
    return TRUE;
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
/**
 * @brief   Read sky7635121 memory data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the memory address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadMem(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_MEMORY_MODULE;
    hdr.Opcode = CmdOpcode_Read;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_MEMORY_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   write data to sky7635121 memory.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space storing the data to write
 * @param[in]  addr             the memory address to write
 * @param[in]  len              the size of data to write
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121WriteMem(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_MEMORY_MODULE;
    hdr.Opcode = CmdOpcode_Write;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_MEMORY_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   Read sky7635121 flash data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the flash address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadFlash(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_FLASH_MODULE;
    hdr.Opcode = CmdOpcode_Read;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_FLASH_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   write data to sky7635121 flash.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space storing the data to write
 * @param[in]  addr             the flash address to write
 * @param[in]  len              the size of data to write
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121WriteFlash(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_FLASH_MODULE;
    hdr.Opcode = CmdOpcode_Write;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_FLASH_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   set sky7635121 parameter.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[in]  moduleID         the module ID to set the parameter
 * @param[in]  paramID          the Parameter ID to set
 * @param[in]  paramVal         the value of the parameter to set
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121SetParameter(SKY7635121Driver *devp, uint8_t moduleID,
                                 uint32_t paramID, uint32_t paramVal) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck(devp != NULL);

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = moduleID;
    hdr.Opcode = CmdOpcode_SetParameter;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_NoDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = paramID;
    hdr.Param1 = paramVal;

    if(FALSE == sky7635121AISSend(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (moduleID != hdr.Dest) &&
       (paramID != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   get sky7635121 parameter.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[in]  moduleID         the module ID to get the parameter
 * @param[in]  paramID          the Parameter ID to get
 * @param[out] paramVal         pointer the buffer to store the value of the parameter
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121GetParameter(SKY7635121Driver *devp, uint8_t moduleID,
                                 uint32_t paramID, uint32_t *paramVal) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck(devp != NULL);

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = moduleID;
    hdr.Opcode = CmdOpcode_GetParameter;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_NoDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = paramID;
    hdr.Param1 = 0;

    if(FALSE == sky7635121AISSend(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (moduleID != hdr.Dest) &&
       (paramID != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    // since no error occurs, send the parameter value read back outside
    *paramVal = hdr.Param1;

    return AISErrMsg_Success;
}

/**
 * @brief   Read sky7635121 mac data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the mac address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadMac(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_MAC_MODULE;
    hdr.Opcode = CmdOpcode_Read;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_MAC_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   Read sky7635121 AVBoot data.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space to store the data read back
 * @param[in]  addr             the avboot address to read
 * @param[in]  len              the size of data to read
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121ReadAvBoot(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_AVBOOT_MODULE;
    hdr.Opcode = CmdOpcode_Read;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_AVBOOT_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}

/**
 * @brief   write data to sky7635121 avboot.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 * @param[out] buf              pointer to the memory space storing the data to write
 * @param[in]  addr             the avboot address to write
 * @param[in]  len              the size of data to write
 *
 * @return                      the ais command flag bytes.
 * @retval AISErrMsg_Success    if the function succeeded.
 */
ais_msg_t sky7635121WriteAvBoot(SKY7635121Driver *devp, uint8_t *buf, uint32_t addr, uint8_t len) {
    ais_msg_hdr_t hdr;

    // parameter checking
    osalDbgCheck((devp != NULL) && (buf != NULL) && (len < AIS_CMD_BUFFER_SIZE - sizeof(ais_msg_hdr_t)));

    // send command
    hdr.Source = SKY7635121_SPIS_MODULE;
    hdr.Dest = SKY7635121_AVBOOT_MODULE;
    hdr.Opcode = CmdOpcode_Write;
    hdr.Reserved = 0;
    hdr.BufferFlag = BufferFlag_UseDataBuffer;
    hdr.Status = ResultStatus_Success;
    hdr.Type = MessageType_Query;
    hdr.Param0 = addr;
    hdr.Param1 = (uint32_t)len;

    if(FALSE == sky7635121AISSend(devp, &hdr, buf, len)) {
        return AISErrMsg_CommFailure;
    }

    // receive the response
    if(FALSE == sky7635121AISReceive(devp, &hdr, NULL, 0)) {
        return AISErrMsg_CommFailure;
    }

    if(ResultStatus_Success != hdr.Status) {
        return (ais_msg_t)hdr.Status;
    }

    if(MessageType_Result != hdr.Type) {
        return AISErrMsg_Failed;
    }

    if((SKY7635121_SPIS_MODULE != hdr.Source) && (SKY7635121_AVBOOT_MODULE != hdr.Dest) &&
       (addr != hdr.Param0)) {
        return AISErrMsg_Failed;
    }

    return AISErrMsg_Success;
}


/**
 * @brief   Authentication Procedure of sky76351-21 chipset.
 *
 * @param[out] devp             pointer to the @p SKY7635121Driver object
 *
 * @return                      TRUE: Authentication Procedure Succeed, FALSE: Authentication Procedure Fail.
 */
bool sky7635121AuthProcedure(SKY7635121Driver *devp) {
    uint8_t hashKey = 0;
    uint32_t auth_sts = 0;
    uint8_t challenge_phrase[AVBOOT_MODULE_CHALLENGE_PHRASE_LEN];
    uint8_t serial_num[AVBOOT_MODULE_SERIAL_NUM_LEN];

    // 1. Request auth status and get unauthorized reply (
    if(AISErrMsg_Success != sky7635121GetParameter(devp, SKY7635121_AVBOOT_MODULE,
                                                   CmdParameter_AVBOOT_AUTH_STS, &auth_sts)) {
        return FALSE;
    }

    // Already authorized
    if(auth_sts == 0x00000001) {
        return TRUE;
    }

    // 2. Request challenge phrase
    if(AISErrMsg_Success != sky7635121ReadAvBoot(devp, challenge_phrase,
                                                 AVBOOT_MODULE_CHALLENGE_PHRASE_ADDR, AVBOOT_MODULE_CHALLENGE_PHRASE_LEN)) {
        return FALSE;
    }

    // 3. Get serial number
    if(AISErrMsg_Success != sky7635121ReadAvBoot(devp, serial_num,
                                                 AVBOOT_MODULE_SERIAL_NUM_ADDR, AVBOOT_MODULE_SERIAL_NUM_LEN)) {
        return FALSE;
    }

    // 4. Get hash key
    for(uint8_t i = 0; i < AVBOOT_MODULE_SERIAL_NUM_LEN; i++) {
        if(serial_num[i] != 0) {
            hashKey += serial_num[i];
        } else {
            break;
        }
    }

    // 5. challenge phrase XOR'd with hashKey
    for(uint8_t i = 0; i < AVBOOT_MODULE_CHALLENGE_PHRASE_LEN; i++) {
        challenge_phrase[i] = challenge_phrase[i] ^ hashKey;
    }

    // 6. Send auth response
    if(AISErrMsg_Success != sky7635121WriteAvBoot(devp, challenge_phrase,
                                                  AVBOOT_MODULE_AUTH_RESPONSE_ADDR, AVBOOT_MODULE_AUTH_RESPONSE_LEN)) {
        return FALSE;
    }

    // 7. validate auth status - get authorized response
    if(AISErrMsg_Success != sky7635121GetParameter(devp, SKY7635121_AVBOOT_MODULE,
                                                   CmdParameter_AVBOOT_AUTH_STS, &auth_sts)) {
        return FALSE;
    }

    return (auth_sts == 0x00000001);
}

/**
 * @brief   Check if the instance is ready.
 *
 * @param[in] devp      pointer to the @p SKY7635121Driver object
 *
 * @return              TRUE: instance is ready, FALSE: instance is not ready.
 */
bool isSky7635121Ready(SKY7635121Driver *devp) {
    return (devp->state == SKY7635121_READY);
}

/**
 * @brief   Initializes an instance.
 *
 * @param[out] devp     pointer to the @p SKY7635121Driver object
 *
 * @init
 */
void sky7635121ObjectInit(SKY7635121Driver *devp) {

    devp->vmt = &vmt_device;

    devp->config = NULL;

    devp->state = SKY7635121_STOP;
}

/**
 * @brief   Configures and activates SKY7635121 Complex Driver peripheral.
 *
 * @param[in] devp      pointer to the @p SKY7635121Driver object
 * @param[in] config    pointer to the @p SKY7635121Config object
 *
 * @api
 */
void sky7635121Start(SKY7635121Driver *devp, const SKY7635121Config *config) {

    osalDbgCheck((devp != NULL) && (config != NULL));
    osalDbgAssert((devp->state == SKY7635121_STOP) || (devp->state == SKY7635121_READY),
                  "sky7635121Start(), invalid state");
    devp->config = config;

    /* Checking if the device is ready.*/
    if(SPI_SLAVE_STS_ERRORS_MASK & sky7635121GetSlaveStatus(devp)) {
        return;
    }

    devp->state = SKY7635121_READY;
}

/**
 * @brief   Deactivates the SKY7635121 Complex Driver peripheral.
 *
 * @param[in] devp       pointer to the @p SKY7635121Driver object
 *
 * @api
 */
void sky7635121Stop(SKY7635121Driver *devp) {
    osalDbgCheck(devp != NULL);
    osalDbgAssert((devp->state == SKY7635121_STOP) || (devp->state == SKY7635121_READY),
                  "sky7635121Stop(), invalid state");

    if(devp->state == SKY7635121_READY) {
        spiStop(devp->config->spip);
    }

    devp->state = SKY7635121_STOP;
}
/** @} */
