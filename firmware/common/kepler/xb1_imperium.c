
#include <string.h>
#include "hal.h"
#include "endian.h"
#include "debug.h"
#include "xb1_imperium.h"

// NOTE: This driver is split into layers according to the Imperium/Infineon spec: phy, link, network, transport, apdu.

// NOTE: This driver favors stabilty/readability over efficiency. In most cases frames are redudntaly copied onto each layer's
// stack rather than building them in place. This is obviously wasteful but it far easier to read and maintain, and this driver
// only gets used a bit during GIP enumeration and then goes dormant so efficiecy isn't particularly valuable.

// Physical layer //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_DATA                0x80
#define REG_DATA_REG_LEN        0x81
#define REG_I2C_STATE           0x82

// This register holds the maximum data register (Addr 0x80) length.
// The allowed values are 0x0010 up to 0x011D.
// Maybe we should consider the link frame header (3 bytes) and CRC (2 bytes)
#define REG_DATA_REG_LEN_MIN    (0x15)
#define REG_DATA_REG_LEN_MAX    (0x0122)

#define REG_I2CSTATE_BUSY       0x80000000
#define REG_I2CSTATE_RESP_RDY   0x40000000

#define XB_I2C_SETUP_TIME_US    100
#define XB_I2C_GUARD_TIME_US    250
#define XB_I2C_RTO_TIME         OSAL_MS2I(100) //response timeout from chip
#define XB_I2C_TIMEOUT_MS       OSAL_MS2I(50)
#define IMPERIUM_I2C_ADDR       (0x40 >> 1)
#if (STM32_I2C_USE_DMA == TRUE)
#define PHY_BUFFER_SIZE_MAX     (2048)
#endif
#define IMPERIUM_OP_MAX_RETRIES (10)

typedef enum {
    FLAG_BLOCK_UNTIL_SET,
    FLAG_BLOCK_UNTIL_CLEAR,
} phyFlagMode_t;

#pragma pack(push,1)
typedef struct {
    uint8_t addr;
    uint8_t payload[];
} phyTxFrame_t;
#pragma pack(pop)

static I2CDriver *I2C = NULL;
static uint8_t I2C_address = 0x0;
static const I2CConfig *I2C_config = NULL;

static ioportid_t reset_gpio = NULL;
static iopadid_t reset_pin;

static size_t max_frame_bytes = 0; // MTU for Imperium I2C link; should be read from IC.
#if (STM32_I2C_USE_DMA == TRUE)
// the buffer used for i2c transfer when DMA is enabled
static uint8_t phy_buf[PHY_BUFFER_SIZE_MAX] __attribute__((section(".nocache")));
#endif

static void phy_init(I2CDriver *i2c, uint8_t i2c_address, const I2CConfig *i2c_config) {
    I2C = i2c;
    I2C_address = i2c_address;
    I2C_config = i2c_config;
}

static imperiumStatus_t phy_xfer(const uint8_t *tx_buf, size_t tx_bytes, uint8_t *rx_buf, size_t rx_bytes) {
    //Dbg_printf(DBG_IMPERIUM, "phy: xfer w(%x, %d) r(%x, %d)", tx_buf, tx_bytes, rx_buf, rx_bytes);
    msg_t result;

    chThdSleepMicroseconds(XB_I2C_GUARD_TIME_US);

#if (STM32_I2C_USE_DMA == TRUE)
    // copy the data to buffer first
    memcpy(phy_buf, tx_buf, tx_bytes);
#endif

    // Don't do it all in one transaction because according to spec we have to wait 100us after register setup...
    i2cAcquireBus(I2C);
#if (STM32_I2C_USE_DMA == TRUE)
    result = i2cMasterTransmitTimeout(I2C, I2C_address, phy_buf, tx_bytes, NULL, 0, XB_I2C_RTO_TIME);
#else
    result = i2cMasterTransmitTimeout(I2C, I2C_address, tx_buf, tx_bytes, NULL, 0, XB_I2C_RTO_TIME);
#endif
    if(result != MSG_OK) {
        i2cStop(I2C);
        i2cStart(I2C, I2C_config);
        i2cReleaseBus(I2C);
        Dbg_printf(DBG_IMPERIUM, "phy: I2C TX failure");
        return IMPERIUM_ERROR;
    }
    i2cReleaseBus(I2C);
    Dbg_printf(DBG_IMPERIUM, "phy: tx(%d)[%b]", tx_bytes, tx_buf, tx_bytes);

    // xbox spec requires us to wait {SETUP_TIME} = 100us between write setup and read
    if(rx_bytes > 0) {
        chThdSleepMicroseconds(XB_I2C_SETUP_TIME_US);

        i2cAcquireBus(I2C);
#if (STM32_I2C_USE_DMA == TRUE)
        result = i2cMasterReceiveTimeout(I2C, I2C_address, phy_buf, rx_bytes, XB_I2C_RTO_TIME);
#else
        result = i2cMasterReceiveTimeout(I2C, I2C_address, rx_buf, rx_bytes, XB_I2C_RTO_TIME);
#endif
        if(result != MSG_OK) {
            i2cStop(I2C);
            i2cStart(I2C, I2C_config);
            i2cReleaseBus(I2C);
            Dbg_printf(DBG_IMPERIUM, "phy: I2C RX failure");
            return IMPERIUM_ERROR;
        }
        i2cReleaseBus(I2C);
#if (STM32_I2C_USE_DMA == TRUE)
        // copy the data read back to rx_buf
        memcpy(rx_buf, phy_buf, rx_bytes);
#endif
        Dbg_printf(DBG_IMPERIUM, "phy: rx(%d)[%b]", rx_bytes, rx_buf, rx_bytes);
    }

    return IMPERIUM_OK;
}

static imperiumStatus_t phy_reg_read(uint8_t addr, uint8_t *rx_buf, size_t rx_bytes) {
    imperiumStatus_t status = phy_xfer(&addr, sizeof(addr), rx_buf, rx_bytes);
    if(status != IMPERIUM_OK) {
        return status;
    }

    buf_reverse(rx_buf, rx_bytes);
    return IMPERIUM_OK;
}

static imperiumStatus_t phy_wait_flag(uint32_t flag, phyFlagMode_t mode, uint32_t *resp_len) {
    uint32_t state;
    int retries = IMPERIUM_OP_MAX_RETRIES;

    phy_reg_read(REG_I2C_STATE, (uint8_t*)&state, sizeof(state));

    switch(mode) {
        case FLAG_BLOCK_UNTIL_SET:
            while(((state & flag) == 0) && (retries--)) {
                chThdSleepMilliseconds(5);
                phy_reg_read(REG_I2C_STATE, (uint8_t*)&state, sizeof(state));
            }
            break;

        case FLAG_BLOCK_UNTIL_CLEAR:
            while(((state & flag) != 0) && (retries--)) {
                chThdSleepMilliseconds(5);
                phy_reg_read(REG_I2C_STATE, (uint8_t*)&state, sizeof(state));
            }
            break;

        default:
            Dbg_printf(DBG_ERROR, "phy: invalid mode parameter: %d", mode);
            return IMPERIUM_ERROR;
    }

    if(retries > 0) {
        if(NULL != resp_len) {
            // lower half of status register is size of response if available
            *resp_len = (state & 0xFFFF);
        }
        return IMPERIUM_OK;
    } else {
        Dbg_printf(DBG_WARN, "phy: check flag %d times fail.", IMPERIUM_OP_MAX_RETRIES);
        return IMPERIUM_ERROR;
    }
}

static imperiumStatus_t phy_data_send(uint8_t *payload, size_t payload_bytes) {
    Dbg_printf(DBG_IMPERIUM, "phy: data_tx(%d)", payload_bytes);
    uint8_t frame_buf[sizeof(phyTxFrame_t) + payload_bytes];
    phyTxFrame_t *frame = (phyTxFrame_t*)frame_buf;
    imperiumStatus_t status = IMPERIUM_OK;

    frame->addr = REG_DATA;
    memcpy(frame->payload, payload, payload_bytes);

    status = phy_wait_flag(REG_I2CSTATE_BUSY, FLAG_BLOCK_UNTIL_CLEAR, NULL);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "phy: phy_wait_flag() failed (%d)", status);
        return status;
    }

    status = phy_xfer((uint8_t*)frame, sizeof(frame_buf), NULL, 0);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "phy: phy_xfer() failed (%d)", status);
        return status;
    }

    return IMPERIUM_OK;
}

static imperiumStatus_t phy_data_recv(uint8_t *payload, size_t *payload_bytes) {
    Dbg_printf(DBG_IMPERIUM, "phy: data_rx(%d)", *payload_bytes);

    uint32_t response_bytes;
    imperiumStatus_t status = IMPERIUM_OK;

    status = phy_wait_flag(REG_I2CSTATE_RESP_RDY, FLAG_BLOCK_UNTIL_SET, &response_bytes);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "phy: phy_wait_flag() failed (%d)", status);
        return status;
    }

    if(response_bytes < *payload_bytes) {
        *payload_bytes = response_bytes;
    }

    uint8_t reg_addr = REG_DATA;
    status = phy_xfer(&reg_addr, sizeof(reg_addr), payload, *payload_bytes);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "phy: phy_xfer() failed (%d)", status);
        return status;
    }

    return IMPERIUM_OK;
}

static size_t phy_max_payload_bytes(void) {
    if(max_frame_bytes == 0) {
        int retries = IMPERIUM_OP_MAX_RETRIES;
        uint16_t reg_data_reg_len;

        while(retries--) {
            imperiumStatus_t status = phy_reg_read(REG_DATA_REG_LEN, (uint8_t*)&reg_data_reg_len, sizeof(reg_data_reg_len));
            if(status != IMPERIUM_OK) {
                Dbg_printf(DBG_ERROR, "phy: phy_reg_read() failed (%d)", status);
                return 0;
            }
            if((reg_data_reg_len >= REG_DATA_REG_LEN_MIN)
               && (reg_data_reg_len <= REG_DATA_REG_LEN_MAX)) {
                break;
            }
        }

        if(retries > 0) {
            max_frame_bytes = (size_t)reg_data_reg_len;
        } else {
            Dbg_printf(DBG_ERROR, "Get REG_DATA_REG_LEN %d times fail, %d", IMPERIUM_OP_MAX_RETRIES, reg_data_reg_len);
            return 0;
        }
    }

    return max_frame_bytes;
}

// Link layer //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define FRAME_FTYPE         0x80
#define FRAME_FTYPE_CONTROL 0x80
#define FRAME_FTYPE_DATA    0x00
#define FRAME_SEQCTR        0x60
#define FRAME_SEQCTR_ACK    0x00
#define FRAME_SEQCTR_NACK   0x20
#define FRAME_SEQCTR_RESET  0x40
#define FRAME_FRNR          0x0c
#define FRAME_FRNR_SHIFT    2
#define FRAME_ACKNR         0x03
#define FRAME_ACKNR_SHIFT   0

#define LINK_CRC_BYTES 2

#pragma pack(push,1)
typedef struct {
    uint8_t fctr;
    uint16_t bytes;
    uint8_t payload[];

    // 16-bit checksum at the end of frame, but can't be included in
    // typedef due to variable length payload[] before it.
    //uint16_t crc;
} linkFrame_t;
#pragma pack(pop)

static uint8_t link_ack_frame = 0x0;
static bool link_ack = false;
static uint8_t frnr = 0;

static size_t link_max_payload_bytes(void) {
    return (phy_max_payload_bytes() + sizeof(linkFrame_t) + LINK_CRC_BYTES);
}

static uint16_t running_CRC(uint16_t seed, uint8_t c) {
    uint32_t h1, h2, h3, h4;

    h1 = (seed ^ c) & 0xFF;
    h2 = h1 & 0x0F;
    h3 = (h2 << 4) ^ h1;
    h4 = h3 >> 4;

    return (((((h3 << 1) ^ h4) << 4) ^ h2) << 3) ^ h4 ^ (seed >> 8);
}

static uint16_t calc_CRC(uint8_t *buf, size_t bytes) {
    unsigned int i;
    uint16_t seed = 0;

    for(i = 0; i < bytes; i++) {
        seed = running_CRC(seed, buf[i]);
    }

    return seed;
}

static uint16_t frame_crc_get(const linkFrame_t *frame) {
    uint16_t bytes = frame->bytes;
    BYTE_SWAP(bytes);

    uint8_t *crc = (uint8_t*)frame + sizeof(*frame) + bytes;
    return (uint16_t)(crc[0] << 8) | crc[1];
}

static bool frame_crc_check(const linkFrame_t *frame) {
    uint16_t bytes = frame->bytes;
    BYTE_SWAP(bytes);

    uint16_t crc_expected = frame_crc_get(frame);
    uint16_t crc_calculated = calc_CRC((uint8_t*)frame, sizeof(*frame) + bytes);

    return (crc_calculated == crc_expected);
}

static void frame_crc_set(linkFrame_t *frame) {
    uint16_t bytes = frame->bytes;
    BYTE_SWAP(bytes);

    uint16_t crc_calc = calc_CRC((uint8_t*)frame, sizeof(*frame) + bytes);
    uint8_t *crc = (uint8_t*)frame + sizeof(*frame) + bytes;

    crc[0] = crc_calc >> 8;
    crc[1] = crc_calc & 0xff;
}

static imperiumStatus_t link_send(uint8_t *payload, size_t payload_bytes) {
    uint8_t frame_buf[sizeof(linkFrame_t) + payload_bytes + LINK_CRC_BYTES];
    linkFrame_t *frame = (linkFrame_t*)frame_buf;

    frame->fctr = 0;

    if(payload_bytes > 0) {
        frame->fctr |= FRAME_FTYPE_DATA;
        frame->fctr |= frnr;
        frnr = (frnr + (1 << FRAME_FRNR_SHIFT)) & FRAME_FRNR;
    } else {
        frame->fctr |= FRAME_FTYPE_CONTROL;
    }

    if(link_ack) {
        frame->fctr |= (link_ack_frame & FRAME_ACKNR);
        link_ack = false;
    }

    frame->bytes = payload_bytes;
    memcpy(frame->payload, payload, payload_bytes);
    BYTE_SWAP(frame->bytes);
    frame_crc_set(frame);

    imperiumStatus_t status = phy_data_send((uint8_t*)frame, sizeof(frame_buf));
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "link: phy_data_send() failed (%d)", status);
        return status;
    }

    return IMPERIUM_OK;
}

static imperiumStatus_t link_recv(uint8_t *payload, size_t *payload_bytes) {
    if(link_ack) {
        // send an ACK frame
        link_send(NULL, 0);
    }

    uint8_t frame_buf[sizeof(linkFrame_t) + *payload_bytes + LINK_CRC_BYTES];
    linkFrame_t *frame = (linkFrame_t*)frame_buf;
    size_t rx_bytes = 0;

    bool repeat;
    do {
        repeat = false;
        rx_bytes = sizeof(frame_buf);
        imperiumStatus_t status = phy_data_recv(frame_buf, &rx_bytes);
        if(status != IMPERIUM_OK) {
            Dbg_printf(DBG_IMPERIUM, "link: phy_data_recv() failed (%d)", status);
            return status;
        }

        if((frame->fctr & FRAME_FTYPE) == FRAME_FTYPE_DATA) {
            link_ack = true;
            link_ack_frame = (frame->fctr & FRAME_FRNR) >> FRAME_FRNR_SHIFT;
        } else {
            if((frame->fctr & FRAME_SEQCTR) == FRAME_SEQCTR_NACK) {
                Dbg_printf(DBG_IMPERIUM, "link: NAK recvd");
                return IMPERIUM_ERROR;
            }

            Dbg_printf(DBG_IMPERIUM, "link: ACK recvd");
            // Was expecting a data frame but got an ACK control frame instead so request another one.
            repeat = true;
            continue;
        }

        if(!frame_crc_check(frame)) {
            Dbg_printf(DBG_IMPERIUM, "link: crc mismatch");
            return IMPERIUM_ERROR;
        }

    } while(repeat);

    BYTE_SWAP(frame->bytes);
    *payload_bytes = rx_bytes - sizeof(linkFrame_t) - LINK_CRC_BYTES;
    memcpy(payload, frame->payload, *payload_bytes);

    return IMPERIUM_OK;
}

// Network layer ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Network layer left unimplemented. It's not needed in this case since all it would do
// is set the channel bits to zero, which is their default state. No headers or
// anything need to be added since the network channel bits share space in the
// transport header.

// Transport layer /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CHAIN_MASK      0x7
#define CHAIN_SINGLE    0x0
#define CHAIN_START     0x1
#define CHAIN_MIDDLE    0x2
#define CHAIN_END       0x4

#pragma pack(push,1)
typedef struct {
    uint8_t chain;
    uint8_t payload[];
} transportFrame_t;
#pragma pack(pop)

typedef enum {
    CHAINSTATE_FIRST,
    CHAINSTATE_MIDDLE,
    CHAINSTATE_LAST,
} transportChainState_t;

// simplified implimentation, only handles single-frame transfers
static imperiumStatus_t transport_send(uint8_t *payload, size_t payload_bytes) {
    if(payload_bytes > (link_max_payload_bytes() - sizeof(transportFrame_t))) {
        return IMPERIUM_ERROR;
    }

    uint8_t frame_buf[sizeof(transportFrame_t) + payload_bytes];
    transportFrame_t *frame = (transportFrame_t*)frame_buf;

    frame->chain = 0;
    memcpy(frame->payload, payload, payload_bytes);

    imperiumStatus_t status = link_send((uint8_t*)frame, sizeof(frame_buf));
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "transport: link_send() failed (%d)", status);
        return status;
    }

    return IMPERIUM_OK;
}

static imperiumStatus_t transport_recv(uint8_t *payload, size_t *payload_bytes) {
    chThdSleepMilliseconds(50);

    size_t frame_bytes_max = link_max_payload_bytes();
    uint8_t frame_buf[frame_bytes_max];
    transportFrame_t *frame = (transportFrame_t*)frame_buf;
    size_t rx_bytes_total = 0;

    transportChainState_t state = CHAINSTATE_FIRST;
    while(state != CHAINSTATE_LAST) {
        size_t rx_bytes = frame_bytes_max - sizeof(transportFrame_t);
        if((rx_bytes_total + rx_bytes) > *payload_bytes) {
            rx_bytes = *payload_bytes - rx_bytes_total;
        }

        size_t frame_bytes = rx_bytes + sizeof(transportFrame_t);
        imperiumStatus_t status = link_recv(frame_buf, &frame_bytes);
        if(status != IMPERIUM_OK) {
            Dbg_printf(DBG_IMPERIUM, "transport: link_recv() failed (%d)", status);
            return status;
        }
        rx_bytes = frame_bytes - sizeof(transportFrame_t);

        memcpy(payload, frame->payload, rx_bytes);
        payload += rx_bytes;
        rx_bytes_total += rx_bytes;

        switch(frame->chain & CHAIN_MASK) {
            case CHAIN_SINGLE:
                if(state != CHAINSTATE_FIRST) {
                    Dbg_printf(DBG_IMPERIUM, "transport: chain error");
                    return IMPERIUM_ERROR;
                }
                state = CHAINSTATE_LAST;
                break;

            case CHAIN_START:
                if(state != CHAINSTATE_FIRST) {
                    Dbg_printf(DBG_IMPERIUM, "transport: chain error");
                    return IMPERIUM_ERROR;
                }
                state = CHAINSTATE_MIDDLE;
                break;

            case CHAIN_MIDDLE:
                if(state != CHAINSTATE_MIDDLE) {
                    Dbg_printf(DBG_IMPERIUM, "transport: chain error");
                    return IMPERIUM_ERROR;
                }
                state = CHAINSTATE_MIDDLE;
                break;

            case CHAIN_END:
                if(state != CHAINSTATE_MIDDLE) {
                    Dbg_printf(DBG_IMPERIUM, "transport: chain error");
                    return IMPERIUM_ERROR;
                }
                state = CHAINSTATE_LAST;
                break;

        }
    }

    *payload_bytes = rx_bytes_total;
    return IMPERIUM_OK;
}

// App layer ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define IMPERIUM_CMD_GET_PARAM                  0x05
#define IMPERIUM_PARAM_PUID                     2 //  product unique id (160 bits)
#define IMPERIUM_PARAM_PUID_LEN                 IMPERIUM_PUID_BYTES //  160 bits
#define IMPERIUM_PARAM_MS_COPYRIGHT_STRING      9 //  "C 2012 Microsoft Corporation. All rights reserved."
#define IMPERIUM_PARAM_MS_COPYRIGHT_STRING_LEN  50
static bool imperium_present = false;

// Some of the security things take a while (e.g. 300-400ms)
#define APDU_TIMEOUT MS2ST(750)

#pragma pack(push, 1)
typedef struct {
    uint8_t bCmd;
    uint16_t wParam;
    uint16_t wLength;
    uint8_t payload[];
} apduFrame_t;
#pragma pack(pop)

// simplified implementation, only does commands, not data
static imperiumStatus_t apdu_send(uint8_t tx_command, uint16_t tx_param, uint16_t rx_bytes) {
    apduFrame_t frame;
    frame.bCmd = tx_command;
    frame.wParam = tx_param;
    frame.wLength = rx_bytes;
    BYTE_SWAP(frame.wParam);
    BYTE_SWAP(frame.wLength);

    Dbg_printf(DBG_IMPERIUM, "apdu: send(%d)[%b]", sizeof(frame), &frame, sizeof(frame));
    imperiumStatus_t status = transport_send((uint8_t*)&frame, sizeof(frame));
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "apdu: transport_send() failed (%d)", status);
        return status;
    }

    return IMPERIUM_OK;
}

static imperiumStatus_t apdu_recv(uint8_t *payload, size_t *payload_bytes) {
    uint8_t frame_buf[sizeof(apduFrame_t) + *payload_bytes];
    size_t rx_bytes = sizeof(frame_buf);

    imperiumStatus_t status = transport_recv(frame_buf, &rx_bytes);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "apdu: transport_recv() failed (%d)", status);
        return status;
    }

    apduFrame_t *frame = (apduFrame_t*)frame_buf;
    Dbg_printf(DBG_IMPERIUM, "apdu: recv(%d)[%b]", rx_bytes, frame, rx_bytes);
    BYTE_SWAP(frame->wParam);
    BYTE_SWAP(frame->wLength);

    if(frame->wLength != (rx_bytes - sizeof(apduFrame_t))) {
        Dbg_printf(DBG_IMPERIUM, "apdu: wrong response size: wLength(%d) rx_bytes(%d)", frame->wLength, rx_bytes);
        return IMPERIUM_ERROR;
    }

    memcpy(payload, frame->payload, frame->wLength);
    *payload_bytes = frame->wLength;

    return IMPERIUM_OK;
}

// Public //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

imperiumStatus_t Imperium_Init(I2CDriver *i2c, const I2CConfig *i2c_config, ioportid_t gpio, iopadid_t pin) {
    phy_init(i2c, IMPERIUM_I2C_ADDR, i2c_config);

    reset_gpio = gpio;
    reset_pin = pin;

    // Reset pin
    if(NULL != reset_gpio) {
        palClearPad(reset_gpio, reset_pin);
        chThdSleepMilliseconds(5);
        palSetPad(reset_gpio, reset_pin);
        chThdSleepMilliseconds(10);
    }

    phy_max_payload_bytes();

    imperiumStatus_t status = apdu_send(IMPERIUM_CMD_GET_PARAM, IMPERIUM_PARAM_MS_COPYRIGHT_STRING, IMPERIUM_PARAM_MS_COPYRIGHT_STRING_LEN);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "init: apdu_send() failed (%d)", status);
        return status;
    }

    uint8_t response_buf[IMPERIUM_PARAM_MS_COPYRIGHT_STRING_LEN];
    size_t rx_bytes = sizeof(response_buf);

    status = apdu_recv(response_buf, &rx_bytes);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "init: apdu_recv() failed (%d)", status);
        return IMPERIUM_ERROR;
    }

    Dbg_printf(DBG_IMPERIUM, "ID('%s')", response_buf);
    return IMPERIUM_OK;
}

imperiumStatus_t Imperium_PUID_get(uint8_t *puid, size_t bytes) {
    // bytes should always be IMPERIUM_PUID_LEN so it doesn't really need to be a parameter,
    // but this enforces that the caller knows how many bytes are written to *puid since
    // it's otherwise ambiguous and could cause all sorts of memory havoc.
    ASSERT(bytes == IMPERIUM_PARAM_PUID_LEN);

    imperiumStatus_t status = apdu_send(IMPERIUM_CMD_GET_PARAM, IMPERIUM_PARAM_PUID, bytes);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "PUID_get: apdu_send() failed (%d)", status);
        return status;
    }

    status = apdu_recv(puid, &bytes);
    if(status != IMPERIUM_OK) {
        Dbg_printf(DBG_IMPERIUM, "PUID_get: apdu_recv() failed (%d)", status);
        return status;
    }

    return IMPERIUM_OK;
}

imperiumStatus_t Imperium_APDUpassthrough_send(uint8_t *buf, size_t bytes) {
    Dbg_printf(DBG_IMPERIUM, "apdu: pasthrough_send(%d)[%b]", bytes, buf, bytes);
    return transport_send(buf, bytes);
}

imperiumStatus_t Imperium_APDUpassthrough_recv(uint8_t *buf, size_t *bytes) {
    imperiumStatus_t status = transport_recv(buf, bytes);
    Dbg_printf(DBG_IMPERIUM, "apdu: pasthrough_recv(%d)[%b]", *bytes, buf, *bytes);
    return status;
}

imperiumStatus_t Imperium_ShutDown(void) {
    link_ack = false;
    link_ack_frame = 0x0;
    frnr = 0;
    max_frame_bytes = 0;
    if(NULL != reset_gpio) {
        palClearPad(reset_gpio, reset_pin);
    }
    chThdSleepMilliseconds(5);

    return IMPERIUM_OK;
}

bool imperium_Start(I2CDriver *i2c, const I2CConfig *i2c_config) {

    // Boot XB1 Imperium security IC if present
    Dbg_printf(DBG_INFO, "Starting imperium...");
    int retries = 5;
    while(retries-- && Imperium_Init(i2c, i2c_config, NULL, 0) != IMPERIUM_OK) {
        Dbg_printf(DBG_WARN, "Imperium Init failed, retrying...");
        chThdSleepMilliseconds(25);
    }
    if(retries > 0) {
        Dbg_printf(DBG_INFO, "Imperium is present.");
        imperium_present = true;
        return true;
    } else {
        Dbg_printf(DBG_WARN, "Imperium not present.");
        imperium_present = false;
        return false;
    }
}

uint8_t getModelID(void) {
#if defined(PROJECT_BE)
    if(imperium_present) {
        return MODEL_ID_XB;
    } else {
        return MODEL_ID_PS;
    }
#else
    return 0;
#endif
}


