/**
 * @file    qualcomm.h
 * @brief   Qualcomm Bluetooth Wireless Audio Processor header.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */
#ifndef _QUALCOMM_H_
#define _QUALCOMM_H_

#include "bt_comm.h"

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/
#define QCC3024_CHIP_ID                                                 (0x01)
#define QCC5126_CHIP_ID                                                 (0x02)

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/
/**
 * @name    Configuration options
 * @{
 */
/**
 * @brief   Qualcomm Mutexes APIs.
 * @details If set to @p TRUE the mutexes APIs are included in the driver..
 * @note    The default is @p TRUE.
 */
#if !defined(QUALCOMM_USE_MUTEX)
#define QUALCOMM_USE_MUTEX                                              TRUE
#endif

/** @} */


/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/


/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief   Set Qualcomm Bluetooth enter pairing mode.
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommEnterPairing(void);

/**
 * @brief   Cancel Qualcomm Bluetooth pairing mode.
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommCancelPairing(void);

/**
 * @brief   Get Qualcomm Bluetooth bd Addr.
 *
 * @param[in] void      None
 *
 * @return              The bd address of Qualcomm Bluetooth chipset
 *                      {0xFFFFFFFF, 0xFF, 0xFFFF} when failed.
 *
 * @api
 */
bdaddr_t qualcommGetBdAddr(void);

/**
 * @brief   Set Qualcomm Bluetooth bd Addr.
 *
 * @param[in] bdaddr    The bd address to set
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSetBdAddr(bdaddr_t bdaddr);

/**
 * @brief   Get Qualcomm Bluetooth all connected profiles.
 *
 * @param[in] void      None
 *
 * @return              All connected profiles of Qualcomm Bluetooth chipset
 *                      0x00 when failed.
 *
 * @api
 */
conn_profile_t qualcommGetProfilesConnected(void);

/**
 * @brief   Get Qualcomm Bluetooth HFP call state.
 *
 * @param[in] void      None
 *
 * @return              The HFP call state of Qualcomm Bluetooth chipset
 *                      hfp_call_state_idle when failed.
 *
 * @api
 */
hfp_call_state_t qualcommGetHFPCallState(void);

/**
 * @brief   Get Qualcomm Bluetooth A2DP Stream state.
 *
 * @param[in] void      None
 *
 * @return              The A2DP Stream state of Qualcomm Bluetooth chipset
 *                      a2dp_stream_idle when failed.
 *
 * @api
 */
a2dp_stream_state_t qualcommGetA2DPStreamState(void);

/**
 * @brief   Set Qualcomm Bluetooth TX Power.
 *
 * @param[in] txpwr     The tx power to set
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSetTxPwr(uint16_t txpwr);

/**
 * @brief   Start Bluetooth continuous TX mode.
 *
 * @param[in] lo_freq   Transmission frequency in MHz.
 * @param[in] level     The transmit level to use.
 *                      Bluecore:
 *                       The lower 8 bits are the internal gain.
 *                       The upper 8 bits are the external gain.
 *                      CDA devices:
 *                       "level" parameter should regarded as a 16-bit word of four nybbles 0xabcd
 *                       "a" is used as attenuation, "b" is used as mag, the bottom two bits of "c"
 *                       as exp and the top two bits must be 0, "d" must be 0.
 *                       Attenuation is an analogue component and corresponds to the number
 *                       of "segments" switched off; 0 switches off none and produces most power;
 *                       15 switches off all and produces no power.
 *                       Magnitude (mag; signed, range 7 to -8) and exponent (exp; unsigned, range
 *                       0 to 3) act together digitally to produce power proportional to
 *                       (1 + Mag / 16) / power(2, exp).
 *                       For example a 10dBm power table entry of {{0,4,0,0,0}, {0,3,0,0,0}, 10}
 *                       corresponds to level parameter values of 0x0400 for basic rate packets
 *                       and 0x0300 for EDR packets.
 *                      See document 80-CF994-1 for more details.
 * @param[in] mod_freq  Modulation offset. 4096 = 1MHz.
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommStartContTx(uint16_t lo_freq, uint16_t level, uint16_t mod_freq);

/**
 * @brief   Pause Qualcomm Bluetooth Radio Test.
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommPauseRadioTest(void);

/**
 * @brief   Send the upgrade command to Qualcomm Bluetooth chipset.
 *
 * @param[in] data      the pointer of the command data to send
 * @param[in] len       the len of command data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSendUpgradeCmd(uint8_t *data, uint8_t len);

/**
 * @brief   Send the upgrade data to Qualcomm Bluetooth chipset.
 *
 * @param[in] data      the pointer of the data to send
 * @param[in] len       the len of data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSendUpgradeData(uint8_t *data, uint8_t len);

/**
 * @brief   Receive the upgrade response from Qualcomm Bluetooth chipset.
 *
 * @param[out] data     the pointer of the data received
 * @param[out] len      the pointer of the len of data received
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommReceiveUpgradeResp(uint8_t *data, uint8_t *len);

/**
 * @brief   Clear the upgrade response data in Qualcomm Bluetooth chipset.
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommClearUpgradeResp(void);

/**
 * @brief   Clear the paired device list from Qualcomm Bluetooth chipset.
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommClearPDL(void);

/**
 * @brief   Set the Qualcomm Bluetooth chipset under test mode
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSetDUT(void);

/**
 * @brief   Get the maximum the trusted devices number of Qualcomm Bluetooth chipset
 *
 * @param[in] void      None
 *
 * @return              The max number of the trusted devices.
 * @retval              0 if the operation completed successfully.
 *
 * @api
 */
uint16_t qualcommGetMaxTrustedDevice(void);

/**
 * @brief   Get the indexed trusted device of Qualcomm Bluetooth chipset
 *
 * @param[in] index     The index of the trusted device to retrieve
 *
 * @return              The bd address of Qualcomm Bluetooth chipset
 *                      {0xFFFFFFFF, 0xFF, 0xFFFF} when failed.
 *
 * @api
 */
bdaddr_t qualcommGetIndexedTdlDevice(uint16_t index);

/**
 * @brief   Start Qualcomm Bluetooth BLE Bonding.
 *
 * @param[in] void      None
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommStartBleBonding(void);

/**
 * @brief   Read Qualcomm Bluetooth Centpp GATT Server data.
 *
 * @param[out] buf      the buffer to store the Centpp Server data
 * @param[out] length   the length of the Centpp Server data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommReadCentppServerData(uint8_t *buf, uint16_t *length);

/**
 * @brief   Write Qualcomm Bluetooth Centpp GATT Server data.
 *
 * @param[in] buf       the buffer which stores the Centpp Server data
 * @param[in] length    the length of the Centpp Server data
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommWriteCentppServerData(uint8_t *buf, uint16_t length);

/**
 * @brief   Send the Centpp Server Read Notification to Qualcomm Bluetooth.
 *
 * @param[in] buf       the buffer which stores the Centpp Server Read Notification
 * @param[in] length    the length of the Centpp Server Read Notification
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSendCentppServerReadNotification(uint8_t *buf, uint16_t length);

/**
 * @brief   Send the Centpp Server Write Notification to Qualcomm Bluetooth.
 *
 * @param[in] buf       the buffer which stores the Centpp Server Write Notification
 * @param[in] length    the length of the Centpp Server Write Notification
 *
 * @return              The operation status.
 * @retval MSG_OK       if the operation completed successfully.
 *
 * @api
 */
msg_t qualcommSendCentppServerWriteNotification(uint8_t *buf, uint16_t length);

/**
 * @brief   Initializes an instance.
 *
 *
 * @init
 */
void qualcommInit(void);

/**
 * @brief   Configures and activates Qualcomm Bluetooth Wireless Audio Processor.
 *
 *
 * @api
 */
void qualcommStart(void);

/**
 * @brief   Deactivates the Qualcomm Bluetooth Wireless Audio Processor.
 *
 *
 * @api
 */
void qualcommStop(void);


/**
 * @brief   Reset the Qualcomm Bluetooth Wireless Audio Processor.
 *
 *
 * @api
 */
void qualcommReset(void);

#ifdef __cplusplus
}
#endif

#endif /* _QUALCOMM_H_ */

/** @} */

