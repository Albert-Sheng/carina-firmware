/**
 * @file    cs47l90_regdefs.h
 * @brief   Register definitions for the CS47L90.
 *
 * @addtogroup BOARD
 * @ingroup BOARD
 * @{
 */

#ifndef _CS47L90_REGDEFS_H_
#define _CS47L90_REGDEFS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @brief   CS47L90 register definitions.
 * @details List all of the register definitions of the CS47L90 here.
 * @note
 */
#define CS47L90_SOFTWARE_RESET          (0x0000U)

#define CS47L90_SYSTEM_CLOCK_1          (0x0101U)
#define CS47L90_ASYNC_CLOCK_1           (0x0112U)

#define CS47L90_FLL1_CONTROL_1          (0x0171U)
#define CS47L90_FLL2_CONTROL_1          (0x0191U)

#define CS47L90_JACK_DETECT_ANALOG      (0x02D3U)

#define CS47L90_ADC_DIGITAL_VOLUME_1L   (0x0311U)
#define CS47L90_ADC_DIGITAL_VOLUME_1R   (0x0315U)
#define CS47L90_DAC_DIGITAL_VOLUME_1L   (0x0411U)
#define CS47L90_DAC_DIGITAL_VOLUME_1R   (0x0415U)
#define CS47L90_DAC_DIGITAL_VOLUME_3L   (0x0421U)
#define CS47L90_DAC_DIGITAL_VOLUME_3R   (0x0425U)

#define CS47L90_IRQ1_RAW_STATUS_1       (0x1880U)
#define CS47L90_IRQ1_RAW_STATUS_7       (0x1886U)
#define CS47L90_IRQ2_RAW_STATUS_7       (0x1986U)

#define CS47L90_INTERRUPT_DEBOUNCE_7    (0x1A06U)

/**
 * @brief   CS47L90 register bit definitions.
 * @details List all of the bit definitions of the CS47L90 registers here.
 * @note
 */
/********************  Bit definition for Software_Reset register  ********************/
#define CS47L90_SW_RST_DEV_ID_Pos       (0U)
#define CS47L90_SW_RST_DEV_ID_Msk       (0xFFFFU << CS47L90_SW_RST_DEV_ID_Pos)  /*!< 0xFFFF */
#define CS47L90_SW_RST_DEV_ID           CS47L90_RST_DEV_ID_Msk                  /*!< CS47L90 device ID */

/********************  Bit definition for System_Clock_1 register  ********************/
#define CS47L90_SYSCLK_FRAC_Pos         (15U)
#define CS47L90_SYSCLK_FRAC_Msk         (0x1U << CS47L90_SYSCLK_FRAC_Pos)       /*!< 0x8000 */
#define CS47L90_SYSCLK_FRAC             CS47L90_SYSCLK_FRAC_Msk                 /*!< Sysclk frequency multiple */
#define CS47L90_SYSCLK_FREQ_Pos         (8U)
#define CS47L90_SYSCLK_FREQ_Msk         (0x7U << CS47L90_SYSCLK_FREQ_Pos)       /*!< 0x0070 */
#define CS47L90_SYSCLK_FREQ             CS47L90_SYSCLK_FREQ_Msk                 /*!< Current sysclk frequency setting */
#define CS47L90_SYSCLK_ENA_Pos          (6U)
#define CS47L90_SYSCLK_ENA_Msk          (0x1U << CS47L90_SYSCLK_ENA_Pos)        /*!< 0x0040 */
#define CS47L90_SYSCLK_ENA              CS47L90_SYSCLK_ENA_Msk                  /*!< Sysclk enable */
#define CS47L90_SYSCLK_SRC_Pos          (0U)
#define CS47L90_SYSCLK_SRC_Msk          (0xFU << CS47L90_SYSCLK_SRC_Pos)        /*!< 0x000F */
#define CS47L90_SYSCLK_SRC              CS47L90_SYSCLK_SRC_Msk                  /*!< Sysclk frequency source */

/********************  Bit definition for Async_Clock_1 register  ********************/
#define CS47L90_ASYNC_FREQ_Pos         (8U)
#define CS47L90_ASYNC_FREQ_Msk         (0x7U << CS47L90_ASYNC_FREQ_Pos)         /*!< 0x0070 */
#define CS47L90_ASYNC_FREQ             CS47L90_ASYNC_FREQ_Msk                   /*!< Current async frequency setting */
#define CS47L90_ASYNC_ENA_Pos          (6U)
#define CS47L90_ASYNC_ENA_Msk          (0x1U << CS47L90_ASYNC_ENA_Pos)          /*!< 0x0040 */
#define CS47L90_ASYNC_ENA              CS47L90_ASYNC_ENA_Msk                    /*!< Async enable */
#define CS47L90_ASYNC_SRC_Pos          (0U)
#define CS47L90_ASYNC_SRC_Msk          (0xFU << CS47L90_ASYNC_SRC_Pos)          /*!< 0x000F */
#define CS47L90_ASYNC_SRC              CS47L90_ASYNC_SRC_Msk                    /*!< Sysclk frequency source */

/********************  Bit definition for FLL1_Control_1 register  ********************/
#define CS47L90_FLL1CTRL_ENA_Pos        (0U)
#define CS47L90_FLL1CTRL_ENA_Msk        (0x1U << CS47L90_FLL1CTRL_ENA_Pos)      /*!< 0x0001 */
#define CS47L90_FLL1CTRL_ENA            CS47L90_FLL1CTRL_ENA_Msk                /*!< FLL1 enable */
#define CS47L90_FLL1CTRL_FRN_Pos        (1U)
#define CS47L90_FLL1CTRL_FRN_Msk        (0x1U << CS47L90_FLL1CTRL_FRN_Pos)      /*!< 0x0002 */
#define CS47L90_FLL1CTRL_FRN            CS47L90_FLL1CTRL_FRN_Msk                /*!< FLL1 free-running mode enable */

/********************  Bit definition for FLL2_Control_2 register  ********************/
#define CS47L90_FLL2CTRL_ENA_Pos        (0U)
#define CS47L90_FLL2CTRL_ENA_Msk        (0x1U << CS47L90_FLL2CTRL_ENA_Pos)      /*!< 0x0001 */
#define CS47L90_FLL2CTRL_ENA            CS47L90_FLL2CTRL_ENA_Msk                /*!< FLL2 enable */
#define CS47L90_FLL2CTRL_FRN_Pos        (1U)
#define CS47L90_FLL2CTRL_FRN_Msk        (0x1U << CS47L90_FLL2CTRL_FRN_Pos)      /*!< 0x0002 */
#define CS47L90_FLL2CTRL_FRN            CS47L90_FLL2CTRL_FRN_Msk                /*!< FLL2 free-running mode enable */

/********************  Bit definition for FLL1_Control_1 register  ********************/
#define CS47L90_JCKDET1_ENA_Pos         (0U)
#define CS47L90_JCKDET1_ENA_Msk         (0x1U << CS47L90_JCKDET1_ENA_Pos)       /*!< 0x0001 */
#define CS47L90_JCKDET1_ENA             CS47L90_FLL1CTRL_ENA_Msk                /*!< Jack detect 1 enable */
#define CS47L90_JCKDET2_ENA_Pos         (1U)
#define CS47L90_JCKDET2_ENA_Msk         (0x1U << CS47L90_JCKDET2_ENA_Pos)       /*!< 0x0002 */
#define CS47L90_JCKDET2_ENA             CS47L90_JCKDET2_ENA_Msk                 /*!< Jack detect 2 enable */

/********************  Bit definition for ADC_Digital_Volume_1L register  ********************/
#define CS47L90_IN1LVOL_Pos             (0U)
#define CS47L90_IN1LVOL_Msk             (0xFFU << CS47L90_IN1LVOL_Pos)          /*!< 0x00FF */
#define CS47L90_IN1LVOL                 CS47L90_IN1LVOL_Msk                     /*!< Input path 1 (Left) digital volume */
#define CS47L90_IN1LMUTE_Pos            (8U)
#define CS47L90_IN1LMUTE_Msk            (0x1U << CS47L90_IN1LMUTE_Pos)          /*!< 0x0100 */
#define CS47L90_IN1LMUTE                CS47L90_IN1LMUTE_Msk                    /*!< Input path 1 (Left) digital mute */
#define CS47L90_IN1LINVU_Pos            (9U)
#define CS47L90_IN1LINVU_Msk            (0x1U << CS47L90_IN1LINVU_Pos)          /*!< 0x0200 */
#define CS47L90_IN1LINVU                CS47L90_IN1LINVU_Msk                    /*!< Input signal paths volume and mute update */
#define CS47L90_IN1LLP_Pos              (11U)
#define CS47L90_IN1LLP_Msk              (0x1U << CS47L90_IN1LLP_Msk)            /*!< 0x0800 */
#define CS47L90_IN1LLP                  CS47L90_IN1LLP_Msk                      /*!< Input path 1 (Left) low power mode enable */
#define CS47L90_IN1L_SRC_Pos            (13U)
#define CS47L90_IN1L_SRC_Msk            (0x3U << CS47L90_IN1L_SRC_Pos)          /*!< 0x6000 */
#define CS47L90_IN1L_SRC                CS47L90_IN1L_SRC_Pos                    /*!< Input path 1 (Left) source */

/********************  Bit definition for ADC_Digital_Volume_1R register  ********************/
#define CS47L90_IN1RVOL_Pos             (0U)
#define CS47L90_IN1RVOL_Msk             (0xFFU << CS47L90_IN1RVOL_Pos)          /*!< 0x00FF */
#define CS47L90_IN1RVOL                 CS47L90_IN1RVOL_Msk                     /*!< Input path 1 (Right) digital volume */
#define CS47L90_IN1RMUTE_Pos            (8U)
#define CS47L90_IN1RMUTE_Msk            (0x1U << CS47L90_IN1RMUTE_Pos)          /*!< 0x0100 */
#define CS47L90_IN1RMUTE                CS47L90_IN1RMUTE_Msk                    /*!< Input path 1 (Right) digital mute */
#define CS47L90_IN1RINVU_Pos            (9U)
#define CS47L90_IN1RINVU_Msk            (0x1U << CS47L90_IN1RINVU_Pos)          /*!< 0x0200 */
#define CS47L90_IN1RINVU                CS47L90_IN1RINVU_Msk                    /*!< Input signal paths volume and mute update */
#define CS47L90_IN1RLP_Pos              (11U)
#define CS47L90_IN1RLP_Msk              (0x1U << CS47L90_IN1RLP_Msk)            /*!< 0x0800 */
#define CS47L90_IN1RLP                  CS47L90_IN1RLP_Msk                      /*!< Input path 1 (Right) low power mode enable */
#define CS47L90_IN1R_SRC_Pos            (13U)
#define CS47L90_IN1R_SRC_Msk            (0x3U << CS47L90_IN1R_SRC_Pos)          /*!< 0x6000 */
#define CS47L90_IN1R_SRC                CS47L90_IN1R_SRC_Pos                    /*!< Input path 1 (Right) source */

/********************  Bit definition for DAC_Digital_Volume_1L register  ********************/
#define CS47L90_OUT1LVOL_Pos            (0U)
#define CS47L90_OUT1LVOL_Msk            (0xFFU << CS47L90_OUT1LVOL_Pos)         /*!< 0x00FF */
#define CS47L90_OUT1LVOL                CS47L90_OUT1LVOL_Msk                    /*!< Output path 1 (Left) digital volume */
#define CS47L90_OUT1LMUTE_Pos           (8U)
#define CS47L90_OUT1LMUTE_Msk           (0x1U << CS47L90_OUT1LMUTE_Pos)         /*!< 0x0100 */
#define CS47L90_OUT1LMUTE               CS47L90_OUT1LMUTE_Msk                   /*!< Output path 1 (Left) digital mute */
#define CS47L90_OUT1LINVU_Pos           (9U)
#define CS47L90_OUT1LINVU_Msk           (0x1U << CS47L90_OUT1LINVU_Pos)         /*!< 0x0200 */
#define CS47L90_OUT1LINVU               CS47L90_OUT1LINVU_Msk                   /*!< Output signal paths volume and mute update */

/********************  Bit definition for DAC_Digital_Volume_1R register  ********************/
#define CS47L90_OUT1RVOL_Pos            (0U)
#define CS47L90_OUT1RVOL_Msk            (0xFFU << CS47L90_OUT1RVOL_Pos)         /*!< 0x00FF */
#define CS47L90_OUT1RVOL                CS47L90_OUT1RVOL_Msk                    /*!< Output path 1 (Right) digital volume */
#define CS47L90_OUT1RMUTE_Pos           (8U)
#define CS47L90_OUT1RMUTE_Msk           (0x1U << CS47L90_OUT1RMUTE_Pos)         /*!< 0x0100 */
#define CS47L90_OUT1RMUTE               CS47L90_OUT1RMUTE_Msk                   /*!< Output path 1 (Right) digital mute */
#define CS47L90_OUT1RINVU_Pos           (9U)
#define CS47L90_OUT1RINVU_Msk           (0x1U << CS47L90_OUT1RINVU_Pos)         /*!< 0x0200 */
#define CS47L90_OUT1RINVU               CS47L90_OUT1RINVU_Msk                   /*!< Output signal paths volume and mute update */

/********************  Bit definition for IRQ1_Raw_Status_1 register  ********************/
#define CS47L90_BOOTDONE_STS1_Pos       (7U)
#define CS47L90_BOOTDONE_STS1_Msk       (0x1U << CS47L90_BOOTDONE_STS1_Pos)     /*!< 0x0080 */
#define CS47L90_BOOTDONE_STS1           CS47L90_BOOTDONE_STS1_Msk               /*!< Boot sequence complete status */
#define CS47L90_CTRLIF_ERR_STS1_Pos     (12U)
#define CS47L90_CTRLIF_ERR_STS1_Msk     (0x1U << CS47L90_CTRLIF_ERR_STS1_Pos)   /*!< 0x1000 */
#define CS47L90_CTRLIF_ERR_STS1         CS47L90_CTRLIF_ERR_STS1_Msk             /*!< Control interface error status */

/********************  Bit definition for IRQ1_Raw_Status_7 register  ********************/
#define CS47L90_JD1_STS1_Pos            (0U)
#define CS47L90_JD1_STS1_Msk            (0x1U << CS47L90_JD1_STS1_Pos)          /*!< 0x0001 */
#define CS47L90_JD1_STS1                CS47L90_JD1_STS1_Msk                    /*!< Jack detect 1 status */
#define CS47L90_JD2_STS1_Pos            (2U)
#define CS47L90_JD2_STS1_Msk            (0x1U << CS47L90_JD2_STS1_Pos)          /*!< 0x0004 */
#define CS47L90_JD2_STS1                (CS47L90_JD2_STS1_Msk)                  /*!< Jack detect 2 status */
#define CS47L90_MICD_CLAMP_STS1_Pos     (4U)
#define CS47L90_MICD_CLAMP_STS1_Msk     (0x1U << CS47L90_MICD_CLAMP_STS1_Pos)   /*!< 0x0010 */
#define CS47L90_MICD_CLAMP_STS1         (CS47L90_MICD_CLAMP_STS1_Msk)           /*!< Mic detect clamp status */

/********************  Bit definition for IRQ2_Raw_Status_7 register  ********************/
#define CS47L90_JD1_STS2_Pos            (0U)
#define CS47L90_JD1_STS2_Msk            (0x1U << CS47L90_JD1_STS2_Pos)          /*!< 0x0001 */
#define CS47L90_JD1_STS2                CS47L90_JD1_STS2_Msk                    /*!< Jack detect 1 status */
#define CS47L90_JD2_STS2_Pos            (2U)
#define CS47L90_JD2_STS2_Msk            (0x1U << CS47L90_JD2_STS2_Pos)          /*!< 0x0004 */
#define CS47L90_JD2_STS2                (CS47L90_JD2_STS2_Msk)                  /*!< Jack detect 2 status */
#define CS47L90_MICD_CLAMP_STS2_Pos     (4U)
#define CS47L90_MICD_CLAMP_STS2_Msk     (0x1U << CS47L90_MICD_CLAMP_STS2_Pos)   /*!< 0x0010 */
#define CS47L90_MICD_CLAMP_STS2         (CS47L90_MICD_CLAMP_STS2_Msk)           /*!< Mic detect clamp status */

/********************  Bit definition for Interrupt_Debounce_7 register  ********************/
#define CS47L90_JD1_DB_Pos              (0U)
#define CS47L90_JD1_DB_Msk              (0x1U << CS47L90_JD1_DB2_Pos)           /*!< 0x0001 */
#define CS47L90_JD1_DB                  CS47L90_JD1_DB_Msk                      /*!< Jack detect 1 debounce */
#define CS47L90_JD2_DB_Pos              (2U)
#define CS47L90_JD2_DB_Msk              (0x1U << CS47L90_JD2_DB2_Pos)           /*!< 0x0004 */
#define CS47L90_JD2_DB                  (CS47L90_JD2_DB_Msk)                    /*!< Jack detect 2 debounce */
#define CS47L90_MICD_CLAMP_DB_Pos       (4U)
#define CS47L90_MICD_CLAMP_DB_Msk       (0x1U << CS47L90_MICD_CLAMP_DB_Pos)     /*!< 0x0010 */
#define CS47L90_MICD_CLAMP_DB           (CS47L90_MICD_CLAMP_DB_Msk)             /*!< Mic detect clamp debounce */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* #ifndef _CS47L90_REGDEFS_H_ */
