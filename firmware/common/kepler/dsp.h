/**
 * @file    dsp.h
 * @brief   Audio DSP header.
 *
 * @addtogroup COMMON
 * @ingroup COMMON
 * @{
 */
#ifndef _DSP_H_
#define _DSP_H_

#include "hal.h"

#if (HAL_USE_DSP == TRUE)

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/
#define CS47L90_CHIP_ID                                                 (0x6364)

typedef enum {
    DSP_OK,     /**< @brief Normal DSP message.  */
    DSP_FAIL    /**< @brief Failure DSP message.  */
} dsp_msg_t;

typedef struct {
    uint16_t x;
    uint16_t y;
} dspTable_t;                                       /**< @brief DSP Table Lookup Structure. */
#define dspTableEnd ((dspTable_t){0xffff, 0x0000})  /**< @brief Marks the end of a DSP Table. */
/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief   Reads the contents of a register.
 *
 *
 * @api
 */
dsp_msg_t dspSetGain(uint16_t gain, const dspTable_t *table, const uint32_t *addr, size_t gain_regs);

/**
 * @brief   Reads from a register in the DSP.
 *
 *
 * @api
 */
dsp_msg_t dspReadReg(uint8_t *buf, uint32_t addr, size_t len);

/**
 * @brief   Writes to a register in the DSP.
 *
 *
 * @api
 */
dsp_msg_t dspWriteReg(uint32_t addr, uint32_t value, uint32_t mask, uint32_t check);

/**
 * @brief   Initializes an instance.
 *
 *
 * @init
 */
void dspInit(void);

/**
 * @brief   Configures and activates the audio DSP.
 *
 *
 * @api
 */
void dspStart(void);

/**
 * @brief   Deactivates the audio DSP.
 *
 *
 * @api
 */
void dspStop(void);


/**
 * @brief   Resets the audio DSP.
 *
 *
 * @api
 */
void dspReset(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* HAL_USE_WSPI == TRUE */

#endif /* #ifndef _DSP_H_ */
