#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "st7701.h"

#define LINE_LCD_BACKLIGHT      PAL_LINE(GPIOI, 8)
#define SDRAM_DEVICE_ADDR       0xC0000000

static const ltdcConfig driverCfg = {
    480, 480,                                                                       // Width, Height (pixels)
    14, 4,                                                                          // Horizontal, Vertical sync (pixels)
    20, 18,                                                                         // Horizontal, Vertical back porch (pixels)
    18, 16,                                                                         // Horizontal, Vertical front porch (pixels)
    0,                                                                              // Sync flags
    0x000000,                                                                       // Clear color (RGB888)

    {
        // Background layer config
        (LLDCOLOR_TYPE*)SDRAM_DEVICE_ADDR,                                      // Frame buffer address
        480, 480,                                                               // Width, Height (pixels)
        480 * LTDC_PIXELBYTES,                                                  // Line pitch (bytes)
        LTDC_PIXELFORMAT,                                                       // Pixel format
        0, 0,                                                                   // Start pixel position (x, y)
        480, 480,                                                               // Size of virtual layer (cx, cy)
        0x00000000,                                                             // Default color (ARGB8888)
        0x000000,                                                               // Color key (RGB888)
        LTDC_BLEND_FIX1_FIX2,                                                   // Blending factors
        0,                                                                      // Palette (RGB888, can be NULL)
        0,                                                                      // Palette length
        0xFF,                                                                   // Constant alpha factor
        LTDC_LEF_ENABLE                                                         // Layer configuration flags
    },

#if STM32LTDC_USE_LAYER2 || STM32LTDC_USE_DOUBLEBUFFERING
    {
        // Foreground layer config (if turned on)
        (LLDCOLOR_TYPE*)(SDRAM_DEVICE_ADDR + (480 * 480 * LTDC_PIXELBYTES)),    // Frame buffer address
        480, 480,                                                               // Width, Height (pixels)
        480 * LTDC_PIXELBYTES,                                                  // Line pitch (bytes)
        LTDC_PIXELFORMAT,                                                       // Pixel format
        0, 0,                                                                   // Start pixel position (x, y)
        480, 480,                                                               // Size of virtual layer (cx, cy)
        0x00000000,                                                             // Default color (ARGB8888)
        0x000000,                                                               // Color key (RGB888)
        LTDC_BLEND_MOD1_MOD2,                                                   // Blending factors
        0,                                                                      // Palette (RGB888, can be NULL)
        0,                                                                      // Palette length
        0xFF,                                                                   // Constant alpha factor
        LTDC_LEF_ENABLE                                                         // Layer configuration flags
    }
#else
    LTDC_UNUSED_LAYER_CONFIG
#endif
};

static GFXINLINE void init_ltdc_clock(void) {
    rccResetLTDC();
    rccEnableLTDC(false);
}

#if STM32LTDC_USE_DMA2D
static GFXINLINE void init_dma2d_clock(void) {
    rccEnableDMA2D(false);
}
#endif

static GFXINLINE void init_board(GDisplay* g) {
    (void)g;

    // Initialize physical display driver
    ST7701_Init(&SPID1);
}

static GFXINLINE void post_init_board(GDisplay* g) {
    (void)g;
}

static GFXINLINE void set_backlight(GDisplay* g, gU8 percent) {
    (void)g;

    // Currently we only support on/off states.
    // The backlight can be dimmed using a PWM peripheral if desired so
    if(percent > 0) {
        palSetLine(LINE_LCD_BACKLIGHT);
    } else {
        palClearLine(LINE_LCD_BACKLIGHT);
    }
}

#endif /* _GDISP_LLD_BOARD_H */

