target_include_directories(
    ${TARGET}
    INTERFACE
        ${CMAKE_CURRENT_LIST_DIR}
)

target_sources(
    ${TARGET}
    INTERFACE
        board.c
        board.h
        board_STM32LTDC.h
        pga2505_lld.h
)
