/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * This file has been automatically generated using ChibiStudio board
 * generator plugin. Do not edit manually.
 */

#ifndef BOARD_H
#define BOARD_H

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*
 * MCU type as defined in the ST header.
 */
#define STM32H750xx

/*
 * Ethernet PHY type.
 */
#define BOARD_PHY_ID                MII_LAN8742A_ID
#define BOARD_PHY_RMII

/*
 * Board oscillators-related settings.
 */
#if !defined(STM32_LSECLK)
#define STM32_LSECLK                0U
#endif

#define STM32_LSEDRV                (3U << 3U)

#if !defined(STM32_HSECLK)
#define STM32_HSECLK                26000000U
#endif

// #define STM32_HSE_BYPASS

// Pins
#define GPIO_1b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) \
    ((x15<<15)|(x14<<14)|(x13<<13)|(x12<<12)|(x11<<11)|(x10<<10)|(x9<<9)|(x8<<8)|(x7<<7)|(x6<<6)|(x5<<5)|(x4<<4)|(x3<<3)|(x2<<2)|(x1<<1)|(x0<<0))
#define GPIO_2b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) \
    ((x15<<30)|(x14<<28)|(x13<<26)|(x12<<24)|(x11<<22)|(x10<<20)|(x9<<18)|(x8<<16)|(x7<<14)|(x6<<12)|(x5<<10)|(x4<<8)|(x3<<6)|(x2<<4)|(x1<<2)|(x0<<0))
#define GPIO_4b(x7, x6, x5, x4, x3, x2, x1, x0) \
    ((x7<<28)|(x6<<24)|(x5<<20)|(x4<<16)|(x3<<12)|(x2<<8)|(x1<<4)|(x0<<0))

#define IN   0U
#define OUT  1U
#define ALT  2U
#define ANLG 3U
#define GPIO_MODER(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) GPIO_2b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0)

#define PP   0U
#define OD   1U
#define GPIO_OTYPER(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) GPIO_1b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0)

#define VLOW 0U
#define LOW  1U
#define MED  2U
#define HIGH 3U
#define GPIO_OSPEEDR(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) GPIO_2b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0)

#define NONE 0U
#define UP   1U
#define DOWN 2U
#define GPIO_PUPDR(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) GPIO_2b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0)

#define GPIO_ODR(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0) GPIO_1b(x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, x0)
#define GPIO_AFR(x7, x6, x5, x4, x3, x2, x1, x0) GPIO_4b(x7, x6, x5, x4, x3, x2, x1, x0)

/// Port A //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PA0      SAI2B_SD
// PA1      LCD_R2
// PA2      LED3 - TIM2_CH3.
// PA3      ULPI_D0.
// PA4      LCD_VSYNC
// PA5      ULPI_CLK.
// PA6      LCD_G2
// PA7      SDRAM_SDNWE
// PA8      LED0 - TIM1_CH1.
// PA9      LPUART - Debug Output.
// PA10     input pullup.
// PA11     input pullup.
// PA12     input pullup.
// PA13     SWDIO.
// PA14     SWCLK.
// PA15     LED4 - TIM2_CH1.
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOA_MODER       GPIO_MODER(  OUT,  ALT,  ALT,   IN,   IN,   IN,  ALT,  OUT,  ALT,  ALT,  ALT,  ALT,  ALT,  OUT,  ALT,  ALT)
#define VAL_GPIOA_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOA_OSPEEDR   GPIO_OSPEEDR( HIGH, HIGH, HIGH, VLOW, VLOW, VLOW,  LOW, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH)
#define VAL_GPIOA_PUPDR       GPIO_PUPDR( NONE, NONE, NONE,   UP,   UP,   UP, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE)
#define VAL_GPIOA_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOA_AFRL          GPIO_AFR(                                                   12,   14,   10,   14,   10,    1,   14,   10)
#define VAL_GPIOA_AFRH          GPIO_AFR(    1,    0,    0,    0,    0,    0,    3,    0                                                )

/// Port B //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PB0      ULPI_D1.
// PB1      ULPI_D2.
// PB2      QSPI - CLK.
// PB3      SPI1_SCK
// PB4      LED5 - TIM3_CH1
// PB5      ULPI_D7.
// PB6      QSPI - NCS.
// PB7      LED8 - TIM4_CH2.
// PB8      LED9 - TIM4_CH3.
// PB9      LCD_B7
// PB10     ULPI_D3.
// PB11     ULPI_D4.
// PB12     ULPI_D5.
// PB13     ULPI_D6.
// PB14     LED1 - TIM1_CH2N.
// PB15     LED2 - TIM12_CH2.
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOB_MODER       GPIO_MODER(  OUT,  OUT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  OUT,  ALT,  ALT,  ALT,  ALT)
#define VAL_GPIOB_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOB_OSPEEDR   GPIO_OSPEEDR( HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH)
#define VAL_GPIOB_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,   UP, NONE, NONE, NONE, NONE, NONE, NONE)
#define VAL_GPIOB_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOB_AFRL          GPIO_AFR(                                                    2,   10,   10,    2,    5,    9,   10,   10)
#define VAL_GPIOB_AFRH          GPIO_AFR(    2,    1,   10,   10,   10,   10,   14,    2                                                )

/// Port C //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PC0      ULPI_STP.
// PC1      SAI1A_SD
// PC2      input pullup.
// PC3      ULPI_NXT.
// PC4      REV_CONTROL.
// PC5      PHANTOM_PWR_LED - output nopull.
// PC6      LED11 - TIM8_CH1.
// PC7      LED6 - TIM3_CH2.
// PC8      LED7 - TIM3_CH3.
// PC9      I2S_CKIN
// PC10     SPI3_SCK.
// PC11     SPI3_MISO.
// PC12     PGA2505_SPI_CS
// PC13     LCD_SPI_CS
// PC14     CS47L90_SPI_CS
// PC15     LCD_RESET
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOC_MODER       GPIO_MODER(  OUT,  OUT,  OUT,   IN,  ALT,  ALT,   IN,  ALT,  ALT,  ALT,  OUT,   IN,  ALT,   IN,  ALT,  ALT)
#define VAL_GPIOC_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOC_OSPEEDR   GPIO_OSPEEDR(  LOW,  LOW,  LOW,  LOW, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,  LOW, VLOW, HIGH, VLOW, HIGH, HIGH)
#define VAL_GPIOC_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,   UP, NONE,   UP, NONE, NONE)
#define VAL_GPIOC_ODR           GPIO_ODR(    0,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    1)
#define VAL_GPIOC_AFRL          GPIO_AFR(                                                    2,    3,    0,    0,   10,    0,    6,   10)
#define VAL_GPIOC_AFRH          GPIO_AFR(    0,    0,    0,    0,    6,    6,    0,    2                                                )


/// Port D //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PD0      SDRAM_D2
// PD1      SDRAM_D3
// PD2      USB_PHY_RESET.
// PD3      PHANTOM_SW.
// PD4      AV_RESETN.
// PD5      LEFT_ENCODER_INT0
// PD6      SPI3_MOSI.
// PD7      SPI1_MOSI
// PD8      SDRAM_D13
// PD9      SDRAM_D14
// PD10     SDRAM_D15
// PD11     SAI2A_SDO
// PD12     LED10 - TIM4_CH1.
// PD13     SAI2A_SCK
// PD14     SDRAM_D0
// PD15     SDRAM_D1
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOD_MODER       GPIO_MODER(  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,   IN,  OUT,   IN,  OUT,  ALT,  ALT)
#define VAL_GPIOD_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   OD,   PP,   PP,   PP,   PP)
#define VAL_GPIOD_OSPEEDR   GPIO_OSPEEDR( HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,  LOW, VLOW, VLOW, VLOW, HIGH, HIGH)
#define VAL_GPIOD_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,   UP, NONE, NONE, NONE, NONE)
#define VAL_GPIOD_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    1,    1)
#define VAL_GPIOD_AFRL          GPIO_AFR(                                                    5,    5,    0,    0,    0,    0,   12,   12)
#define VAL_GPIOD_AFRH          GPIO_AFR(   12,   12,   10,    2,   10,   12,   12,   12                                                )

/// Port E //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PE0      SDRAM_NBL0
// PE1      SDRAM_NBL1
// PE2      QSPI - BK1_IO2.
// PE3      SAI1B_SD
// PE4      SAI1A_LRCLK
// PE5      SAI1A_BCLK
// PE6      LED13 - TIM15_CH2.
// PE7      SDRAM_D4
// PE8      SDRAM_D5
// PE9      SDRAM_D6
// PE10     SDRAM_D7
// PE11     SDRAM_D8
// PE12     SDRAM_D9
// PE13     SDRAM_D10
// PE14     SDRAM_D11
// PE15     SDRAM_D12
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOE_MODER       GPIO_MODER(  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT)
#define VAL_GPIOE_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOE_OSPEEDR   GPIO_OSPEEDR( HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH)
#define VAL_GPIOE_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE)
#define VAL_GPIOE_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOE_AFRL          GPIO_AFR(                                                   12,    4,    6,    6,    6,    9,   12,   12)
#define VAL_GPIOE_AFRH          GPIO_AFR(   12,   12,   12,   12,   12,   12,   12,   12                                                )

/// Port F //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PF0      SDRAM_A0
// PF1      SDRAM_A1
// PF2      SDRAM_A2
// PF3      SDRAM_A3
// PF4      SDRAM_A4
// PF5      SDRAM_A5
// PF6      QSPI - BK1_IO3.
// PF7      LED14 - TIM17_CH1.
// PF8      QSPI - BK1_IO0.
// PF9      QSPI - BK1_IO1.
// PF10     LCD_DE
// PF11     SDRAM_SDNRAS
// PF12     SDRAM_A6
// PF13     SDRAM_A7
// PF14     SDRAM_A8
// PF15     SDRAM_A9
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOF_MODER       GPIO_MODER(  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT)
#define VAL_GPIOF_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOF_OSPEEDR   GPIO_OSPEEDR( HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH)
#define VAL_GPIOF_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE)
#define VAL_GPIOF_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOF_AFRL          GPIO_AFR(                                                    1,    9,   12,   12,   12,   12,   12,   12)
#define VAL_GPIOF_AFRH          GPIO_AFR(   12,   12,   12,   12,   12,   14,   10,    10                                               )

/// Port G //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PG0      SDRAM_A10
// PG1      SDRAM_A11
// PG2      MIC_MUTE
// PG3      REV_DRIVE.
// PG4      SDRAM_BA0
// PG5      SDRAM_BA1
// PG6      LCD_R7
// PG7      LCD_CLK
// PG8      SDRAM_SDCLK
// PG9      SPI1_MISO
// PG10     LCD_B2
// PG11     LCD_B3
// PG12     LCD_B4
// PG13     RIGHT_ENCODER_INT0
// PG14     RIGHT_ENCODER_INT1
// PG15     SDRAM_SDNCAS
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOG_MODER       GPIO_MODER(  ALT,   IN,   IN,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,   IN,   IN,  ALT,  ALT)
#define VAL_GPIOG_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOG_OSPEEDR   GPIO_OSPEEDR( HIGH,  LOW,  LOW, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, VLOW, VLOW, HIGH, HIGH)
#define VAL_GPIOG_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,   UP, NONE, NONE, NONE)
#define VAL_GPIOG_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOG_AFRL          GPIO_AFR(                                                   14,   14,   12,   12,    0,    0,   12,   12)
#define VAL_GPIOG_AFRH          GPIO_AFR(   12,    0,    0,    9,   14,   14,    5,   12                                                )

/// Port H //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PH0      input pullup.
// PH1      input pullup.
// PH2      SDRAM_SDCKE0
// PH3      SDRAM_SDNE0
// PH4      I2C2_SCL
// PH5      I2C2_SDA
// PH6      LED12 - TIM12_CH1.
// PH7      input pullup.
// PH8      SPI3_CS.
// PH9      LCD_R3
// PH10     LCD_R4
// PH11     LCD_R5
// PH12     LCD_R6
// PH13     UART4_TX
// PH14     LCD_G3
// PH15     LCD_G4
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOH_MODER       GPIO_MODER(  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  ALT,  OUT,   IN,  ALT,  ALT,  ALT,  ALT,  ALT,   IN,   IN)
#define VAL_GPIOH_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   OD,   OD,   PP,   PP,   PP,   PP)
#define VAL_GPIOH_OSPEEDR   GPIO_OSPEEDR( HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,  LOW, VLOW, HIGH, HIGH, HIGH, HIGH, HIGH, VLOW, VLOW)
#define VAL_GPIOH_PUPDR       GPIO_PUPDR( NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,   UP, NONE, NONE, NONE, NONE, NONE,   UP,   UP)
#define VAL_GPIOH_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOH_AFRL          GPIO_AFR(                                                    0,    2,    4,    4,   12,   12,    0,    0)
#define VAL_GPIOH_AFRH          GPIO_AFR(   14,   14,    8,   14,   14,   14,   14,    0                                                )

/// Port I //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PI0      LCD_G5
// PI1      LCD_G6
// PI2      LCD_G7
// PI3      ENCODER_SELECT
// PI4      LEFT_ENCODER_INT1
// PI5      LCD_B5
// PI6      LCD_B6
// PI7      SAI2A_LRCLK
// PI8      LCD_BKLT_EN
// PI9      UART4_RX
// PI10     LCD_HSYNC
// PI11     ULPI_DIR.
// PI12     input pullup.
// PI13     input pullup.
// PI14     input pullup.
// PI15     input pullup.
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOI_MODER       GPIO_MODER(   IN,   IN,   IN,   IN,  ALT,  ALT,  ALT,  OUT,  ALT,  ALT,  ALT,   IN,   IN,  ALT,  ALT,  ALT)
#define VAL_GPIOI_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOI_OSPEEDR   GPIO_OSPEEDR( VLOW,  LOW,  LOW, VLOW, HIGH, HIGH, HIGH,  LOW, HIGH, HIGH, HIGH,  LOW, VLOW, HIGH, HIGH, HIGH)
#define VAL_GPIOI_PUPDR       GPIO_PUPDR(   UP,   UP,   UP,   UP, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE)
#define VAL_GPIOI_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOI_AFRL          GPIO_AFR(                                                   10,   14,   14,    0,    0,   14,   14,   14)
#define VAL_GPIOI_AFRH          GPIO_AFR(    0,    0,    0,    0,   10,   14,    8,    0                                                )

/// Port J //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PJ0      input pullup.
// PJ1      input pullup.
// PJ2      input pullup.
// PJ3      input pullup.
// PJ4      input pullup.
// PJ5      input pullup.
// PJ6      input pullup.
// PJ7      input pullup.
// PJ8      input pullup.
// PJ9      input pullup.
// PJ10     input pullup.
// PJ11     input pullup.
// PJ12     input pullup.
// PJ13     input pullup.
// PJ14     input pullup.
// PJ15     input pullup.
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOJ_MODER       GPIO_MODER(   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN)
#define VAL_GPIOJ_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOJ_OSPEEDR   GPIO_OSPEEDR( VLOW,  LOW,  LOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW)
#define VAL_GPIOJ_PUPDR       GPIO_PUPDR(   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP)
#define VAL_GPIOJ_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOJ_AFRL          GPIO_AFR(                                                    0,    0,    0,    0,    0,    0,    0,    0)
#define VAL_GPIOJ_AFRH          GPIO_AFR(    0,    0,    0,    0,    0,    0,    0,    0                                                )

/// Port K //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PK0      input pullup.
// PK1      input pullup.
// PK2      input pullup.
// PK3      input pullup.
// PK4      input pullup.
// PK5      input pullup.
// PK6      input pullup.
// PK7      input pullup.
// PK8      input pullup.
// PK9      input pullup.
// PK10     input pullup.
// PK11     input pullup.
// PK12     input pullup.
// PK13     input pullup.
// PK14     input pullup.
// PK15     input pullup.
//                                          15    14    13    12    11    10     9     8     7     6     5     4     3     2     1     0
#define VAL_GPIOK_MODER       GPIO_MODER(   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN,   IN)
#define VAL_GPIOK_OTYPER     GPIO_OTYPER(   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP,   PP)
#define VAL_GPIOK_OSPEEDR   GPIO_OSPEEDR( VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW, VLOW)
#define VAL_GPIOK_PUPDR       GPIO_PUPDR(   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP,   UP)
#define VAL_GPIOK_ODR           GPIO_ODR(    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#define VAL_GPIOK_AFRL          GPIO_AFR(                                                    0,    0,    0,    0,    0,    0,    0,    0)
#define VAL_GPIOK_AFRH          GPIO_AFR(    0,    0,    0,    0,    0,    0,    0,    0                                                )

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* BOARD_H */
