#pragma once

#include "pga2505/pga2505.h"

#include <hal.h>

/**
 * SPI peripheral device.
 */
static SPIDriver* spid = &SPID1;

/**
 * SPI peripheral configuration.
 */
static const SPIConfig spi_cfg = {
    .circular = false,
    .end_cb = NULL,
    .ssport = GPIOC,
    .sspad = 12,
    .cfg1 = SPI_CFG1_MBR_DIV256 | SPI_CFG1_DSIZE_VALUE(16 - 1),  // 16-bit
};

// System is configured to use DMA for SPI transactions.
// This provides memory regions accessible to the DMA peripheral.
static uint16_t data_write __attribute__((section(".ram8")));
static uint16_t data_read  __attribute__((section(".ram8")));

static
pga2505_error
pga2505_lld_init(pga2505* d)
{
    (void)d;

    return PGA2505_OK;
}

static
void
pga2505_lld_acquire_bus(pga2505* d)
{
    (void)d;

    spiStart(spid, &spi_cfg);
    spiAcquireBus(spid);
}

static
void
pga2505_lld_release_bus(pga2505* d)
{
    (void)d;

    spiReleaseBus(spid);
    spiStop(spid);
}

static
pga2505_error
pga2505_lld_write_word(pga2505* d, const uint16_t data)
{
    (void)d;

    data_write = data;

    spiSelect(spid);
    spiSend(spid, 1, (void*)&data_write);
    spiUnselect(spid);

    return PGA2505_OK;
}

static
pga2505_error
pga2505_lld_read_word(pga2505* d, uint16_t* const data)
{
    (void)d;

    spiSelect(spid);
    spiReceive(spid, 1, &data_read);
    spiUnselect(spid);

    *data = data_read;

    return PGA2505_OK;
}
