/**
 * @file    led_cfgs.c
 * @brief   LED driver configuration code.
 *
 * @addtogroup board/carina
 * @{
 */
#include "hal.h"

#if (HAL_USE_LED == TRUE)
#include "led.h"
#include "led_cfgs.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/
static void pwmpcb(PWMDriver *pwmp);
static void pwmc1cb(PWMDriver *pwmp);
static void pwmc2cb(PWMDriver *pwmp);
static void pwmc3cb(PWMDriver *pwmp);


/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/
static PWMConfig pwmd1_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
        {PWM_COMPLEMENTARY_OUTPUT_ACTIVE_HIGH, pwmc2cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd2_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc3cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd3_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc2cb},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc3cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd4_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc2cb},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc3cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd8_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd12_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc2cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd15_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc2cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

static PWMConfig pwmd17_cfg = {
    LED_PWM_CLOCK_FREQUENCY,       /* 10kHz PWM clock frequency.               */
    LED_PWM_PERIOD,                /* Initial PWM period LED_PWM_PERIOD/10 MS. */
    pwmpcb,
    {
        {PWM_OUTPUT_ACTIVE_HIGH, pwmc2cb},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL}
    },
    0,
    0,
    0
};

LEDDriver leds = {
    {
#if LED_REVERSE_ENABLE
        {
            &PWMD17, &pwmd17_cfg, 0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED14 PF7 TIM17_CH1*/
        },
        {
            &PWMD15, &pwmd15_cfg, 1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED13 PE6 TIM15_CH2*/
        },
        {
            &PWMD12, &pwmd12_cfg, 0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED12 PH6 TIM12_CH1*/
        },
        {
            &PWMD8,  &pwmd8_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED11 PC6 TIM8_CH1*/
        },
        {
            &PWMD4,  &pwmd4_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED10 PD12 TIM4_CH1*/
        },
        {
            &PWMD4,  &pwmd4_cfg,  2, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED9 PB8 TIM4_CH3*/
        },
        {
            &PWMD4,  &pwmd4_cfg,  1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED8 PB7 TIM4_CH2*/
        },
        {
            &PWMD3,  &pwmd3_cfg,  2, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED7 PC8 TIM3_CH3*/
        },
        {
            &PWMD3,  &pwmd3_cfg,  1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED6 PC7 TIM3_CH2*/
        },
        {
            &PWMD3,  &pwmd3_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED5 PA6 TIM3_CH1*/
        },
        {
            &PWMD2,  &pwmd2_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED4 PA15 TIM2_CH1*/
        },
        {
            &PWMD2,  &pwmd2_cfg,  2, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED3 PA2 TIM2_CH3*/
        },
        {
            &PWMD12, &pwmd12_cfg, 1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED2 PB15 TIM12_CH2*/
        },
        {
            &PWMD1,  &pwmd1_cfg,  1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED1 PB14 TIM1_CH2N*/
        },
        {
            &PWMD1,  &pwmd1_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED0 PA8 TIM1_CH1*/
        },
#else
        {
            &PWMD1,  &pwmd1_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED0 PA8 TIM1_CH1*/
        },
        {
            &PWMD1,  &pwmd1_cfg,  1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED1 PB14 TIM1_CH2N*/
        },
        {
            &PWMD12, &pwmd12_cfg, 1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED2 PB15 TIM12_CH2*/
        },
        {
            &PWMD2,  &pwmd2_cfg,  2, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED3 PA2 TIM2_CH3*/
        },
        {
            &PWMD2,  &pwmd2_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED4 PA15 TIM2_CH1*/
        },
        {
            &PWMD3,  &pwmd3_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED5 PA6 TIM3_CH1*/
        },
        {
            &PWMD3,  &pwmd3_cfg,  1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED6 PC7 TIM3_CH2*/
        },
        {
            &PWMD3,  &pwmd3_cfg,  2, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED7 PC8 TIM3_CH3*/
        },
        {
            &PWMD4,  &pwmd4_cfg,  1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED8 PB7 TIM4_CH2*/
        },
        {
            &PWMD4,  &pwmd4_cfg,  2, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED9 PB8 TIM4_CH3*/
        },
        {
            &PWMD4,  &pwmd4_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED10 PD12 TIM4_CH1*/
        },
        {
            &PWMD8,  &pwmd8_cfg,  0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED11 PC6 TIM8_CH1*/
        },
        {
            &PWMD12, &pwmd12_cfg, 0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED12 PH6 TIM12_CH1*/
        },
        {
            &PWMD15, &pwmd15_cfg, 1, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED13 PE6 TIM15_CH2*/
        },
        {
            &PWMD17, &pwmd17_cfg, 0, LED_MIN_BRIGHTNESS, LED_UNINIT /*LED14 PF7 TIM17_CH1*/
        },
#endif
    },
    LED_MAX_BRIGHTNESS,
    LED_FADE_IN_STEP,
    LED_FADE_OUT_STEP,
};

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/
static void pwmpcb(PWMDriver *pwmp) {

    (void)pwmp;
}

static void pwmc1cb(PWMDriver *pwmp) {

    (void)pwmp;
}

static void pwmc2cb(PWMDriver *pwmp) {

    (void)pwmp;
}

static void pwmc3cb(PWMDriver *pwmp) {

    (void)pwmp;
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

#endif /* HAL_USE_LED == TRUE */
/** @} */
