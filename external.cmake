include(FetchContent)


########################################################################################################################
# Logitech CMake tools
########################################################################################################################
FetchContent_Declare(
    logitech_cmake_tools
    GIT_REPOSITORY git@gitlab.com:astro-firmware-team/cmake.git
    GIT_TAG        b4f81d3c6dd3d00de532fe164e95a1dea6d94360
)
FetchContent_MakeAvailable(logitech_cmake_tools)
set(LOGITECH_CMAKE_TOOLS ${logitech_cmake_tools_SOURCE_DIR}/cmake)
list(APPEND CMAKE_MODULE_PATH ${LOGITECH_CMAKE_TOOLS})


########################################################################################################################
# ChibiOS
########################################################################################################################
FetchContent_Declare(
    chibios
    GIT_REPOSITORY git@gitlab.com:astro-firmware-team/chibios.git
    GIT_TAG        feature/20.3.3_custom
)
FetchContent_MakeAvailable(chibios)
list(APPEND CMAKE_MODULE_PATH ${chibios_SOURCE_DIR}/cmake)


########################################################################################################################
# Common
########################################################################################################################
FetchContent_Declare(
    common
    GIT_REPOSITORY git@gitlab.com:astro-firmware-team/common.git
    GIT_TAG        52a10d421a3e4bdabd8713ddf7f7e9e86925fdde
)
FetchContent_MakeAvailable(common)
list(APPEND CMAKE_MODULE_PATH ${common_SOURCE_DIR}/cmake)


########################################################################################################################
# Drivers
########################################################################################################################
FetchContent_Declare(
    drivers
    GIT_REPOSITORY git@gitlab.com:astro-firmware-team/drivers.git
    GIT_TAG        5074b88b699825bc95437d5be14f2f1690f0391a
)
FetchContent_MakeAvailable(drivers)
list(APPEND CMAKE_MODULE_PATH ${drivers_SOURCE_DIR}/cmake)


########################################################################################################################
# CentPP
########################################################################################################################
FetchContent_Declare(
        centpp
        GIT_REPOSITORY git@github.com:Logitech/mbg-centpp.git
        GIT_TAG        102514ad1d8e2f17ed1d2056bdc865e83ae57368
)
FetchContent_MakeAvailable(centpp)
list(APPEND CMAKE_MODULE_PATH ${centpp_SOURCE_DIR}/cmake)
